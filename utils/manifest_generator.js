var fs = require('fs');
var path = require('path');
var crypto = require('crypto');
var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var manifest = {
    packageUrl: 'http://localhost:8000/remote-assets/jsb-link/',
    remoteManifestUrl: 'http://localhost:8000/remote-assets/jsb-link/',
    remoteVersionUrl: 'http://localhost:8000/remote-assets/jsb-link/',
    version: '0.0.0.0',
    assets: {},
    searchPaths: []
};

var dest = '/remote-assets/';
var src = './jsb/';

var structureType = {
    CODE:'code',
    LOBBY:'lobby',
    GAMES:'games'
}

function loadManifestsSettings(type, subtype = null){

    var manifestsSetting = fs.readFileSync('manifest_settings.json');
    var jsonContent = JSON.parse(manifestsSetting);

    var subSetting;
    if(subtype != null){
        
        for(var i = 0; i < jsonContent[type].length; i++){
            if(jsonContent[type][i].id == subtype){
                subSetting = jsonContent[type][i];
            }
        }
    }else {
        subSetting = jsonContent[type];
    }

    setManifestParameters(jsonContent.mainSetting, subSetting);
}

function setManifestParameters(mainSetting, subSetting){

    // sdes zadaem puti po kotorim budet rabotat zagrurka
    manifest.packageUrl = mainSetting.url;
    manifest.remoteManifestUrl = mainSetting.url + subSetting.destPath + subSetting.dir + 'project.manifest';
    manifest.remoteVersionUrl = mainSetting.url + subSetting.destPath + subSetting.dir + 'version.manifest';
    manifest.version = subSetting.version;

    src = mainSetting.source + subSetting.sourcesPath;
    dest = mainSetting.dest + subSetting.destPath + subSetting.dir;

    readDir(path.join(src, '/' + subSetting.type), manifest.assets);

    var destManifest = path.join(dest, 'project.manifest');
    var destVersion = path.join(dest, 'version.manifest');

    mkdirSync(dest);

    fs.writeFile(destManifest, JSON.stringify(manifest), (err) => {
        if (err) throw err;
        console.log('Manifest successfully generated');
    });

    delete manifest.assets;
    delete manifest.searchPaths;

    fs.writeFile(destVersion, JSON.stringify(manifest), (err) => {
        if (err) throw err;
        console.log('Version successfully generated');
    });
    console.log('==============================================================');
    console.log(manifest);
}

function chooseTypeForGeneration(){
    
    rl.question('Choose setting type for generation code|lobby|games \n', (answer) => {
        switch(answer){
            case structureType.CODE:
                loadManifestsSettings(answer);
                rl.close();
                break;
            case structureType.LOBBY:
                loadManifestsSettings(answer);
                rl.close();
                break;
            case structureType.GAMES:
                rl.question('Choose game for generation: \n', (gameName) => {
                    loadManifestsSettings(answer, gameName);
                    rl.close();
                });
                break;
            default:
                console.log('WARNING!!! Choose setting type for generation between code|lobby|game');
                chooseTypeForGeneration();
        }
    });
}

function readDir (dir, obj) {
    var stat = fs.statSync(dir);
    if (!stat.isDirectory()) {
        return;
    }
    var subpaths = fs.readdirSync(dir), subpath, size, md5, compressed, relative;
    for (var i = 0; i < subpaths.length; ++i) {
        if (subpaths[i][0] === '.') {
            continue;
        }
        subpath = path.join(dir, subpaths[i]);
        stat = fs.statSync(subpath);
        if (stat.isDirectory()) {
            readDir(subpath, obj);
        }
        else if (stat.isFile()) {
            
            // Size in Bytes
            size = stat['size'];
            md5 = crypto.createHash('md5').update(fs.readFileSync(subpath, 'binary')).digest('hex');
            compressed = path.extname(subpath).toLowerCase() === '.zip';

            relative = path.relative(src, subpath);
            relative = relative.replace(/\\/g, '/');
            relative = encodeURI(relative);
            obj[relative] = {
                'size' : size,
                'md5' : md5
            };
            if (compressed) {
                obj[relative].compressed = true;
            }
        }
    }
}

function mkdirSync(path) {
    try{
        if (!fs.existsSync(path)) {
            var dirName = "";
            var filePathSplit = path.split('/');
            for (var index = 0; index < filePathSplit.length; index++) {
                dirName += filePathSplit[index]+'/';
                if (!fs.existsSync(dirName))
                    fs.mkdirSync(dirName);
            }
        }
    }
    catch(e) {
        if ( e.code != 'EEXIST' ) throw e;
    }
}

chooseTypeForGeneration();




import { RequestType } from '../Communicator/SlotNetworkCommunicator';

export default interface INetworkCommunicator {
    setParser(parser: any): void;
    sendRequst(type: RequestType, request: XMLHttpRequest, url: string, body?: string): void;
}

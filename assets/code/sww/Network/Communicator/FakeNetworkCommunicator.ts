import DomainManager from '../../Core/Foundation/Domain/DomainManager';
import { SlotGameNotification, SlotGameNotificationTopic } from '../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import INotification from '../../Core/Notifications/INotification';
import IObserver from '../../Core/Notifications/IObserver';
import Notification from '../../Core/Notifications/Notification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import SlotGameDomain from '../../Games/Host/SlotGameDomain';
import ISlotGameProfile from '../../Games/Host/SlotGameProfileRepository/Interfaces/ISlotGameProfile';
import { SlotGameRepositoryDataEnum } from '../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../Games/Host/SlotGameRepositoryEnum';
import IWin from '../../Games/Host/SlotSpinResultRepository/Interfaces/IWin';
import LoaderUtil from '../../Utils/LoaderUtil';
import SlotResponseParser from '../../Utils/Slots/SlotResponseParser';
import TypeUtil from '../../Utils/TypeUtil';
import INetworkCommunicator from '../Interfaces/INetworkCommunicator';

export enum RequestType {
    GET = 'GET',
    POST = 'POST',
    DELETE = 'DELETE'
}

export default class FakeNetworkCommunicator implements IObserver, INetworkCommunicator {

    private _responseParser: SlotResponseParser;

    private _fakeSpinCount: number = 0;

    private _ffsc: number = 0;
    private _seq: number = 2;

    constructor() {
        this.registerListener();
    }

    public setParser(parser: any): void  {
        this._responseParser = parser;
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== SlotGameNotificationTopic.Network) {
            return;
        }

        switch (notification.id) {
            case SlotGameNotification.SpinRequest:
                return this.createSpinRequest(notification.arguments);
            default :
                return;
        }
    }

    public sendRequst(type: RequestType, request: XMLHttpRequest, url: string, body?: string): void {
        request.open(type, url);

        if (type === RequestType.POST && body !== undefined) {
            request.setRequestHeader('Content-Type', 'application/json');
            request.send(body);
        } else {
            request.send();
        }
    }

    public createTokenRequest(url: string): void {
        LoaderUtil.loadJSON('resources/FakeServerResponse/token.json').then((result) => {
            this._responseParser
            .pasrseGameProfile(result);

            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                           SlotGameNotification.SlotGameToken));
        });
    }

    public createStartGameRequest(params: any): void {
        LoaderUtil.loadJSON('resources/FakeServerResponse/config.json').then((result) => {
            this._responseParser.parseUserProfile(result);
            this._responseParser.parsePaytablePaySymbolsInfo(result);
            this._responseParser.waysParser(result);
            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                           SlotGameNotification.SlotStartGameInformation));
        });
    }

    public createSpinRequest(data: any): void {
        // tslint:disable-next-line:no-magic-numbers
        if (this._fakeSpinCount > 5) {
            this._fakeSpinCount = 0;
        }

        LoaderUtil.loadJSON(`resources/FakeServerResponse/spin_${this._fakeSpinCount}.json`).then((result) => {
            this._responseParser.parseSpinResult(result);
            this._responseParser.parseUserProfile(result);

            const slotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));

            const spinResultWin = slotGameDomain.getDALProvider()
                .getRepository(SlotGameRepositoryEnum.SpinResult)
                .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

            if (spinResultWin.win > 0) {
                LoaderUtil.loadJSON('resources/FakeServerResponse/collect.json').then((res) => {
                    this._responseParser.parseUserProfile(res);
                    NotificationManager.instance
                        .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
                    this._fakeSpinCount++;
                });
            } else {
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
                this._fakeSpinCount++;
            }
        });
    }

    public createFreeSpinRequest(data: any): void {
        LoaderUtil.loadJSON(`resources/FakeServerResponse/freeSpin/spin_${this._ffsc}.json`).then((result) => {
            this._responseParser.parseSpinResult(result);
            this._responseParser.parseUserProfile(result);

            const slotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
            const spinResultWin = slotGameDomain.getDALProvider()
                .getRepository(SlotGameRepositoryEnum.SpinResult)
                .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

            if (spinResultWin.win > 0) {
                this._ffsc = 0;
                LoaderUtil.loadJSON('resources/FakeServerResponse/collect.json').then((res) => {
                    this._responseParser.parseUserProfile(res);
                    NotificationManager.instance
                        .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
                });
            } else {
                this._ffsc++;
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
            }

            // NotificationManager.instance
            //     .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));

        });
    }

    public createCollectRequest(): void {
        const request = new XMLHttpRequest();

        const requestBody = JSON.stringify([{action: 'collect'}]);

        request.onload = () => { this.collectRequestHandler(request); };
        this.sendRequst(RequestType.POST, request, this.slotGameProfile.gameRequestUrl, requestBody);
    }

    private tokenRequestHandler(request: XMLHttpRequest) {
        this._responseParser
            .pasrseGameProfile(JSON.parse(request.responseText));

        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                       SlotGameNotification.SlotGameToken));
    }

    private startApplicationRequestHandler(request: XMLHttpRequest) {
        const response: JSON = JSON.parse(request.responseText);
        this._responseParser.parseUserProfile(response);
        this._responseParser.parsePaytablePaySymbolsInfo(response);
        this._responseParser.waysParser(response);
        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                       SlotGameNotification.SlotStartGameInformation));
    }

    private spinRequestHandler(request: XMLHttpRequest) {
        const response: JSON = JSON.parse(request.responseText);
        this._responseParser.parseSpinResult(response);
        this._responseParser.parseUserProfile(response);

        const slotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        const spinResultWin = slotGameDomain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        if (spinResultWin.win > 0) {
            this.createCollectRequest();
        } else {
            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
        }
    }

    private collectRequestHandler(request: XMLHttpRequest) {
        const response: JSON = JSON.parse(request.responseText);
        this._responseParser.parseUserProfile(response);

        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
    }

    private responseErrorHandler(request: XMLHttpRequest, event: Event) {
        // TODO need add request error handler
    }

    private registerListener(): void {
        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Network, SlotGameNotification.SpinRequest);
    }

    private get slotGameProfile(): ISlotGameProfile {
        return DomainManager.instance
            .getDomain(SlotGameDomain.NAME)
            .getDALProvider()
            .getRepository(SlotGameRepositoryEnum.GameProfile)
            .read<ISlotGameProfile>(SlotGameRepositoryDataEnum.SlotGameProfile);
    }
}

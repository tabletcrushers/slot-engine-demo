import DomainManager from '../../Core/Foundation/Domain/DomainManager';
import { SlotGameNotification, SlotGameNotificationTopic } from '../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import INotification from '../../Core/Notifications/INotification';
import IObserver from '../../Core/Notifications/IObserver';
import Notification from '../../Core/Notifications/Notification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import SlotGameDomain from '../../Games/Host/SlotGameDomain';
import ISlotGameProfile from '../../Games/Host/SlotGameProfileRepository/Interfaces/ISlotGameProfile';
import { SlotGameRepositoryDataEnum } from '../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../Games/Host/SlotGameRepositoryEnum';
import IWin from '../../Games/Host/SlotSpinResultRepository/Interfaces/IWin';
import SlotResponseParser from '../../Utils/Slots/SlotResponseParser';
import TypeUtil from '../../Utils/TypeUtil';
import INetworkCommunicator from '../Interfaces/INetworkCommunicator';

export enum RequestType {
    GET = 'GET',
    POST = 'POST',
    DELETE = 'DELETE'
}

export default class SlotNetworkCommunicator implements IObserver, INetworkCommunicator {

    private _responseParser: SlotResponseParser;

    constructor() {
        this.registerListener();
    }

    public setParser(parser: any): void  {
        this._responseParser = parser;
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== SlotGameNotificationTopic.Network) {
            return;
        }

        switch (notification.id) {
            case SlotGameNotification.SpinRequest:
                return this.createSpinRequest(notification.arguments);
            default :
                return;
        }
    }

    public sendRequst(type: RequestType, request: XMLHttpRequest, url: string, body?: string): void {
        request.open(type, url);

        if (type === RequestType.POST && body !== undefined) {
            request.setRequestHeader('Content-Type', 'application/json');
            request.send(body);
        } else {
            request.send();
        }
    }

    public createTokenRequest(url: string): void {
        const request = new XMLHttpRequest();
        request.onload = () => { this.tokenRequestHandler(request); };
        this.sendRequst(RequestType.GET, request, url);
    }

    public createStartGameRequest(params: any): void {
        const request = new XMLHttpRequest();

        const requestBody = JSON.stringify([
            {
                action: 'config',
                context: null
            }
        ]);

        request.onload = () => { this.startApplicationRequestHandler(request); };
        this.sendRequst(RequestType.POST, request, this.slotGameProfile.gameRequestUrl, requestBody);
    }

    public createSpinRequest(data: any): void {
        const request = new XMLHttpRequest();

        const requestBody = JSON.stringify([
            {
                action: 'bet',
                context: [data.lines, data.bet]
            },
            {
                action: 'play',
                context: null
            }
        ]);

        request.onload = () => { this.spinRequestHandler(request); };
        this.sendRequst(RequestType.POST, request, this.slotGameProfile.gameRequestUrl, requestBody);
    }

    public createFreeSpinRequest(data: any): void {
        const gameProfile: ISlotGameProfile = this.slotGameProfile;

        const request = new XMLHttpRequest();
        const url = `${gameProfile.gameRequestUrl}&seq=${gameProfile.seq}&gid=${gameProfile.gameRound}`;

        const requestBody = JSON.stringify([
            {
                action: 'play',
                context: null
            }
        ]);

        request.onload = () => { this.spinRequestHandler(request); };
        this.sendRequst(RequestType.POST, request, url, requestBody);
    }

    public createCollectRequest(): void {
        const request = new XMLHttpRequest();
        const requestBody = JSON.stringify([{action: 'collect'}]);

        request.onload = () => { this.collectRequestHandler(request); };
        this.sendRequst(RequestType.POST, request, this.slotGameProfile.gameRequestUrl, requestBody);
    }

    private tokenRequestHandler(request: XMLHttpRequest) {
        this._responseParser
            .pasrseGameProfile(JSON.parse(request.responseText));

        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                       SlotGameNotification.SlotGameToken));
    }

    private startApplicationRequestHandler(request: XMLHttpRequest) {
        const response: JSON = JSON.parse(request.responseText);
        this._responseParser.parseUserProfile(response);
        this._responseParser.parsePaytablePaySymbolsInfo(response);
        this._responseParser.waysParser(response);
        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                       SlotGameNotification.SlotStartGameInformation));
    }

    private spinRequestHandler(request: XMLHttpRequest) {
        const response: JSON = JSON.parse(request.responseText);
        this._responseParser.parseSpinResult(response);
        this._responseParser.parseUserProfile(response);

        const slotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        const spinResultWin = slotGameDomain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        if (spinResultWin.win > 0) {
            this.createCollectRequest();
        } else {
            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
        }
    }

    private collectRequestHandler(request: XMLHttpRequest) {
        const response: JSON = JSON.parse(request.responseText);
        this._responseParser.parseUserProfile(response);

        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult));
    }

    private responseErrorHandler(request: XMLHttpRequest, event: Event) {
        // TODO need add request error handler
    }

    private registerListener(): void {
        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Network, SlotGameNotification.SpinRequest);
    }

    private get slotGameProfile(): ISlotGameProfile {
        return DomainManager.instance
            .getDomain(SlotGameDomain.NAME)
            .getDALProvider()
            .getRepository(SlotGameRepositoryEnum.GameProfile)
            .read<ISlotGameProfile>(SlotGameRepositoryDataEnum.SlotGameProfile);
    }
}

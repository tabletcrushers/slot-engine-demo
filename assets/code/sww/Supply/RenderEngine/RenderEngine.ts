import AggregateNode from '../../../../showcases/Network/AggregateNode';
import IFeature from '../../Core/Foundation/Feature/IFeature';
import ISuspendable from '../../Core/Foundation/Suspend/ISuspendable';
import IReelItemDTO from '../../Core/Games/Slots/Configuration/DTO/IReelItemDTO';
import IAnimationDTO from '../../Core/Games/Slots/Features/WinAnimations/IAnimationDTO';
import IWinLine from '../../Core/Games/Slots/Features/WinAnimations/IWinLine';
import {
    ReelEngineNotification,
    ReelEngineNotificationTopic
} from '../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import FrameAtlasProvider from '../../Core/Games/Slots/ReelsEngine/Core/Implementation/Providers/FrameAtlasProvider';
import IReelItem from '../../Core/Games/Slots/ReelsEngine/Core/Items/IReelItem';
import AnimationFrameCreator from '../../Core/Implementation/Creators/AnimationFrameCreator';
import NodeCreator from '../../Core/Implementation/Creators/NodeCreator';
import IObserver from '../../Core/Notifications/IObserver';
import Notification from '../../Core/Notifications/Notification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import LoaderUtil from '../../Utils/LoaderUtil';
import Pool from '../../Utils/ObjectPool/Pool';
import IAnimation from '../Animation/IAnimation';
import RenderReelItem from './RenderReelItem';

export default class RenderEngine implements IObserver, IFeature, ISuspendable {

    public _isActive: boolean;

    private _renderNode: AggregateNode;
    private _pool: Pool<RenderReelItem>;
    private _itemsConfig: Map<number, IReelItemDTO>;
    private _frameAtlasProvider: FrameAtlasProvider;
    private _usingItemsMap: Map<number, Array<RenderReelItem>>;
    private _renderedItems: Array<RenderReelItem>;
    private _winLineItems: Array<RenderReelItem>;
    private _counter: { [key: number]: number };
    private _solidBackground: cc.Node;
    private _fadeMode: boolean;

    private _animationNodePool: Pool<IAnimation>;
    private _animationsConfig: Map<number, IAnimationDTO>;
    private _clips: Map<string, cc.AnimationClip> = new Map();
    private _animations: Array<IAnimation> = [];

    constructor() {
        this._pool = new Pool<RenderReelItem>(new NodeCreator());
        this._itemsConfig = new Map<number, IReelItemDTO>();
        this._usingItemsMap = new Map();
        this._renderedItems = [];
        this._winLineItems = [];
        this._counter = {};
        this._isActive = true;

        this._animationNodePool = new Pool<IAnimation>(new AnimationFrameCreator());

        NotificationManager.instance.addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStart)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStop);
    }

    public start(): void {
    }

    public stop(): void {
    }

    public resume(): void {
        this._isActive = true;
    }

    public suspend(): void {
        this._isActive = false;
    }

    public get isActive(): boolean {
        return this._isActive;
    }

    public get renderNode(): AggregateNode {
        return this._renderNode;
    }

    public set renderNode(renderNode: AggregateNode) {
        this._renderNode = renderNode;
    }

    public get frameAtlasProvider(): FrameAtlasProvider {
        return this._frameAtlasProvider;
    }

    public set frameAtlasProvider(frameAtlasProvider: FrameAtlasProvider) {
        this._frameAtlasProvider = frameAtlasProvider;
    }

    public get fadeMode(): boolean {
        return this._fadeMode;
    }

    public set fadeMode(fadeMode: boolean) {
        this._fadeMode = fadeMode;
    }

    public configure(itemsConfig: Array<IReelItemDTO>): void {
        for (const item of itemsConfig) {
            this._itemsConfig.set(item.id, item);
        }

        // tslint:disable-next-line:no-magic-numbers
        this.initUsingItems(itemsConfig, 5);
        this.initSolidBackground();
    }

    public configureAnimations(animationsConfig: Map<number, IAnimationDTO>): void {
        animationsConfig.forEach((item, id) => {
            LoaderUtil.loadAnimation(`FlamingPearl/animations/${item.clipName}`).then((clip: cc.AnimationClip) => {
                this._clips.set((clip as any)._name, clip);
            });
        });

        this._animationsConfig = animationsConfig;
    }

    public renderReelItems(visibleItems: Array<IReelItem>): void {
        visibleItems.forEach((el) => {
            this._counter[el.id] = (this._counter[el.id] || 0) + 1;
        });

        for (const item of this._renderedItems) {
            item.hide();
        }

        this._renderedItems.length = 0;

        visibleItems.forEach((item) => {
            const itemConfig = this._itemsConfig.get(item.id);

            if (this._usingItemsMap.get(item.id).length < this._counter[item.id]) {
                this.addRenderReelItems(itemConfig, this._counter[item.id] - this._usingItemsMap.get(item.id).length);
            }

            const renderItem = this._usingItemsMap.get(item.id)[--this._counter[item.id]];
            renderItem.UUID = item.getUUID();
            renderItem.x = item.renderTransform.x + itemConfig.offsetX;
            renderItem.y = item.renderTransform.y + itemConfig.offsetY;
            renderItem.show();

            this._renderedItems.push(renderItem);
        });
    }

    public showSolidBackground(): void {
        this._renderNode.addChild(this._solidBackground);
    }

    public hideSolidBackground(): void {
        this.renderNode.removeChild(this._solidBackground);
    }

    public renderWinLineAnimations(winLine: IWinLine, noWinSymbols?: IWinLine): void {
        for (const item of winLine.lineItems) {
            if (!this._animationsConfig.get(item.id)) {
                if (this._fadeMode) {
                    this.showWinSymbol(item);
                }

                continue;
            }

            const renderItemIndex = this._renderedItems.findIndex((renderItem) => renderItem.getUUID() === item.getUUID());
            const animation = this.createAnimation(this._animationsConfig.get(item.id).clipName);

            this.hideSymbol(renderItemIndex);
            animation.UUID = item.getUUID();
            animation.x = item.renderTransform.x - this._animationsConfig.get(item.id).offsetX + this._itemsConfig.get(item.id).offsetX;
            animation.y = item.renderTransform.y - this._animationsConfig.get(item.id).offsetY + this._itemsConfig.get(item.id).offsetY;
            animation.play(this._animationsConfig.get(item.id).clipName);
            animation.onFinished((event: cc.Event.EventCustom) => {
            });
        }

        if (this._fadeMode) {
            for (const item of noWinSymbols.lineItems) {
                this.showWinSymbol(item);
            }
        }
    }

    public stopWinLineAnimations(): void {
        for (const animation of this._animations) {
            const renderItemIndex = this._renderedItems.findIndex((renderItem) => renderItem.getUUID() === animation.getUUID());

            this.showSymbol(renderItemIndex);
            (animation as any).getComponent(cc.Animation).stop();
            animation.parent = null;
            this._animationNodePool.put(animation);
        }

        for (const item of this._winLineItems) {
            const renderItemIndex = this._renderedItems.findIndex((renderItem) => renderItem.getUUID() === item.getUUID());

            this.showSymbol(renderItemIndex);
            item.parent = null;
        }

        this._winLineItems.length = 0;
        this._animations.length = 0;
    }

    public observe(notification: Notification): void {
        // switch (notification.id) {
        //     case SlotGameNotification.ForceStartRender:
        //         cc.log('SlotGameNotification.ForceStartRender');
        //         this._spinning = true;

        //         return;
        //     case ReelEngineNotification.ReelsEngineStart:
        //         cc.log('ReelEngineNotification.ReelsEngineStart');
        //         this._spinning = true;

        //         return;
        //     case ReelEngineNotification.ReelsEngineStop:
        //         cc.log('ReelEngineNotification.ReelsEngineStop');
        //         this._spinning = false;

        //         return;
        //     default:
        // }
    }

    public hideSymbol(index: number): void {
        this._renderedItems[index].hide();
    }

    public showSymbol(index: number): void {
        this._renderedItems[index].show();
    }

    private sortByDrawOrder(items: Array<IReelItem>): void {
        items.sort((a, b) => this.getDrawOrder(a.id) > this.getDrawOrder(b.id) ? 1 : -1);
    }

    // TODO: rewrite using new config
    private getDrawOrder(id: number): number {
        return this._itemsConfig.get(id).drawOrder;
    }

    private initUsingItems(itemsConfig: Array<IReelItemDTO>, initialCount: number = 1): void {
        for (const item of itemsConfig) {
            if (!this._usingItemsMap.has(item.id)) {
                this._usingItemsMap.set(item.id, []);
            }

            this.addRenderReelItems(item, initialCount);
        }
    }

    private initSolidBackground(): void {
        this._solidBackground = new cc.Node();

        const graphics = this._solidBackground.addComponent(cc.Graphics);

        graphics.lineWidth = 0;
        graphics.clear();
        // TODO - Serhiy, MAKE TODO for numbering!!!
        // tslint:disable:no-magic-numbers
        graphics.fillColor = new cc.Color(0, 0, 0, 155);
        graphics.fillRect(0, 0, 1680, 1050);
    }

    private showWinSymbol(item: IReelItem): void {
        const itemConfig = this._itemsConfig.get(item.id);
        const renderItemIndex = this._renderedItems.findIndex((renderItem) => renderItem.getUUID() === item.getUUID());
        const symbol = this.createReelItem(itemConfig.textureName);

        symbol.UUID = item.getUUID();
        symbol.x = item.renderTransform.x + itemConfig.offsetX;
        symbol.y = item.renderTransform.y + itemConfig.offsetY;

        symbol.show();
        this._winLineItems.push(symbol);
        this.hideSymbol(renderItemIndex);
    }

    private addRenderReelItems(itemConfig: IReelItemDTO, count: number): void {
        for (let i = 0; i < count; i++) {
            this._usingItemsMap.get(itemConfig.id).push(this.createReelItem(itemConfig.textureName));
        }
    }

    private createReelItem(textureName: string): RenderReelItem {
        const item = this._pool.get();

        item.parent = this.renderNode;
        item.setSpriteFrame(this._frameAtlasProvider.getFrame(textureName));

        return item;
    }

    private createAnimation(name: string): IAnimation {
        const animation = this._animationNodePool.get();

        animation.addClip(this._clips.get(name), name);
        animation.parent = this.renderNode;

        this._animations.push(animation);

        return animation;
    }
}

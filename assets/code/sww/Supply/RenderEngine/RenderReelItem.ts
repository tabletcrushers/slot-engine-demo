export default class RenderReelItem extends cc.Node {

    private _sprite: cc.Sprite;
    private _UUID: string;

    constructor() {
        super();

        this.anchorX = 0;
        this.anchorY = 0;
        this._sprite = this.addComponent(cc.Sprite);
        this._sprite.spriteFrame = new cc.SpriteFrame();
        this.hide();
    }

    public setTexture(texture: cc.Texture2D): void {
        this._sprite.spriteFrame.setTexture(texture);
    }

    public setSpriteFrame(spriteFrame: cc.SpriteFrame): void {
        this._sprite.spriteFrame = spriteFrame;
    }

    public show(): void {
        this.active = true;
    }

    public hide(): void {
        this.active = false;
    }

    public getUUID(): string {
        return this._UUID;
    }

    public set UUID(uuid: string) {
        this._UUID = uuid;
    }
}

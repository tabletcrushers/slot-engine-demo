import AggregateNode from '../../../../showcases/Network/AggregateNode';

export default interface IRootCarrier {
    setRoot(root: AggregateNode): void;
}

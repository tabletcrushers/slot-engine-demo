import AggregateNode from '../../../../showcases/Network/AggregateNode';

export default interface INodeCarrier {
    setNode(node: AggregateNode): void;
}

export default class Color {
    public red: number;
    public green: number;
    public blue: number;
    public alpha: number;
}

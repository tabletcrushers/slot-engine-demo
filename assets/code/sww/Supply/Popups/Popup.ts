import INotification from '../../Core/Notifications/INotification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import IPopup from './IPopup';
import { PopupsNotifications, PopupsNotificationTopic } from './Notification/PopupsNotifications';
import PopupNode from './view/PopupNode';
import PopupViewComponent from './view/PopupViewComponent';

export default class Popup implements IPopup {

    public readonly name: string = 'Popup';

    public view: PopupViewComponent;

    constructor(view: PopupViewComponent) {
        this.view = view;
    }

    public show(): void {
        this.view.show();
    }

    public hide(): void {
        this.view.hide();
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== PopupsNotificationTopic.Default) {
            return;
        }
        switch (notification.id) {
            case PopupsNotifications.ShowPopup:
                return this.show();
            case PopupsNotifications.HidePopup:
                return this.hide();
            default :
                // do nothing
        }
    }

    protected registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.ShowPopup)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.HidePopup);
    }

    protected removeListeners(): void {
        NotificationManager.instance
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.ShowPopup)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.HidePopup);
    }
}

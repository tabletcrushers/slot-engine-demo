import ActionPopup from './ActionPopup';
import IPopup from './IPopup';
import SimplePopup from './SimplePopup';
import ActionPopupViewComponent from './view/ActionPopupViewComponent';
import PopupNode from './view/PopupNode';
import SimplePopupViewComponent from './view/SimplePopupViewComponent';

const {ccclass, property} = cc._decorator;

@ccclass
export default class PopupFactory {

    private static _instance: PopupFactory;

    constructor() {
        if (PopupFactory._instance === undefined) {
            PopupFactory._instance = this;
        } else {
            throw Error('PopupManager instance already created');
        }
    }

    public static get instance(): PopupFactory {
        if (this._instance === undefined) {
            return new PopupFactory();
        } else {
            return this._instance;
        }
    }

    public generateSimplePopup(content: PopupNode): IPopup {
        content.addComponent(SimplePopupViewComponent);

        return new SimplePopup(content.getComponent(SimplePopupViewComponent));
    }

    public ganerateActionPopup(content: PopupNode): IPopup {
        content.addComponent(ActionPopupViewComponent);

        return new ActionPopup(content.getComponent(ActionPopupViewComponent));
    }
}

import AggregateNode from '../../../../showcases/Network/AggregateNode';
import NodeUtil from '../../Utils/NodeUtil';
import TypeUtil from '../../Utils/TypeUtil';
import IPointOffsetDTO from './interfaces/IPointOffsetDTO';
import IPopupConfigDTO from './interfaces/IPopupConfigDTO';
import IPopupElementDTO from './interfaces/IPopupElementDTO';
import IPopupWindowDTO from './interfaces/IPopupWindowDTO';
import { PopupType } from './PopupsWarehouse';
import VideoComponent from './view/VideoComponent';

export default class PopupGenerator {
    public static POPUP: string = 'popup';
    public static GENERAL_ELEMENTS: string = 'generalElements';
    public static WINDOW_ELEMENTS: string = 'windowElements';
    public static FADER: string = 'fader';
    public static WINDOWS_NODE: string = 'windowsNode';

    private static _instance: PopupGenerator;
    private _config: object;
    private _popupElementsContainer: AggregateNode;
    private _createdPopup: AggregateNode;
    private _lastPopupGenerated: string;
    private _defoultGeneralElementPoints: Array<IPointOffsetDTO>;

    constructor() {
        if (PopupGenerator._instance === undefined) {
            PopupGenerator._instance = this;
        } else {
            throw Error('PopupGenerator instance already created');
        }
    }

    public static get instance(): PopupGenerator {
        if (this._instance === undefined) {
            return new PopupGenerator();
        } else {
            return this._instance;
        }
    }

    public set config(data: object) {
        this._config = data;
    }

    public get config(): object {
        return this._config;
    }

    public set partsContainer(data: AggregateNode) {
        this._popupElementsContainer = data;
    }

    public generatePopups(type: PopupType, isFaderShow: boolean): AggregateNode {
        if (this._config === undefined || this._popupElementsContainer === undefined) {
            throw Error('PopupConfig or prefab undefined in PopupGenerator');
        }

        if (this._lastPopupGenerated === type && this._createdPopup.childrenCount > 0) {
            return this._createdPopup;
        }

        this._createdPopup = new AggregateNode(PopupGenerator.POPUP);
        switch (type) {
            case PopupType.PaytablePopup:
                this.create(this._config[PopupType.PaytablePopup]);
                break;
            default:
        }

        if (isFaderShow) {
            // tslint:disable-next-line:max-line-length
            const fader = NodeUtil.cloneNode(this._popupElementsContainer.getChildByName(PopupGenerator.FADER), false);
            // fader.set(cc.view.getVisibleSizeInPixel());
            this._createdPopup.addChild(fader, 0);
        }

        return this._createdPopup;
    }

    private create(info: IPopupConfigDTO): void {
        let generalNode: AggregateNode;
        if (info.generalElements.length > 0) {
            generalNode = new AggregateNode(PopupGenerator.GENERAL_ELEMENTS);
            this.addGeneralElements(generalNode, info.generalElements);
            this._createdPopup.addChild(generalNode, 2);
        }

        const baseNode: AggregateNode = new AggregateNode(PopupGenerator.WINDOWS_NODE);
        info.windows.forEach((configObj: IPopupWindowDTO, index: number) => {
            const windowNode = new AggregateNode(`Window${index}`);
            this.addWindowElements(windowNode, configObj.windowElements);
            this.setPointProperty(windowNode, configObj.generalElementsOffsets);
            baseNode.addChild(windowNode, index, index);
        });

        this._createdPopup.setProperty('arePagesLooped', Object.getOwnPropertyDescriptor(info, 'arePagesLooped').value);
        this._createdPopup.addChild(baseNode, 1);
    }

    // tslint:disable-next-line:max-line-length
    private addWindowElements(contNode: AggregateNode, config: Array<IPopupElementDTO>): void {
        let node: AggregateNode;
        const windowElementsContainer: AggregateNode = this._popupElementsContainer.getChildByName(PopupGenerator.WINDOW_ELEMENTS);
        config.forEach((value: IPopupElementDTO) => {
            if (value.id === undefined || value.id === null) {
                throw new Error(`One of windowElements has invalid ID`);
            }

            node = new AggregateNode(value.id);
            windowElementsContainer.getChildByName(value.id) !== null ?
                    // tslint:disable-next-line:max-line-length
                    node.addChild(NodeUtil.cloneNode(windowElementsContainer.getChildByName(value.id), false)) : node.addChild(new AggregateNode(value.id));

            this.addPropertiesToNode(node, value, ['id', 'background', 'type']);
            node.x = value.dX || 0;
            node.y = value.dY || 0;

            if (value.background !== undefined && value.background !== null) {
                if (windowElementsContainer.getChildByName(value.background) !== null) {
                    const clone = NodeUtil.cloneNode(windowElementsContainer.getChildByName(value.background), false);
                    clone.x = clone.y = 0;
                    node.addChild(clone, 0);
                }
            }

            if (value.type === 'Video') {
                const delayString = 'delay';
                node.addComponent(VideoComponent);
                const videoComponent = node.getComponent(VideoComponent);
                videoComponent.isReady = false;
                videoComponent.startPlayDelay = value[delayString] || 0;
                videoComponent.resourceType = cc.VideoPlayer.ResourceType.LOCAL;
                videoComponent.clip = cc.url.raw(`resources/video/${value.id}.webm`);
                videoComponent.keepAspectRatio = true;
                node.width = value.width || 0;
                node.height = value.height || 0;
            }

            contNode.addChild(node);
        });
    }

    // tslint:disable-next-line:max-line-length
    private addGeneralElements(contNode: AggregateNode, config: Array <IPopupElementDTO>): void {
        let node: AggregateNode;
        let elementPoint: IPointOffsetDTO;
        this._defoultGeneralElementPoints = new Array<IPointOffsetDTO>();
        // tslint:disable-next-line:max-line-length
        const interactiveElementsContainer: AggregateNode = this._popupElementsContainer.getChildByName(PopupGenerator.GENERAL_ELEMENTS);
        config.forEach((value: IPopupElementDTO) => {
            if (value.id !== undefined && interactiveElementsContainer.getChildByName(value.id)) {
                node = NodeUtil.cloneNode(interactiveElementsContainer.getChildByName(value.id), false);
                this.addPropertiesToNode(node, value, ['id', 'background', 'type']);

                elementPoint = {
                    id: value.id,
                    pointDX: value.dX || 0,
                    pointDY: value.dY || 0,
                };
                this._defoultGeneralElementPoints.push(elementPoint);
                node.scaleX = value.scaleX || 1;
                node.scaleY = value.scaleY || 1;
                contNode.addChild(node);
            }
        });
    }

    /** Set general elements points to whith page elements */
    private setPointProperty(contNode: AggregateNode, elementsOffset: Array<IPointOffsetDTO>): void {
        const propName = 'generalElementPoints';
        // **Clone array objects */
        contNode[propName] = this._defoultGeneralElementPoints.map((value) => ({...value}));
        if (elementsOffset) {
            elementsOffset.forEach ((element) => {
                // tslint:disable-next-line:max-line-length
                const generalElement = TypeUtil.castTo<Array<IPointOffsetDTO>>(contNode[propName]).find((value: IPointOffsetDTO) => value.id === element.id);
                generalElement.pointDX += element.pointDX || 0;
                generalElement.pointDY += element.pointDY || 0;
            });
        }
    }

    private addPropertiesToNode(targetNode: AggregateNode, config: IPopupElementDTO, properties: Array<string>): void {
        properties.forEach((value) => {
            targetNode[value] = config[value];
        });
    }
}

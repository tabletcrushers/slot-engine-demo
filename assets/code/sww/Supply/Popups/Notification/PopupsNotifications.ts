export enum PopupsNotificationTopic {
    Default = 'Default',
    ApplicationPopup = 'ApplicationPopup',
    GamePopup = 'GamePopup',
    FeaturePopup = 'FeaturePopup'
}

export enum PopupsNotifications {
    ShowSinglePopup = 'ShowSinglePopup',
    ShowMultiplePopup = 'ShowMultiplePopup',
    HidePopup = 'HidePopup',
    NextPage = 'NextPage',
    PreviousPage = 'PreviousPage',
    OpenPopup = 'OpenPopup',
    ClosePopup = 'ClosePopup',
    SkipPopup = 'SkipPopup',
    Confirm = 'Confirm',
    SetTextToPopup = 'SetTextToPopup',
    EnablePopupButton = 'EnablePopupButton',
    DisablePopupButton = 'DisablePopupButton',
    StartPopupAction = 'StartPopupAction',
    FinishPopupAction = 'FinishedPopupAction'
}

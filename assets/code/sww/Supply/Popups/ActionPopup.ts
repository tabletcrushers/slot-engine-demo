import INotification from '../../Core/Notifications/INotification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import { PopupsNotifications, PopupsNotificationTopic } from './Notification/PopupsNotifications';
import Popup from './Popup';
import SimplePopup from './SimplePopup';
import ActionPopupViewComponent from './view/ActionPopupViewComponent';
import PopupNode from './view/PopupNode';
import PopupViewComponent from './view/PopupViewComponent';

export default class ActionPopup extends SimplePopup {

    public readonly name: string = 'ActionPopup';

    public view: ActionPopupViewComponent;

    constructor(view: PopupViewComponent) {
        super(view);
        this.registerListeners();
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== PopupsNotificationTopic.Default) {
            return;
        }

        switch (notification.id) {
            case PopupsNotifications.StartPopupAction:
                return this.startAction();
            case PopupsNotifications.FinishPopupAction:
                return this.finishAction();
            default:
                super.observe(notification);
        }
    }

    public enableButton(type: string): void {
        if (type === 'ActionButton') {
            this.enableActionButton();
        } else {
            super.enableButton(type);
        }
    }

    public disableButton(type: string): void {
        if (type === 'ActionButton') {
            this.disableActionButton();
        } else {
            super.disableButton(type);
        }
    }

    public enableActionButton(): void {
        this.view.enableActionButton();
    }

    public disableActionButton(): void {
        this.view.disableActionButton();
    }

    protected registerListeners(): void {
        super.registerListeners();

        NotificationManager.instance
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.StartPopupAction)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.FinishPopupAction);
    }

    protected removeListeners(): void {
        super.removeListeners();

        NotificationManager.instance
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.StartPopupAction)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.FinishPopupAction);
    }

    protected startAction(): void {
    }

    protected finishAction(): void {
    }
}

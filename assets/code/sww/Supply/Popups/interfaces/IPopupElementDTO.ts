export default interface IPopupElementDTO {
    type: string;
    id: string;
    background: string;
    width: number;
    height: number;
    dX: number;
    dY: number;
    scaleX: number;
    scaleY: number;
}

export default interface IPaytablePaylineConfigDTO {
    linesPerPage: Array<{id: string; columns: number; rows: number}>;
    xAxisLineDX: number;
    yAxisLineDX: number;
    firstLineX: number;
    firstLineY: number;
    gridNestSize: number;
    gridNestOffsetX: number;
    gridNestOffsetY: number;
    gridBorderColor: {red: number; green: number; blue: number};
    gridFillColor: {red: number; green: number; blue: number};
}

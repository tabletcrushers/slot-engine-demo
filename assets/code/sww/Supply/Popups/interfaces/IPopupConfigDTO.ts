import IPopupElementDTO from './IPopupElementDTO';
import IPopupWindowDTO from './IPopupWindowDTO';

export default interface IPopupConfigDTO {
    arePagesLooped: boolean;
    /** generalElements - elements that are contained and act on the main container in which the Node whith windows(pages) are located */
    generalElements: Array<IPopupElementDTO> ;
    /** windows - windows(pages) that will be shown in popup */
    windows: Array<IPopupWindowDTO>;
}

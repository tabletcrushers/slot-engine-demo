import IPointOffsetDTO from './IPointOffsetDTO';
import IPopupElementDTO from './IPopupElementDTO';

export default interface IPopupWindowDTO {
    windowElements: Array<IPopupElementDTO>;
    generalElementsOffsets: Array<IPointOffsetDTO>;
}

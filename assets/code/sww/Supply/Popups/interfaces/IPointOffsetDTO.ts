export default interface IPointOffsetDTO {
    id: string;
    pointDX: number;
    pointDY: number;
}

export default interface IPopupView {
    init(): void;
    show(): void;
    close(): void;
    confirm(): void;
    showPage(pageIndex: number): void;
}

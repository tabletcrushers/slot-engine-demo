import AggregateNode from '../../../../../showcases/Network/AggregateNode';
import Notification from '../../../Core/Notifications/Notification';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import IPaytableSymbol from '../../../Games/Host/SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbol';
import ILine from '../../../Games/Host/WinLineRepository/interfaces/ILine';
import MathUtil from '../../../Utils/MathUtil';
import TypeUtil from '../../../Utils/TypeUtil';
import IPaytablePaylineConfigDTO from '../interfaces/IPaytablePaylineConfigDTO';
import IPointOffsetDTO from '../interfaces/IPointOffsetDTO';
import { PopupsNotifications, PopupsNotificationTopic } from '../Notification/PopupsNotifications';
import PopupGenerator from '../PopupGenerator';
import PopupView from './PopupView';
import VideoComponent from './VideoComponent';

export default class PaytablePopupView extends PopupView {
    protected _pagesMap: Array<AggregateNode>;
    protected _previousBttn: AggregateNode;
    protected _nextBttn: AggregateNode;
    protected _arePagesLooped: boolean = false;
    protected _winCombinations: Array<ILine>;
    protected _paytableSymbolsInfo: Array<IPaytableSymbol>;
    protected _paytableUISettings: IPaytablePaylineConfigDTO;
    protected _lastVideoComponentActive: VideoComponent;
    protected _generalElementsCoords: Map<number, Map<string, IPointOffsetDTO>>;

    public setNode(node: AggregateNode): void {
        this._arePagesLooped = Object.getOwnPropertyDescriptor(node, 'arePagesLooped').value;
        this._pagesMap = TypeUtil.castTo<AggregateNode>(node.getChildByName(PopupGenerator.WINDOWS_NODE))
                                    .getChildren().slice();

        this._generalElementsCoords = new Map<number, Map<string, IPointOffsetDTO>>();
        const propName = 'generalElementPoints';
        this._pagesMap.forEach((page, index) => {
            page.getChildren().forEach(((value: AggregateNode) => {
                if (value.hasProperty('type') && Object.getOwnPropertyDescriptor(value, 'type').value === 'PaylinesDraw') {
                    this.payLinesDraw(value);
                } else if (Object.getOwnPropertyDescriptor(value, 'id').value === 'paytable_info') {
                    this.setPaytableSymbolsInfo(value.getChildByName('paytable_info'));
                }
            }));

            /** Create map general elements points for each page. */
            this._generalElementsCoords.set(index, new Map<string, IPointOffsetDTO>());
            TypeUtil.castTo<Array<IPointOffsetDTO>>(page[propName]).forEach((value) => {
                this._generalElementsCoords.get(index).set(value.id, value);
            });
        });

        node.getChildByName(PopupGenerator.WINDOWS_NODE).removeAllChildren(false);
        super.setNode(node);
    }

    public setWinlinesConfig(payLineArr: Array<ILine>, paytableSymolsInfo: Array<IPaytableSymbol>) {
        this._winCombinations = payLineArr;
        this._paytableSymbolsInfo = paytableSymolsInfo;
    }

    public set paytableUISettings(config: IPaytablePaylineConfigDTO) {
        this._paytableUISettings = config;
    }

    public set startPageNumber(index: number) {
        this._currentPageIndex = index;
    }

    public init(): void {
        this._currentPageIndex = 0;
        // this._winCombinations = new Array<ILine>();

        if (this._popupNode.getChildByName(PopupGenerator.GENERAL_ELEMENTS) === null) {
            return super.init();
        }

        this._previousBttn = this._popupNode.getChildByName(PopupGenerator.GENERAL_ELEMENTS).getChildByName ('LeftArrow');
        this._nextBttn = this._popupNode.getChildByName(PopupGenerator.GENERAL_ELEMENTS).getChildByName('RightArrow');
        this._closeBttn = this._popupNode.getChildByName(PopupGenerator.GENERAL_ELEMENTS).getChildByName ('PaytablePopup_closeBttn');

        this._popupNode.getChildByName(PopupGenerator.WINDOWS_NODE).addChild(this._pagesMap[this._currentPageIndex], 0);
        this.registerListeners();
    }

    public show(): void {
        this.setArrowState();
        this.setGeneralElementsCoords();
        super.show();
    }

    protected setPaytableSymbolsInfo(node: AggregateNode): void {
        const symbolInfoLabels = node.getChildren();
        if (symbolInfoLabels === null) {
            // tslint:disable-next-line:max-line-length
            throw new Error((`Please add labels for symbols to ${Object.getOwnPropertyDescriptor(node, 'id').value} layer in PopupsPrefab`));
        }

        let symbolInfo: IPaytableSymbol;
        symbolInfoLabels.forEach((value) => {
            symbolInfo = this._paytableSymbolsInfo.find((symbol) => (symbol.name === value.name));

            if (symbolInfo === undefined ||  symbolInfo === null) {
                // tslint:disable-next-line:max-line-length
                throw new Error((`Inforamtion about ${value.name} not recieved from server. Add it to response or remove label from prefab`));
            }

            value.getChildren().forEach((label) => {
                // tslint:disable-next-line:max-line-length
                label.getComponent(cc.Label).string = TypeUtil.castTo<string>(symbolInfo.symbolsPay.find((symbolPay) => String(symbolPay.count) === label.name).pay);
            });
        });
    }

    protected payLinesDraw(node: AggregateNode): void {
        // tslint:disable-next-line:max-line-length
        let xAxisMaxNodes;
        let yAxisMaxNodes;

        // tslint:disable-next-line:max-line-length
        const linesPerPageInfo = this._paytableUISettings.linesPerPage.find((value) => node.hasProperty('id') && Object.getOwnPropertyDescriptor(node, 'id').value === value.id);

        xAxisMaxNodes = linesPerPageInfo.columns;
        yAxisMaxNodes = linesPerPageInfo.rows;

        if (xAxisMaxNodes === undefined || xAxisMaxNodes === undefined) {
            // tslint:disable-next-line:max-line-length
            throw new Error(`Different info between commonUISettings.json and popupConfig.json for -> ${Object.getOwnPropertyDescriptor(node, 'id').value} invalid`);
        }

        const linesPerPage = xAxisMaxNodes * yAxisMaxNodes;
        // tslint:disable-next-line:max-line-length
        const linesConfigs = this._winCombinations.length > linesPerPage ? this._winCombinations.splice(0, linesPerPage) : this._winCombinations;
        let lineNode: AggregateNode;
        let winCellColor: cc.Color;
        const columns = 5; // TODO: change when can take info from other place
        const rows = 3; // TODO: change when can take info from other place
        const cellWidth = this._paytableUISettings.gridNestSize;
        const cellHeight = cellWidth;
        const cellOffsetX = this._paytableUISettings.gridNestOffsetX;
        const cellOffsetY = this._paytableUISettings.gridNestOffsetY;
        // tslint:disable-next-line:max-line-length
        const gridBorderColor = cc.color(this._paytableUISettings.gridBorderColor.red, this._paytableUISettings.gridBorderColor.green, this._paytableUISettings.gridBorderColor.blue);
        // tslint:disable-next-line:max-line-length
        const gridFillColor = cc.color(this._paytableUISettings.gridFillColor.red, this._paytableUISettings.gridFillColor.green, this._paytableUISettings.gridFillColor.blue);
        const lineDXOffset = this._paytableUISettings.xAxisLineDX;
        const lineDYOffset = this._paytableUISettings.yAxisLineDX;
        let lineX = this._paytableUISettings.firstLineX;
        let lineY = this._paytableUISettings.firstLineY;

        linesConfigs.forEach((value: ILine, index: number) => {
            if (index >= linesPerPage) {
                return;
            }

            lineNode = new AggregateNode();
            winCellColor = cc.color(value.color.red, value.color.green, value.color.blue);
            const graphicComponent = lineNode.addComponent(cc.Graphics);
            let cellX = 0;
            let cellY = 0;
            for (let cIndex = 0; cIndex < columns; cIndex++) {
                for (let lIndex = 0; lIndex < rows; lIndex++) {
                        graphicComponent.fillColor = graphicComponent.strokeColor = value.nodes[cIndex] === lIndex ?
                                                winCellColor : gridFillColor;
                        graphicComponent.fillRect(cellX, cellY, cellWidth, cellHeight);
                        graphicComponent.strokeColor = gridBorderColor;
                        graphicComponent.stroke();
                        cellY -= cellHeight + cellOffsetY;
                }
                cellY = 0;
                cellX += cellWidth + cellOffsetX;
            }

            if (index > 0 && index % xAxisMaxNodes === 0) {
                lineX = this._paytableUISettings.firstLineX;
                lineY -= (cellHeight + cellOffsetY) * rows - cellOffsetY + lineDYOffset;
            }

            lineNode.x = lineX;
            lineNode.y = lineY;
            lineX += (cellWidth + cellOffsetX) * columns - cellOffsetX + lineDXOffset;
            cellX = cellY = 0;
            node.addChild(lineNode);
        });
    }

    public showPage(pageIndex: number): void {
        const fromPageIndex = this._currentPageIndex;
        const toPageIndex = this._arePagesLooped ? MathUtil.circulateInBounds(pageIndex, 0, this._pagesMap.length)
                                    : pageIndex;

        this.changePage(fromPageIndex, toPageIndex);
        this.setArrowState();
    }

    protected changePage(previousPageIndex: number, currentPageIndex: number): void {
        this._currentPageIndex = currentPageIndex;
        this._baseNode.getChildByName(PopupGenerator.POPUP)
                        .getChildByName(PopupGenerator.WINDOWS_NODE)
                        .removeChild(this._pagesMap[previousPageIndex]);

        this.setGeneralElementsCoords();

        this._baseNode.getChildByName(PopupGenerator.POPUP)
                        .getChildByName(PopupGenerator.WINDOWS_NODE)
                        .addChild(this._pagesMap[currentPageIndex], 0);
    }

    protected setArrowState(): void {
        if (!this._arePagesLooped) {
            this._previousBttn.active = this._currentPageIndex !== 0;
            this._nextBttn.active = this._currentPageIndex !== this._pagesMap.length - 1;
        }
    }

    protected startVideo(event): void {
        const target: AggregateNode = TypeUtil.castTo<AggregateNode>(event.target);
        const videoPlayer = target.getComponent(VideoComponent);
        setTimeout(videoPlayer.play(), videoPlayer.startPlayDelay);
    }

    /** Set general elements(bttns,arrows, etc) coords */
    protected setGeneralElementsCoords(): void {
        const elementsMap = this._generalElementsCoords.get(this.currentPageIndex);
        this._previousBttn.x = elementsMap.get(this._previousBttn.name).pointDX;
        this._previousBttn.y = elementsMap.get(this._previousBttn.name).pointDY;
        this._nextBttn.x = elementsMap.get(this._nextBttn.name).pointDX;
        this._nextBttn.y = elementsMap.get(this._nextBttn.name).pointDY;
        this._closeBttn.x = elementsMap.get(this._closeBttn.name).pointDX;
        this._closeBttn.y = elementsMap.get(this._closeBttn.name).pointDY;
    }

    protected registerListeners(): void {
        super.registerListeners();

        this._baseNode.on(VideoComponent.READY, this.startVideo);
        this._previousBttn.on(AggregateNode.EventType.TOUCH_START, this.prevClickHandler, this);
        this._nextBttn.on(AggregateNode.EventType.TOUCH_START, this.nextClickHandler, this);
    }

    protected removeListeners(): void {
        super.removeListeners();

        this._baseNode.off(VideoComponent.READY, this.startVideo);
        this._previousBttn.off(AggregateNode.EventType.TOUCH_START, this.prevClickHandler, this);
        this._nextBttn.off(AggregateNode.EventType.TOUCH_START, this.nextClickHandler, this);
    }

    private nextClickHandler(): void {
        NotificationManager.instance.dispatch(new Notification(PopupsNotificationTopic.FeaturePopup, PopupsNotifications.NextPage, this));
    }

    private prevClickHandler(): void {
        // tslint:disable-next-line:max-line-length
        NotificationManager.instance.dispatch(new Notification(PopupsNotificationTopic.FeaturePopup, PopupsNotifications.PreviousPage, this));
    }
}

import PopupViewComponent from './PopupViewComponent';

const {ccclass, property} = cc._decorator;

@ccclass
export default class SimplePopupViewComponent extends PopupViewComponent {

    private _closeButton: cc.Node;
    private _centerText: cc.Node;
    private _background: cc.Node;

    public onLoad() {
        this._closeButton = this.node.getChildByName('CloseButton');
        this._centerText = this.node.getChildByName('Text');
        this._background = this.node.getChildByName('Background');
    }

    public setText(value: string): void {
        this._centerText.getComponent(cc.Label).string = value;
    }

    public enableCloseButton(): void {
        this._closeButton.active = true;
    }

    public disableCloseButton(): void {
        this._closeButton.active = false;
    }
}

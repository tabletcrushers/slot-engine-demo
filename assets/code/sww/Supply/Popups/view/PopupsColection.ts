import AggregateNode from '../../../../../showcases/Network/AggregateNode';
import NodeUtil from '../../../Utils/NodeUtil';
import TypeUtil from '../../../Utils/TypeUtil';
import PopupGenerator from '../PopupGenerator';

const {ccclass, property} = cc._decorator;

@ccclass
export default class PopupsColection extends cc.Component {

    @property(cc.Prefab)
    public popupPrefabs: Array<cc.Prefab> = [];

    public onLoad(): void {
        PopupGenerator.instance.partsContainer = NodeUtil.cloneNode(TypeUtil.castTo<AggregateNode>(this.popupPrefabs[0]), false);
    }
}

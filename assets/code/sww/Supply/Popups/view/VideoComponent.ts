export default class VideoComponent extends cc.VideoPlayer {
    public static READY: string = 'ready';

    private _isReadyToPlay: boolean;
    private _delay: number;

    public set isReady(value: boolean) {
        this._isReadyToPlay = value;
    }

    public get isReady(): boolean {
        return this._isReadyToPlay;
    }

    public set startPlayDelay(value: number) {
        this._delay = value;
    }

    public get startPlayDelay(): number {
        return this._delay;
    }

    protected onEnable(): void {
        super.onEnable();
        if (this._isReadyToPlay ===  true) {
            this.readyToPlay();
        } else {
            this.node.on('ready-to-play', this.readyToPlay, this);
            this._isReadyToPlay = true;
        }
    }

    protected onDisable(): void {
        this.stop();
        this.node.off('ready-to-play', this.readyToPlay, this);
        super.onDisable();
    }

    private readyToPlay(): void {
        this.node.dispatchEvent(new cc.Event.EventCustom(VideoComponent.READY, true));
    }
}

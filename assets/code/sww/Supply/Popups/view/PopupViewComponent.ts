import PopupNode from './PopupNode';

const {ccclass, property} = cc._decorator;

@ccclass
export default class PopupViewComponent extends cc.Component {

    public hide(): void {
        this.node.active = false;
    }

    public show(): void {
        this.node.active = true;
    }

    public get content(): PopupNode {
        return this.node;
    }
}

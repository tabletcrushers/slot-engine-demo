import AggregateNode from '../../../../../showcases/Network/AggregateNode';
import Notification from '../../../Core/Notifications/Notification';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import INodeCarrier from '../../Interfaces/INodeCarrier';
import IPopupView from '../interfaces/IPopupView';
import { PopupsNotifications, PopupsNotificationTopic } from '../Notification/PopupsNotifications';

export default class PopupView implements INodeCarrier, IPopupView {

    protected _id: string;
    protected _baseNode: AggregateNode;
    protected _popupNode: AggregateNode;
    protected _closeBttn: AggregateNode;
    protected _confirmBttn: AggregateNode;
    protected _currentPageIndex: number;

    constructor(id: string, rootNode: AggregateNode) {
        this._id = id;
        this._baseNode = rootNode;
        this._popupNode = new AggregateNode();
    }

    public start() {

    }

    public setNode(node: AggregateNode): void {
        this._popupNode = node;
        this.init();
    }

    public init(): void {
        this._closeBttn = this._popupNode.getChildByName ('CloseButton');
        this._confirmBttn = this._popupNode.getChildByName('OkButton');
        this.registerListeners();
    }

    public show(): void {
        this._baseNode.addChild(this._popupNode);
    }

    public close(): void {
        this._baseNode.removeChild(this._popupNode, false);
    }

    public destroy(): void {
        this.close();
        this.removeListeners();
        this._popupNode.removeAllChildren(true);
        this._popupNode.destroy();
        this._closeBttn = null;
        this._baseNode = null;
        this._popupNode = null;
    }

    public confirm(): void {
        this.close();
    }

    public showPage(pageIndex: number): void {

    }

    public get id(): string {
        return this._id;
    }

    public get isActive(): boolean {
        return this._popupNode.parent !== null && this._popupNode.parent !== undefined;
    }

    public get currentPageIndex(): number {
        return this._currentPageIndex;
    }

    protected registerListeners(): void {
        this._closeBttn.on(AggregateNode.EventType.TOUCH_START, this.closeClickHandler, this);
    }

    protected removeListeners(): void {
        this._closeBttn.off(AggregateNode.EventType.TOUCH_START, this.closeClickHandler, this);
    }

    private closeClickHandler(): void {
        NotificationManager.instance.dispatch(new Notification(PopupsNotificationTopic.FeaturePopup, PopupsNotifications.ClosePopup, this));
    }
}

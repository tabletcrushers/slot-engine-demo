import SimplePopupViewComponent from './SimplePopupViewComponent';

const {ccclass, property} = cc._decorator;

@ccclass
export default class ActionPopupViewComponent extends SimplePopupViewComponent {

    private _actionButton: cc.Node;

    public onLoad() {
        super.onLoad();

        this._actionButton = this.node.getChildByName('ActionButton');
    }

    public enableActionButton(): void {
        this._actionButton.active = true;
    }

    public disableActionButton(): void {
        this._actionButton.active = false;
    }
}

import IObserver from '../../Core/Notifications/IObserver';
import PopupViewComponent from './view/PopupViewComponent';

export default interface IPopup extends IObserver {

    readonly name: string;
    view: PopupViewComponent;

    show(): void;
    hide(): void;
}

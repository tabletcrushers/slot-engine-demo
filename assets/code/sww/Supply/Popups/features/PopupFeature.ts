import AggregateNode from '../../../../../showcases/Network/AggregateNode';
import DomainManager from '../../../Core/Foundation/Domain/DomainManager';
import IFeature from '../../../Core/Foundation/Feature/IFeature';
import INotification from '../../../Core/Notifications/INotification';
import IObserver from '../../../Core/Notifications/IObserver';
import Notification from '../../../Core/Notifications/Notification';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import SlotGameDomain from '../../../Games/Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../Games/Host/SlotGameRepositoryEnum';
import IPaytableSymbols from '../../../Games/Host/SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbols';
import ILines from '../../../Games/Host/WinLineRepository/interfaces/ILines';
import TypeUtil from '../../../Utils/TypeUtil';
import IRootCarrier from '../../Interfaces/IRootCarrier';
import IPaytablePaylineConfigDTO from '../interfaces/IPaytablePaylineConfigDTO';
import { PopupsNotifications, PopupsNotificationTopic } from '../Notification/PopupsNotifications';
import PopupGenerator from '../PopupGenerator';
import { PopupType } from '../PopupsWarehouse';
import PaytablePopupView from '../view/PaytablePopupView';
import PopupView from '../view/PopupView';

export default class PopupFeature implements IFeature, IRootCarrier, IObserver {

    public static NAME = 'PopupFeature';
    public _popupView: PopupView;
    public _root: AggregateNode;
    public _popupStack: Array<PopupView> = [];
    public _renderNode: AggregateNode;
    public _paytableSettings: IPaytablePaylineConfigDTO;

    public _openNotification: Notification;
    public _closeNotification: Notification;

    constructor() {
        this.registerListeners();
    }

    public setRoot(root: AggregateNode): void {
        this._root = root;
        this._renderNode = TypeUtil.castTo<AggregateNode>(this._root.parent).getChildByName('PopupRenderNode');

        this.initNotifications();
    }

    public setPaytableConfig(config: IPaytablePaylineConfigDTO): void {
        this._paytableSettings = config;
    }

    public initNotifications(): void {
        this._openNotification = new Notification(PopupsNotificationTopic.Default, PopupsNotifications.OpenPopup);
        this._closeNotification = new Notification(PopupsNotificationTopic.Default, PopupsNotifications.ClosePopup);
    }

    public start(): void {

    }

    public observe(notification: INotification): void {
        if (notification.topicId !== PopupsNotificationTopic.FeaturePopup || this._popupView === undefined) {
            return;
        }

        switch (notification. id) {
            case PopupsNotifications.ClosePopup:
            case PopupsNotifications.Confirm:
                this.destroyPopup(notification.arguments);

                break;
            case PopupsNotifications.NextPage:
                this._popupView.showPage(this._popupView.currentPageIndex + 1);
                break;
            case PopupsNotifications.PreviousPage:
                this._popupView.showPage(this._popupView.currentPageIndex - 1);
                break;
            default:
        }
    }

    public showPopup(type: string): void {
        // not complete. Need do some logic for simple poups open/close
        // const popupNode: AggregateNode;

        switch (type) {
            case PopupType.SimplePopup:
                this._popupView = new PopupView(name, this._renderNode);
                break;
            default:
                this._popupView = new PopupView(name, this._renderNode);
        }

        this._popupStack.push(this._popupView);
        this._popupView.show();

        NotificationManager.instance.dispatch(this._openNotification);
    }

    public showMultiplePopup(name: string): void {

        const openedTargetPopup: PopupView = this._popupStack.find((view) => view.id === name);
        if (openedTargetPopup !== undefined) {
            this.destroyPopup(openedTargetPopup);

            return;
        }

        let popupsNode: AggregateNode;
        switch (name) {
            case PopupType.PaytablePopup:
                popupsNode = PopupGenerator.instance.generatePopups(PopupType.PaytablePopup, true);
                this._popupView = new PaytablePopupView(name, this._renderNode);
                TypeUtil.castTo<PaytablePopupView>(this._popupView).paytableUISettings = this._paytableSettings;

                const domain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
                const dalProvider = domain.getDALProvider();
                // tslint:disable-next-line:max-line-length
                const payLines = dalProvider.getRepository(SlotGameRepositoryEnum.LinesRepository).read<ILines>(SlotGameRepositoryDataEnum.Lines);
                // tslint:disable-next-line:max-line-length
                const paytableSymolsInfo = dalProvider.getRepository(SlotGameRepositoryEnum.PaytableRepository).read<IPaytableSymbols>(SlotGameRepositoryDataEnum.PaytableSymbols);
                TypeUtil.castTo<PaytablePopupView>(this._popupView).setWinlinesConfig(payLines.lines, paytableSymolsInfo.symbols);

                break;
            default:
        }

        this._popupStack.push(this._popupView);
        TypeUtil.castTo<PaytablePopupView>(this._popupView).setNode(popupsNode);
        this._popupView.show();

        NotificationManager.instance.dispatch(this._openNotification);
    }

    public registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, PopupsNotificationTopic.FeaturePopup, PopupsNotifications.ClosePopup)
            .addObserver(this, PopupsNotificationTopic.FeaturePopup, PopupsNotifications.Confirm)
            .addObserver(this, PopupsNotificationTopic.FeaturePopup, PopupsNotifications.NextPage)
            .addObserver(this, PopupsNotificationTopic.FeaturePopup, PopupsNotifications.PreviousPage);
    }

    public removePopupFromQueue(view: PopupView): void {
        this._popupStack.splice(this._popupStack.lastIndexOf(view));
    }

    private destroyPopup(target: PopupView): void {
        this.removePopupFromQueue(target);
        TypeUtil.castTo<PopupView>(target).destroy();

        if (this._popupStack.length === 0) {
            NotificationManager.instance.dispatch(this._closeNotification);
        }
    }
}

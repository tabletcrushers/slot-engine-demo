import PopupNode from './view/PopupNode';

export enum PopupType {
    SimplePopup = 'SimplePopup',
    ActionPopup = 'ActionPopup',
    PaytablePopup = 'PaytablePopup'
}

export default class PopupsWarehouse {

    private static _instance: PopupsWarehouse;
    private _popupsContent: Array<PopupNode> = [];

    constructor() {
        if (PopupsWarehouse._instance === undefined) {
            PopupsWarehouse._instance = this;
        } else {
            throw Error('PopupManager instance already created');
        }
    }

    public static get instance(): PopupsWarehouse {
        if (this._instance === undefined) {
            return new PopupsWarehouse();
        } else {
            return this._instance;
        }
    }

    public addPopupContent(popup: PopupNode) {
        this._popupsContent.push(popup);
    }

    public getPopupContentByName(name: string): PopupNode {
        return this._popupsContent.find((value: PopupNode) => value.name === name);
    }
}

import INotification from '../../Core/Notifications/INotification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import { PopupsNotifications, PopupsNotificationTopic } from './Notification/PopupsNotifications';
import Popup from './Popup';
import PopupNode from './view/PopupNode';
import PopupViewComponent from './view/PopupViewComponent';
import SimplePopupViewComponent from './view/SimplePopupViewComponent';

const {ccclass, property} = cc._decorator;

@ccclass
export default class SimplePopup extends Popup {

    public readonly name: string = 'SimplePopup';
    public view: SimplePopupViewComponent;

    constructor(view: PopupViewComponent) {
        super(view);
        this.registerListeners();
    }

    public setText(value: string): void {
        this.view.setText(value);
    }

    public enableButton(type): void {
        if (type === 'CloseButton') {
            this.view.enableCloseButton();
        }
    }

    public disableButton(type): void {
        if (type === 'CloseButton') {
            this.view.disableCloseButton();
        }
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== PopupsNotificationTopic.Default) {
            return;
        }
        switch (notification.id) {
            case PopupsNotifications.SetTextToPopup:
                return this.setText(notification.arguments);
            case PopupsNotifications.EnablePopupButton:
                return this.enableButton(notification.arguments);
            case PopupsNotifications.DisablePopupButton:
                return this.disableButton(notification.arguments);
            default:
                super.observe(notification);
        }
    }

    protected registerListeners(): void {
        super.registerListeners();

        NotificationManager.instance
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.SetTextToPopup)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.EnablePopupButton)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.DisablePopupButton);
    }

    protected removeListeners(): void {
        super.removeListeners();

        NotificationManager.instance
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.SetTextToPopup)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.EnablePopupButton)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.DisablePopupButton);
    }
}

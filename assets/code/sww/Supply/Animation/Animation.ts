export default class Animation extends cc.Node {

    private _UUID: string;

    constructor() {
        super();

        this.anchorX = 0;
        this.anchorY = 0;
        this.addComponent(cc.Sprite);
        this.addComponent(cc.Animation);
    }

    public addClip(clip: cc.AnimationClip, name: string): void {
        this.getComponent(cc.Animation).addClip(clip, name);
    }

    public play(clipName: string): void {
        this.getComponent(cc.Animation).play(clipName);
    }

    public stop(clipName: string): void {
        this.getComponent(cc.Animation).stop(clipName);
    }

    public onFinished(callback: (event: cc.Event.EventCustom) => void): void {
        this.getComponent(cc.Animation).on('finished', callback);
    }

    public getUUID(): string {
        return this._UUID;
    }

    public set UUID(uuid: string) {
        this._UUID = uuid;
    }
}

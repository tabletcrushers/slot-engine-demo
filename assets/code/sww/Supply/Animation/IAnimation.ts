import IUUID from '../../Core/Foundation/Identify/IUUID';

export default interface IAnimation extends IUUID {
    x: number;
    y: number;
    parent: cc.Node;
    UUID: string;
    addClip(clip: cc.AnimationClip, name: string);
    play(clipName: string): void;
    stop(clipName: string): void;
    onFinished(callback: (event: cc.Event.EventCustom) => void): void;
}

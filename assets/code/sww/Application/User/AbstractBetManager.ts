import DomainManager from '../../Core/Foundation/Domain/DomainManager';
import ApplicationDomain from '../Host/ApplicationDomain';
import IBetManager from './IBetManager';

export default abstract class AbstractBetManager implements IBetManager {

    public static readonly InvalidBet: number = -1000000;
    protected _currentBet: number;
    public abstract previousBet(): number;
    public abstract nextBet(): number;
    public abstract getValidBet(): number;

    public get currentBet(): number {
        return this._currentBet;
    }

    protected isUpdateBetPossible(value: number): boolean {
        return (DomainManager.instance.getDomain(ApplicationDomain.NAME) as ApplicationDomain).isPossibleBet(value);
    }
}

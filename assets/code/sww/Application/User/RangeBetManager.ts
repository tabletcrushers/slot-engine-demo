import MathUtil from '../../Utils/MathUtil';
import AbstractBetManager from './AbstractBetManager';

export default class RangeBetManager extends AbstractBetManager {

    private _range: Array<number>;
    private _currentPos: number;
    private _circulation: boolean;

    constructor(range: Array<number>, circulation: boolean = false) {
        super();

        this._range = range;
        this._currentPos = 0;
        this._currentBet = this._range[this._currentPos];
        this._circulation = circulation;
    }

    public previousBet(): number {
        let nextPos = this._currentPos;
        let prevBet;

        if (this._circulation) {
            nextPos = MathUtil.circulateInBounds(this._currentPos - 1, 0, this._range.length);
            prevBet = this._range[nextPos];
        } else {
            nextPos = MathUtil.circulateInBounds(this._currentPos - 1, 0, this._range.length + MathUtil.Epsilon9);
            prevBet = this._range[nextPos];
        }

        if (prevBet !== undefined && this.isUpdateBetPossible(prevBet)) {
            this._currentPos = nextPos;
            this._currentBet = prevBet;
        }

        return this._currentBet;
    }

    public nextBet(): number {
        let nextPos = this._currentPos;
        let nextBet;

        if (this._circulation) {
            nextPos = MathUtil.circulateInBounds(this._currentPos + 1, 0, this._range.length);
            nextBet = this._range[nextPos];
        } else {
            nextBet = this._range[++nextPos];
        }

        if (nextBet !== undefined && this.isUpdateBetPossible(nextBet)) {
            this._currentPos = nextPos;
            this._currentBet = nextBet;
        }

        return this._currentBet;
    }

    public getValidBet(): number {
        for (let i = this._currentPos; i >= 0; i--) {
            if (this.isUpdateBetPossible(this._range[i])) {
                if (this._range[i] !== this._currentBet) {
                    this._currentPos = i;
                    this._currentBet = this._range[i];
                }

                return this._currentBet;
            }
        }

        return AbstractBetManager.InvalidBet;
    }
}

import MathUtil from '../../Utils/MathUtil';
import AbstractBetManager from './AbstractBetManager';

export default class StepBetManager extends AbstractBetManager {

    private _range: Array<number>;
    private _step: number;
    private _circulation: boolean;

    constructor(range: Array<number>, step: number, circulation: boolean = false) {
        super();

        this._range = range;
        this._step = step;
        this._currentBet = this._range[0];
        this._circulation = circulation;
    }

    public previousBet(): number {
        const prevBet = this._circulation ?
            MathUtil.circulateInBounds(this._currentBet - this._step, this._range[0], this._range[1] + this._step) :
            MathUtil.circulateInBounds(this._currentBet - this._step, this._range[0] - this._step, this._range[1]);

        if (prevBet >= this._range[0] && this.isUpdateBetPossible(prevBet)) {
            this._currentBet = prevBet;
        }

        return this._currentBet;
    }

    public nextBet(): number {
        const nextBet = this._circulation ?
            MathUtil.circulateInBounds(this._currentBet + this._step, this._range[0], this._range[1] + this._step) :
            MathUtil.circulate(this._currentBet + this._step, this._range[1] + this._step + MathUtil.Epsilon9);

        if (nextBet <= this._range[1] && this.isUpdateBetPossible(nextBet)) {
            this._currentBet = nextBet;
        }

        return this._currentBet;
    }

    public getValidBet(): number {
        let validBet = this._currentBet;

        while (validBet >= this._range[0]) {
            if (this.isUpdateBetPossible(validBet)) {
                if (validBet !== this._currentBet) {
                    this._currentBet = validBet;
                }

                return this._currentBet;
            }

            validBet -= this._step;
        }

        return AbstractBetManager.InvalidBet;
    }
}

export default interface IBetManager {
    currentBet: number;
    previousBet(): number;
    nextBet(): number;
    getValidBet(): number;
}

import AggregateNode from '../../../../../../showcases/Network/AggregateNode';
import IRootCarrier from '../../../../Supply/Interfaces/IRootCarrier';
import PopupsWarehouse from '../../../../Supply/Popups/PopupsWarehouse';
import PopupNode from '../../../../Supply/Popups/view/PopupNode';

export default class UpdatePopupView implements IRootCarrier {

    private _updateProgressPopup: PopupNode;
    private _root: AggregateNode;

    public setRoot(root: AggregateNode): void {
        this._root = root;
    }

    public showUpdatePopup(): void {
        this._updateProgressPopup = PopupsWarehouse.instance.getPopupContentByName('UpdateProgressPopup');
        // tslint:disable-next-line:no-magic-numbers
        this._updateProgressPopup.setAnchorPoint(cc.v2(0.5, 0.5));
        this._updateProgressPopup.setPosition(cc.v2(0, 0));
        this._root.addChild(this._updateProgressPopup);
    }

    public updateProgression(progress: number): void {
        this._updateProgressPopup.getChildByName('ProgressBar').getComponent(cc.ProgressBar).progress = progress;
    }
}

export default class ManifestConfig {

    public static readonly CODE_MANIFEST_PATH: string = '/resources/versions/code/';
    public static readonly LOBBY_MANIFEST_PATH: string = '/resources/versions/lobby/';
    public static readonly GAMES_MANIFEST_PATH: string = '/resources/versions/games/';

    public static readonly MANIFEST_FILE_NAME: string = 'project.manifest';
}

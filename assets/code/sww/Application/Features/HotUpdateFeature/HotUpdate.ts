import Notification from '../../../Core/Notifications/Notification';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import { ApplicationNotification, ApplicationNotificationTopic } from '../../Notifications/ApplicationNotifications';

// ALL log() will be move to custom loger write to file result and send to server
export default class HotUpdate {

    public id: string;
    public newVersionFound: boolean;

    private _manifestUrl: string;
    private _updating: boolean;
    private _storagePath: string;
    private _am = null;
    private _checkListener = null;
    private _updateListener = null;

    public checkUpdate(): Promise<any> {
        return new Promise((res, rej) => {
            if (this._am === null) {
                cc.log('ERROR: Asset Manager not initialization => need call init()');
                rej(false);

                return;
            }

            if (this._updating) {
                cc.log('Checking or updating ...');
                rej(false);

                return;
            }

            if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
                cc.log(this.manifestUrl);
                this._am.loadLocalManifest(this.manifestUrl);
            }

            if (!this._am.getLocalManifest() || !this._am.getLocalManifest().isLoaded()) {
                cc.log('Failed to load local manifest ...');
                rej(false);

                return;
            }

            // listen status check block
            // ----------------------------------------------------------------------------
            this._checkListener = new jsb.EventListenerAssetsManager(this._am, (event) => {
                cc.log(event.getEventCode());
                let result: boolean;
                switch (event.getEventCode()) {
                    case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                        cc.log('No local manifest file found, hot update skipped.');
                        result = false;
                        break;
                    case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
                        cc.log('ERROR_DOWNLOAD_MANIFEST.');
                        result = false;
                        break;
                    case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                        cc.log('ERROR_PARSE_MANIFEST.');
                        result = false;
                        break;
                    case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                        result = false;
                        cc.log('Already up to date with the latest remote version.');
                        break;
                    case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                        this.newVersionFound = true;
                        result = true;
                        cc.log('New version found, please try to update.');
                        break;
                    default:
                        return; // Not remove this !!!!!!!!!!
                }
                cc.eventManager.removeListener(this._checkListener);
                this._checkListener = null;
                this._updating = false;

                res(result);
            });
            // ----------------------------------------------------------------------------

            cc.eventManager.addListener(this._checkListener, 1);
            this._am.checkUpdate();
            this._updating = true;
        });
    }

    public hotUpdate(): Promise<any> {

        return new Promise((res, rej) => {
            if (this._am && !this._updating) {
                cc.log(this.id);
                // update listener block
                // -----------------------------------------------------------------------------
                this._updateListener = new jsb.EventListenerAssetsManager(this._am, (event) => {
                    let  needRestart = false;
                    let  failed = false;
                    let result: boolean;
                    switch (event.getEventCode()) {
                        case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                            cc.log('No local manifest file found, hot update skipped.');
                            failed = true;
                            result = false;
                            break;
                        case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                            const msg = event.getMessage();
                            const percent: number = event.getDownloadedFiles() / event.getTotalFiles();
                            NotificationManager.instance
                                .dispatch(new Notification(ApplicationNotificationTopic.AssetsUpdate,
                                                           ApplicationNotification.HotUpdateProgress,
                                                           percent));
                            if (msg) {
                                cc.log(msg);
                            }

                            return;
                        case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
                        case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                            cc.log('Fail to download manifest file, hot update skipped.');
                            failed = true;
                            result = false;
                            break;
                        case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                            cc.log('Already up to date with the latest remote version.');
                            failed = true;
                            result = false;
                            break;
                        case jsb.EventAssetsManager.UPDATE_FINISHED:
                            cc.log('update finished');
                            needRestart = true;
                            this.newVersionFound = false;
                            result = true;
                            break;
                        case jsb.EventAssetsManager.UPDATE_FAILED:
                            cc.log('Update failed.');
                            this._updating = false;
                            result = false;
                            this._am.downloadFailedAssets();
                            break;
                        case jsb.EventAssetsManager.ERROR_UPDATING:
                            cc.log(this.id);
                            cc.log(event.getAssetId());
                            cc.log('Asset update error');
                            result = false;
                            break;
                        case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                            cc.log('Error decompress');
                            result = false;
                            break;
                        default:
                            return;
                    }

                    if (failed) {
                        cc.eventManager.removeListener(this._updateListener);
                        this._updateListener = null;
                        this._updating = false;
                        rej(result);
                    }

                    if (needRestart) {
                        cc.eventManager.removeListener(this._updateListener);
                        this._updateListener = null;
                        const searchPaths = jsb.fileUtils.getSearchPaths();

                        cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
                        jsb.fileUtils.setSearchPaths(searchPaths);
                        res(result);
                    }
                });
                // -----------------------------------------------------------------------------

                cc.eventManager.addListener(this._updateListener, 1);

                if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
                    this._am.loadLocalManifest(this.manifestUrl);
                }

                // this._failCount = 0;
                this._am.update();
                this._updating = true;
            }
        });
    }

    public init(): void {

        if (!cc.sys.isNative) {
            return;
        }

        const writablePath: string = jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/';
        this._storagePath = `${writablePath}SWWapp/${this.id}`;

        this._am = new jsb.AssetsManager('', this._storagePath, this.versionCompareHandle);
        this._am.retain();
        this._am.setVerifyCallback((path, asset) => {
            const compressed = asset.compressed;
            const expectedMD5 = asset.md5;
            const relativePath = asset.path;
            const size = asset.size;

            return true;
        });

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            // Some Android device may slow down the download process when concurrent tasks is too much.
            // The value may not be accurate, please do more test and find what's most suitable for your game.
            const maxConcurrentTask = 2;
            this._am.setMaxConcurrentTask(maxConcurrentTask);
        }

        this.newVersionFound = false;
    }

    public set manifestUrl(url: string) {
        this._manifestUrl = cc.url.raw(url);
    }

    public get manifestUrl(): string {
        return this._manifestUrl;
    }

    public onDestroy(): void {
        if (this._updateListener) {
            cc.eventManager.removeListener(this._updateListener);
            this._updateListener = null;
        }
        if (this._am) {
            this._am.release();
        }
    }

    private versionCompareHandle(versionA, versionB): number {

        const vA = versionA.split('.');
        const vB = versionB.split('.');
        for (let i = 0; i < vA.length; ++i) {
            const a = parseInt(vA[i], null);
            const b = parseInt(vB[i] || 0, null);
            if (a === b) {
                continue;
            } else {
                return a - b;
            }
        }
        if (vB.length > vA.length) {
            return -1;
        } else {
            return 0;
        }
    }
}

import AggregateNode from '../../../../../showcases/Network/AggregateNode';
import IFeature from '../../../Core/Foundation/Feature/IFeature';
import INotification from '../../../Core/Notifications/INotification';
import IObserver from '../../../Core/Notifications/IObserver';
import Notification from '../../../Core/Notifications/Notification';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import IRootCarrier from '../../../Supply/Interfaces/IRootCarrier';
import { ApplicationNotification, ApplicationNotificationTopic } from '../../Notifications/ApplicationNotifications';
import ManifestConfig from './Config/ManifestConfig';
import HotUpdate from './HotUpdate';
import HotUpdateManager from './HotUpdateManager';
import UpdatePopupView from './View/UpdatePopupView';

export default class HotUpdateFeature implements IFeature, IObserver, IRootCarrier {

    public static NAME = 'HotUpdateFeature';

    private _hotUpdateManager: HotUpdateManager;
    private _updatePopupView: UpdatePopupView;
    private _root: AggregateNode;

    constructor() {
        this._hotUpdateManager = new HotUpdateManager();
        this._updatePopupView = new UpdatePopupView();

        this.registerListeners();
    }

    public setRoot(root: AggregateNode): void {
        this._root = root;
        this._updatePopupView.setRoot(root);
    }

    public start(): void {
        // temp need save this in some other place
        if (cc.sys.isNative) {
            this.checkHotUpdate();
        } else {
            this.hotUpdateFinished();
        }

    }

    public observe(notification: INotification): void {
        if (notification.topicId !== ApplicationNotificationTopic.AssetsUpdate) {
            return;
        }

        switch (notification.id) {
            case ApplicationNotification.HotUpdateChecked:
                // in here we can show popup/button or other for user about new version
                const allUpToDate = notification.arguments.allUpToDate;
                if (!allUpToDate) {
                    this.hotUpdate();
                } else {
                    this.hotUpdateFinished();
                }

                return;
            case ApplicationNotification.HotUpdateFinished:
                return this.hotUpdateFinished();
            case ApplicationNotification.HotUpdateProgress:
                this._updatePopupView.updateProgression(notification.arguments);
                break;
            default:
                return;
        }
    }

    public createCodeUpdate(): void {
        const codeHotUpdate = new HotUpdate();
        codeHotUpdate.id = 'code';
        codeHotUpdate.init();
        codeHotUpdate.manifestUrl = ManifestConfig.CODE_MANIFEST_PATH + ManifestConfig.MANIFEST_FILE_NAME;
        this._hotUpdateManager.addHotUpdate(codeHotUpdate);
    }

    public cerateResourcesUpdate(resId: string): void {
        const gameHotUpdate = new HotUpdate();
        gameHotUpdate.id = resId;
        gameHotUpdate.init();
        gameHotUpdate.manifestUrl = `${ManifestConfig.GAMES_MANIFEST_PATH}${resId}/${ManifestConfig.MANIFEST_FILE_NAME}`;
        this._hotUpdateManager.addHotUpdate(gameHotUpdate);
    }

    public initAllUpdaters() {
        this._hotUpdateManager.initAllHotUpdaters();
    }

    public async checkHotUpdate(id?: string): Promise<any> {

        if (id !== undefined) {
            await this._hotUpdateManager.checkUpdate(id);
        } else {
            await this._hotUpdateManager.checkUpdateInAllUpdaters();
        }

        const allUpToDate = this._hotUpdateManager.isAllAlreadyUpToDate();
        NotificationManager.instance
            .dispatch(new Notification(ApplicationNotificationTopic.AssetsUpdate,
                                       ApplicationNotification.HotUpdateChecked,
                                       {id, allUpToDate}));
    }

    public async hotUpdate(id?: string): Promise<any> {
        this._updatePopupView.showUpdatePopup();

        if (id !== undefined) {
            await this._hotUpdateManager.update(id);
        } else {
            await this._hotUpdateManager.updateAllReadyUpdaters();
        }
        NotificationManager.instance
            .dispatch(new Notification(ApplicationNotificationTopic.AssetsUpdate,
                                       ApplicationNotification.HotUpdateFinished,
                                       id));
    }

    private hotUpdateFinished(): void {
        NotificationManager.instance.dispatch(new Notification(ApplicationNotificationTopic.Domain,
                                                               ApplicationNotification.HotUpdateFinished));
    }

    private registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, ApplicationNotificationTopic.AssetsUpdate, ApplicationNotification.HotUpdateProgress)
            .addObserver(this, ApplicationNotificationTopic.AssetsUpdate, ApplicationNotification.HotUpdateChecked)
            .addObserver(this, ApplicationNotificationTopic.AssetsUpdate, ApplicationNotification.HotUpdateFinished);
    }
}

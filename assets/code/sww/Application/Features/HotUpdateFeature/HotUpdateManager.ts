import HotUpdate from './HotUpdate';

export default class HotUpdateManager {

    public hotUpdaters: Array<HotUpdate> = [];

    public addHotUpdate(updater: HotUpdate): void {
        this.hotUpdaters.push(updater);
    }

    public initAllHotUpdaters(): void {
        this.hotUpdaters.forEach((hotUpdate) => hotUpdate.init());
    }

    public async updateAllReadyUpdaters(): Promise<any> {
        if (this.isAllAlreadyUpToDate()) {
            return;
        }

        let i: number;
        for (i = 0; i < this.hotUpdaters.length; i++) {
            if (this.hotUpdaters[i].newVersionFound) {
                await this.hotUpdaters[i].hotUpdate();
            }
        }

        if (this.isAllAlreadyUpToDate) {
            cc.game.restart();
        }
    }

    public async checkUpdateInAllUpdaters(): Promise<any> {
        let i: number;
        for (i = 0; i < this.hotUpdaters.length; i++) {
            await this.hotUpdaters[i].checkUpdate();
        }
    }

    public async checkUpdate(id: string): Promise<any> {
        const updater = this.getHotUpdater(id);
        updater.checkUpdate();
    }

    public async update(id: string): Promise<any> {
        this.getHotUpdater(id).hotUpdate();
    }

    public getHotUpdater(id: string): HotUpdate {
        let hotUpdate: HotUpdate;
        hotUpdate = this.hotUpdaters.find((hotUp) => hotUp.id === id);

        return hotUpdate;
    }

    public isAllAlreadyUpToDate(): boolean {
        const needUpdateModule = this.hotUpdaters.find((hotUpdater) => hotUpdater.newVersionFound === true);

        return needUpdateModule === undefined;
    }
}

import AggregateNode from '../../../../../showcases/Network/AggregateNode';
import IFeature from '../../../Core/Foundation/Feature/IFeature';
import IRootCarrier from '../../../Supply/Interfaces/IRootCarrier';
import PreloaderView from './View/PreloaderView';

export default class PreloadFeature implements IFeature, IRootCarrier {

    public static NAME = 'PreloadFeature';

    private _root: AggregateNode;
    private _preloaderView: PreloaderView;

    constructor() {
        this._preloaderView = new PreloaderView();
    }

    public setRoot(root: AggregateNode): void {
        this._root = root;
        this._preloaderView.setNode(this._root.getChildByName('ProgressBar'));
    }

    public start(): void {
        this._preloaderView.startPreloadApp('FlamingPearl');
    }
}

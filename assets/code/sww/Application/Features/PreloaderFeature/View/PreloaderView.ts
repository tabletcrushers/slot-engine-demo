import AggregateNode from '../../../../../../showcases/Network/AggregateNode';
import INodeCarrier from '../../../../Supply/Interfaces/INodeCarrier';

export default class PreloaderView implements INodeCarrier {

    private _loadSceneProgressBar: AggregateNode;

    public setNode(node: any): void {
        this._loadSceneProgressBar = node;
    }

    public setPreloadProgress(value: number): void {
        if (!this._loadSceneProgressBar._components) {
            return;
        }
        this._loadSceneProgressBar.getComponent(cc.ProgressBar).progress = value;
    }

    public startPreloadApp(id: string) {
        this.preloadScene(id);
    }

    private preloadScene(id: string) {
        cc.loader.onProgress = this.progressLoadScene.bind(this);

        cc.director.preloadScene(id, (error) => {
            if (error !== null) {

                throw(error);
            }
            cc.director.loadScene(id);
        });
    }

    private progressLoadScene(completedCount: number, totalCount: number, item: any): void {
        const presentLoaded = completedCount / totalCount;
        if (presentLoaded > 0 && presentLoaded < 1) {
            this.setPreloadProgress(presentLoaded);
        }
    }
}

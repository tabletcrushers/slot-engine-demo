// notif topics
export enum ApplicationNotificationTopic {
    Server = 'Server',
    Domain = 'Domain',
    AssetsUpdate = 'AseetsUpdate',
    UserProfile = 'UserProfile',
    Game = 'Game'
}

// notif id
export enum UserProfilerNotification {
    BalanceChanged = 'BalanceChanged',
    NotifyBalance = 'NotifyBalance',
    SetUserNickName = 'SetUserNickName',
    UpdateBalance = 'UpdateBalance',
    UpdateLevel = 'UpdateLevel'
}

export enum ApplicationNotification {
    // game
    StartGame = 'StartGame',
    StartPreloadingGame = 'StartPreloadingGame',
    // hot update
    HotUpdateChecked = 'HotUpdateChecked',
    HotUpdateFinished = 'HotUpdateFinished',
    HotUpdateProgress = 'HotUpdateProgress',
    // user
    RegisterUser = 'RegisterUser'
}

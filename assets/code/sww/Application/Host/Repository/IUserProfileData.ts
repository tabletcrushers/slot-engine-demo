export default interface IUserProfileData {
    name: string;
    level: number;
    balance: number;
}

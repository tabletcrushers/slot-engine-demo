import AggregateNode from '../../../../showcases/Network/AggregateNode';
import IDALRepository from '../../Core/Foundation/DAL/IDALRepository';
import Domain from '../../Core/Foundation/Domain/Domain';
import DomainManager from '../../Core/Foundation/Domain/DomainManager';
import INotification from '../../Core/Notifications/INotification';
import IObserver from '../../Core/Notifications/IObserver';
import Notification from '../../Core/Notifications/Notification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import FlamingPearlDomain from '../../Games/Slots/FlamingPearl/Domain/FlamingPearlDomain';
import IRootCarrier from '../../Supply/Interfaces/IRootCarrier';
import HotUpdateFeature from '../Features/HotUpdateFeature/HotUpdateFeature';
import PreloadFeature from '../Features/PreloaderFeature/PreloadFeature';
import { ApplicationNotification, ApplicationNotificationTopic } from '../Notifications/ApplicationNotifications';
import ITokenData from './Repository/IApplicationData';
import IUserProfileData from './Repository/IUserProfileData';

export default class ApplicationDomain extends Domain implements IObserver, IRootCarrier {

    public static readonly NAME: string = 'ApplicationDomain';

    private _userProfile: IDALRepository;
    private _rootContent: AggregateNode;

    // notifications
    private _startGameNotification: INotification;

    constructor() {
        super();
        DomainManager.instance.addDomain(ApplicationDomain.NAME, this);
    }

    public setRoot(root: AggregateNode): void {
        this._rootContent = root;
    }

    public init(): void {
        this.initRepositories();
        this.registerListeners();
        this.createNotifications();
        this.initFeatures();
        // // this must be calling when load json or take some informarion about game what need start in this method
        // // need start factory what create all need blocks like domain/networkCommunicator and others
        this.prepareStartApplication();
    }

    public get userData(): IUserProfileData {
        return this._userProfile.read<IUserProfileData>('UserInfo');
    }

    public set userData(userData: IUserProfileData) {

        this._userProfile.write('UserInfo', userData);
    }

    public get token(): ITokenData {
        return this._userProfile.read<ITokenData>('Token');
    }

    public set token(token: ITokenData) {
        this._userProfile.write('Token', token);
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== ApplicationNotificationTopic.Server &&
            notification.topicId !== ApplicationNotificationTopic.Domain) {
            return;
        }
        switch (notification.id) {
            case ApplicationNotification.HotUpdateFinished:

                NotificationManager.instance
                    .dispatch(this._startGameNotification);

                return;
            case ApplicationNotification.StartPreloadingGame:

                return this.getFeature<PreloadFeature>(PreloadFeature.NAME).start();
            default:
                return;
        }
    }

    // TODO: need to be rewrited, just for testing
    public isPossibleBet(bet: number): boolean {
        return (this.userData.balance - bet) >= 0;
    }

    protected initRepositories(): void {
        this.getDALProvider().addRepository('UserProfileData');
        this._userProfile = this.getDALProvider().getRepository('UserProfileData');
    }

    protected registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, ApplicationNotificationTopic.Domain, ApplicationNotification.HotUpdateFinished)
            .addObserver(this, ApplicationNotificationTopic.Domain, ApplicationNotification.StartPreloadingGame);
    }

    protected removeListeners(): void {
        NotificationManager.instance
            .removeObserver(this, ApplicationNotificationTopic.Domain, ApplicationNotification.HotUpdateFinished)
            .removeObserver(this, ApplicationNotificationTopic.Domain, ApplicationNotification.StartPreloadingGame);
    }

    protected createNotifications(): void {
        this._startGameNotification = new Notification(ApplicationNotificationTopic.Game, ApplicationNotification.StartGame);
    }

    protected initFeatures(): void {

        const hotUpdateFeature: HotUpdateFeature = new HotUpdateFeature();
        hotUpdateFeature.setRoot(this._rootContent);
        hotUpdateFeature.createCodeUpdate();
        hotUpdateFeature.cerateResourcesUpdate('game1'); // tepm
        hotUpdateFeature.initAllUpdaters();

        this.addFeature(HotUpdateFeature.NAME, hotUpdateFeature);

        const preloadFeature: PreloadFeature = new PreloadFeature();
        preloadFeature.setRoot(this._rootContent);

        this.addFeature(PreloadFeature.NAME, preloadFeature);
    }

    private prepareStartApplication(): void {
        // TODO: temp for game in future here init lobby host
        // need create SlotGameFactory where will be create all classes for single game
        DomainManager.instance.addDomain(FlamingPearlDomain.NAME, new FlamingPearlDomain());

        this.getFeature<HotUpdateFeature>(HotUpdateFeature.NAME).start();
    }
}

import AggregateNode from '../../../../showcases/Network/AggregateNode';
import PopupsWarehouse from '../../Supply/Popups/PopupsWarehouse';
import TypeUtil from '../../Utils/TypeUtil';
import ApplicationDomain from '../Host/ApplicationDomain';

const {ccclass, property} = cc._decorator;

@ccclass
export default class ApplicationMainComponent extends cc.Component {

    @property(cc.Prefab)
    public updateProgressPopupPrefab: cc.Prefab = null;

    private applicationMain: ApplicationDomain;

    public start(): void {
        cc.view.setResizeCallback(this.onStageResize.bind(this));
        this.onStageResize();
    }

    public onLoad(): void {
        PopupsWarehouse.instance.addPopupContent(cc.instantiate(this.updateProgressPopupPrefab));

        this.applicationMain = new ApplicationDomain();
        this.applicationMain.setRoot(TypeUtil.castTo<AggregateNode>(this.node));
        this.applicationMain.init();
    }

    public get isNativePlatform(): boolean {
        return cc.sys.isNative;
    }

    // TODO - move number values to config based on art
    // move onStageResize to ResizeableComponent level
    public onStageResize() {
        // tslint:disable-next-line:no-magic-numbers
        const originSize: cc.Size = new cc.Size(1680, 1050);
        const screenSize: cc.Size = cc.view.getVisibleSizeInPixel();
        cc.view.setDesignResolutionSize(screenSize.width, screenSize.height, cc.ResolutionPolicy.NO_BORDER);

        const originAspect = originSize.height / originSize.width;
        const stageAspect = screenSize.height / screenSize.width;
        let scale = 1;

        scale = stageAspect > originAspect
            ? screenSize.width / originSize.width
            : screenSize.height / originSize.height;

        this.node.setScale(scale, scale);
    }
}

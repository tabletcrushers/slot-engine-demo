import WinLineSegment from '../../../../../../showcases/LinesConfigurator/WinLineSegment';
import Point from '../../../../Core/Geometry/Point';

export default interface IDrawLine {

    segments: Array<WinLineSegment>;
    anchorPoints: Array<Point>;
}

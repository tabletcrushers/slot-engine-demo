import Color from '../../../../Supply/Custom/Color';

export default interface ILine {
    id: number;
    serverId: number;
    mode: string;
    color: Color;
    nodes: Array<number>;
}

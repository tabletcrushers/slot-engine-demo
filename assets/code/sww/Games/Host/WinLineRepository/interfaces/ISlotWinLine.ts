export default interface ISlotWinLine {
    winSymbolsLine: Array<number>;
    winLineId: number;
}

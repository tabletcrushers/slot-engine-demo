import IDrawLine from './IDrawLine';
import ILine from './ILine';

export default interface ILines {
    // TODO: need refactoring to map with key id line and map server id with generated in parser
    lines: Array<ILine>;
    linesView: Array<IDrawLine>;
}

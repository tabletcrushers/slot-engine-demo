export default interface ITriggerSymbols {
    winSymbolsLine: Array<ISymbolPosition>;
    trigger: string;
}

export interface ISymbolPosition {
    field: number;
    reel: number;
    indexOnReel: number;
}

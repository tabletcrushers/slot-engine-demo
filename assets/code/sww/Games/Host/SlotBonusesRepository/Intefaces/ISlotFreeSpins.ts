export default interface ISlotFreeSpins {
    gameFieldId: number;
    count: number;
    gameRound: number;
    isActive: boolean;
}

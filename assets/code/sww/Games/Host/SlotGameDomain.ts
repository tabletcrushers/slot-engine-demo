import SlotSpinMode from '../../../../showcases/LinesConfigurator/stateForDomain/SlotSpinMode';
import SpinMode from '../../../../showcases/LinesConfigurator/stateForDomain/SpinMode';
import AggregateNode from '../../../../showcases/Network/AggregateNode';
import ICommonUISettingsDTO from '../../../../showcases/PopupGen/src/ICommonUISettingsDTO';
import {
    ApplicationNotification,
    ApplicationNotificationTopic
} from '../../Application/Notifications/ApplicationNotifications';
import IBetManager from '../../Application/User/IBetManager';
import StepBetManager from '../../Application/User/StepBetManager';
import IDALRepository from '../../Core/Foundation/DAL/IDALRepository';
import Domain from '../../Core/Foundation/Domain/Domain';
import IUpdatable from '../../Core/Foundation/Update/IUpdatable';
import IGameConfigDTO from '../../Core/Games/Slots/Configuration/DTO/IGameConfigDTO';
import IGameModeConfigDTO from '../../Core/Games/Slots/Configuration/DTO/IGameModeConfigDTO';
import IModeDTO from '../../Core/Games/Slots/Configuration/DTO/IModeDTO';
import GameConfiguration from '../../Core/Games/Slots/Configuration/GameConfiguration';
import JsonConfigurator from '../../Core/Games/Slots/Configuration/JsonConfigurator';
import SlotGameConfiguratorFeature from '../../Core/Games/Slots/Features/SlotGameConfigurator/SlotGameConfiguratorFeature';
import WinLinesAnimation from '../../Core/Games/Slots/Features/WinAnimations/WinLinesAnimations';
import AllWinLinesDrawerFeature from '../../Core/Games/Slots/Features/WinLineDrawer/Features/AllWinLinesDrawerFeature';
import SingleWinLineShowingFeature from '../../Core/Games/Slots/Features/WinLineDrawer/Features/SingleWinLineShowingFeature';
import LineDrawerFeature from '../../Core/Games/Slots/Features/WinLineDrawer/LineDrawerFeature';
import AnimationFrameShowing from '../../Core/Games/Slots/Features/WinLineDrawer/Utils/AnimationFrameShowing';
import WinLinesDrawer from '../../Core/Games/Slots/Features/WinLineDrawer/Utils/WinLinesDrawer';
import {
    ActionPanelNotification,
    ReelEngineNotification,
    ReelEngineNotificationTopic,
    SlotGameNotification,
    SlotGameNotificationTopic,
    WinLineNotification
} from '../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import FrameAtlasProvider from '../../Core/Games/Slots/ReelsEngine/Core/Implementation/Providers/FrameAtlasProvider';
import ReelsEngine from '../../Core/Games/Slots/ReelsEngine/Core/ReelsEngine';
import INotification from '../../Core/Notifications/INotification';
import IObserver from '../../Core/Notifications/IObserver';
import Notification from '../../Core/Notifications/Notification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import FakeNetworkCommunicator from '../../Network/Communicator/FakeNetworkCommunicator';
import IRootCarrier from '../../Supply/Interfaces/IRootCarrier';
import PopupFeature from '../../Supply/Popups/features/PopupFeature';
import IPaytablePaylineConfigDTO from '../../Supply/Popups/interfaces/IPaytablePaylineConfigDTO';
import { PopupsNotifications, PopupsNotificationTopic } from '../../Supply/Popups/Notification/PopupsNotifications';
import PopupGenerator from '../../Supply/Popups/PopupGenerator';
import RenderEngine from '../../Supply/RenderEngine/RenderEngine';
import ColorUtil from '../../Utils/Colors/ColorUtil';
import LoaderUtil from '../../Utils/LoaderUtil';
import SlotResponseParser from '../../Utils/Slots/SlotResponseParser';
import TypeUtil from '../../Utils/TypeUtil';
import { SlotGameRepositoryDataEnum } from './SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from './SlotGameRepositoryEnum';
import IPaytableSymbol from './SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbol';
import IPaytableSymbols from './SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbols';
import ILine from './WinLineRepository/interfaces/ILine';
import ILines from './WinLineRepository/interfaces/ILines';

export default class SlotGameDomain extends Domain implements IObserver, IRootCarrier, IUpdatable {

    public static readonly NAME: string = 'SlotGameDomain';
    public _canUpdate: boolean = false;
    protected _slotNetworkCommunicator: FakeNetworkCommunicator;
    protected _root: AggregateNode;
    protected _mode: SlotSpinMode;
    private _betManager: IBetManager;
    private _startPreloadNotification: INotification;

    constructor() {
        super();

        this.initRepositories();
        this.registerListeners();
        this.registerModes();
        this.createNotifications();

        // features
        this._slotNetworkCommunicator = new FakeNetworkCommunicator();
        this._slotNetworkCommunicator.setParser(new SlotResponseParser());
    }

    public setRoot(root: AggregateNode): void {
        this._root = root;
    }

    public registerModes(): void {
        // nugno dorabobtat spin mode
        // da, bratok, ya soglasen. Kto uvidit etot koment, poluchit ot menya pivo
        this.addMode('SpinMode', new SpinMode());
    }

    public init(): void {
        // tslint:disable-next-line:no-magic-numbers
        this._betManager = new StepBetManager([1, 17], 4, false);

        this.initFeatures();
    }

    public observe(notification: INotification): void {

        switch (notification.id) {
            // network
            case ApplicationNotification.StartGame:
                return this.prepareGameInformation();
            case SlotGameNotification.LockUpdate:
                this._canUpdate = false;

                return;
            case SlotGameNotification.UnlockUpdate:
                this._canUpdate = true;

                return;
            case SlotGameNotification.SlotGameToken:
                return this._slotNetworkCommunicator.createStartGameRequest(null);
            case SlotGameNotification.SlotStartGameInformation:
                this.loadConfiguration().then(() => {
                    NotificationManager.instance.dispatch(this._startPreloadNotification);
                });

                return;
            case ReelEngineNotification.ReelsEngineStop:
                    this._mode.stopSpin();

                    return;
            case ActionPanelNotification.SpinStart:
                this._slotNetworkCommunicator.createSpinRequest({lines: 10, bet: this._betManager.currentBet});
                this._mode.startSpin({lines: 10, bet: this._betManager.currentBet});

                return;
            case PopupsNotifications.ShowSinglePopup:
                this.getFeature<PopupFeature>(PopupFeature.NAME).showPopup(notification.arguments);

                return;
            case PopupsNotifications.ShowMultiplePopup:
                this.getFeature<PopupFeature>(PopupFeature.NAME).showMultiplePopup(notification.arguments);

                return;
            default:
                return;
        }
    }

    public update(delta: number): void {
        if (!this._canUpdate) {
            return;
        }

        this._mode.update(delta);
    }

    public get betManager(): IBetManager {
        return this._betManager;
    }

    public getPaytableSymbolByName(name: string): IPaytableSymbol {
        return this.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.PaytableRepository)
            .read<IPaytableSymbols>(SlotGameRepositoryDataEnum.PaytableSymbols)
            .symbols.find((symbol) => symbol.name === name);
    }

    public getLine(id: number): ILine {
        const linesRepository = this.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.LinesRepository)
            .read<ILines>(SlotGameRepositoryDataEnum.Lines);

        return linesRepository.lines.find((line) => line.serverId === id);
    }

    public getCommonUISetting<T>(uiComponentName: string): T {
        // tslint:disable-next-line:max-line-length
        return this.getDALProvider().getRepository(SlotGameRepositoryEnum.CommonUISettings).read<T>(uiComponentName);
    }

    protected initRepositories(): void {
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.GameProfile);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.PaytableRepository);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.LinesRepository);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.SpinResult);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.AutoPlayRepository);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.GameConfigurationRepository);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.CommonUISettings);
        this.getDALProvider().addRepository(SlotGameRepositoryEnum.PopupsConfigRepository);
    }

    protected registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, ApplicationNotificationTopic.Game, ApplicationNotification.StartGame)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotGameToken)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotStartGameInformation)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotGameStarted)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.LockUpdate)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.UnlockUpdate)
            .addObserver(this, SlotGameNotificationTopic.Domain, ActionPanelNotification.SpinStart)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult)
            // reel engine
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStart)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStop)
            // win line
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.WinLineShowed)
            // popups
            .addObserver(this, PopupsNotificationTopic.GamePopup, PopupsNotifications.ShowSinglePopup)
            .addObserver(this, PopupsNotificationTopic.GamePopup, PopupsNotifications.ShowMultiplePopup);
    }

    protected removeListeners(): void {
        NotificationManager.instance
            .removeObserver(this, ApplicationNotificationTopic.Game, ApplicationNotification.StartGame)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotGameToken)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotStartGameInformation)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotGameStarted)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.LockUpdate)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.UnlockUpdate)
            .removeObserver(this, SlotGameNotificationTopic.Domain, ActionPanelNotification.SpinStart)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult)
            .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStart)
            .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStop)
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.WinLineShowed)
            .removeObserver(this, PopupsNotificationTopic.GamePopup, PopupsNotifications.ShowSinglePopup)
            .removeObserver(this, PopupsNotificationTopic.GamePopup, PopupsNotifications.ShowMultiplePopup);
    }

    protected createNotifications(): void {
        this._startPreloadNotification =
            new Notification(ApplicationNotificationTopic.Domain, ApplicationNotification.StartPreloadingGame);
    }

    protected initFeatures(): void {
        const reelsEngine = new ReelsEngine();

        const renderEngine = new RenderEngine();
        renderEngine.renderNode = this._root;
        renderEngine.frameAtlasProvider = FrameAtlasProvider.instance;

        const lineDrawerFeature = new LineDrawerFeature();
        lineDrawerFeature.setRoot(this._root);

        const popupFeature = new PopupFeature();
        popupFeature.setRoot(this._root);
        popupFeature.setPaytableConfig(this.getCommonUISetting<IPaytablePaylineConfigDTO>('paytable'));

        const spinMode = this.getMode<SpinMode>('SpinMode');
        spinMode.setRoot(this._root);

        const winLineDrawer = new WinLinesDrawer();
        winLineDrawer.setRoot(this._root);

        let particleFrameDrawer: AnimationFrameShowing;
        // temp
        LoaderUtil.loadPrefab(`animations/FrameAnimation.prefab`).then((prefab: cc.Prefab) => {
            particleFrameDrawer = new AnimationFrameShowing(prefab);
            particleFrameDrawer.setRoot(cc.find('Canvas/LineRenderNode') as AggregateNode);

            this.addFeature('ReelsEngine', reelsEngine);
            this.addFeature('RenderEngine', renderEngine);
            this.addFeature(WinLinesAnimation.NAME, new WinLinesAnimation(reelsEngine, renderEngine));
            this.addFeature(SlotGameConfiguratorFeature.NAME, new SlotGameConfiguratorFeature(reelsEngine, renderEngine));
            this.addFeature(AllWinLinesDrawerFeature.NAME, new AllWinLinesDrawerFeature(winLineDrawer));
            this.addFeature(SingleWinLineShowingFeature.NAME, new SingleWinLineShowingFeature(particleFrameDrawer));
            this.addFeature(PopupFeature.NAME, popupFeature);

            this._mode = spinMode;
            this._mode.init();
            this._mode.registerListeners();
        });
    }

    protected prepareGameInformation(): void {
    }

    private async loadConfiguration() {
        await this.loadGameConfigurations();
        await this.inscribeColorForLine();
        await this.loadUIComponentSetting();
        await this.loadPopupsConfiguration();
    }

    private async loadGameConfigurations() {
        const gameConfigurationRepository: IDALRepository =
            this.getDALProvider().getRepository(SlotGameRepositoryEnum.GameConfigurationRepository);

            // TODO - path to global config
        await LoaderUtil.loadGameConfiguration('resources/FlamingPearl/ReelsEngineConfiguration/GameConfig.json')
            .then((config: IGameConfigDTO) => {
                const gameConfiguration: GameConfiguration = this.createGameConfiguration(config);
                gameConfigurationRepository.write(SlotGameRepositoryEnum.GameConfigurationRepository, gameConfiguration);
            });
    }

    private async inscribeColorForLine() {
        // TODO - path to global config
        await LoaderUtil.loadJSON('/resources/WaysConfiguration/ways_drawer_config.json').then((drawConfig: any) => {
            const repository = this.getDALProvider()
                .getRepository(SlotGameRepositoryEnum.LinesRepository)
                .read<ILines>(SlotGameRepositoryDataEnum.Lines);

            repository.lines.forEach((line, index) => {
                line.color = ColorUtil.hexToRGBA(drawConfig.waysColor[index]);
                line.mode = drawConfig.mode;
            });
        });
    }

    private loadUIComponentSetting(): void {
        LoaderUtil.loadJSON<ICommonUISettingsDTO>('resources/UIConfig/commonUISettings.json').then((config) => {
            Object.keys(config).forEach((value) => {
                this.getDALProvider().getRepository(SlotGameRepositoryEnum.CommonUISettings).write(value, config[value]);
            });
        });
    }

    private loadPopupsConfiguration(): void {
        LoaderUtil.loadJSON('resources/PopupsConfiguration/popupsConfig.json').then((config) => {
            PopupGenerator.instance.config = config;
        });

    // TODO - GameAssetsAnalyzer?
    private createGameConfiguration(config: IGameConfigDTO): GameConfiguration {
        const gameConfiguration: GameConfiguration = new GameConfiguration(config.itemsConfiguration);
        const configurator: JsonConfigurator = new JsonConfigurator();

        config.modes.forEach((gameMode: IGameModeConfigDTO) => {
            const modeDTO: IModeDTO = TypeUtil.provideInterfaceInstance<IModeDTO>();
            modeDTO.reelsEngineConfiguration = configurator.makeReelsEngineConfiguration(
                gameMode.configuration,
                config.screen
            );
            modeDTO.fakeItemsConfiguration = configurator.makeFakeItems(
                gameMode.fakeItemsConfiguration
            );

            gameConfiguration.set(gameMode.id, modeDTO);
        });

        return gameConfiguration;
    }
}

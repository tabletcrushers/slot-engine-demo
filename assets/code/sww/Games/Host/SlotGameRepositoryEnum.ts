export enum SlotGameRepositoryEnum {
    GameProfile,
    PaytableRepository,
    LinesRepository,
    SpinResult,
    AutoPlayRepository,
    GameConfigurationRepository,
    ScreenRepository,
    ItemsRepository,
    CommonUISettings,
    PopupsConfigRepository
}

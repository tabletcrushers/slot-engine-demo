export default interface ISlotGameProfile {
    name: string;
    dir: string;
    gameRequestUrl: string;
    token: string;
    // TODO temp need think where save this info
    seq: number;
    gameRound: number;
}

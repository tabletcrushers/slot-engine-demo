export enum SlotGameRepositoryDataEnum {
    SlotGameProfile,
    ServerSpinResult,
    SpinResultWin,
    TriggerSymbols,
    FreeSpins,
    Lines,
    SpinResultWinLines,
    PaytableSymbols
}

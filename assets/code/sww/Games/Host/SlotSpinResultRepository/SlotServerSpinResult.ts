import GameFieldSpinResultDTO from '../../../Core/Games/Slots/Configuration/DTO/SpinResult/GameFieldSpinResultDTO';
import ReelSpinResultDTO from '../../../Core/Games/Slots/Configuration/DTO/SpinResult/ReelSpinResultDTO';
import SpinResultDTO from '../../../Core/Games/Slots/Configuration/DTO/SpinResult/SpinResultDTO';
import ISlotServerSpinResult from './Interfaces/ISlotServerSpinResult';

// TODO:NOTE
/**
 * I am PRETTY DAMN SURE these data should be otherwhere
 * Andrey, please re-design Analyzer
 */

export default class SlotServerSpinResult implements ISlotServerSpinResult {

    public gameFields: Map<number, Map<number, number[]>>;

    public toSpinResultDTO(): SpinResultDTO {
        const spinResultDTO: SpinResultDTO = new SpinResultDTO();
        this.gameFields.forEach((gameFieldServerResult: Map<number, number[]>, gameFieldKey: number) => {
            const gameFieldSpinResultDTO: GameFieldSpinResultDTO = new GameFieldSpinResultDTO();
            gameFieldServerResult.forEach((reelServerResult: number[], reelKey: number) => {
                gameFieldSpinResultDTO.set(reelKey, new ReelSpinResultDTO(reelServerResult));
            });
            spinResultDTO.set(gameFieldKey, gameFieldSpinResultDTO);
        });

        return spinResultDTO;
    }
}

import SpinResultDTO from '../../../../Core/Games/Slots/Configuration/DTO/SpinResult/SpinResultDTO';

export default interface ISlotServerSpinResult {
    gameFields: Map<number, Map<number, Array<number>>>;
    toSpinResultDTO(): SpinResultDTO;
}

import IPaytableSymbol from './IPaytableSymbol';

export default interface IPaytableSymbols {
    symbols: Array<IPaytableSymbol>;
}

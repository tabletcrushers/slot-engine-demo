import IPaytableSymbolPay from './IPaytableSymbolPay';

export default interface IPaytableSymbol {
    id: number;
    name: string;
    symbolsPay: Array<IPaytableSymbolPay>;
    multiplier: number;
    wild: boolean;
    scatter: boolean;
}

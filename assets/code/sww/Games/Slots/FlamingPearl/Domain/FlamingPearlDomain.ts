import FlamingPearlFreeSpinMode from '../../../../../../showcases/LinesConfigurator/stateForDomain/FlamingPearlFreeSpinMode';
import FlamingPearlSpinMode from '../../../../../../showcases/LinesConfigurator/stateForDomain/FlamingPearlSpinMode';
import AggregateNode from '../../../../../../showcases/Network/AggregateNode';
import { AutoPlayNotification,
    FreeSpinsNotification,
    ReelEngineNotification,
    ReelEngineNotificationTopic,
    SlotGameNotification,
    SlotGameNotificationTopic } from '../../../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import INotification from '../../../../Core/Notifications/INotification';
import NotificationManager from '../../../../Core/Notifications/NotificationManager';
import SlotGameDomain from '../../../Host/SlotGameDomain';
import FlamingPearlActionPanel from '../Features/ActionPanel/FlamingPearlActionPanel';
import FlamingPearlActionPanelView from '../Features/ActionPanel/View/FlamingPearlActionPanelView';
import FlamingPearlAutoSpinMode from '../Modes/FlamingPearlAutoSpinMode';

export default class FlamingPearlDomain extends SlotGameDomain {

    constructor() {
        super();
    }

    public setRoot(root: AggregateNode): void {
        super.setRoot(root);
    }

    public registerModes(): void {
        this.addMode('SpinMode', new FlamingPearlSpinMode());
        this.addMode('FreeSpinMode', new FlamingPearlFreeSpinMode());
        this.addMode('AutoPlayMode', new FlamingPearlAutoSpinMode());
    }

    public observe(notification: INotification): void {
        switch (notification.id) {
            case ReelEngineNotification.VisibleItemsUpdated:
                this._mode.renderVisibleItems();

                return;
            case SlotGameNotification.SwitchMode:
                this._mode.onExit();
                this._mode = this.getMode(notification.arguments);
                this._mode.onEnter();

                return;
            case FreeSpinsNotification.FreeSpinsStarted:
                this._mode.startSpin(null);
                this._slotNetworkCommunicator.createFreeSpinRequest(null);

                return;
            case FreeSpinsNotification.FreeSpinsFinished:
                this._mode.startSpin(null);
                this._slotNetworkCommunicator.createFreeSpinRequest(null);

                return;

            case AutoPlayNotification.AutoPlayStart:
                this._mode.startSpin(null);

                return;

            default :
                super.observe(notification);
        }
    }

    protected initFeatures(): void {

        const flamingPearlActionPanel = new FlamingPearlActionPanel(new FlamingPearlActionPanelView(), this.betManager);
        flamingPearlActionPanel.setRoot(this._root);

        this.addFeature(FlamingPearlActionPanel.NAME, flamingPearlActionPanel);

        super.initFeatures();

        const freeSpinMode =  this.getMode<FlamingPearlFreeSpinMode>('FreeSpinMode');
        freeSpinMode.setRoot(this._root);
        freeSpinMode.init();

        this.getMode<FlamingPearlAutoSpinMode>('AutoPlayMode').init();
    }

    protected prepareGameInformation(): void {
        super.prepareGameInformation();
        const gameId = 15;
        // tslint:disable-next-line:max-line-length
        const url = `http://35.198.75.187/!v8/api/admin.js?args[1][usr]=demo_api&args[1][passw]=demo_api_pass&action=get_demo&args[0][game_id]=${gameId}`;
        this._slotNetworkCommunicator.createTokenRequest(url);
    }

    protected registerListeners(): void {
        super.registerListeners();
        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode)
            .addObserver(this, SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsStarted)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.VisibleItemsUpdated)
            .addObserver(this, SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsFinished);
    }

    protected removeListeners(): void {
        super.removeListeners();
        NotificationManager.instance
            .removeObserver(this, SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsStarted)
            .removeObserver(this, SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsFinished);
    }
}

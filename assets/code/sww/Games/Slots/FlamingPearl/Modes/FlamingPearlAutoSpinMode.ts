import SlotSpinMode from '../../../../../../showcases/LinesConfigurator/stateForDomain/SlotSpinMode';
import DomainManager from '../../../../Core/Foundation/Domain/DomainManager';
import WinLinesAnimation from '../../../../Core/Games/Slots/Features/WinAnimations/WinLinesAnimations';
import AllWinLinesDrawerFeature from '../../../../Core/Games/Slots/Features/WinLineDrawer/Features/AllWinLinesDrawerFeature';
import {
    ActionPanelNotification,
    ActionPanelTopic,
    AutoPlayNotification,
    AutoPlayNotificationTopic,
    FreeSpinsNotification,
    SlotGameNotification,
    SlotGameNotificationTopic,
    WinLineNotification
} from '../../../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import ReelsEngine from '../../../../Core/Games/Slots/ReelsEngine/Core/ReelsEngine';
import INotification from '../../../../Core/Notifications/INotification';
import IObserver from '../../../../Core/Notifications/IObserver';
import Notification from '../../../../Core/Notifications/Notification';
import NotificationManager from '../../../../Core/Notifications/NotificationManager';
import { PopupsNotifications, PopupsNotificationTopic } from '../../../../Supply/Popups/Notification/PopupsNotifications';
import RenderEngine from '../../../../Supply/RenderEngine/RenderEngine';
import ISlotFreeSpins from '../../../Host/SlotBonusesRepository/Intefaces/ISlotFreeSpins';
import SlotGameDomain from '../../../Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../Host/SlotGameRepositoryEnum';
import IWin from '../../../Host/SlotSpinResultRepository/Interfaces/IWin';
import SlotServerSpinResult from '../../../Host/SlotSpinResultRepository/SlotServerSpinResult';
import FlamingPearlActionPanel from '../Features/ActionPanel/FlamingPearlActionPanel';

export default class FlamingPearlAutoSpinMode extends SlotSpinMode implements IObserver {

    private _domain: SlotGameDomain;
    private _spinStartNotification: Notification;
    private _autoPlayEndedNotification: Notification;
    private _timerID: number;
    private readonly AUTOPLAY_DELAY: number = 400;

    constructor() {
        super();

        // TODO: fix it, we don't need to use this notification
        this._spinStartNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.SpinStart);
        this._autoPlayEndedNotification = new Notification(AutoPlayNotificationTopic.AutoPlay, AutoPlayNotification.AutoPlayEnded);
    }

    public init(): void {
        super.init();

        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinStart)
            .addObserver(this, AutoPlayNotificationTopic.AutoPlay, AutoPlayNotification.AutoPlaySpinCount);
        this._domain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;

        this.setAutoPlayMode(false);
        this.setAutoSpinCount(0);
    }

    public update(delta: number): void {
        this._domain.getFeature<ReelsEngine>('ReelsEngine').update(delta);
        // TODO: replace '0' to logical definition
        this._domain.getFeature<RenderEngine>('RenderEngine')
            .renderReelItems(this._domain.getFeature<ReelsEngine>('ReelsEngine').getVisibleItems().getGameFieldReport(0));

        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).update(delta);
    }

    public onEnter(): void {
        super.onEnter();

        this.startAutoPlaySpin();
    }

    public startSpin(spinData: any): void {
        super.startSpin(spinData);

        this._domain.getFeature<ReelsEngine>('ReelsEngine').start();
        this._domain.getFeature<RenderEngine>('RenderEngine').start();
        this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).stop();
        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).stop();
    }

    public stopSpin(): void {
        super.stopSpin();

        this._timerID = null;

        const spinResultWin = this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        if (this.getAutoSpinCount() <= 0) {
            NotificationManager.instance.dispatch(this._autoPlayEndedNotification);
            this.setAutoSpinCount(0);
            this.setAutoPlayMode(false);

            if (this.freeSpinFeature.isActive) {
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'FreeSpinMode'));

                return;
            } else if (spinResultWin.win !== 0) {
                this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).start();
                this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).start();
            }

            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'SpinMode'));
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onAllReelsStopped();

            return;
        }

        this._domain.getFeature<RenderEngine>('RenderEngine').stop();

        if (this.freeSpinFeature.isActive) {
            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'FreeSpinMode'));
        } else if (spinResultWin.win === 0) {
            this.startAutoPlaySpin();
        } else {
            this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).start();
            this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).start();
        }
    }

    public registerListeners(): void {
        super.registerListeners();

        NotificationManager.instance
            .addObserver(this, ActionPanelTopic.ActionPanel, ActionPanelNotification.SpinStop)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult)
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.ClosePopup)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.OpenPopup);
    }

    public removeListeners(): void {
        super.removeListeners();

        NotificationManager.instance
            .removeObserver(this, ActionPanelTopic.ActionPanel, ActionPanelNotification.SpinStop)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult)
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.ClosePopup)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.OpenPopup);
    }

    public observe(notifaction: INotification): void {
        switch (notifaction.id) {
            case FreeSpinsNotification.FreeSpinStart:
                this.setAutoSpinCount(this.getAutoSpinCount() - 1);

                if (this.getAutoSpinCount() <= 0) {
                    NotificationManager.instance.dispatch(this._autoPlayEndedNotification);
                }

                return;

            case SlotGameNotification.SpinResult:
                const serverSpinResult = this._domain.getDALProvider()
                    .getRepository(SlotGameRepositoryEnum.SpinResult)
                    .read<SlotServerSpinResult>(SlotGameRepositoryDataEnum.ServerSpinResult);

                setTimeout(() => {
                    this._domain
                        .getFeature<ReelsEngine>('ReelsEngine')
                        .setSpinResult(serverSpinResult.toSpinResultDTO(), true);
                        // tslint:disable-next-line:no-magic-numbers
                        }, 1000);

                return;

            case ActionPanelNotification.SpinStop:
                this.setAutoSpinCount(0);

                if (this._timerID !== null) {
                    clearTimeout(this._timerID);

                    if (this.freeSpinFeature.isActive) {
                        NotificationManager.instance
                            .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'FreeSpinMode'));
                    } else {
                        NotificationManager.instance
                            .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'SpinMode'));
                    }

                    this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onAllReelsStopped();
                }

                return;

            case AutoPlayNotification.AutoPlaySpinCount:
                this.setAutoPlayMode(true);
                this.setAutoSpinCount(notifaction.arguments);

                return;

            case WinLineNotification.AllWinLineShowed:
                this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).stop();
                this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).stop();

                if (this.getAutoSpinCount() === 0) {
                    this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onAllReelsStopped();

                    return;
                }

                this.startAutoPlaySpin();

                return;
            case PopupsNotifications.OpenPopup:
                this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).setButtonsActivity(false);

                return;
            case PopupsNotifications.ClosePopup:
                this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).setButtonsActivity(true);

                return;
            default:
                return;
        }
    }

    protected getAutoSpinCount(): number {
        return this._domain.getDALProvider().getRepository(SlotGameRepositoryEnum.AutoPlayRepository).read('spinCount');
    }

    protected getAutoPlayMode(): boolean {
        return this._domain.getDALProvider().getRepository(SlotGameRepositoryEnum.AutoPlayRepository).read('autoPlayMode');
    }

    protected setAutoPlayMode(flag: boolean): void {
        this._domain.getDALProvider().getRepository(SlotGameRepositoryEnum.AutoPlayRepository).write('autoPlayMode', flag);
    }

    protected setAutoSpinCount(spinCount: number): void {
        this._domain.getDALProvider().getRepository(SlotGameRepositoryEnum.AutoPlayRepository).write('spinCount', spinCount);
    }

    protected startAutoPlaySpin(): void {
        this._timerID = setTimeout(() => {
            this._timerID = null;

            this.setAutoSpinCount(this.getAutoSpinCount() - 1);

            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, AutoPlayNotification.AutoPlayStart))
                .dispatch(this._spinStartNotification);
        },                         this.AUTOPLAY_DELAY) as any;
    }

    private get freeSpinFeature(): ISlotFreeSpins {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<ISlotFreeSpins>(SlotGameRepositoryDataEnum.FreeSpins);
    }
}

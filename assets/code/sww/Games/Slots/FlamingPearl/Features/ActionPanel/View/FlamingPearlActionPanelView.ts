// tslint:disable:no-magic-numbers
import AggregateNode from '../../../../../../../../showcases/Network/AggregateNode';
import IActionPanelView from '../../../../../../Core/Games/Slots/ActionPanel/IActionPanelView';
import { ActionPanelNotification, ActionPanelTopic } from '../../../../../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import Notification from '../../../../../../Core/Notifications/Notification';
import NotificationManager from '../../../../../../Core/Notifications/NotificationManager';
import IRootCarrier from '../../../../../../Supply/Interfaces/IRootCarrier';
import { PopupsNotifications, PopupsNotificationTopic } from '../../../../../../Supply/Popups/Notification/PopupsNotifications';
import { PopupType } from '../../../../../../Supply/Popups/PopupsWarehouse';
import { AutoPlayConfig } from './AutoPlayConfig';

export default class FlamingPearlActionPanelView implements IActionPanelView, IRootCarrier {

    private _node: cc.Node;
    private _betValue: cc.Node;
    private _balanceValue: cc.Node;
    private _winValue: cc.Node;
    private _betDecreaseBtn: cc.Node;
    private _betIncreaseBtn: cc.Node;
    private _spinBtn: cc.Node;
    private _spinStopBtn: cc.Node;
    private _spinLogo: cc.Node;
    private _logoAction: NodeJS.Timer;
    private _autoPlayBody: cc.Node;
    private _autoPlayBtn: cc.Node;
    private _autoPlayCounter: cc.Node;
    private _autoPlayPressedBtn: cc.Node;
    private _autoPlayPopupOpen: boolean = false;
    private _infoBtn: cc.Node;
    private _settingsBtn: cc.Node;
    private _homeBtn: cc.Node;
    private _FPFont: cc.BitmapFont;

    private readonly _spinStartNotification: Notification;
    private readonly _spinStopNotification: Notification;
    private readonly _betIncreaseNotification: Notification;
    private readonly _betDecreaseNotification: Notification;
    private readonly _showPaytablePopup: Notification;

    constructor() {
        this._spinStartNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.SpinStart);
        this._spinStopNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.SpinStop);
        this._betIncreaseNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.BetIncrease);
        this._betDecreaseNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.BetDecrease);
        // tslint:disable-next-line:max-line-length
        this._showPaytablePopup = new Notification(PopupsNotificationTopic.GamePopup, PopupsNotifications.ShowMultiplePopup, PopupType.PaytablePopup);
    }

    public setRoot(root: AggregateNode): void {
        this._node = root;
    }

    public init(): void {
        this._FPFont = cc.find('Canvas/UI/Buttons/spinStopBtn/autoPlayCounter').getComponent(cc.Label).font;

        this.addUIElements();
        this.playSpinLogoRotating();

    }

    public disable(): void {
        this._autoPlayBtn.getComponent(cc.Button).interactable = true;
        this.switchAutoPlayPopupState(false);
        this.setVisibleState(false);
    }

    public enable(): void {
        this.setVisibleState(true);
    }

    public disableSpinBtn(): void {
        this._spinBtn.getComponent(cc.Button).interactable = false;
    }

    public enableSpinBtn(): void {
        this._spinBtn.getComponent(cc.Button).interactable = true;
    }

    public enableAutoPlayBtn(): void {
        this._autoPlayBtn.getComponent(cc.Button).interactable = true;
    }

    public disableAutoPlayBtn(): void {
        this._autoPlayBtn.getComponent(cc.Button).interactable = false;
    }

    public disableSpinStopBtn(): void {
        this._spinStopBtn.getComponent(cc.Button).interactable = false;
    }

    public enableSpinStopBtn(): void {
        this._spinStopBtn.getComponent(cc.Button).interactable = true;
    }

    public enableInfoBtn(): void {
        this._infoBtn.getComponent(cc.Button).interactable = true;
    }

    public disableInfoBtn(): void {
        this._infoBtn.getComponent(cc.Button).interactable = false;
    }

    public enableSettingsBtn(): void {
        this._settingsBtn.getComponent(cc.Button).interactable = true;
    }

    public disableSettingsBtn(): void {
        this._settingsBtn.getComponent(cc.Button).interactable = false;
    }

    public enableHomeBtn(): void {
        this._homeBtn.getComponent(cc.Button).interactable = true;
    }

    public disableHomeBtn(): void {
        this._homeBtn.getComponent(cc.Button).interactable = false;
    }

    public updateBet(bet: string): void {
        this._betValue.getComponent(cc.Label).string = bet;
    }

    public updateBalance(balance: string): void {
        this._balanceValue.getComponent(cc.Label).string = balance;
    }

    public updateWin(win: string): void {
        this._winValue.getComponent(cc.Label).string = win;
    }

    public onAllReelsStopped(): void {
        this.playSpinLogoRotating();
    }

    public onAutoPlayEnd(): void {
        this._spinBtn.active = true;
        this._spinStopBtn.active = false;
    }

    public updateAutoPlaySpinCount(value: string): void {
        this._autoPlayCounter.getComponent(cc.Label).string = value;
    }

    public switchAutoPlayPopupState(isOpen: boolean): void {
        this._autoPlayPressedBtn.active = isOpen;
        this._autoPlayBtn.active = !isOpen;
        this._autoPlayPopupOpen = isOpen;
        this._autoPlayBody.stopAllActions();

        const dX = AutoPlayConfig.origPos.x;
        const dY = this._autoPlayPopupOpen ? AutoPlayConfig.origPos.y : AutoPlayConfig.origPos.y - this._autoPlayBody.height;

        this._autoPlayBody.runAction(cc.moveTo(0.2, cc.v2(dX, dY)));
    }

    private setVisibleState(val: boolean): void {
        this._betDecreaseBtn.getComponent(cc.Button).interactable = val;
        this._betIncreaseBtn.getComponent(cc.Button).interactable = val;
        this._spinBtn.getComponent(cc.Button).interactable = val;
        this._infoBtn.getComponent(cc.Button).interactable = val;
        this._autoPlayBtn.getComponent(cc.Button).interactable = val;
    }

    private addUIElements() {
        // TODO - БЛЭТ! Никаких cc.find !!!
        this._betValue = cc.find('Canvas/UI/betValue');
        this._balanceValue = cc.find('Canvas/UI/balanceValue');
        this._winValue = cc.find('Canvas/UI/winValue');
        this._betDecreaseBtn = cc.find('Canvas/UI/Buttons/minusBtn');
        this._betIncreaseBtn = cc.find('Canvas/UI/Buttons/plusBtn');
        this._spinBtn = cc.find('Canvas/UI/Buttons/spinBtn');
        this._spinStopBtn = cc.find('Canvas/UI/Buttons/spinStopBtn');
        this._autoPlayCounter = cc.find('Canvas/UI/Buttons/spinStopBtn/autoPlayCounter');
        this._spinLogo = cc.find('Canvas/UI/Buttons/spinBtn/spinLogo');
        this._autoPlayBody = cc.find('Canvas/UI/Buttons/autoPlayContainer/autoPlayPopup/autoplayBody');
        this._autoPlayBtn = cc.find('Canvas/UI/Buttons/autoPlayContainer/autoplayBtn');
        this._autoPlayPressedBtn = cc.find('Canvas/UI/Buttons/autoPlayContainer/autoplayPressedBtn');
        this._infoBtn = cc.find('Canvas/UI/Buttons/infoBtn');
        this._settingsBtn = cc.find('Canvas/UI/Buttons/settingsBtn');
        this._homeBtn = cc.find('Canvas/UI/Buttons/homeBtn');

        this._betDecreaseBtn.on(cc.Node.EventType.TOUCH_END, this.onBetDecreaseBtnClick.bind(this));
        this._betIncreaseBtn.on(cc.Node.EventType.TOUCH_END, this.onBetIncreaseBtnClick.bind(this));
        this._spinBtn.on(cc.Node.EventType.TOUCH_END, this.onSpinBtnClick.bind(this));
        this._spinStopBtn.on(cc.Node.EventType.TOUCH_END, this.onSpinStopBtnClick.bind(this));
        this._autoPlayBtn.on(cc.Node.EventType.TOUCH_END, this.onAutoPlayBtnClick.bind(this));
        this._autoPlayPressedBtn.on(cc.Node.EventType.TOUCH_END, this.onAutoPlayBtnClick.bind(this));
        this._infoBtn.on(cc.Node.EventType.TOUCH_END, this.onInfoBttnClick.bind(this));

        this.initAutoPlay();
    }

    private initAutoPlay(): void {
        this._autoPlayPressedBtn.active = false;

        for (let i = 0; i < AutoPlayConfig.labels.length; i++) {
            const node = new AggregateNode();

            if (i > AutoPlayConfig.containItems) {
                this._autoPlayBody.height += AutoPlayConfig.labelStep;
                // TODO - БЛЭТ! Никаких cc.find !!!
                cc.find('Canvas/UI/Buttons/autoPlayContainer/autoPlayPopup').height += AutoPlayConfig.labelStep;
            }

            node.setPosition(AutoPlayConfig.labelPoint.x, AutoPlayConfig.labelPoint.y + i * AutoPlayConfig.labelStep);
            node.setAnchorPoint(0.5, 0.5);
            node.addComponent(cc.Label).string = AutoPlayConfig.labels[i];
            node.getComponent(cc.Label).fontSize = AutoPlayConfig.fontSize;
            node.getComponent(cc.Label).font = this._FPFont;
            node.addComponent(cc.Button).transition = cc.Button.Transition.COLOR;

            node.getComponent(cc.Button).node.on(cc.Node.EventType.TOUCH_END, this.onAutoPlayItemClicked.bind(this, i));

            this._autoPlayBody.addChild(node);
        }

        this._autoPlayBody.y -= this._autoPlayBody.height;
    }

    private onAutoPlayItemClicked(itemIndex: number, event): void {
        const spinCount: number = parseInt(AutoPlayConfig.labels[itemIndex].split(' ')[0], 10);

        this.updateAutoPlaySpinCount(spinCount.toString());
        this._spinBtn.active = false;
        this._spinStopBtn.active = true;

        this.onAutoPlayBtnClick(event);

        NotificationManager.instance
            .dispatch(new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.AutoSpin, spinCount));
    }

    private onBetDecreaseBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._betDecreaseNotification);
        }
    }

    private onBetIncreaseBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._betIncreaseNotification);
        }
    }

    private onSpinBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._spinStartNotification);
            this.stopSpinLogoRotating();
        }
    }

    private onSpinStopBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            this._spinBtn.active = true;
            this._spinStopBtn.active = false;

            NotificationManager.instance.dispatch(this._spinStopNotification);
        }
    }

    private playSpinLogoRotating(): void {
        this._logoAction = setInterval(() => {
            this._spinLogo.runAction(cc.rotateBy(2, 360).easing(cc.easeInOut(3)));
        },                             4000);
    }

    private stopSpinLogoRotating(): void {
        clearInterval(this._logoAction);
        this._spinLogo.stopAllActions();
        this._spinLogo.rotation = 0;
    }

    private onAutoPlayBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === false) {
            return;
        }

        this.switchAutoPlayPopupState(!this._autoPlayPopupOpen);
    }

    private onInfoBttnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._showPaytablePopup);
        }
    }
}

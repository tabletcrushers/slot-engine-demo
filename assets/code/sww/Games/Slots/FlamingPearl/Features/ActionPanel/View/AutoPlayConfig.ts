export const AutoPlayConfig = {
    origPos: {
        x: 1,
        y: -6
    },
    labelPoint: {
        x: 90,
        y: 120
    },
    fontSize: 30,
    containItems: 3,
    labelStep: 40,
    labels : [
        '10 SPINS',
        '25 SPINS',
        '50 SPINS',
        '99 SPINS'
    ]
};

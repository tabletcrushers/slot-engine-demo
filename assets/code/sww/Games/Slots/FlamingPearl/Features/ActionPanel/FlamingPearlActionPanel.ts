import AggregateNode from '../../../../../../../showcases/Network/AggregateNode';
import ApplicationDomain from '../../../../../Application/Host/ApplicationDomain';
import AbstractBetManager from '../../../../../Application/User/AbstractBetManager';
import IBetManager from '../../../../../Application/User/IBetManager';
import DomainManager from '../../../../../Core/Foundation/Domain/DomainManager';
import ActionPanel from '../../../../../Core/Games/Slots/ActionPanel/ActionPanel';
import IActionPanelView from '../../../../../Core/Games/Slots/ActionPanel/IActionPanelView';
import {
    ActionPanelNotification,
    ActionPanelTopic,
    AutoPlayNotification,
    AutoPlayNotificationTopic,
    ReelEngineNotification,
    ReelEngineNotificationTopic,
    SlotGameNotification,
    SlotGameNotificationTopic
} from '../../../../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import Notification from '../../../../../Core/Notifications/Notification';
import NotificationManager from '../../../../../Core/Notifications/NotificationManager';
import TypeUtil from '../../../../../Utils/TypeUtil';
import SlotGameDomain from '../../../../Host/SlotGameDomain';
import { SlotGameRepositoryEnum } from '../../../../Host/SlotGameRepositoryEnum';
import FlamingPearlActionPanelView from './View/FlamingPearlActionPanelView';

export default class FlamingPearlActionPanel extends ActionPanel {

    private _applicationDomain: ApplicationDomain;
    private _slotDomain: SlotGameDomain;
    private _accumulateWin: number;

    constructor(view: IActionPanelView, betManager: IBetManager) {
        super(view, betManager);
        this._accumulateWin = 0;
        this._slotDomain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;
    }

    protected registerListeners(): void {
        super.registerListeners();

        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStart)
            .addObserver(this, AutoPlayNotificationTopic.AutoPlay, AutoPlayNotification.AutoPlayEnded)
            .addObserver(this, ActionPanelTopic.ActionPanelView, ActionPanelNotification.SpinStop)
            .addObserver(this, ActionPanelTopic.ActionPanelView, ActionPanelNotification.AutoSpin);
    }

    public setRoot(root: AggregateNode): void {
        TypeUtil.castTo<FlamingPearlActionPanelView>(this.view).setRoot(root);
        TypeUtil.castTo<FlamingPearlActionPanelView>(this.view).init();
    }

    public start(): void {
        super.start();
        this.updateBalance();
    }

    public onReelsStart(): void {
        this.view.updateWin('GOOD LUCK!');
    }

    public onAllReelsStopped(): void {
        // const win = (DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain).getSpinResultWin();
        const validBet: number = this.betManager.getValidBet();

        // this.view.updateWin(win);

        if (validBet !== AbstractBetManager.InvalidBet) {
            this.setButtonsActivity(true);

            (this.view as FlamingPearlActionPanelView).onAllReelsStopped();
        }
    }

    public updateWin(win: number): void {
        // TODO - templates for output like ¥${value}
        this.view.updateWin(win.toString());
    }

    public accumulateWin(win: number): void {
        this._accumulateWin += win;
        this.view.updateWin(this._accumulateWin.toString());
    }

    public setAccumulateWinToZero(): void {
        this._accumulateWin = 0;
    }

    // this method temp need disccus and find better solution
    public reduceBalance(): void {
        this._applicationDomain = DomainManager.instance.getDomain(ApplicationDomain.NAME) as ApplicationDomain;
        // temp need take multiplie line from configuration not magic number
        // tslint:disable-next-line:no-magic-numbers
        this.view.updateBalance((this._applicationDomain.userData.balance - (this.betManager.currentBet * 10)).toString());
    }

    public updateBalance(): void {
        this._applicationDomain = DomainManager.instance.getDomain(ApplicationDomain.NAME) as ApplicationDomain;
        this.view.updateBalance(this._applicationDomain.userData.balance.toString());
    }

    public observe(notification: Notification): void {
        switch (notification.id) {
            case ActionPanelNotification.SpinStart:
                this.setButtonsActivity(false);
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, ActionPanelNotification.SpinStart));

                const validBet: number = this.betManager.getValidBet();

                if (validBet !== AbstractBetManager.InvalidBet) {
                    this.view.updateBet(this.betManager.currentBet.toString());
                } else {
                    NotificationManager.instance.dispatch(
                        new Notification(ActionPanelTopic.ActionPanel, ActionPanelNotification.NegativeBet));
                }
                break;

            case ReelEngineNotification.ReelStart:
                const autoPlaySpinCount =
                    this._slotDomain.getDALProvider().getRepository(SlotGameRepositoryEnum.AutoPlayRepository).read('spinCount');

                if (autoPlaySpinCount >= 0) {
                    (this.view as FlamingPearlActionPanelView).updateAutoPlaySpinCount(autoPlaySpinCount.toString());
                }

                return;

            case AutoPlayNotification.AutoPlayEnded:
                (this.view as FlamingPearlActionPanelView).onAutoPlayEnd();

                return;

            case SlotGameNotification.SwitchMode:
                switch (notification.arguments) {
                    case 'FreeSpinMode':
                        (this.view as FlamingPearlActionPanelView).disableSpinStopBtn();
                        (this.view as FlamingPearlActionPanelView).disableAutoPlayBtn();
                        (this.view as FlamingPearlActionPanelView).disableHomeBtn();
                        (this.view as FlamingPearlActionPanelView).disableInfoBtn();
                        (this.view as FlamingPearlActionPanelView).disableSettingsBtn();

                        return;

                    case 'AutoPlayMode':
                        (this.view as FlamingPearlActionPanelView).enableSpinStopBtn();
                        (this.view as FlamingPearlActionPanelView).enableHomeBtn();
                        (this.view as FlamingPearlActionPanelView).enableSettingsBtn();

                        return;

                    case 'SpinMode':
                        (this.view as FlamingPearlActionPanelView).enableAutoPlayBtn();
                        (this.view as FlamingPearlActionPanelView).enableHomeBtn();
                        (this.view as FlamingPearlActionPanelView).enableInfoBtn();
                        (this.view as FlamingPearlActionPanelView).enableSettingsBtn();

                        return;

                    default:
                    return;
                }

            case ActionPanelNotification.SpinStop:
                NotificationManager.instance.dispatch(new Notification(ActionPanelTopic.ActionPanel, ActionPanelNotification.SpinStop));

                return;

            case ActionPanelNotification.BetIncrease:
                this.betManager.nextBet();
                this.view.updateBet(this.betManager.currentBet.toString());
                break;

            case ActionPanelNotification.BetDecrease:
                this.betManager.previousBet();
                this.view.updateBet(this.betManager.currentBet.toString());
                break;

            case ActionPanelNotification.AutoSpin:
                NotificationManager.instance
                    .dispatch(new Notification(AutoPlayNotificationTopic.AutoPlay,
                                               AutoPlayNotification.AutoPlaySpinCount,  notification.arguments))
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'AutoPlayMode'));
                break;

            default:
        }
    }
}

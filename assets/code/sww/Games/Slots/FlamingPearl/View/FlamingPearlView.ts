import AggregateNode from '../../../../../../showcases/Network/AggregateNode';
import DomainManager from '../../../../Core/Foundation/Domain/DomainManager';
import { SlotGameNotification,
        SlotGameNotificationTopic } from '../../../../Core/Games/Slots/Notifications/SlotsGameNotifications';
import FrameAtlasProvider from '../../../../Core/Games/Slots/ReelsEngine/Core/Implementation/Providers/FrameAtlasProvider';
import Notification from '../../../../Core/Notifications/Notification';
import NotificationManager from '../../../../Core/Notifications/NotificationManager';
import TypeUtil from '../../../../Utils/TypeUtil';
import SlotGameDomain from '../../../Host/SlotGameDomain';

const { ccclass, property } = cc._decorator;

@ccclass
export default class FlamingPearlView extends cc.Component {

    @property(cc.SpriteAtlas)
    public symbolsAtlas: cc.SpriteAtlas = null;

    public gameConfig: {
        type: cc.Asset;
        default: null;
    };

    private _slotGameDomain: SlotGameDomain;

    constructor() {
        super();
    }

    public start(): void {
        cc.view.enableAntiAlias(true);
        cc.view.setResizeCallback(this.onStageResize.bind(this));
        this.onStageResize();
    }

    public onLoad(): void {

        FrameAtlasProvider.instance.registerAtlas(this.symbolsAtlas);

        this._slotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        const rootNode: AggregateNode = TypeUtil.castTo<AggregateNode>(this.node.getChildByName('RenderNode'));
        this._slotGameDomain.setRoot(rootNode);
        this._slotGameDomain.init();

        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SlotGameStarted));
    }

    public update(delta: number) {
        this._slotGameDomain.update(delta);
    }

    // TODO - move number values to config based on art
    // move onStageResize to ResizeableComponent level
    public onStageResize() {
        // tslint:disable-next-line:no-magic-numbers
        const originSize: cc.Size = new cc.Size(1680, 1050);
        const screenSize: cc.Size = cc.view.getVisibleSizeInPixel();
        cc.view.setDesignResolutionSize(screenSize.width, screenSize.height, cc.ResolutionPolicy.NO_BORDER);

        const originAspect = originSize.height / originSize.width;
        const stageAspect = screenSize.height / screenSize.width;
        let scale = 1;

        // tslint:disable-next-line:prefer-conditional-expression
        if (stageAspect > originAspect) {
            scale = screenSize.width / originSize.width;
        } else {
            scale = screenSize.height / originSize.height;
        }

        this.node.setScale(scale, scale);
    }
}

export default interface IPolynominalOrigins {
    position1: number;
    position2: number;
    position3: number;
    position4: number;
}

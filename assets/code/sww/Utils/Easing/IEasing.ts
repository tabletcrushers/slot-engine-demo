import IPolynominalOrigins from './IPolynominalOrigins';
import IPolynominals from './IPolynominals';

export default interface IEasing {
    name: string;
    finalValueTimes: Array<number>;
    calculate(startValue: number, valueDifference: number, timePercentage: number): number;
    getPolynomOrigins(): IPolynominalOrigins;
    isEqualByPolinomial(polynominals: IPolynominals): boolean;
}

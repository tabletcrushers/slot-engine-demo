export enum EasingType {
    easeOutBack = 'easeOutBack',
    backInQuartic = 'backInQuartic',
    noEasing = 'noEasing'
}

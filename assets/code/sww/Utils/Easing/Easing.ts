import MathUtil from '../MathUtil';
import EasingUtil, { IPolynominalOrigins, IPolynominals } from './EasingUtil';
import IEasing from './IEasing';

export default class Easing implements IEasing {

    private readonly _name: string;
    private readonly _polynominals: IPolynominals;
    private _finalValueTimes: Array<number>;

    constructor(name: string, positions: IPolynominalOrigins) {
        this._name = name;
        this._polynominals = EasingUtil.getPolynominals(positions);
        this.calculateFinalValueTimes();
    }

    public get name(): string {
        return this._name;
    }

    public get finalValueTimes(): Array<number> {
        return this._finalValueTimes;
    }

    public calculate(startValue: number, valueDifference: number, timePercentage: number): number {
        const t2 = timePercentage * timePercentage;
        const t3 = t2 * timePercentage;

        return startValue + valueDifference *
        (
            this._polynominals.p5 * t3 * t2 +
            this._polynominals.p4 * t2 * t2 +
            this._polynominals.p3 * t3 +
            this._polynominals.p2 * t2 +
            this._polynominals.p1 * timePercentage
        );
    }

    public isEqualByPolinomial(polynominals: IPolynominals): boolean {
        return this._polynominals === polynominals;
    }

    public getPolynomOrigins(): IPolynominalOrigins {

        return EasingUtil.getPolynomOrigins(this._polynominals);
    }

    private calculateFinalValueTimes(): void {
        // tslint:disable:no-magic-numbers
        this._finalValueTimes = new Array<number>();
        const startValue = 0;
        const endValue = 1;
        let prevValue = 0;
        let value = 0;
        for (let i = startValue; i <= endValue; i += MathUtil.Epsilon2) {
            value = this.calculate(startValue, endValue, i);
            if (MathUtil.isInBounds(
                endValue,
                Math.min(prevValue, value),
                Math.max(prevValue, value))) {
                    this._finalValueTimes.push(MathUtil.round(i, 2));
            }
            prevValue = value;
        }
        if (this._finalValueTimes[this._finalValueTimes.length - 1] !== endValue) {
            this._finalValueTimes.push(endValue);
        }
    }
}

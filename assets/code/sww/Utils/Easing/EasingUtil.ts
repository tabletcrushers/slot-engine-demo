import Easing from './Easing';
import { EasingType } from './EasingType';
import IEasing from './IEasing';
import IPolynominalOrigins from './IPolynominalOrigins';
import IPolynominals from './IPolynominals';

// tslint:disable:no-magic-numbers
export default abstract class EasingUtil {

    public static registerEasing(easing: IEasing) {

        if (EasingsCollection.easings === undefined) {
            EasingsCollection.easings = new Map<string, IEasing>();
        }
        EasingsCollection.easings.set(easing.name, easing);
    }

    public static getEasing(easingType: EasingType): IEasing {

        return EasingsCollection.easings.get(easingType);
    }

    public static getPolynominals(positions: IPolynominalOrigins): IPolynominals {

        const position0 = 0;
        const position5 = 1;

        const p1 = (positions.position1 - position0) * 5;
        const p2 = (positions.position2 - position0) * 10 - p1 * 4;
        const p3 = (positions.position3 - position0) * 10 + (positions.position1 - positions.position2) * 30;
        const p4 = (positions.position4 + position0) * 5 - (positions.position3 + positions.position1) * 20 + positions.position2 * 30;
        const p5 = position5 - position0 - p4 - p3 - p2 - p1;

        return {p5, p4, p3, p2, p1};
    }

    public static getPolynomOrigins(polynominals: IPolynominals): IPolynominalOrigins {

        const position1 = polynominals.p1 / 5;
        const position2 = (polynominals.p2 + 4 * polynominals.p1) / 10;
        const position3 = (polynominals.p3 - 30 * (position1 - position2)) / 10;
        const position4 = (polynominals.p4 + 20 * (position3 + position1) - 30 * position2) / 5;
        const position5 = polynominals.p5 - polynominals.p4 - polynominals.p3 - polynominals.p2 - polynominals.p1;

        return {position1, position2, position3, position4};
    }
}

// tslint:disable:no-magic-numbers
abstract class EasingsCollection {

    public static easings: Map<string, IEasing> = new Map<string, IEasing>([
        [
            EasingType.easeOutBack,
            new Easing(EasingType.easeOutBack, {position1: 1.2, position2: 1.5, position3: 1.3, position4: 1})
        ],
        [
            EasingType.backInQuartic,
            new Easing(EasingType.backInQuartic, {position1: 0, position2: -0.3, position3: -0.7, position4: -0.6})
        ],
        [
            EasingType.noEasing,
            new Easing(EasingType.noEasing, {position1: 0.2, position2: 0.4, position3: 0.6, position4: 0.8})
        ]
    ]);
}

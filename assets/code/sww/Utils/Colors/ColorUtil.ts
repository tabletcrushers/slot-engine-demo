import Color from '../../Supply/Custom/Color';

export default class ColorUtil {

    public static hexToRGBA(hex: string): Color {
        const h = hex.replace('#', '');
        const dec = parseInt(h, 16);

        // tslint:disable-next-line:no-bitwise no-magic-numbers
        return {red: (dec >> 16) & 255, green: (dec >> 8) & 255, blue: dec & 255, alpha: 255};
    }
}

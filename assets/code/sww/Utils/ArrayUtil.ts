export default class ArrayUtil {

    public static equals<T>(arr1: Array<T>, arr2: Array<T>): boolean {

        if ((arr1.length !== arr2.length)) {
            return false;
        }

        return arr1.every((element, index) => element === arr2[index]);
    }
}

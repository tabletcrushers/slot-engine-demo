import IReelItemBuilderConsumer from '../Core/Games/Slots/ReelsEngine/Core/Items/IReelItemBuilderConsumer';
import IReelConsumer from '../Core/Games/Slots/ReelsEngine/Core/Reel/IReelConsumer';
import IReelCorrector from '../Core/Games/Slots/ReelsEngine/Presentation/Correction/IReelCorrector';
import IReelSpinResultCorrector from '../Core/Games/Slots/ReelsEngine/Presentation/Correction/IReelSpinResultCorrector';

export default class TypeUtil {

    // TODO - make matchTable for is* checks
    public static isReelConsumer(type: any): boolean {
        return (type as IReelConsumer).registerReel !== undefined;
    }

    public static isReelItemBuilderConsumer(type: any): boolean {
        return (type as IReelItemBuilderConsumer).setReelItemBuilder !== undefined;
    }

    public static isReelCorrector(type: any): boolean {
        return (type as IReelCorrector).isReelCorrector !== undefined;
    }

    public static isReelSpinResultCorrector(type: any): boolean {
        return (type as IReelSpinResultCorrector).extend !== undefined;
    }

    public static provideInterfaceInstance<T>(): T {
        return this.castTo<T>(new Object());
    }

    public static castTo<T>(type: any): T {
        return type as T;
    }
}

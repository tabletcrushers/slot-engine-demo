export default class MathUtil {

    public static Epsilon2 = 0.01;
    public static Epsilon3 = 0.001;
    public static Epsilon6 = 0.000001;
    public static Epsilon9 = 0.000000001;

    public static circulateInBounds(value: number, minBound: number, maxBound: number): number {
        const delta: number = maxBound - minBound;
        if (value >= maxBound) {
            return minBound + (value - maxBound) % delta;
        } else if (value < minBound) {
            return maxBound - (maxBound - value) % delta;
        }

        return value;
    }

    public static circulate(value: number, bound: number): number {
        let result: number = value % bound;
        if (result < 0) {
            result += bound;
        }

        return result;
    }

    public static isInBounds(value: number, minBound: number, maxBound: number): boolean {
        return value >= minBound && value <= maxBound;
    }

    public static areEqual(value1: number, value2: number, tolerance: number): boolean {
        return Math.abs(value1 - value2) <= tolerance;
    }

    public static round(value: number, decimalCount: number): number {
        // tslint:disable-next-line:no-magic-numbers
        const limitator = Math.pow(10, decimalCount);

        return Math.floor(value * limitator) / limitator;
    }

    public static getRandomInt(limit: number): number {
        return Math.floor(Math.random() * limit);
    }
}

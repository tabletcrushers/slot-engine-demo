import WinLineSegment from '../../../../showcases/LinesConfigurator/WinLineSegment';
import { WinLineSegmentType } from '../../../../showcases/LinesConfigurator/WinLineSegmentType';
import Point from '../../Core/Geometry/Point';

// tslint:disable:no-magic-numbers

// tslint:disable:no-magic-numbers
export default class LinePointsCalculator {

    private rectWindth = 220;
    private rectHeight = 200;

    public calculateLinePointsPosition(winLine: Array<number>,
                                       positionMatrix: Array<Array<Point>>,
                                       nodeWindth: number,
                                       nodeHeight: number): Array<any> {

        const result = new Array<Point>();

        result.push(this.calculateStartPoint(winLine, positionMatrix, nodeWindth, nodeHeight));

        winLine.forEach((element, index) => {
            result.push(
                this.createPoint(
                    positionMatrix[index][element].x,
                    positionMatrix[index][element].y)
            );
        });

        result.push(this.calculateFinishPoint(winLine, positionMatrix, nodeWindth, nodeHeight));

        return result;
    }

    public calculateWinPointsPosition(line: Array<number>,
                                      positionMatrix: Array<Array<Point>>,
                                      nodeWindth: number,
                                      nodeHeight: number): Array<Point> {

        const result = new Array<Point>();

        line.forEach((element, index) => {
                result.push(this.createPoint(positionMatrix[index][element].x,
                                             positionMatrix[index][element].y));

        });

        return result;
    }

    public calculeLineSegments(line: Array<number>,
                               positionMatrix: Array<Array<Point>>,
                               nodeWindth: number,
                               nodeHeight: number): Array<WinLineSegment> {

        const segments = new Array<WinLineSegment>();
        const startPoint = this.calculateStartPoint(line, positionMatrix, nodeWindth, nodeHeight);

        let prevPoint = startPoint;
        let prevReel;

        line.forEach((raw, reel) => {
            const point = this.createPoint(positionMatrix[reel][raw].x,
                                           positionMatrix[reel][raw].y);

            if (prevReel === undefined) {
                const intersectPoint = this.getIntersectionWithRect(point, prevPoint, point, this.rectWindth, this.rectHeight);

                segments.push(new WinLineSegment(startPoint, intersectPoint, WinLineSegmentType.Outside));
                segments.push(new WinLineSegment(intersectPoint, point, WinLineSegmentType.Inside, reel));
            } else {
                const intersectPoint1 = this.getIntersectionWithRect(prevPoint, prevPoint, point, this.rectWindth, this.rectHeight);
                const intersectPoint2 = this.getIntersectionWithRect(point, prevPoint, point, this.rectWindth, this.rectHeight);

                segments.push(new WinLineSegment(prevPoint, intersectPoint1, WinLineSegmentType.Inside, prevReel));
                segments.push(new WinLineSegment(intersectPoint1, intersectPoint2, WinLineSegmentType.Outside));
                segments.push(new WinLineSegment(intersectPoint2, point, WinLineSegmentType.Inside, reel));
            }

            prevReel = reel;
            prevPoint = point;
        });

        // calculate segments for last line
        const finishPoint = this.calculateFinishPoint(line, positionMatrix, nodeWindth, nodeHeight);

        const finishIntesactionPoint = this.getIntersectionWithRect(prevPoint, prevPoint, finishPoint, this.rectWindth, this.rectHeight);

        segments.push(new WinLineSegment(prevPoint, finishIntesactionPoint, WinLineSegmentType.Inside, line.length - 1));
        segments.push(new WinLineSegment(finishIntesactionPoint, finishPoint, WinLineSegmentType.Outside));

        return segments;
    }

    public calculateTileSegments(line: Array<number>,
                                 positionMatrix: Array<Array<Point>>,
                                 nodeWindth: number,
                                 nodeHeight: number): Array<any> {

        const segments = new Array<WinLineSegment>();
        const startPoint = this.calculateStartPoint(line, positionMatrix, nodeWindth, nodeHeight);

        let prevPoint = startPoint;

        line.forEach((raw, reel) => {
            const point = this.createPoint(positionMatrix[reel][raw].x,
                                           positionMatrix[reel][raw].y);

            segments.push(new WinLineSegment(prevPoint, point, WinLineSegmentType.Outside));
            prevPoint = point;
        });

        // calculate segments for last line
        const finishPoint = this.calculateFinishPoint(line, positionMatrix, nodeWindth, nodeHeight);
        segments.push(new WinLineSegment(prevPoint, finishPoint, WinLineSegmentType.Outside));

        return segments;
    }

    private calculateStartPoint(winLine: Array<number>,
                                positionMatrix: Array<Array<Point>>,
                                nodeWindth: number,
                                nodeHeight: number): Point {

        return this.createPoint(
                    positionMatrix[0][winLine[0]].x - nodeWindth * 0.5,
                    positionMatrix[0][winLine[0]].y);
    }

    private calculateFinishPoint(winLine: Array<number>,
                                 positionMatrix: Array<Array<Point>>,
                                 nodeWindth: number,
                                 nodeHeight: number): Point {

        return  this.createPoint(
                    positionMatrix[winLine.length - 1][winLine[winLine.length - 1]].x + nodeWindth * 0.5,
                    positionMatrix[winLine.length - 1][winLine[winLine.length - 1]].y);
    }

    private getIntersectionWithRect(centerPoint, linePoint1, linePoint2, windth, height): Point {
        const rectTopSegmet = new WinLineSegment();
        const rectRightSegmet = new WinLineSegment();
        const rectBottomSegmet = new WinLineSegment();
        const rectLeftSegmet = new WinLineSegment();

        const arr = new Array<WinLineSegment>();

        rectTopSegmet.point1 = new Point(centerPoint.x - (windth * 0.5), centerPoint.y + (height * 0.5));
        rectTopSegmet.point2 = new Point(centerPoint.x + (windth * 0.5), centerPoint.y + (height * 0.5));
        arr.push(rectTopSegmet);

        rectRightSegmet.point1 = new Point(centerPoint.x + (windth * 0.5), centerPoint.y - (height * 0.5));
        rectRightSegmet.point2 = new Point(centerPoint.x + (windth * 0.5), centerPoint.y + (height * 0.5));
        arr.push(rectRightSegmet);

        rectBottomSegmet.point1 = new Point(centerPoint.x - (windth * 0.5), centerPoint.y - (height * 0.5));
        rectBottomSegmet.point2 = new Point(centerPoint.x + (windth * 0.5), centerPoint.y - (height * 0.5));
        arr.push(rectBottomSegmet);

        rectLeftSegmet.point1 = new Point(centerPoint.x - (windth * 0.5), centerPoint.y - (height * 0.5));
        rectLeftSegmet.point2 = new Point(centerPoint.x - (windth * 0.5), centerPoint.y + (height * 0.5));
        arr.push(rectLeftSegmet);

        let intersectPoint1;

        // tslint:disable-next-line:prefer-for-of
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            intersectPoint1 = this.getPointOfIntersection(linePoint1, linePoint2, element.point1, element.point2);
            if (intersectPoint1 !== null) {
                return intersectPoint1;
            }
        }

        return intersectPoint1;
    }

    private getPointOfIntersection(p1: Point, p2: Point, p3: Point, p4: Point): Point {
        const d: number = (p1.x - p2.x) * (p4.y - p3.y) - (p1.y - p2.y) * (p4.x - p3.x);
        const da: number = (p1.x - p3.x) * (p4.y - p3.y) - (p1.y - p3.y) * (p4.x - p3.x);
        const db: number = (p1.x - p2.x) * (p1.y - p3.y) - (p1.y - p2.y) * (p1.x - p3.x);

        const ta: number = da / d;
        const tb: number = db / d;

        if (ta >= 0 && ta <= 1 && tb >= 0 && tb <= 1) {
            const dx: number = p1.x + ta * (p2.x - p1.x);
            const dy: number = p1.y + ta * (p2.y - p1.y);

            return new Point(dx, dy);
        }

        return null;
    }

    private createPoint(x: number, y: number): Point {
        const point = new Point();
        point.x = x;
        point.y = y;

        return point;
    }
}

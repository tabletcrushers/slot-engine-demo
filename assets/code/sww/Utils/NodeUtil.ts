import AggregateNode from '../../../showcases/Network/AggregateNode';

export default class NodeUtil {

    public static cloneNode(node: AggregateNode, parent: boolean = true): AggregateNode {
        const clone = cc.instantiate(node);
        if (parent) {
            clone.parent = node.parent;
        }

        return clone;
    }
}

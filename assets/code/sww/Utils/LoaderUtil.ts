import IReelsEngineFakeItemsDTO from '../Core/Games/Slots/Configuration/DTO/FakeItems/IReelsEngineFakeItemsDTO';
import IGameConfigDTO from '../Core/Games/Slots/Configuration/DTO/IGameConfigDTO';
import IGameFieldConfigDTO from '../Core/Games/Slots/Configuration/DTO/IGameFieldConfigDTO';
import IReelConfigDTO from '../Core/Games/Slots/Configuration/DTO/IReelConfigDTO';
import IReelDTO from '../Core/Games/Slots/Configuration/DTO/IReelDTO';
import IReelItemsConfigDTO from '../Core/Games/Slots/Configuration/DTO/IReelItemsConfigDTO';
import IReelsEngineConfigDTO from '../Core/Games/Slots/Configuration/DTO/IReelsEngineConfigDTO';
import ITypeDTO from '../Core/Games/Slots/Configuration/DTO/ITypeDTO';
import TypeUtil from './TypeUtil';

export default class LoaderUtil {

    public static async loadJSON<T>(path: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            const rawUrl: string = cc.url.raw(path);
            cc.loader.load({url: rawUrl, type: 'json'}, (error, result: cc.JsonAsset) => {
                resolve(TypeUtil.castTo<T>(result.json));
            });
        });
    }

    public static async loadReelsEngineConfiguration(path: string): Promise<IReelsEngineConfigDTO> {
        return new Promise<IReelsEngineConfigDTO>((resolve, reject) => {
            const rawUrl: string = cc.url.raw(path);
            cc.loader.load({url: rawUrl, type: 'json'}, (error, result: cc.JsonAsset) => {
                const reelsEngineConfig: IReelsEngineConfigDTO = TypeUtil.castTo<IReelsEngineConfigDTO>(result.json);
                // TODO:DISCUSS - custom loader with loaded state
                // counters are antipattern
                let subLoaderCount: number = 0;

                reelsEngineConfig.gameFields.forEach((gameFieldConfig: IGameFieldConfigDTO) => {
                    gameFieldConfig.reels.forEach((reelConfig: IReelConfigDTO) => {
                        // tslint:disable-next-line:no-magic-numbers
                        addSubLoader(2);
                        this.loadJSON<ITypeDTO>(reelConfig.stateMachineConfigurationPath).then((stateMachineConfig) => {
                            reelConfig.stateMachineConfiguration = stateMachineConfig;
                            removeSubLoader(1);
                        });
                        this.loadJSON<IReelDTO>(reelConfig.configurationPath).then((config) => {
                            reelConfig.configuration = config;
                            removeSubLoader(1);
                        });
                    });
                });

                function addSubLoader(count: number) {
                    subLoaderCount += count;
                }

                function removeSubLoader(count: number) {
                    subLoaderCount -= count;
                    if (subLoaderCount === 0) {
                        resolve(reelsEngineConfig);
                    }
                }
            });
        });
    }

    // TODO : THINK - nested loaders system
    public static async loadGameConfiguration(path: string): Promise<IGameConfigDTO> {
        return new Promise<IGameConfigDTO>((resolve, reject) => {
            const rawUrl: string = cc.url.raw(path);
            cc.loader.load({url: rawUrl, type: 'json'}, (error, result: cc.JsonAsset) => {
                const gameConfig: IGameConfigDTO = TypeUtil.castTo<IGameConfigDTO>(result.json);
                let subLoaderCount: number = 0;

                addSubLoader(1);
                this.loadJSON<IReelItemsConfigDTO>(gameConfig.itemsConfigurationPath).then((itemsConfig) => {
                    gameConfig.itemsConfiguration = itemsConfig;
                    removeSubLoader(1);
                });

                gameConfig.modes.forEach((gameModeConfig) => {
                    // tslint:disable-next-line:no-magic-numbers
                    addSubLoader(2);
                    this.loadReelsEngineConfiguration(gameModeConfig.configurationPath)
                        .then((reelsEngineConfig) => {
                            gameModeConfig.configuration = reelsEngineConfig;
                            removeSubLoader(1);
                    });
                    this.loadJSON<IReelsEngineFakeItemsDTO>(gameModeConfig.fakeItemsConfigurationPath)
                        .then((fakesConfig) => {
                            gameModeConfig.fakeItemsConfiguration = fakesConfig;
                            removeSubLoader(1);
                    });
                });

                function addSubLoader(count: number) {
                    subLoaderCount += count;
                }

                function removeSubLoader(count: number) {
                    subLoaderCount -= count;
                    if (subLoaderCount === 0) {
                        resolve(gameConfig);
                    }
                }
            });
        });
    }

    public static async loadAnimation(path: string): Promise<cc.AnimationClip> {
        return new Promise<cc.AnimationClip>((resolve, reject) => {
            cc.loader.loadRes(path, cc.AnimationClip, (error, clip) => {
                resolve(clip);
            });
        });
    }

    public static async loadPrefab(path: string): Promise<cc.Prefab> {
        return new Promise<cc.Prefab>((resolve, reject) => {
            cc.loader.loadRes(path, cc.Prefab, (error, prefab) => {
                resolve(prefab);
            });
        });
    }
}

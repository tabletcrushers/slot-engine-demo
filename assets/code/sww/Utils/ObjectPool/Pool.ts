import ICreator from '../../Core/Implementation/Creators/ICreator';

export default class Pool<T> {

    private _pool: Array<T>;
    private _creator: ICreator<T>;

    constructor(creator: ICreator<T>, initialCount: number = 10) {
        this._creator = creator;
        this._pool = [];

        this.init(initialCount);
    }

    get size(): number {
        return this._pool.length;
    }

    public get(): T {
        if (this._pool.length <= 0) {
            return this.createObject();
        }

        return this._pool.splice(0, 1)[0];
    }

    public put(obj: T): void {
        this._pool.push(obj);
    }

    private init(initialCount: number): void {
        while (initialCount > 0) {
            this.put(this.createObject());

            initialCount--;
        }
    }

    private createObject(): T {
        return this._creator.create();
    }
}

export default class WaysConfigurator {

    public generateWays(rowsMatrix: Array<number>): Array<Array<number>> {
        const resultWays: Array<Array<number>> = new Array<Array<number>>();

        this.createWay(new Array<number>(), 0, rowsMatrix, resultWays);

        return resultWays;
    }

    private createWay(prevWay: Array<number>, rowId: number, rowsMatrix: Array<number>, resultWays: Array<Array<number>>) {
        if (rowId < rowsMatrix.length) {
            for (let i = 0; i < rowsMatrix[rowId]; i++) {
                const prevWayCopy = prevWay.slice();
                prevWayCopy.push(i);
                this.createWay(prevWayCopy, rowId + 1, rowsMatrix, resultWays);
            }
        } else {
            resultWays.push(prevWay);
        }
    }
}

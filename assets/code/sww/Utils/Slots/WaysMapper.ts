import ArrayUtil from '../ArrayUtil';

export default class WaysMapper {

    public waysMapping(serverWays: Array<Array<number>>, generatedWays: Array<Array<number>>): Map<number, number> {
        const waysMap: Map<number, number> = new Map<number, number>();
        serverWays.forEach((serverWay, serverWayIndex) => {
            const generateWayIndex = generatedWays.findIndex((generatedWay) => ArrayUtil.equals(serverWay, generatedWay));
            waysMap.set(serverWayIndex, generateWayIndex);
        });

        return waysMap;
    }
}

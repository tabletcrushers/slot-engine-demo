import ApplicationDomain from '../../Application/Host/ApplicationDomain';
import IUserProfileData from '../../Application/Host/Repository/IUserProfileData';
import DomainManager from '../../Core/Foundation/Domain/DomainManager';
import ISlotFreeSpins from '../../Games/Host/SlotBonusesRepository/Intefaces/ISlotFreeSpins';
import SlotGameDomain from '../../Games/Host/SlotGameDomain';
import ISlotGameProfile from '../../Games/Host/SlotGameProfileRepository/Interfaces/ISlotGameProfile';
import { SlotGameRepositoryDataEnum } from '../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../Games/Host/SlotGameRepositoryEnum';
import IPaytableSymbol from '../../Games/Host/SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbol';
import IPaytableSymbolPay from '../../Games/Host/SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbolPay';
import IPaytableSymbols from '../../Games/Host/SlotPaytableRepository/Interfaces/Paytable/IPaytableSymbols';
import IWin from '../../Games/Host/SlotSpinResultRepository/Interfaces/IWin';
import SlotServerSpinResult from '../../Games/Host/SlotSpinResultRepository/SlotServerSpinResult';
import ILine from '../../Games/Host/WinLineRepository/interfaces/ILine';
import ILines from '../../Games/Host/WinLineRepository/interfaces/ILines';
import ISlotWinLine from '../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import ITriggerSymbols, { ISymbolPosition } from '../../Games/Host/WinLineRepository/interfaces/ITriggerSymbols';
import TypeUtil from '../TypeUtil';
import WaysConfigurator from './WaysConfigurator';
import WaysMapper from './WaysMapper';

export default class SlotResponseParser {

    public pasrseGameProfile(response: any): void {
        const slotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        const slotGameRepository = slotGameDomain.getDALProvider().getRepository(SlotGameRepositoryEnum.GameProfile);

        const gameProfile: ISlotGameProfile = TypeUtil.provideInterfaceInstance<ISlotGameProfile>();
        gameProfile.token = response.response.token;
        gameProfile.gameRequestUrl = response.response.game_api;
        gameProfile.name = response.response.gameName;
        gameProfile.dir = response.response.gameAlias;

        slotGameRepository.write(SlotGameRepositoryDataEnum.SlotGameProfile, gameProfile);
    }

    public parseUserProfile(response: any): void {
        const applicationDomain = TypeUtil.castTo<ApplicationDomain>(DomainManager.instance.getDomain(ApplicationDomain.NAME));
        const userData: IUserProfileData = TypeUtil.provideInterfaceInstance<IUserProfileData>();
        userData.balance = response.platform.balance;
        applicationDomain.userData = userData;
    }

    public parseSpinResult(response: any): void {
        const slotDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        this.parseWinSymbols(response, slotDomain);
        this.parseSpinWin(response, slotDomain);
        this.parseFreeSpins(response, slotDomain);
    }

    public parsePaytablePaySymbolsInfo(response: any): void {
        const slotDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        const paytableSymbols: IPaytableSymbols = TypeUtil.provideInterfaceInstance<IPaytableSymbols>();
        const configEvent = response.events.find((block) => block.event === 'config');

        this.createAllIcons(configEvent.context.symbols, paytableSymbols);
        this.writeScatterIcons(configEvent.context.symbolsPay.scatter, paytableSymbols);
        this.writeWildIcons(configEvent.context.wildSymbols, paytableSymbols);

        const paytable = Object.keys(configEvent.context.paytable);

        paytable.forEach((key) => {
            this.writePayingSymbols(configEvent.context.paytable[key], paytableSymbols);
        });

        slotDomain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.PaytableRepository)
            .write(SlotGameRepositoryDataEnum.PaytableSymbols, paytableSymbols);
    }

    public waysParser(response: any): void {

        const domainSlot: SlotGameDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
        const linesRepsoitory: ILines = TypeUtil.provideInterfaceInstance<ILines>();
        linesRepsoitory.lines = new Array<ILine>();
        const configEvent = response.events.find((block) => block.event === 'config');
        const serverLines = configEvent.context.availablePayLines;
        const waysConfigurator: WaysConfigurator = new WaysConfigurator();
        const waysMapper: WaysMapper = new WaysMapper();

        // revert line to decart coordinate
        // -------------------------------------------------------------------------------------
        serverLines.forEach((raw, i) => {
            raw.forEach((index, j) => {
                serverLines[i][j] = (Math.abs(index - (configEvent.context.window.rows - 1)));
            });
        });
        // -------------------------------------------------------------------------------------

        const ways: Array<Array<number>> =
            waysConfigurator.generateWays(this.createSimpleMatrix(configEvent.context.window.reels, configEvent.context.window.rows));
        const waysMaped: Map<number, number> = waysMapper.waysMapping(serverLines, ways);

        serverLines.forEach((line, index) => {
            const lineData = TypeUtil.provideInterfaceInstance<ILine>();
            lineData.serverId = index;
            lineData.id = waysMaped.get(index);
            lineData.nodes = ways[lineData.id];
            linesRepsoitory.lines.push(lineData);
        });

        domainSlot.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.LinesRepository)
            .write(SlotGameRepositoryDataEnum.Lines, linesRepsoitory);

    }

    // Private methods block
    private createAllIcons(symbols: Array<string>, payTableSymbols: IPaytableSymbols): IPaytableSymbols {

        payTableSymbols.symbols = new Array<IPaytableSymbol>();
        symbols.forEach((name, index) => {
            const paytableSymbol: IPaytableSymbol = new Object() as IPaytableSymbol;
            paytableSymbol.name = name;
            paytableSymbol.id = index;
            payTableSymbols.symbols.push(paytableSymbol);
        });

        return payTableSymbols;
    }

    private writeWildIcons(wildNames: Array<string>, payTableSymbols: IPaytableSymbols): IPaytableSymbols {

        wildNames.forEach((name) => {
            const paytableSymbol: IPaytableSymbol =  payTableSymbols.symbols.find((symbol) => symbol.name === name);
            paytableSymbol.wild = true;
        });

        return payTableSymbols;
    }

    private writeScatterIcons(scatterNames: Array<string>, payTableSymbols: IPaytableSymbols): IPaytableSymbols {

        scatterNames.forEach((name) => {
            const paytableSymbol: IPaytableSymbol =  payTableSymbols.symbols.find((symbol) => symbol.name === name);
            paytableSymbol.scatter = true;
        });

        return payTableSymbols;
    }

    private writePayingSymbols(symbols: Array<any>, payTableSymbols: IPaytableSymbols): IPaytableSymbols {

        symbols.forEach((element) => {
            const paytableSymbol: IPaytableSymbol =  payTableSymbols.symbols.find((symbol) => symbol.name === element.on.of);
            let symbolPay: IPaytableSymbolPay;
            paytableSymbol.symbolsPay = [];
            const countList: Array<number> = element.on.occurs;
            for (let i = 0; i < countList.length; i++) {
                symbolPay = TypeUtil.provideInterfaceInstance<IPaytableSymbolPay>();
                symbolPay.count = countList[i];
                symbolPay.pay = element.pay[i];
                paytableSymbol.symbolsPay.push(symbolPay);
            }
        });

        return payTableSymbols;
    }

    private createWinSymbolsLine(response: any): Array<number> {
        if (response.context.context.direction === 'right') {
            return this.rigthDiractionWin(response);
        } else {
            return this.leftDiractionWin(response);
        }
    }

    private leftDiractionWin(response: any): Array<number> {
        const result = new Array<number>();
        const slotDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));

        const winLine = slotDomain.getLine(response.context.context.paylineId);
        let count = response.context.occurs;

        winLine.nodes.forEach((element) => {
            if (count > 0) {
                result.push(element);
            } else {
                result.push(-1); // it's mean in this line this element not
            }
            count--;
        });

        return result;
    }

    private rigthDiractionWin(response: any): Array<number> {
        const result = new Array<number>();
        const slotDomain = TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));

        const winLine = slotDomain.getLine(response.context.context.paylineId);
        let count = winLine.nodes.length - response.context.occurs;

        winLine.nodes.forEach((element) => {
            if (count > 0) {
                result.push(-1);
            } else {
                result.push(element); // it's mean in this line this element not
            }
            count--;
        });

        return result;
    }

    private createTriggerWinSymbols(response: any): ITriggerSymbols {
        const result = TypeUtil.provideInterfaceInstance<ITriggerSymbols>();
        result.winSymbolsLine = new Array<ISymbolPosition>();

        response.context.context.forEach((winSybol) => {
            const sumbolPosition = TypeUtil.provideInterfaceInstance<ISymbolPosition>();
            sumbolPosition.reel = winSybol.reel;
            // tslint:disable-next-line:no-magic-numbers
            sumbolPosition.indexOnReel = 2 - winSybol.row;
            result.winSymbolsLine.push(sumbolPosition);
        });

        return result;
    }

    // temp anly for server what
    private createSimpleMatrix(reels: number, raws: number): Array<number> {
        const matrix = new Array<number>();
        for (let i = 0; i < reels; i++) {
            matrix.push(raws);
        }

        return matrix;
    }

    // spin result private block

    private parseWinSymbols(response: any, slotDomain: SlotGameDomain): void {
        const spinResult = new SlotServerSpinResult();
        const serverSpinResult = (response.events.find((element) => element.event === 'playedSpin')).context;
        const reels = new Map <number, Array<number>>();

        spinResult.gameFields = new Map<number, Map<number, Array<number>>>();

        serverSpinResult.forEach((reel, index) => {
            const reelSymbols = new Array<number>();
            reel.forEach((symbol) => {
                reelSymbols.push(slotDomain.getPaytableSymbolByName(symbol).id);
            });
            // reverse() need for move response to decarte coordinates
            reels.set(index, reelSymbols.reverse());
        });

        spinResult.gameFields.set(0, reels);

        slotDomain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .write(SlotGameRepositoryDataEnum.ServerSpinResult, spinResult);
    }

    private parseSpinWin(response: any, slotDomain: SlotGameDomain): void {
        let win = 0;
        let spinWin = 0;

        const slotSpinResultRepository = slotDomain.getDALProvider().getRepository(SlotGameRepositoryEnum.SpinResult);

        const winResult = TypeUtil.provideInterfaceInstance<IWin>();
        const slotWinLines = new Array<ISlotWinLine>();
        const triggerSymbols = new Array<ITriggerSymbols>();
        response.events.forEach((element) => {
            if (element.event === 'spinWin') {

                if (element.context.mode === 'line') {
                    const winLine = TypeUtil.provideInterfaceInstance<ISlotWinLine>();
                    winLine.winLineId = element.context.context.paylineId;
                    winLine.winSymbolsLine = this.createWinSymbolsLine(element);
                    slotWinLines.push(winLine);
                }

                if (element.context.mode === 'scatter') {
                    const symbols: ITriggerSymbols = this.createTriggerWinSymbols(element);
                    symbols.trigger = element.context.mode;
                    triggerSymbols.push(symbols);
                }
            }

            if (element.event === 'bonusWin') {
                spinWin += element.context.pay;
            }
        });

        const gameEnd = response.events.find((element) => element.event === 'gameEnd');
        if (gameEnd !== undefined) {
            win = gameEnd.context.win;
        }

        slotSpinResultRepository.write(SlotGameRepositoryDataEnum.TriggerSymbols, triggerSymbols);
        slotSpinResultRepository.write(SlotGameRepositoryDataEnum.SpinResultWinLines, slotWinLines);

        winResult.win = win;
        winResult.spinWin = spinWin;

        slotSpinResultRepository.write(SlotGameRepositoryDataEnum.SpinResultWin, winResult);

        if (response.platform.gameRound !== undefined) {
            const gameProfile: ISlotGameProfile = slotDomain
                .getDALProvider()
                .getRepository(SlotGameRepositoryEnum.GameProfile)
                .read<ISlotGameProfile>(SlotGameRepositoryDataEnum.SlotGameProfile);

            gameProfile.gameRound = response.platform.gameRound.id;
        }
    }

    private parseFreeSpins(response: any, slotDomain: SlotGameDomain): void {
        const spinTrigger = (response.events.find((element) => element.event === 'spinTrigger'));
        const freeSpinsResult = TypeUtil.provideInterfaceInstance<ISlotFreeSpins>();
        const gameProfile: ISlotGameProfile = slotDomain
                .getDALProvider()
                .getRepository(SlotGameRepositoryEnum.GameProfile)
                .read<ISlotGameProfile>(SlotGameRepositoryDataEnum.SlotGameProfile);

        let freeSpinsCount = 0;
        if (spinTrigger !== undefined && spinTrigger.context.bonus === 'freespins') {
            freeSpinsResult.isActive = true;
            freeSpinsResult.gameRound = response.platform.gameRound.id;

            const enterBonus = response.events.find((element) => element.event === 'enterBonus');
            if (enterBonus !== undefined) {
                // tslint:disable-next-line:no-magic-numbers
                gameProfile.seq = 2;
                freeSpinsCount = enterBonus.context.left;
            }
        }

        const playedBonusSpin = (response.events.find((element) => element.event === 'playedBonusSpin'));
        if (playedBonusSpin !== undefined) {
            gameProfile.seq++;
            freeSpinsResult.isActive = true;
            freeSpinsCount = playedBonusSpin.context.left;
        }

        if (response.events.find((element) => element.event === 'playedBonusSpins')) {
            gameProfile.seq = 0;
            freeSpinsResult.isActive = false;
        }
        freeSpinsResult.count = freeSpinsCount;

        slotDomain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .write(SlotGameRepositoryDataEnum.FreeSpins, freeSpinsResult);
    }
}

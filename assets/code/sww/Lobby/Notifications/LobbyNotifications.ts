export enum LobbyNotificationTopic {
    TopPanel = 'TopPanel',
    GameList = 'GameList'
}

export enum LobbyNotification {
    EntredLobby = 'EntredLobby',
    StartGame = 'StartGame'
}

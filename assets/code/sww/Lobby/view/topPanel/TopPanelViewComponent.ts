const {ccclass, property} = cc._decorator;

@ccclass
export default class TopPanelViewComponent extends cc.Component {

    private _background: cc.Node;
    private _balance: cc.Node;
    private _level: cc.Node;
    private _nickName: cc.Node;

    public onLoad(): void {
        this.init();
        this.regiterButtonListeners();
    }

    public onStart(): void {
    }

    public setBalance(value: number): void {
        this.setText(value.toString(), this._balance);
    }

    public setLevel(value: number): void {
        this.setText(value.toString(), this._level);
    }

    public setNickName(name: string): void {
        this.setText(name, this._nickName);
    }

    public init(): void {
        this._background = this.node.getChildByName('Background');
        this._balance = this.node.getChildByName('Balance');
        this._level = this.node.getChildByName('Level');
        this._nickName = this.node.getChildByName('User');
    }

    protected regiterButtonListeners(): void {
    }

    private setText(value: string, node: cc.Node): void {
        node.getChildByName('Text').getComponent(cc.Label).string = value;
    }
}

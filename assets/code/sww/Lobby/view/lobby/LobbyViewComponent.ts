import LobbyDomain from '../../Domain/LobbyDomain';
import TopPanel from '../../model/topPanel/TopPanel';
import TopPanelViewComponent from '../topPanel/TopPanelViewComponent';

const {ccclass, property} = cc._decorator;

@ccclass
export default class LobbyView extends cc.Component {

    @property(cc.Node)
    private lobbyElements: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:
    public onLoad(): void {
       this.checkForUpdate();
       this.init();
    }

    public start(): void {
        const lobbyLogic: LobbyDomain = new LobbyDomain();
    }

    private init(): void {
        const topPanelContent: TopPanelViewComponent = this.lobbyElements.getChildByName('TopPanel').getComponent('TopPanelViewComponent');
        const topPanel = new TopPanel(topPanelContent as TopPanelViewComponent);
    }

    private checkForUpdate(): void {
        // TODO add logic for check updates in manifest
    }
}

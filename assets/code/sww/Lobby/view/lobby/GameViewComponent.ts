import Notification from '../../../Core/Notifications/Notification';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import { LobbyNotification, LobbyNotificationTopic } from '../../Notifications/LobbyNotifications';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameViewComponent extends cc.Component {

    @property(cc.Node)
    private gamesList: Array<cc.Node> = [];

    private _gameListView: cc.Node;

    public onLoad(): void {
        this.init();
        this.registerListeners();
    }

    private init(): void {
        this._gameListView = this.node.getChildByName('GamesListView');
    }

    private registerListeners(): void {

        this.gamesList.forEach((button) => {
            button.on(cc.Node.EventType.TOUCH_START, this.btnTouchStart.bind(this));
            button.on(cc.Node.EventType.TOUCH_MOVE, this.btnTouchMove.bind(this));
            button.on(cc.Node.EventType.TOUCH_END, this.btnTouchEnd.bind(this));
            button.on(cc.Node.EventType.TOUCH_CANCEL, this.btnTouchCancel.bind(this));
        });
    }

    private btnTouchStart(params: cc.Event.EventTouch): void {
        const gameId: string = params.currentTarget.name;
        this.startGameById(gameId);
    }

    private btnTouchMove(params: cc.Event.EventCustom): void {
    }

    private btnTouchEnd(params: cc.Event.EventCustom): void {
    }

    private btnTouchCancel(params: cc.Event.EventCustom): void {
    }

    private startGameById(gameId: string): void {

        // temp logic for start game
        NotificationManager.instance.dispatch(new Notification(LobbyNotificationTopic.TopPanel, LobbyNotification.StartGame, gameId));
        cc.director.loadScene(gameId);
    }
}

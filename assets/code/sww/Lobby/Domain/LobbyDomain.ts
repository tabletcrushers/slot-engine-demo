import ApplicationDomain from '../../Application/Host/ApplicationDomain';
import { ApplicationNotification,
        ApplicationNotificationTopic,
        UserProfilerNotification } from '../../Application/Notifications/ApplicationNotifications';
import IDALProvider from '../../Core/Foundation/DAL/IDALProvider';
import Domain from '../../Core/Foundation/Domain/Domain';
import DomainManager from '../../Core/Foundation/Domain/DomainManager';
import INotification from '../../Core/Notifications/INotification';
import IObserver from '../../Core/Notifications/IObserver';
import Notification from '../../Core/Notifications/Notification';
import NotificationManager from '../../Core/Notifications/NotificationManager';
import SlotNetworkCommunicator from '../../Network/Communicator/SlotNetworkCommunicator';
import { LobbyNotificationTopic } from '../Notifications/LobbyNotifications';

export default class LobbyDomain extends Domain implements IObserver {

    public static readonly NAME: string = 'LobbyDomain';

    private appDomian: ApplicationDomain;
    private provider: IDALProvider;

    constructor() {
        super();
        this.init();
        this.regiterListeners();

        // temp
        const server = new SlotNetworkCommunicator();
        NotificationManager.instance
            .dispatch(new Notification(ApplicationNotificationTopic.Server, ApplicationNotification.StartGame));
    }

    public observe(notification: INotification): void {
        this.handleServerNotification(notification);
    }

    private handleServerNotification(notification: INotification): void {
        if (notification.topicId !== ApplicationNotificationTopic.Server) {
            return;
        }

        switch (notification.id) {
            case ApplicationNotification.StartGame:
                this.updateTopPanel();
                break;
            default:
        }
    }

    private updateTopPanel(): void {
        NotificationManager.instance
            .dispatch(
                new Notification(LobbyNotificationTopic.TopPanel,
                                 UserProfilerNotification.SetUserNickName,
                                 this.appDomian.userData.name))
            .dispatch(
                new Notification(LobbyNotificationTopic.TopPanel,
                                 UserProfilerNotification.UpdateBalance,
                                 this.appDomian.userData.balance))
            .dispatch(
                new Notification(LobbyNotificationTopic.TopPanel,
                                 UserProfilerNotification.UpdateLevel,
                                 this.appDomian.userData.level));
    }

    private init(): void {

        this.appDomian = new ApplicationDomain();
        this.provider = this.appDomian.getDALProvider();

        DomainManager.instance.addDomain(ApplicationDomain.NAME, this.appDomian);
    }

    private regiterListeners(): void {
        NotificationManager.instance
        // TODO:CHECK - right replacement?
            .addObserver(this, ApplicationNotificationTopic.Server, ApplicationNotification.StartGame);
    }
}

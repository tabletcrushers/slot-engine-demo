import { UserProfilerNotification } from '../../../Application/Notifications/ApplicationNotifications';
import INotification from '../../../Core/Notifications/INotification';
import IObserver from '../../../Core/Notifications/IObserver';
import NotificationManager from '../../../Core/Notifications/NotificationManager';
import { LobbyNotification, LobbyNotificationTopic } from '../../Notifications/LobbyNotifications';
import TopPanelViewComponent from '../../view/topPanel/TopPanelViewComponent';

export default class TopPanel implements IObserver {

    private _content: TopPanelViewComponent;

    constructor(content: TopPanelViewComponent) {
        this._content = content;

        this.registerListeners();
    }

    public observe(notification: INotification): void {
        if (notification.topicId !== LobbyNotificationTopic.TopPanel) {
            return;
        }
        switch (notification.id) {
            case UserProfilerNotification.SetUserNickName:
                return this.setNickName(notification.arguments);
            case UserProfilerNotification.UpdateBalance:
                return this.updateBalance(notification.arguments);
            case UserProfilerNotification.UpdateLevel:
                return this.updateLevel(notification.arguments);
            default:
        }
    }

    public updateBalance(value: number): void {
        this._content.setBalance(value);
    }

    public updateLevel(value: number): void {
        this._content.setLevel(value);
    }

    public setNickName(name: string): void {
        this._content.setNickName(name);
    }

    private registerListeners(): void {
        NotificationManager.instance.addObserver(this, LobbyNotificationTopic.TopPanel, LobbyNotification.StartGame);
        NotificationManager.instance.addObserver(this, LobbyNotificationTopic.TopPanel, LobbyNotification.EntredLobby);

        NotificationManager.instance.addObserver(this, LobbyNotificationTopic.TopPanel, UserProfilerNotification.SetUserNickName);
        NotificationManager.instance.addObserver(this, LobbyNotificationTopic.TopPanel, UserProfilerNotification.UpdateBalance);
        NotificationManager.instance.addObserver(this, LobbyNotificationTopic.TopPanel, UserProfilerNotification.UpdateLevel);
    }

    private unregisterListeners(): void {
        NotificationManager.instance.removeObserver(this, LobbyNotificationTopic.TopPanel, LobbyNotification.StartGame);
        NotificationManager.instance.removeObserver(this, LobbyNotificationTopic.TopPanel, LobbyNotification.EntredLobby);

        NotificationManager.instance.removeObserver(this, LobbyNotificationTopic.TopPanel, UserProfilerNotification.SetUserNickName);
        NotificationManager.instance.removeObserver(this, LobbyNotificationTopic.TopPanel, UserProfilerNotification.UpdateBalance);
        NotificationManager.instance.removeObserver(this, LobbyNotificationTopic.TopPanel, UserProfilerNotification.UpdateLevel);
    }
}

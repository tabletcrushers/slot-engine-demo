import INotification from './INotification';
import IObserver from './IObserver';

export default class NotificationManager {

    private static _instance: NotificationManager;
    private _map: Map<IObserver, Map<string, Array<string>>>;

    constructor() {
        if (NotificationManager._instance === undefined) {
            NotificationManager._instance = this;
            this.flush();
        } else {
            throw Error('NotificationManager instance already created');
        }
    }

    public static get instance(): NotificationManager {
        if (this._instance === undefined) {
            return new NotificationManager();
        } else {
            return this._instance;
        }
    }

    public addObserver(observer: IObserver, topicId: string, notificationId: string): NotificationManager {
        const notificationMap: Map<string, Array<string>> =
            this._map.get(observer) || new Map<string, Array<string>>();
        const notificationIds: Array<string> = notificationMap.get(topicId) || new Array<string>();

        if (notificationIds.indexOf(notificationId) === -1) {
            notificationIds.push(notificationId);
            notificationMap.set(topicId, notificationIds);
            this._map.set(observer, notificationMap);
        }

        return this;
    }

    public dispatch(notification: INotification): NotificationManager {
        this._map.forEach((notificationMap: Map<string, Array<string>>, observer: IObserver) => {
            notificationMap.forEach((notificationIds: Array<string>, notificationTopicId: string) => {
                if (notificationTopicId === notification.topicId) {
                    notificationIds.forEach((notificationId: string) => {
                        if (notificationId === notification.id) {
                            observer.observe(notification);
                        }
                    });
                }
            });
        });

        return this;
    }

    public removeObserver(observer: IObserver, topicId: string, notificationId: string): NotificationManager {
        if (this._map.has(observer)) {
            const notificationMap: Map<string, Array<string>> = this._map.get(observer);
            notificationMap.forEach((notificationIds, notificationTopicId, map) => {
                if (notificationTopicId === topicId) {
                    notificationIds.splice(notificationIds.indexOf(notificationId), 1);
                }
            });
        }

        return this;
    }

    public flush(): void {
        this._map = new Map<IObserver, Map<string, Array<string>>>();
    }
}

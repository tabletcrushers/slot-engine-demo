import INotification from './INotification';

export default class Notification implements INotification {

    private _topicId: string;
    private _id: string;
    private _args: any;

    constructor(topicId: string, id: string, args: any = null) {
        this._topicId = topicId;
        this._id = id;
        this._args = args;
    }

    public get topicId(): string {
        return this._topicId;
    }

    public get id(): string {
        return this._id;
    }

    public get arguments(): any {
        return this._args;
    }
}

import INotification from './INotification';

export default interface IObserver {
    observe(notification: INotification);
}

export default interface INotification {
    id: string;
    topicId: string;
    arguments: any;
}

import IGeometry from './IGeometry';
import Point from './Point';
import Segment from './Segment';

export default class HorizontalSegment extends Segment {

    // NOTE - actually I don't like such override since it hides super
    // but it was done due ti performance needs.
    // Will be removed if we will need to check intersection of different types of Segments
    public intersects(geometry: HorizontalSegment): boolean {
        return(
            (geometry.beginPoint.x >= this.beginPoint.x && geometry.beginPoint.x < this.endPoint.x) ||
            (geometry.endPoint.x > this.beginPoint.x && geometry.endPoint.x <= this.endPoint.x) ||
            (geometry.beginPoint.x <= this.beginPoint.x && geometry.endPoint.x >= this.endPoint.x)
        );
    }

    public clone(): HorizontalSegment {
        return new HorizontalSegment(this.beginPoint.clone(), this.endPoint.clone());
    }
}

import IGeometry from './IGeometry';
import Point from './Point';
import Segment from './Segment';

export default class VerticalSegment extends Segment {

    // NOTE - actually I don't like such override since it hides super
    // but it was done due ti performance needs.
    // Will be removed if we will need to check intersection of different types of Segments
    public intersects(geometry: VerticalSegment): boolean {
        return(
            (geometry.beginPoint.y >= this.beginPoint.y && geometry.beginPoint.y < this.endPoint.y) ||
            (geometry.endPoint.y > this.beginPoint.y && geometry.endPoint.y <= this.endPoint.y) ||
            (geometry.beginPoint.y <= this.beginPoint.y && geometry.endPoint.y >= this.endPoint.y)
        );
    }

    public clone(): VerticalSegment {
        return new VerticalSegment(this.beginPoint.clone(), this.endPoint.clone());
    }
}

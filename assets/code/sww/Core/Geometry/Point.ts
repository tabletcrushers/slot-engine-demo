import IGeometry from './IGeometry';

export default class Point implements IGeometry {

    public x: number;
    public y: number;

    public static add(point1: Point, point2: Point): Point {
        const result = point1.clone();
        result.add(point2);

        return result;
    }

    public static subtract(point1: Point, point2: Point): Point {
        const result = point1.clone();
        result.subtract(point2);

        return result;
    }

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    public add(point: Point): void {
        this.x += point.x;
        this.y += point.y;
    }

    public subtract(point: Point): void {
        this.x -= point.x;
        this.y -= point.y;
    }

    public intersects(point: Point): boolean {
        return this.x === point.x && this.y === point.y;
    }

    public clone(): Point {
        return new Point(this.x, this.y);
    }
}

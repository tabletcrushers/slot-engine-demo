import IGeometry from './IGeometry';

export default class Rectangle implements IGeometry {

    public width: number;
    public height: number;
    private _x: number;
    private _y: number;
    private _top: number;
    private _right: number;

    // TODO - add get/set for all fields and top / right recalc in it

    constructor(x: number = 0, y: number = 0, width: number = 0, height: number = 0) {
        this.width = width;
        this.height = height;
        this._x = x;
        this._y = y;
        // TODO - remove from ctor, move to accessors
        this._top = y + height;
        this._right = x + width;
    }

    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
        this._right = value + this.width;
    }

    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
        this._top = value + this.height;
    }

    public get right(): number {
        return this._right;
    }

    public get top(): number {
        return this._top;
    }

    public intersects(rect: Rectangle): boolean {
        return rect.x < this.right && this.x < rect.right && rect.y < this.top && this.y < rect.top;
    }

    public clone(): Rectangle {
        return new Rectangle(this.x, this.y, this.width, this.height);
    }
}

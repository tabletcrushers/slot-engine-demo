import IGeometry from './IGeometry';
import Point from './Point';

export default class Segment implements IGeometry {

    public beginPoint: Point;
    public endPoint: Point;

    constructor(beginPoint: Point, endPoint: Point) {
        this.beginPoint = beginPoint;
        this.endPoint = endPoint;
    }

    public get length(): number {
        const dx = this.beginPoint.x - this.endPoint.x;
        const dy = this.beginPoint.y - this.endPoint.y;

        return Math.sqrt(dx * dx + dy * dy);
    }

    public intersects(geometry: Segment): boolean {
        return(
            (geometry.beginPoint.y >= this.beginPoint.y && geometry.beginPoint.y < this.endPoint.y) ||
            (geometry.endPoint.y > this.beginPoint.y && geometry.endPoint.y <= this.endPoint.y) ||
            (geometry.beginPoint.y <= this.beginPoint.y && geometry.endPoint.y >= this.endPoint.y)
        );
    }

    public clone(): Segment {
        return new Segment(this.beginPoint.clone(), this.endPoint.clone());
    }
}

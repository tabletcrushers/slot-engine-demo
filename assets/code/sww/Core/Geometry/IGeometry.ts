export default interface IGeometry {
    intersects(geometry: IGeometry): boolean;
    clone(): IGeometry;
}

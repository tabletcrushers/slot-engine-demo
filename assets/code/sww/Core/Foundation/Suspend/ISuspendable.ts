export default interface ISuspendable {
    isActive: boolean;
    resume(): void;
    suspend(): void;
}

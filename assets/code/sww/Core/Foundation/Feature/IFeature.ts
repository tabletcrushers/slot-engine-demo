export default interface IFeature {
    start(): void;
}

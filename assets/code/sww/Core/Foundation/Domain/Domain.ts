import ISlotSpinMode from '../../../../../showcases/LinesConfigurator/stateForDomain/ISlotSpinMode';
import TypeUtil from '../../../Utils/TypeUtil';
import DALProvider from '../DAL/DALProvider';
import IDALProvider from '../DAL/IDALProvider';
import IFeature from '../Feature/IFeature';
import UUID from '../Identify/UUID';
import IDomain from './IDomain';

export default class Domain implements IDomain {

    protected uuid: UUID;
    private _provider: IDALProvider;
    private _features: Map<string, IFeature>;
    private _modes: Map<string, ISlotSpinMode>;

    constructor() {
        this.uuid = new UUID();
        this._provider = new DALProvider();
        this._features = new Map<string, IFeature>();
        this._modes = new Map<string, ISlotSpinMode>();
    }

    public getDALProvider(): IDALProvider {
        return this._provider;
    }

    public getDomainKey(): UUID {
        return this.uuid;
    }

    public getFeature<T>(key: string): T {
        return TypeUtil.castTo<T>(this._features.get(key));
    }

    public addFeature(key: string, feature: IFeature) {
        this._features.set(key, feature);
    }

    public getMode<T>(key: string): T {
        return TypeUtil.castTo<T>(this._modes.get(key));
    }

    public addMode(key: string, mode: ISlotSpinMode) {
        this._modes.set(key, mode);
    }
}

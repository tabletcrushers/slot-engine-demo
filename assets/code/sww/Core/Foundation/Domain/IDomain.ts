import IDALProvider from '../DAL/IDALProvider';
import UUID from '../Identify/UUID';

export default interface IDomain {
    getDomainKey(): UUID;
    getDALProvider(): IDALProvider;
}

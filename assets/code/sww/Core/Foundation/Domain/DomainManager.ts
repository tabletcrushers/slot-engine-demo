import IDomain from './IDomain';

export default class DomainManager {

    private static _instance: DomainManager;

    private _domains: Map<string, IDomain>;

    constructor() {
        if (DomainManager._instance === undefined) {
            DomainManager._instance = this;
            this._domains = new Map<string, IDomain>();
        } else {
            throw Error('HostManager instance already created');
        }
    }

    public static get instance(): DomainManager {
        if (this._instance === undefined) {
            return new DomainManager();
        } else {
            return this._instance;
        }
    }

    public addDomain(key: string, host: IDomain): void {
        if (this._domains.has(key)) {
            throw(new Error(`Domain ${key} already exists`));
        }
        this._domains.set(key, host);
    }

    public getDomain(key: string): IDomain {
        if (!this._domains.has(key)) {
            throw(new Error(`Domain ${key} not found`));
        }

        return this._domains.get(key);
    }
}

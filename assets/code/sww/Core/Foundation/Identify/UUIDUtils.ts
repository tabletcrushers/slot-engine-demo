// TODO:CLEANUP duplicates
export default class UUIDUtils {
    public static getUUID(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (char) => {
            // tslint:disable-next-line:no-bitwise no-magic-numbers
            const random = Math.random() * 16 | 0;
            const value = char === 'x' ? random : (random % 4 + 8);

            return value.toString(16);
        });
    }
}

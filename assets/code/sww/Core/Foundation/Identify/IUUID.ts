export default interface IUUID {
    getUUID(): string;
}

export default class DALRepository {

    private _storage: Map<string, any>;

    constructor() {
        this._storage = new Map<string, any>();
    }

    public write(identificator: string, data: any): any {
        this._storage.set(identificator, data);
    }

    public read<T>(identificator: string): T {
        if (this._storage.has(identificator)) {
            return this._storage.get(identificator) as T;
        }

        return null;
    }
}

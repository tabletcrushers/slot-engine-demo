import DALRepository from './DALRepository';
import IDALProvider from './IDALProvider';
import IDALRepository from './IDALRepository';

export default class DALProvider implements IDALProvider {

    private _repositories: Map<string, IDALRepository>;

    constructor() {
        this._repositories = new Map<string, IDALRepository>();
    }

    public addRepository(identificator: string): void {
        if (!this._repositories.has(identificator)) {
            this._repositories.set(identificator, new DALRepository());
        }
    }

    public getRepository(identificator: string): IDALRepository {
        if (this._repositories.has(identificator)) {
            return this._repositories.get(identificator);
        }

        return null;
    }

    public removeRepository(identificator: string): void {
        if (this._repositories.has(identificator)) {
            this._repositories.delete(identificator);
        }
    }
}

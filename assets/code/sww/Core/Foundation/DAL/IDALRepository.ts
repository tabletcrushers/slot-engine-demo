export default interface IDALRepository {
    read<T>(dentificator: any): T;
    write(dentificator: any, data: any): any;
}

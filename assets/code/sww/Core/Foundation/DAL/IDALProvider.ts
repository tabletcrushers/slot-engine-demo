import IDALRepository from './IDALRepository';

export default interface IDALProvider {
    addRepository(identificator: any): void;
    getRepository(identificator: any): IDALRepository;
    removeRepository(identificator: any): void;
}

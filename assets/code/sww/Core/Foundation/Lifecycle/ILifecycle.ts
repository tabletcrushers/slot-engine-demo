import { LifecycleState } from './LifecycleState';

export default interface ILifecycle {
    state: LifecycleState;
    start(): void;
    stop(): void;
}

export enum LifecycleState {
    None,
    Started,
    Stopped
}

export default interface IUpdatable {
    // NOTE - I'd like to see some time struct here but Cocos uses  [ update (seconds) ] signature. So we will use number so far
    update(delta: number): void;
}

import MathUtil from '../../../Utils/MathUtil';
import { OBSERVABLE_LIST_CHANGE_EVENT, ObservableListChangeAction } from './ObservableListChangeAction';
import ObservableListChangeArgs from './ObservableListChangeArgs';

export default class ObservableList<T> extends Array<T> {

    public subscribers: { [key: number]: () => Array<void> } = {};

    constructor(list?: Array<T>) {
        super();

        if (list instanceof Array) {
            this.push(...list);
        }

        Object.setPrototypeOf(this, Object.create(ObservableList.prototype));
    }

    public on(event: string, callback: () => void): void {
        if (!(this.subscribers[event] instanceof Array)) {
            this.subscribers[event] = new Array();
        }

        this.subscribers[event].push(callback);
    }

    public emit(event: string, args: ObservableListChangeArgs): void {

        if (this.subscribers[event] && this.subscribers[event].length > 0) {
            for (const subscriberCallback of this.subscribers[event]) {
                subscriberCallback(args);
            }
        }
    }

    public add(item: T | Array<T>): number {
        let index;

        index = item instanceof Array ? index = this.push(...item) : this.push(item);

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.Add,
            [item],
            index - 1,
            null,
            -1
        ));

        return index - 1;
    }

    public insert(index: number, item: T): number {
        this.inBoundsContract(index);
        this.splice(index, 0, item);

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.Insert,
            [item],
            index,
            null,
            -1
        ));

        return index;
    }

    public insertRange(index: number, items: Array<T>): number {
        this.inBoundsContract(index);
        this.splice(index, 0, ...items);

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.Insert,
            items,
            index,
            null,
            -1
        ));

        return index;
    }

    public replace(index: number, count: number, items: Array<T>): Array<T> {
        this.inBoundsContract(index);

        const replacedItems = this.splice(index, count, ...items);

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.Replace,
            items,
            index,
            replacedItems,
            index
        ));

        return replacedItems;
    }

    public remove(item: T): number {
        const index = this.indexOf(item);

        this.inBoundsContract(index);
        this.splice(index, 1);

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.Remove,
            null,
            -1,
            [item],
            index
        ));

        return index;
    }

    public removeAt(index: number): T {
        this.inBoundsContract(index);
        const item = this.splice(index, 1)[0];

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.RemoveAt,
            null,
            -1,
            [item],
            index
        ));

        return item;
    }

    public removeRange(index: number, count: number): void {
        this.inBoundsContract(index);
        count = Math.min(count, this.length - index);
        const items = this.splice(index, count);

        this.emit(OBSERVABLE_LIST_CHANGE_EVENT, new ObservableListChangeArgs(
            ObservableListChangeAction.RemoveAt,
            null,
            -1,
            items,
            index
        ));
    }

    private inBoundsContract(index: number): void {
        if (!MathUtil.isInBounds(index, 0, this.length)) {
            throw new Error('Unaccepted argument');
        }
    }
}

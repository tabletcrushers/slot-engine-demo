export enum ObservableListChangeAction {
    Add = 'Add',
    Insert = 'Insert',
    Remove = 'Remove',
    RemoveAt = 'RemoveAt',
    Replace = 'Replace'
}

export const OBSERVABLE_LIST_CHANGE_EVENT = 'Changed';

import { ObservableListChangeAction } from './ObservableListChangeAction';

export default class ObservableListChangeArgs {

    private _action: ObservableListChangeAction;
    private _newItems: Array<any>;
    private _newIndex: number;
    private _oldItems: Array<any>;
    private _oldIndex: number;

    constructor(action: ObservableListChangeAction, newItems: Array<any>, newIndex: number, oldItems: Array<any>, oldIndex: number) {
        this._action = action;
        this._newItems = newItems;
        this._newIndex = newIndex;
        this._oldItems = oldItems;
        this._oldIndex = oldIndex;
    }

    public get action(): ObservableListChangeAction {
        return this._action;
    }

    public get newItems(): Array<any> {
        return this._newItems;
    }

    public get newIndex(): number {
        return this._newIndex;
    }

    public get oldItems(): Array<any> {
        return this._oldItems;
    }

    public get oldIndex(): number {
        return this._oldIndex;
    }
}

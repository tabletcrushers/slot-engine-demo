export default class MapExtended<T, U> implements Map<T, U> {

    public [Symbol.toStringTag]: 'Map';
    private _map: Map<T, U>;

    constructor() {
        this._map = new Map<T, U>();
    }

    public clear(): void {
        this._map.clear();
    }

    public delete(key: T): boolean {
        return this._map.delete(key);
    }

    public forEach(callbackfn: (value: U, key: T, map: Map<T, U>) => void, thisArg?: any): void {
        this._map.forEach(callbackfn, thisArg);
    }

    public get(key: T): U {
        return this._map.get(key);
    }

    public has(key: T): boolean {
        return this._map.has(key);
    }

    public set(key: T, value: U): this {
        this._map.set(key, value);

        return this;
    }

    public get size(): number {
        return this._map.size;
    }

    public [Symbol.iterator](): IterableIterator<[T, U]> {
        return this._map[Symbol.iterator]();
    }

    public entries(): IterableIterator<[T, U]> {
        return this._map.entries();
    }

    public keys(): IterableIterator<T> {
        return this._map.keys();
    }

    public values(): IterableIterator<U> {
        return this._map.values();
    }
}

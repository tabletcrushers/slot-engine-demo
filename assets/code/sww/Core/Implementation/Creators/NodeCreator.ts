import RenderReelItem from '../../../Supply/RenderEngine/RenderReelItem';
import ICreator from './ICreator';

export default class NodeCreator implements ICreator<RenderReelItem> {
    public create(): RenderReelItem {
        return new RenderReelItem();
    }
}

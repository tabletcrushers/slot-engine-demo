import Animation from '../../../Supply/Animation/Animation';
import IAnimation from '../../../Supply/Animation/IAnimation';
import ICreator from './ICreator';

export default class AnimationFrameCreator implements ICreator<IAnimation> {
    public create(): IAnimation {
        return new Animation();
    }
}

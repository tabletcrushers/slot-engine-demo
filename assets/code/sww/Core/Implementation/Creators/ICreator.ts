export default interface ICreator<T> {
    create(): T;
}

import IReelsEngineConfiguration from '../ReelsEngine/Configuration/IReelsEngineConfiguration';
import ReelsEngineConfiguration from '../ReelsEngine/Configuration/ReelsEngineConfiguration';
import GameField from '../ReelsEngine/Core/GameField/GameField';
import IGameField from '../ReelsEngine/Core/GameField/IGameField';
import CommandBuilder from '../ReelsEngine/Core/Implementation/Builders/CommandBuilder';
import GeometryCirculatorBuilder from '../ReelsEngine/Core/Implementation/Builders/GeometryCirculatorBuilder';
import MovementRuleBuilder from '../ReelsEngine/Core/Implementation/Builders/MovementRuleBuilder';
import OverlapCheckerBuilder from '../ReelsEngine/Core/Implementation/Builders/OverlapCheckerBuilder';
import PlacementRuleBuilder from '../ReelsEngine/Core/Implementation/Builders/PlacementRuleBuilder';
import PredicateBuilder from '../ReelsEngine/Core/Implementation/Builders/PredicateBuilder';
import ReelBuilder from '../ReelsEngine/Core/Implementation/Builders/ReelBuilder';
import StateBuilder from '../ReelsEngine/Core/Implementation/Builders/StateBuilder';
import StateMachineBuilder from '../ReelsEngine/Core/Implementation/Builders/StateMachineBuilder';
import TransitionBuilder from '../ReelsEngine/Core/Implementation/Builders/TransitionBuilder';
import ViewportBuilder from '../ReelsEngine/Core/Implementation/Builders/ViewportBuilder';
import IReel from '../ReelsEngine/Core/Reel/IReel';
import IGameFieldFakeItemsDTO from './DTO/FakeItems/IGameFieldFakeItemsDTO';
import IReelFakeItemsDTO from './DTO/FakeItems/IReelFakeItemsDTO';
import IReelsEngineFakeItemsDTO from './DTO/FakeItems/IReelsEngineFakeItemsDTO';
import IReelsEngineConfigDTO from './DTO/IReelsEngineConfigDTO';
import IScreenDTO from './DTO/IScreenDTO';

export default class JsonConfigurator {

    public makeReelsEngineConfiguration(
        reelsEngineDTO: IReelsEngineConfigDTO,
        screenDTO: IScreenDTO
    ): IReelsEngineConfiguration {
        const movementRuleBuilder: MovementRuleBuilder = new MovementRuleBuilder(screenDTO);
        const commandBuilder: CommandBuilder = new CommandBuilder();
        const predicateBuilder: PredicateBuilder = new PredicateBuilder();
        const stateBuilder: StateBuilder = new StateBuilder(movementRuleBuilder, commandBuilder);
        const transitionBuilder: TransitionBuilder = new TransitionBuilder(predicateBuilder);
        const stateMachineBuilder: StateMachineBuilder = new StateMachineBuilder(stateBuilder, transitionBuilder);
        const viewportBuilder: ViewportBuilder = new ViewportBuilder();
        const placementRuleBuilder: PlacementRuleBuilder = new PlacementRuleBuilder();
        const geometryCirculatorBuilder: GeometryCirculatorBuilder = new GeometryCirculatorBuilder();
        const overlapCheckerBuilder: OverlapCheckerBuilder = new OverlapCheckerBuilder();
        const reelBuilder: ReelBuilder = new ReelBuilder(
            stateMachineBuilder,
            viewportBuilder,
            geometryCirculatorBuilder,
            overlapCheckerBuilder,
            placementRuleBuilder);

        const gameFields: Map<number, IGameField> = new Map<number, IGameField>();
        reelsEngineDTO.gameFields.forEach((gameFieldDTO) => {
            const gameField: IGameField = new GameField(gameFieldDTO.id);

            gameFieldDTO.reels.forEach((reelDTO) => {
                reelDTO.startEvent = reelsEngineDTO.startEvent;
                reelDTO.stopEvent = reelsEngineDTO.stopEvent;
                const reel: IReel = reelBuilder.build(reelDTO);
                gameField.addReel(reel);
            });

            gameFields.set(gameField.id, gameField);
        });

        return new ReelsEngineConfiguration(gameFields);
    }

    public makeFakeItems(reelsEngineFakeItemsDTO: IReelsEngineFakeItemsDTO): Map<number, Map<number, Array<number>>> {
        const reelsEngneFakeItems: Map<number, Map<number, Array<number>>> = new Map<number, Map<number, Array<number>>>();
        reelsEngineFakeItemsDTO.gameFields.forEach((gameFieldFakeItemsDTO: IGameFieldFakeItemsDTO) => {
            const gameFieldFakeItems: Map<number, Array<number>> = new Map<number, Array<number>>();
            gameFieldFakeItemsDTO.reels.forEach((reelFakeItemsDTO: IReelFakeItemsDTO) => {
                gameFieldFakeItems.set(reelFakeItemsDTO.id, reelFakeItemsDTO.strip);
            });
            reelsEngneFakeItems.set(gameFieldFakeItemsDTO.id, gameFieldFakeItems);
        });

        return reelsEngneFakeItems;
    }
}

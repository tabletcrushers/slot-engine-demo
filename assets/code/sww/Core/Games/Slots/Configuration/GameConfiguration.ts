import MapExtended from '../../../Implementation/Enumerables/MapExtended';
import IModeDTO from './DTO/IModeDTO';
import IReelItemsConfigDTO from './DTO/IReelItemsConfigDTO';

export default class GameConfiguration extends MapExtended<string, IModeDTO> {

    private _itemsConfiguration: IReelItemsConfigDTO;

    constructor(itemsConfiguration: IReelItemsConfigDTO) {
        super();
        this._itemsConfiguration = itemsConfiguration;
    }

    public get itemsConfiguration(): IReelItemsConfigDTO {
        return this._itemsConfiguration;
    }
}

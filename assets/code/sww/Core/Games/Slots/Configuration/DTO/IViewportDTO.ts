import Point from '../../../../Geometry/Point';
import ITypeDTO from './ITypeDTO';

export default interface IViewportDTO extends ITypeDTO {
    parameters: {
        pivot: Point;
        geometry: ITypeDTO;
    };
}

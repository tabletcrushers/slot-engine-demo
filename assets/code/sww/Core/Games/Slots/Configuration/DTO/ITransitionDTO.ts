import ITypeDTO from './ITypeDTO';

export default interface ITransitionDTO extends ITypeDTO {
    parameters: {
        fromState: string;
        toState: string;
        event: string;
        predicates: Array<ITypeDTO>;
    };
}

import IReelsEngineConfiguration from '../../ReelsEngine/Configuration/IReelsEngineConfiguration';

export default interface IModeDTO {
    reelsEngineConfiguration: IReelsEngineConfiguration;
    fakeItemsConfiguration: Map<number, Map<number, Array<number>>>;
}

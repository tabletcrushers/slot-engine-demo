import MapExtended from '../../../../../Implementation/Enumerables/MapExtended';
import GameFieldSpinResultDTO from './GameFieldSpinResultDTO';

export default class SpinResultDTO extends MapExtended<number, GameFieldSpinResultDTO> {

    constructor() {
        super();
    }
}

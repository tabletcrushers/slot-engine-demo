export default class ReelSpinResultDTO extends Array<number> {

    public stopItemId: number;
    public stopItemPercentage: number;
    public headItemsCount: number;
    public tailItemsCount: number;

    constructor(list?: Array<number>) {
        super();

        if (list instanceof Array) {
            this.push(...list);
        }

        Object.setPrototypeOf(this, Object.create(ReelSpinResultDTO.prototype));

        this.stopItemId = 0;
        this.stopItemPercentage = 0;
        this.headItemsCount = 0;
        this.tailItemsCount = 0;
    }
}

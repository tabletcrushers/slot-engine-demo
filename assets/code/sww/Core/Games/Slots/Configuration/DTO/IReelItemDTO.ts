import IAnimationDTO from '../../Features/WinAnimations/IAnimationDTO';
import IGeometryDTO from './IGeometryDTO';

export default interface IReelItemDTO {
    id: number;
    drawOrder: number;
    basicGeometry: IGeometryDTO;
    frameGeometry: IGeometryDTO;
    textureName: string;
    offsetX: number;
    offsetY: number;
    animations: Array<IAnimationDTO>;
}

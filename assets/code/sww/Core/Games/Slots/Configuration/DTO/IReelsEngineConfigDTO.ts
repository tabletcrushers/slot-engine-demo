import IGameFieldConfigDTO from './IGameFieldConfigDTO';

export default interface IReelsEngineConfigDTO {
    startEvent: string;
    stopEvent: string;
    gameFields: Array<IGameFieldConfigDTO>;
}

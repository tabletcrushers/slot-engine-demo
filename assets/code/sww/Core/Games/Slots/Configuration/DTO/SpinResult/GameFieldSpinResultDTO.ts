import MapExtended from '../../../../../Implementation/Enumerables/MapExtended';
import ReelSpinResultDTO from './ReelSpinResultDTO';

export default class GameFieldSpinResultDTO extends MapExtended<number, ReelSpinResultDTO> {

    constructor() {
        super();
    }
}

export default interface IScreenDTO {
    width: number;
    height: number;
}

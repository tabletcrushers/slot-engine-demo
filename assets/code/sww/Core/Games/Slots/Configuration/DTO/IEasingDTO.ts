export default interface IEasingDTO {
    name: string;
    p5: number;
    p4: number;
    p3: number;
    p2: number;
    p1: number;
}

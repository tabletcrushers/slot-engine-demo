import ITypeDTO from './ITypeDTO';

export default interface IStateDTO extends ITypeDTO {
    parameters: {
        name: string;
        commands: {
            onEnter: ITypeDTO;
            onExit: ITypeDTO;
        };
        movementRule: ITypeDTO;
    };
}

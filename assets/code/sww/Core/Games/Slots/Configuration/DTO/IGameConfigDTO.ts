import IGameModeConfigDTO from './IGameModeConfigDTO';
import IReelItemsConfigDTO from './IReelItemsConfigDTO';
import IScreenDTO from './IScreenDTO';

export default interface IGameConfigDTO {
    itemsConfigurationPath: string;
    itemsConfiguration: IReelItemsConfigDTO;
    modes: Array<IGameModeConfigDTO>;
    screen: IScreenDTO;
}

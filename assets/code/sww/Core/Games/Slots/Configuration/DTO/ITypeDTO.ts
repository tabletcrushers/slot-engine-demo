export default interface ITypeDTO {
    type: string;
    parameters: any;
}

export default interface IReelFakeItemsDTO {
    id: number;
    strip: Array<number>;
}

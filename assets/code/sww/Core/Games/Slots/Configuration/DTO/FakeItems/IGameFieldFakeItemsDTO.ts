import IReelFakeItemsDTO from './IReelFakeItemsDTO';

export default interface IGameFieldFakeItemsDTO {
    id: number;
    reels: Array<IReelFakeItemsDTO>;
}

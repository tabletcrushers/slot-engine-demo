import IGeometryDTO from './IGeometryDTO';
import IReelItemDTO from './IReelItemDTO';

export default interface IReelItemsConfigDTO {
    items: Array<IReelItemDTO>;
    geometries: Array<IGeometryDTO>;
}

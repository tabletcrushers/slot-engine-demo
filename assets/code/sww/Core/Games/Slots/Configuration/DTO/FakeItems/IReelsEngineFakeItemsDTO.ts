import IGameFieldFakeItemsDTO from './IGameFieldFakeItemsDTO';

export default interface IReelsEngineFakeItemsDTO {
    gameFields: Array<IGameFieldFakeItemsDTO>;
}

import ITypeDTO from './ITypeDTO';
import IViewportDTO from './IViewportDTO';

export default interface IReelDTO extends ITypeDTO {
    parameters: {
        dynamicsType: string;
        startEvent: string;
        stopEvent: string;
        viewport: IViewportDTO;
        placementRule: ITypeDTO;
        geometryCirculator: ITypeDTO;
        overlapChecker: ITypeDTO;
    };
}

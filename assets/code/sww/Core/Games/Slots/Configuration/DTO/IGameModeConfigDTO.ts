import IReelsEngineFakeItemsDTO from './FakeItems/IReelsEngineFakeItemsDTO';
import IReelsEngineConfigDTO from './IReelsEngineConfigDTO';

export default interface IGameModeConfigDTO {
    id: string;
    configurationPath: string;
    configuration: IReelsEngineConfigDTO;
    fakeItemsConfigurationPath: string;
    fakeItemsConfiguration: IReelsEngineFakeItemsDTO;
}

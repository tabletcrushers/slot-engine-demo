import IReelDTO from './IReelDTO';
import ITypeDTO from './ITypeDTO';

export default interface IReelConfigDTO extends ITypeDTO {
    id: number;
    // TODO - create proper DTOs for lower level entities
    stateMachineConfigurationPath: string;
    stateMachineConfiguration: ITypeDTO;
    configurationPath: string;
    configuration: IReelDTO;
    startEvent: string;
    stopEvent: string;
}

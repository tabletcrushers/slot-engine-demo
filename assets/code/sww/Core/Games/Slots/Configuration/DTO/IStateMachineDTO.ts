import IStateDTO from './IStateDTO';
import ITransitionDTO from './ITransitionDTO';
import ITypeDTO from './ITypeDTO';

export default interface IStateMachineDTO extends ITypeDTO {
    parameters: {
        initialState: string;
        lifecycleState: string;
        states: Array<IStateDTO>;
        transitions: Array<ITransitionDTO>;
    };
}

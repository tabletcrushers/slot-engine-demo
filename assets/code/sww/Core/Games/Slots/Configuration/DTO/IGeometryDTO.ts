import ITypeDTO from './ITypeDTO';

export default interface IGeometryDTO extends ITypeDTO {
    alias: string;
    name: string;
}

import IReelConfigDTO from './IReelConfigDTO';

export default interface IGameFieldConfigDTO {
    id: number;
    reels: Array<IReelConfigDTO>;
}

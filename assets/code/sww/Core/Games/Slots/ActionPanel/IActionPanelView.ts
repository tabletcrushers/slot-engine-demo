import IRootCarrier from '../../../../Supply/Interfaces/IRootCarrier';

export default interface IActionPanelView extends IRootCarrier {
    disable(): void;
    enable(): void;
    updateBet(bet: string): void;
    updateBalance(balance: string): void;
    updateWin(win: string): void;
}

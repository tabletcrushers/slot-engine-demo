import ActionPanelView from '../../../../../../showcases/ActionPanel/ActionPanelView';
import AggregateNode from '../../../../../../showcases/Network/AggregateNode';
import { UserProfilerNotification } from '../../../../Application/Notifications/ApplicationNotifications';
import AbstractBetManager from '../../../../Application/User/AbstractBetManager';
import IBetManager from '../../../../Application/User/IBetManager';
import IRootCarrier from '../../../../Supply/Interfaces/IRootCarrier';
import TypeUtil from '../../../../Utils/TypeUtil';
import IFeature from '../../../Foundation/Feature/IFeature';
import IObserver from '../../../Notifications/IObserver';
import Notification from '../../../Notifications/Notification';
import NotificationManager from '../../../Notifications/NotificationManager';
import { ActionPanelNotification, ActionPanelTopic, SlotGameNotificationTopic } from '../Notifications/SlotsGameNotifications';
import IActionPanel from './IActionPanel';
import IActionPanelView from './IActionPanelView';

export default class ActionPanel implements IActionPanel, IObserver, IFeature, IRootCarrier {

    public static NAME: string = 'ActionPanel';

    protected view: IActionPanelView;
    protected betManager: IBetManager;

    constructor(view: IActionPanelView, betManager: IBetManager) {
        this.view = view;
        this.betManager = betManager;

        this.registerListeners();
    }

    public setRoot(root: AggregateNode): void {
        TypeUtil.castTo<ActionPanelView>(this.view).setRoot(root);
        TypeUtil.castTo<ActionPanelView>(this.view).init();
    }

    public start(): void {
        this.view.updateBet(this.betManager.currentBet.toString());
    }

    public setButtonsActivity(val: boolean): void {
        val ? this.view.enable() : this.view.disable();
    }

    // TODO: discuss, maybe will be better to make it empty
    public observe(notification: Notification): void {
        switch (notification.id) {
            case ActionPanelNotification.SpinStart:
                NotificationManager.instance.dispatch(
                    new Notification(SlotGameNotificationTopic.Domain,
                                     ActionPanelNotification.SpinStart, { bet: this.betManager.currentBet }));
                this.setButtonsActivity(false);

                const validBet = this.betManager.getValidBet();

                if (validBet !== AbstractBetManager.InvalidBet) {
                    this.view.updateBet(this.betManager.currentBet.toString());
                } else {
                    NotificationManager.instance.dispatch(
                        new Notification(ActionPanelTopic.ActionPanel, ActionPanelNotification.NegativeBet));
                }

                setTimeout(() => {
                    this.setButtonsActivity(true);

                    if (validBet === AbstractBetManager.InvalidBet) {
                        (this.view as ActionPanelView).disableSpinBtn();
                    }
                    // tslint:disable-next-line:no-magic-numbers
                },         2000);
                break;

            case ActionPanelNotification.BetIncrease:
                this.betManager.nextBet();
                this.view.updateBet(this.betManager.currentBet.toString());
                break;

            case ActionPanelNotification.BetDecrease:
                this.betManager.previousBet();
                this.view.updateBet(this.betManager.currentBet.toString());
                break;

            case UserProfilerNotification.BalanceChanged:
                this.view.updateBalance(notification.arguments.balance);
                break;

            default:
        }
    }

    protected registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, ActionPanelTopic.ActionPanelView, ActionPanelNotification.SpinStart)
            .addObserver(this, ActionPanelTopic.ActionPanelView, ActionPanelNotification.BetIncrease)
            .addObserver(this, ActionPanelTopic.ActionPanelView, ActionPanelNotification.BetDecrease);
    }
}

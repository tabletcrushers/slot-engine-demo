import ITransform from '../Transform/ITransform';
import AbstractMovementRule from './AbstractMovementRule';
import { MovementDirection } from './MovementDirection';

export default class NoMovementRule extends AbstractMovementRule {

    constructor() {
        super(0, null);
        this._direction = MovementDirection.None;
    }

    public updateTransform(targetTransform: ITransform, delta: number): void {
    }
}

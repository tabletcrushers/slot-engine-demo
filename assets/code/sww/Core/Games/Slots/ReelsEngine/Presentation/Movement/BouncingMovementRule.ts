import TypeUtil from '../../../../../../Utils/TypeUtil';
import ITransform from '../Transform/ITransform';
import AbstractEasingMovementRule from './AbstractEasingMovementRule';

export default class BouncingMovementRule extends AbstractEasingMovementRule {

    public setup(...parameters: any[]): void {
        this._valueDifference = this._velocity * this._duration;
        this._startValue =
        this._previousValue = TypeUtil.castTo<ITransform>(parameters[0])[this._propertyToUpdate] - this._valueDifference;
    }

    public reset(): void {
        super.reset();
        if (this._easing !== undefined) {
            this._timePassed = this._duration * this._easing.finalValueTimes[0];
        }
    }

    public updateTransform(targetTransform: ITransform, delta: number): void {
        this.update(delta);
        targetTransform[this._propertyToUpdate] = this._currentValue;
    }
}

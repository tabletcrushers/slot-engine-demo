import IReelItem from '../../Core/Items/IReelItem';
import IReelSpinResult from '../../Result/IReelSpinResult';
import ReelSpinResult from '../../Result/ReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import AbstractSpinResultCorrector from './AbstractSpinResultCorrector';

export default abstract class AbstractStackCorrector extends AbstractSpinResultCorrector {

    protected readonly _stackedId: number;
    protected readonly _stackFullSize: number;
    protected readonly _aliases: Array<number>;

    constructor(stackedId: number, stackFullSize: number, aliases: Array<number>) {
        super();

        this._stackedId = stackedId;
        this._stackFullSize = stackFullSize;
        this._aliases = aliases.slice();
        this._aliases.unshift(stackedId);
    }

    public extend(reelSpinResult: IReelSpinResult, itemsToAddNum: number, zone: ReelSpinResultZoneType): void {
    }

    protected isItemPartOfStack(item: IReelItem): boolean {
        return this._aliases.indexOf(item.id) > -1;
    }

    protected increaseStack(reelSpinResult: ReelSpinResult, size: number, zone: ReelSpinResultZoneType): void {
        for (let i = 0; i < size; i++) {
            reelSpinResult.addItemToZone(this._reelItemBuilder.build(this._stackedId), zone);
        }
    }
}

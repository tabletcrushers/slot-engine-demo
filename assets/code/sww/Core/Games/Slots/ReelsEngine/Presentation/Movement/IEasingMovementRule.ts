import IEasing from '../../../../../../Utils/Easing/IEasing';

export default interface IEasingMovementRule {
    easing: IEasing;
}

import IReelItemBuilder from '../../Core/Items/IReelItemBuilder';
import IReel from '../../Core/Reel/IReel';
import IReelCorrector from './IReelCorrector';

export class RemovalCorrector implements IReelCorrector {

    public isReelCorrector: boolean = true;
    private _idsToRemove: Array<number>;

    constructor(idsToRemove: Array<number>) {
        this._idsToRemove = idsToRemove;
    }

    public setReelItemBuilder(builder: IReelItemBuilder) {
    }

    public correct(reel: IReel): void {
        for (let i = reel.itemList.length - 1; i > -1; i--) {
            const item = reel.itemList[i];

            if (this._idsToRemove.indexOf(item.id) > -1) {
                reel.itemList.remove(item);
            }
        }
    }
}

import Rectangle from '../../../../../Geometry/Rectangle';
import ReelItemList from '../../Core/ItemLists/ReelItemList';
import RectangleReelItem from '../../Core/Items/RectangleReelItem';
import IPlacementRule from './IPlacementRule';

export default class HorizontalRectanglePlacementRule implements IPlacementRule {

    public Apply(list: ReelItemList<RectangleReelItem>): Rectangle {
        let width = 0;
        list.forEach((item) => {
            item.transform.x = width;
            item.arrangeRenderGeometry();
            width += item.geometry.width;
        });

        return new Rectangle(0, 0, width, 1);
    }
}

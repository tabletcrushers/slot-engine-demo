import IEasing from '../../../../../../Utils/Easing/IEasing';
import AbstractMovementRule from './AbstractMovementRule';
import IEasingMovementRule from './IEasingMovementRule';
import ILimitedMovementRule from './ILimitedMovementRule';

export default abstract class AbstractEasingMovementRule extends AbstractMovementRule
    implements ILimitedMovementRule, IEasingMovementRule {

    protected readonly _duration: number;
    protected readonly _easing: IEasing;
    protected _startValue: number;
    protected _currentValue: number;
    protected _previousValue: number;
    protected _valueDifference: number;
    protected _timePassed: number;
    private _completed: boolean;

    constructor(easing: IEasing, velocity: number, duration: number, propertyToUpdate: string) {
        if (duration < 0) {
            throw new Error('Duration can not be negative!');
        }
        super(velocity, propertyToUpdate);

        this._easing = easing;
        this._duration = duration;
    }

    public get completed(): boolean {
        return this._completed;
    }

    public get easing(): IEasing {
        return this._easing;
    }

    public reset(): void {
        super.reset();
        this._timePassed = 0;
        this._completed = false;
    }

    protected update(delta: number): void {
        this.incrementTime(delta);
        this._currentValue = this._easing.calculate(
            this._startValue, this._valueDifference, this._timePassed / this._duration);
        this.defineDirection(this._currentValue - this._previousValue);
        this._previousValue = this._currentValue;
    }

    private incrementTime(delta: number): void {
        this._timePassed += delta;
        if (this._timePassed >= this._duration) {
            this._timePassed = this._duration;
            this._completed = true;
        }
    }
}

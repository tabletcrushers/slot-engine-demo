import MathUtil from '../../../../../../Utils/MathUtil';
import VerticalSegment from '../../../../../Geometry/VerticalSegment';
import Transform from '../Transform/Transform';
import IGeometryCirculator from './IGeometryCirculator';

export default class VerticalSegmentGeometryCirculator implements IGeometryCirculator {

    private _baseGeometry: VerticalSegment;

    public set baseGeometry(value: VerticalSegment) {
        this._baseGeometry = value;
    }

    public circulate(pointer: Transform) {
        pointer.y = MathUtil.circulateInBounds(pointer.y, this._baseGeometry.beginPoint.y, this._baseGeometry.endPoint.y);
    }
}

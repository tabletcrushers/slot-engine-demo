import HorizontalSegment from '../../../../../Geometry/HorizontalSegment';
import Transform from '../Transform/Transform';
import AbstractViewport from './AbstractViewport';

export default class HorizontalSegmentViewport extends AbstractViewport {

    protected _geometry: HorizontalSegment;
    protected _worldGeometry: HorizontalSegment;

    constructor(geometry: HorizontalSegment, pivot: Transform) {
        super(geometry, pivot);
        this.geometry = geometry;
    }

    public get geometry(): HorizontalSegment {
        return this._geometry;
    }
    public set geometry(value: HorizontalSegment) {
        this._geometry = value;
        this._worldGeometry = this._geometry.clone();
        this.updateWorldGeometry();
    }

    protected updateWorldGeometry(): void {
        this._worldGeometry.beginPoint.x = this._geometry.beginPoint.x + this._transform.x;
        this._worldGeometry.endPoint.x = this._geometry.endPoint.x + this._transform.x;
    }
}

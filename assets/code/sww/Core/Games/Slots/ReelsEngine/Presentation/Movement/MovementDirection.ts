export enum MovementDirection {
    None,
    Positive,
    Negative
}

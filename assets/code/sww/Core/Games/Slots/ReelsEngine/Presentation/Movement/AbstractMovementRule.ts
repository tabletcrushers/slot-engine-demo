import ITransform from '../Transform/ITransform';
import IMovementRule from './IMovementRule';
import { MovementDirection } from './MovementDirection';

export default abstract class AbstractMovementRule implements IMovementRule {

    protected readonly  _propertyToUpdate: string;
    protected _velocity: number;
    protected _direction: MovementDirection;
    protected _boundaryEvaluated: boolean;

    constructor(velocity: number, propertyToUpdate: string) {
        this.velocity = velocity;
        this._propertyToUpdate = propertyToUpdate;
        this.reset();
    }

    public get direction(): MovementDirection {
        return this._direction;
    }

    public get velocity(): number {
        return this._velocity;
    }
    public set velocity(value: number) {
        this._velocity = value;
        this.defineDirection();
    }

    public get propertyToUpdate(): string {
        return this._propertyToUpdate;
    }

    public get boundaryEvaluated(): boolean {
        return this._boundaryEvaluated;
    }

    public setup(...parameters: any[]): void {
    }

    public reset(): void {
        this.defineDirection();
    }

    public abstract updateTransform(targetTransform: ITransform, delta: number): void;

    protected defineDirection(velocityAssociation?: number): void {
        const association: number = velocityAssociation === undefined ? this._velocity : velocityAssociation;
        this._direction = association > 0
        ? MovementDirection.Positive
        : (association < 0 ? MovementDirection.Negative : MovementDirection.None);
    }
}

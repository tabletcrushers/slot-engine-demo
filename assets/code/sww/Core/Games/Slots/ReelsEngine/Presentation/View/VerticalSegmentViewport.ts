import VerticalSegment from '../../../../../Geometry/VerticalSegment';
import Transform from '../Transform/Transform';
import AbstractViewport from './AbstractViewport';

export default class VerticalSegmentViewport extends AbstractViewport {

    protected _geometry: VerticalSegment;
    protected _worldGeometry: VerticalSegment;

    constructor(geometry: VerticalSegment, pivot: Transform) {
        super(geometry, pivot);
        this.geometry = geometry;
    }

    public get geometry(): VerticalSegment {
        return this._geometry;
    }
    public set geometry(value: VerticalSegment) {
        this._geometry = value;
        this._worldGeometry = this._geometry.clone();
        this.updateWorldGeometry();
    }

    protected updateWorldGeometry(): void {
        this._worldGeometry.beginPoint.y = this._geometry.beginPoint.y + this._transform.y;
        this._worldGeometry.endPoint.y = this._geometry.endPoint.y + this._transform.y;
    }
}

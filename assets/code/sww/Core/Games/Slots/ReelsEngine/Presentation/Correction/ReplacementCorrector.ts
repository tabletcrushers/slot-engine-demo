import IReelItemBuilder from '../../Core/Items/IReelItemBuilder';
import IReel from '../../Core/Reel/IReel';
import IReelCorrector from './IReelCorrector';

export class ReplacementCorrector implements IReelCorrector {

    public isReelCorrector: boolean = true;
    private _reelItemBuilder: IReelItemBuilder;
    private readonly _replacementSymbols: Map<number, number>;

    constructor(replacementSymbols: Map<number, number>) {
        this._replacementSymbols = replacementSymbols;
    }

    public setReelItemBuilder(builder: IReelItemBuilder) {
        this._reelItemBuilder = builder;
    }

    public correct(reel: IReel): void {
        for (let i = 0; i < reel.itemList.length; i++) {
            const item = reel.itemList[i];

            if (this._replacementSymbols.has(item.id)) {
                const newItem = this._reelItemBuilder.build(this._replacementSymbols.get(item.id));

                reel.itemList.replace(i, 1, [newItem]);
            }
        }
    }
}

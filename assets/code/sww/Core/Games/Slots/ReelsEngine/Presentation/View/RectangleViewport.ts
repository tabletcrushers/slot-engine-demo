import Rectangle from '../../../../../Geometry/Rectangle';
import Transform from '../Transform/Transform';
import AbstractViewport from './AbstractViewport';

export default class RectangleViewport extends AbstractViewport {

    protected _geometry: Rectangle;
    protected _worldGeometry: Rectangle;

    constructor(geometry: Rectangle, pivot: Transform) {
        super(geometry, pivot);
        this.geometry = geometry;
    }

    public get geometry(): Rectangle {
        return this._geometry;
    }
    public set geometry(value: Rectangle) {
        this._geometry = value;
        this._worldGeometry = this._geometry.clone();
        this.updateWorldGeometry();
    }

    protected updateWorldGeometry(): void {
        this._worldGeometry.x = this._geometry.x + this._transform.x;
        this._worldGeometry.y = this._geometry.y + this._transform.y;
    }
}

import ITransform from './ITransform';

export default class Transform implements ITransform {

    public x: number;
    public y: number;
    public rotation: number;
    public scale: number;

    public static add(transform1: Transform, transform2: Transform): Transform {
        const result = transform1.clone();
        result.add(transform2);

        return result;
    }

    public static subtract(transform1: Transform, transform2: Transform): Transform {
        const result = transform1.clone();
        result.subtract(transform2);

        return result;
    }

    constructor(x: number = 0, y: number = 0, rotation: number = 0, scale: number = 1) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.scale = scale;
    }

    public equals(target: Transform): boolean {

        return this.x === target.x
        && this.y === target.y
        && this.rotation === target.rotation
        && this.scale === target.scale;
    }

    public add(transform: Transform): void {
        this.x += transform.x;
        this.y += transform.y;
        // NOTE - check this. I am not sure strict addition is a correct operation here
        this.rotation += transform.rotation;
        this.scale += transform.scale;
    }

    public subtract(transform: Transform): void {
        this.x -= transform.x;
        this.y -= transform.y;
        // NOTE - check this. I am not sure strict addition is a correct operation here
        this.rotation -= transform.rotation;
        this.scale -= transform.scale;
    }

    public copyFrom(target: Transform): void {
        this.x = target.x;
        this.y = target.y;
        this.rotation = target.rotation;
        this.scale = target.scale;
    }

    public clone(): Transform {
        return new Transform(this.x, this.y, this.rotation, this.scale);
    }
}

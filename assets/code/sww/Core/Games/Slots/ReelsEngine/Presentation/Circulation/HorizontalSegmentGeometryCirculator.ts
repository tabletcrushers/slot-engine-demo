import MathUtil from '../../../../../../Utils/MathUtil';
import HorizontalSegment from '../../../../../Geometry/HorizontalSegment';
import Transform from '../Transform/Transform';
import IGeometryCirculator from './IGeometryCirculator';

export default class HorizontalSegmentGeometryCirculator implements IGeometryCirculator {

    private _baseGeometry: HorizontalSegment;

    public set baseGeometry(value: HorizontalSegment) {
        this._baseGeometry = value;
    }

    public circulate(pointer: Transform) {
        pointer.x = MathUtil.circulateInBounds(pointer.x, this._baseGeometry.beginPoint.x, this._baseGeometry.endPoint.x);
    }
}

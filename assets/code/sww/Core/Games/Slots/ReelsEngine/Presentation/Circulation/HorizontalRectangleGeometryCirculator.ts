import MathUtil from '../../../../../../Utils/MathUtil';
import Rectangle from '../../../../../Geometry/Rectangle';
import Transform from '../Transform/Transform';
import IGeometryCirculator from './IGeometryCirculator';

export default class HorizontalRectangleGeometryCirculator implements IGeometryCirculator {

    private _baseGeometry: Rectangle;

    public set baseGeometry(value: Rectangle) {
        this._baseGeometry = value;
    }

    public circulate(pointer: Transform) {
        pointer.x = MathUtil.circulateInBounds(pointer.x, this._baseGeometry.x, this._baseGeometry.right);
    }
}

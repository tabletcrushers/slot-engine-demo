import IReelItemBuilderConsumer from '../../Core/Items/IReelItemBuilderConsumer';
import IReel from '../../Core/Reel/IReel';
import ICorrector from './ICorrector';

export default interface IReelCorrector extends ICorrector, IReelItemBuilderConsumer {
    // TODO - think of reflection improvement. Or wait until ES will do it =).
    // So far isReelCorrector remain as hack for TypeUtil
    isReelCorrector: boolean;
    correct(reel: IReel): void;
}

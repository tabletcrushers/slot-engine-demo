import Rectangle from '../../../../../../Geometry/Rectangle';
import Transform from '../../Transform/Transform';
import RectangleViewport from '../RectangleViewport';
import IOverlapChecker from './IOverlapChecker';

export default class VerticalRectangleOverlapChecker implements IOverlapChecker {

    private _baseGeometry: Rectangle;
    private _offset: Transform;

    public set baseGeometry(value: Rectangle) {
        this._baseGeometry = value;
        this._offset = new Transform(0, -this._baseGeometry.height);
    }

    public get offset(): Transform {
        return this._offset;
    }

    public check(viewport: RectangleViewport): boolean  {
        return viewport.transform.y + viewport.geometry.height > this._baseGeometry.height;
    }
}

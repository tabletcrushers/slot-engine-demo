import IGeometry from '../../../../../Geometry/IGeometry';
import IReelItem from '../../Core/Items/IReelItem';
import ITransform from '../Transform/ITransform';

export default interface IViewport {

    pivot: ITransform;
    geometry: IGeometry;
    transform: ITransform;
    overlaps: boolean;
    offset: ITransform;

    updateTransform(origin: ITransform);
    processItem(item: IReelItem);
}

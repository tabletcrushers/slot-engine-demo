import Rectangle from '../../../../../Geometry/Rectangle';
import ReelItemList from '../../Core/ItemLists/ReelItemList';
import RectangleReelItem from '../../Core/Items/RectangleReelItem';
import IPlacementRule from './IPlacementRule';

export default class VerticalRectanglePlacementRule implements IPlacementRule {

    public Apply(list: ReelItemList<RectangleReelItem>): Rectangle {
        let height = 0;
        list.forEach((item) => {
            item.transform.y = height;
            item.arrangeRenderGeometry();
            height += item.geometry.height;
        });

        return new Rectangle(0, 0, 1, height);
    }
}

import ITransform from '../Transform/ITransform';
import ILimitedMovementRule from './ILimitedMovementRule';
import LinearMovementRule from './LinearMovementRule';
import { MovementDirection } from './MovementDirection';

export default class BringingMovementRule extends LinearMovementRule implements ILimitedMovementRule {

    private _completed: boolean;
    private _finalValue: number;

    public updateTransform(targetTransform: ITransform, delta: number): void {
        super.updateTransform(targetTransform, delta);

        const currentValue: number = targetTransform[this._propertyToUpdate];
        if (this._direction === MovementDirection.Negative) {
            if (currentValue < this._finalValue) {
                targetTransform[this._propertyToUpdate] = this._finalValue;
                this._completed = true;

                return;
            }
        } else {
            if (!this._boundaryEvaluated) {
                this._boundaryEvaluated = currentValue < this._finalValue;
            } else if (currentValue > this._finalValue) {
                targetTransform[this._propertyToUpdate] = this._finalValue;
                this._completed = true;

                return;
            }
        }
    }

    public get completed(): boolean {
        return this._completed;
    }

    public reset(): void {
        super.reset();
        this._completed = false;
    }

    public setup(...parameters: any[]): void {
        this._finalValue = parameters[0];
        this._boundaryEvaluated = parameters[1];
    }
}

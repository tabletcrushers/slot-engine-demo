import IGeometry from '../../../../../Geometry/IGeometry';
import ITransform from '../Transform/ITransform';

export default interface IGeometryCirculator {
    baseGeometry: IGeometry;
    circulate(pointer: ITransform);
}

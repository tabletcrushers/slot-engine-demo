import VerticalSegment from '../../../../../../Geometry/VerticalSegment';
import Transform from '../../Transform/Transform';
import VerticalSegmentViewport from '../VerticalSegmentViewport';
import IOverlapChecker from './IOverlapChecker';

export default class VerticalSegmentOverlapChecker implements IOverlapChecker {

    private _baseGeometry: VerticalSegment;
    private _offset: Transform;

    public set baseGeometry(value: VerticalSegment) {
        this._baseGeometry = value;
        this._offset = new Transform(0, -this._baseGeometry.length);
    }

    public get offset(): Transform {
        return this._offset;
    }

    public check(viewport: VerticalSegmentViewport): boolean  {
        return viewport.transform.y + viewport.geometry.length > this._baseGeometry.endPoint.y;
    }
}

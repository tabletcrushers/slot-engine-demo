import ITransform from '../Transform/ITransform';
import AbstractMovementRule from './AbstractMovementRule';

export default class LinearMovementRule extends AbstractMovementRule {

    public updateTransform(targetTransform: ITransform, delta: number): void {
        targetTransform[this._propertyToUpdate] += delta * this.velocity;
    }
}

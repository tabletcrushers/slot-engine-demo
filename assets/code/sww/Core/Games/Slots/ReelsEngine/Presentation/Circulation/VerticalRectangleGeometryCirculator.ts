import MathUtil from '../../../../../../Utils/MathUtil';
import Rectangle from '../../../../../Geometry/Rectangle';
import Transform from '../Transform/Transform';
import IGeometryCirculator from './IGeometryCirculator';

export default class VerticalRectangleGeometryCirculator implements IGeometryCirculator {

    private _baseGeometry: Rectangle;

    public set baseGeometry(value: Rectangle) {
        this._baseGeometry = value;
    }

    public circulate(pointer: Transform) {
        pointer.y = MathUtil.circulateInBounds(pointer.y, this._baseGeometry.y, this._baseGeometry.top);
    }
}

import IReelItemBuilder from '../../Core/Items/IReelItemBuilder';
import IReel from '../../Core/Reel/IReel';
import ReelSpinResult from '../../Result/ReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import IReelSpinResultCorrector from './IReelSpinResultCorrector';

export default abstract class AbstractSpinResultCorrector implements IReelSpinResultCorrector {

    protected _reelItemBuilder: IReelItemBuilder;

    public setReelItemBuilder(builder: IReelItemBuilder) {
        this._reelItemBuilder = builder;
    }

    public correct(reel: IReel, reelSpinResult: ReelSpinResult): void {
        this.correctHead(reel, reelSpinResult);
        this.correctTail(reel, reelSpinResult);

        reelSpinResult.stopItemForced = true;
    }

    public abstract extend(reelSpinResult: ReelSpinResult, itemsToAddNum: number, zone: ReelSpinResultZoneType): void;

    protected abstract correctHead(reel: IReel, reelSpinResult: ReelSpinResult): void;

    protected abstract correctTail(reel: IReel, reelSpinResult: ReelSpinResult): void;
}

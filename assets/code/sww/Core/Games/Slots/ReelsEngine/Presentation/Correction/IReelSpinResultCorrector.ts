import IReelItemBuilderConsumer from '../../Core/Items/IReelItemBuilderConsumer';
import IReel from '../../Core/Reel/IReel';
import IReelSpinResult from '../../Result/IReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import ICorrector from './ICorrector';

export default interface IReelSpinResultCorrector extends ICorrector, IReelItemBuilderConsumer {
    correct(reel: IReel, reelSpinResult: IReelSpinResult): void;
    extend(reelSpinResult: IReelSpinResult, itemsToAddNum: number, zone: ReelSpinResultZoneType);
}

import IEasing from '../../../../../../Utils/Easing/IEasing';
import ITransform from '../Transform/ITransform';
import AbstractEasingMovementRule from './AbstractEasingMovementRule';

export default class AcceleratingMovementRule extends AbstractEasingMovementRule {

    constructor(easing: IEasing, velocity: number, duration: number, propertyToUpdate: string) {
        super(easing, velocity, duration, propertyToUpdate);

        this._startValue =
        this._previousValue = 0;
        this._valueDifference = this._velocity;
    }

    public updateTransform(targetTransform: ITransform, delta: number): void {
        this.update(delta);
        this.velocity = this._currentValue;
        targetTransform[this._propertyToUpdate] += delta * this.velocity;
    }
}

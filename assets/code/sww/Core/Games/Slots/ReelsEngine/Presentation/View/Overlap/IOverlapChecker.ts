import IGeometry from '../../../../../../Geometry/IGeometry';
import ITransform from '../../Transform/ITransform';
import IViewport from '../IViewport';

export default interface IOverlapChecker {
    baseGeometry: IGeometry;
    offset: ITransform;
    check(viewport: IViewport): boolean;
}

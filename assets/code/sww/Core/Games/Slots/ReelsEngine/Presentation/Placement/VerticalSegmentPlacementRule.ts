import Point from '../../../../../Geometry/Point';
import VerticalSegment from '../../../../../Geometry/VerticalSegment';
import ReelItemList from '../../Core/ItemLists/ReelItemList';
import VerticalSegmentReelItem from '../../Core/Items/VerticalSegmentReelItem';
import IPlacementRule from './IPlacementRule';

export default class VerticalSegmentPlacementRule implements IPlacementRule {

    public Apply(list: ReelItemList<VerticalSegmentReelItem>): VerticalSegment {
        let y = 0;
        list.forEach((item) => {
            item.transform.y = y;
            item.arrangeRenderGeometry();
            y += item.geometry.length;
        });

        return new VerticalSegment(new Point(), new Point(0, y));
    }
}

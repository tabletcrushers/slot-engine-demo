import IGeometry from '../../../../../Geometry/IGeometry';
import IReelItem from '../../Core/Items/IReelItem';
import Transform from '../Transform/Transform';
import IViewport from './IViewport';

export default abstract class AbstractViewport implements IViewport {

    public pivot: Transform;
    protected abstract _geometry: IGeometry;
    protected abstract _worldGeometry: IGeometry;
    protected _transform: Transform;
    protected _overlaps: boolean;
    protected _offset: Transform;

    constructor(geometry: IGeometry, pivot: Transform) {
        this.pivot = pivot;
        this._transform = new Transform();
    }

    public abstract get geometry(): IGeometry ;
    public abstract set geometry(value: IGeometry);

    public get transform(): Transform {
        return this._transform;
    }

    public get overlaps(): boolean {
        return this._overlaps;
    }
    public set overlaps(value: boolean) {
        this._overlaps = value;
    }

    public get offset(): Transform {
        return this._offset;
    }
    public set offset(value: Transform) {
        this._offset = value;
    }

    public updateTransform(origin: Transform): void {
        this._transform.copyFrom(origin);
        this.updateWorldGeometry();
    }

    public processItem(item: IReelItem): void {
        item.visible = this._worldGeometry.intersects(item.renderGeometry);

        if (item.visible) {
            this.updateItemRenderTransform(item);
        } else {
            if (this._overlaps) {
                this._transform.add(this._offset);
                this.updateWorldGeometry();

                item.visible = this._worldGeometry.intersects(item.renderGeometry);

                if (item.visible) {
                    this.updateItemRenderTransform(item);
                }

                this._transform.subtract(this._offset);
                this.updateWorldGeometry();
            }
        }
    }

    protected abstract updateWorldGeometry(): void;

    private updateItemRenderTransform(item: IReelItem): void {
        // const transform: Transform = Transform.subtract(item.transform, this._transform);
        // transform.add(this.pivot);
        // item.renderTransform = transform;

        item.renderTransform.x = item.transform.x - this._transform.x + this.pivot.x;
        item.renderTransform.y = item.transform.y - this._transform.y + this.pivot.y;
    }
}

import HorizontalSegment from '../../../../../../Geometry/HorizontalSegment';
import Transform from '../../Transform/Transform';
import HorizontalSegmentViewport from '../HorizontalSegmentViewport';
import IOverlapChecker from './IOverlapChecker';

export default class HorizontalSegmentOverlapChecker implements IOverlapChecker {

    private _baseGeometry: HorizontalSegment;
    private _offset: Transform;

    public set baseGeometry(value: HorizontalSegment) {
        this._baseGeometry = value;
        this._offset = new Transform(-this._baseGeometry.length, 0);
    }

    public get offset(): Transform {
        return this._offset;
    }

    public check(viewport: HorizontalSegmentViewport): boolean  {
        return viewport.transform.x + viewport.geometry.length > this._baseGeometry.endPoint.x;
    }
}

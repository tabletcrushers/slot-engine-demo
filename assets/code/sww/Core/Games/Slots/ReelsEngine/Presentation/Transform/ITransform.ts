export default interface ITransform {
    x: number;
    y: number;
    rotation: number;
    scale: number;
    add(transform: ITransform): void;
    subtract(transform: ITransform): void;
    copyFrom(target: ITransform): void;
    clone(): ITransform;
    equals(target: ITransform): boolean;
}

import Rectangle from '../../../../../../Geometry/Rectangle';
import Transform from '../../Transform/Transform';
import RectangleViewport from '../RectangleViewport';
import IOverlapChecker from './IOverlapChecker';

export default class HorizontalRectangleOverlapChecker implements IOverlapChecker {

    private _baseGeometry: Rectangle;
    private _offset: Transform;

    public set baseGeometry(value: Rectangle) {
        this._baseGeometry = value;
        this._offset = new Transform(-this._baseGeometry.width, 0);
    }

    public get offset(): Transform {
        return this._offset;
    }

    public check(viewport: RectangleViewport): boolean  {
        return viewport.transform.x + viewport.geometry.width > this._baseGeometry.width;
    }
}

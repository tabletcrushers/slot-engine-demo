import IReelItem from '../../Core/Items/IReelItem';
import IReel from '../../Core/Reel/IReel';
import ReelSpinResult from '../../Result/ReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import { MovementDirection } from '../Movement/MovementDirection';
import AbstractStackCorrector from './AbstractStackCorrector';

export class StackEnvelopeCorrector extends AbstractStackCorrector {

    constructor(stackedId: number, stackFullSize: number, aliases: Array<number>) {
        super(stackedId, stackFullSize, aliases);
    }

    protected correctHead(reel: IReel, reelSpinResult: ReelSpinResult): void {
        const direction: MovementDirection = reel.movementRule.direction;
        let checkItem: IReelItem = reel.itemList.getNextByDirection(reel.tailVisibleItem, direction);
        if (!this.isItemPartOfStack(checkItem)) {
            return;
        }

        let counter: number = 0;
        while (counter < this._stackFullSize && this.isItemPartOfStack(checkItem)) {
            counter++;
            checkItem = direction === MovementDirection.None
                ? reel.itemList.getNext(checkItem)
                : reel.itemList.getPreviousByDirection(checkItem, direction);
        }

        const size: number = this._stackFullSize - counter;
        this.increaseStack(reelSpinResult, size, ReelSpinResultZoneType.Head);
    }

    protected correctTail(reel: IReel, reelSpinResult: ReelSpinResult): void {
        const direction: MovementDirection = reel.movementRule.direction;
        let checkItem: IReelItem = reel.itemList.getNextByDirection(reel.tailVisibleItem, direction);
        checkItem = direction === MovementDirection.None
                ? reel.itemList.getPrevious(checkItem)
                : reel.itemList.getNextByDirection(checkItem, direction);
        if (!this.isItemPartOfStack(checkItem)) {
            return;
        }

        let counter: number = 0;
        while (counter < this._stackFullSize && this.isItemPartOfStack(checkItem)) {
            counter++;
            checkItem = direction === MovementDirection.None
                ? reel.itemList.getPrevious(checkItem)
                : reel.itemList.getNextByDirection(checkItem, direction);
        }

        const size: number = this._stackFullSize - counter;
        this.increaseStack(reelSpinResult, size, ReelSpinResultZoneType.Tail);
    }
}

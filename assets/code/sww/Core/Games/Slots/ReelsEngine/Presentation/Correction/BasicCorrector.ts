import MathUtil from '../../../../../../Utils/MathUtil';
import IReelItem from '../../Core/Items/IReelItem';
import IReel from '../../Core/Reel/IReel';
import IReelSpinResult from '../../Result/IReelSpinResult';
import ReelSpinResult from '../../Result/ReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import { MovementDirection } from '../Movement/MovementDirection';
import AbstractSpinResultCorrector from './AbstractSpinResultCorrector';

export class BasicCorrector extends AbstractSpinResultCorrector {

    protected readonly _allowedNeighbors: Array<number>;
    protected readonly _forbiddenNeighbors: Array<number>;
    protected readonly _minDistance: number;

    constructor(allowedNeighbors: Array<number>, forbiddenNeighbors: Array<number>, minDistance: number) {
        super();

        this._allowedNeighbors = allowedNeighbors;
        this._forbiddenNeighbors = forbiddenNeighbors;
        this._minDistance = minDistance;
    }

    public extend(reelSpinResult: IReelSpinResult, itemsToAddNum: number, zone: ReelSpinResultZoneType): void {
        this.insertItemsIntoSpinResult(reelSpinResult, itemsToAddNum, zone);
    }

    protected correctHead(reel: IReel, reelSpinResult: ReelSpinResult): void {
        const direction: MovementDirection = reel.movementRule.direction;
        const headItem: IReelItem = reel.itemList.getNextByDirection(reel.tailVisibleItem, direction);
        const tailItem: IReelItem = reel.itemList.getNextByDirection(headItem, direction);

        const inBoundCount: number = Math.min(this._minDistance, reelSpinResult.length);
        const startIndex: number = reelSpinResult.length - 1;
        const endIndex: number = startIndex - inBoundCount;
        for (let i = startIndex; i > endIndex; i--) {
            let currentItem: IReelItem = tailItem;

            const cyclesCount: number = this._minDistance - (reelSpinResult.length - 1 - i);
            for (let j = 0; j < cyclesCount; j++) {
                currentItem = reel.itemList.getPreviousByDirection(currentItem, direction);
                const allowed: boolean = this.isNeighborhoodAllowed(reelSpinResult[i], currentItem);
                if (!allowed) {
                    const numtoAdd: number = this._minDistance - j;
                    this.insertItemsIntoSpinResult(reelSpinResult, numtoAdd, ReelSpinResultZoneType.Head);
                    break;
                }
            }
        }
    }

    protected correctTail(reel: IReel, reelSpinResult: ReelSpinResult): void {
        const direction: MovementDirection = reel.movementRule.direction;
        const headItem: IReelItem = reel.itemList.getNextByDirection(reel.tailVisibleItem, direction);

        const inBoundCount: number = Math.min(this._minDistance, reelSpinResult.length);
        const startIndex: number = 0;
        const endIndex: number = inBoundCount;
        for (let i = startIndex; i < endIndex; i++) {
            let currentItem: IReelItem = headItem;

            const cyclesCount: number = this._minDistance - i;
            for (let j = 0; j < cyclesCount; j++) {
                currentItem = reel.itemList.getPreviousByDirection(currentItem, direction);
                const allowed: boolean = this.isNeighborhoodAllowed(reelSpinResult[i], currentItem);
                if (!allowed) {
                    const numtoAdd: number = this._minDistance - j;
                    this.insertItemsIntoSpinResult(reelSpinResult, numtoAdd, ReelSpinResultZoneType.Tail);
                    break;
                }
            }
        }
    }

    private isNeighborhoodAllowed(item1: IReelItem, item2: IReelItem): boolean {
        return (this._forbiddenNeighbors.indexOf(item1.id) > -1)
            && (this._forbiddenNeighbors.indexOf(item2.id) > -1);
    }

    private insertItemsIntoSpinResult(reelSpinResult: IReelSpinResult, numToAdd: number, zone: ReelSpinResultZoneType): void {
        for (let i = 0; i < numToAdd; i++) {
            const random: number = MathUtil.getRandomInt(this._allowedNeighbors.length);
            const item: IReelItem = this._reelItemBuilder.build(this._allowedNeighbors[random]);
            reelSpinResult.addItemToZone(item, zone);
        }
    }
}

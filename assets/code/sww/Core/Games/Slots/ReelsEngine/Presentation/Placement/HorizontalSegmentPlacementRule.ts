import HorizontalSegment from '../../../../../Geometry/HorizontalSegment';
import Point from '../../../../../Geometry/Point';
import ReelItemList from '../../Core/ItemLists/ReelItemList';
import HorizontalSegmentReelItem from '../../Core/Items/HorizontalSegmentReelItem';
import IPlacementRule from './IPlacementRule';

export default class HorizontalSegmentPlacementRule implements IPlacementRule {

    public Apply(list: ReelItemList<HorizontalSegmentReelItem>): HorizontalSegment {
        let x = 0;
        list.forEach((item: HorizontalSegmentReelItem) => {
            item.transform.x = x;
            item.arrangeRenderGeometry();
            x += item.geometry.length;
        });

        return new HorizontalSegment(new Point(), new Point(x, 0));
    }
}

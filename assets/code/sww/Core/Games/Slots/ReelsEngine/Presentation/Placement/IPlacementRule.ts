import IGeometry from '../../../../../Geometry/IGeometry';
import ReelItemList from '../../Core/ItemLists/ReelItemList';

export default interface IPlacementRule {
    Apply(list: ReelItemList): IGeometry;
}

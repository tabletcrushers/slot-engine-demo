import IReel from '../../Core/Reel/IReel';
import ReelSpinResult from '../../Result/ReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import AbstractStackCorrector from './AbstractStackCorrector';

export default class StackCorrector extends AbstractStackCorrector {

    constructor(stackedId: number, stackFullSize: number, aliases: Array<number>) {
        super(stackedId, stackFullSize, aliases);
    }

    protected correctHead(reel: IReel, reelSpinResult: ReelSpinResult): void {
        if (!this.isItemPartOfStack(reelSpinResult[reelSpinResult.length - 1])) {
            return;
        }

        let counter: number = 0;
        while (counter < reelSpinResult.length && this.isItemPartOfStack(reelSpinResult[reelSpinResult.length - 1 - counter])) {
            counter++;
        }

        const size: number = this._stackFullSize - counter;
        this.increaseStack(reelSpinResult, size, ReelSpinResultZoneType.Head);
    }

    protected correctTail(reel: IReel, reelSpinResult: ReelSpinResult): void {
        if (!this.isItemPartOfStack(reelSpinResult[0])) {
            return;
        }

        let counter: number = 0;
        while (counter < reelSpinResult.length && this.isItemPartOfStack(reelSpinResult[0])) {
            counter++;
        }

        const size: number = this._stackFullSize - counter;
        this.increaseStack(reelSpinResult, size, ReelSpinResultZoneType.Tail);
    }
}

import ITransform from '../Transform/ITransform';
import { MovementDirection } from './MovementDirection';

export default interface IMovementRule {
    direction: MovementDirection;
    velocity: number;
    propertyToUpdate: string;
    boundaryEvaluated: boolean;
    setup(...parameters: any[]): void;
    reset(): void;
    updateTransform(targetTransform: ITransform, delta: number): void;
}

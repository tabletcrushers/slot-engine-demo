import ReelSpinResultDTO from '../../../Configuration/DTO/SpinResult/ReelSpinResultDTO';
import ReelConfiguration from '../../Configuration/ReelConfiguration';
import VerticalSegmentReelItem from '../Items/VerticalSegmentReelItem';
import AbstractReel from './AbstractReel';

export default class VerticalSegmentReel extends AbstractReel {

    constructor(
        id: number,
        configuration: ReelConfiguration
    ) {
        super(id, configuration);
    }

    public getVisibleDump(overHeadNum: number, overTailNum: number): ReelSpinResultDTO {
        throw new Error('Not implemented yet!');
    }

    protected updatePointerOnListChange(changeRange: Array<VerticalSegmentReelItem>, direction: number): void {
        let shift: number = 0;
        changeRange.forEach((item) => {
            shift += item.geometry.length;
        });

        this._pointer.y += direction * shift;
        this._geometryCirculator.circulate(this._pointer);
    }
}

import IStateMachineDTO from '../../../../Configuration/DTO/IStateMachineDTO';
import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import IState from '../../Reel/ReelStateMachine/Machine/IState';
import IStateMachine from '../../Reel/ReelStateMachine/Machine/IStateMachine';
import StateMachine from '../../Reel/ReelStateMachine/Machine/StateMachine';
import ITransition from '../../Reel/ReelStateMachine/Transition/ITransition';
import ITypeBuilder from './ITypeBuilder';
import StateBuilder from './StateBuilder';
import TransitionBuilder from './TransitionBuilder';

export default class StateMachineBuilder implements ITypeBuilder {

    private readonly _stateBuilder: StateBuilder;
    private readonly _transitionBuilder: TransitionBuilder;

    constructor(stateBuilder: StateBuilder, transitionBuilder: TransitionBuilder) {
        this._stateBuilder = stateBuilder;
        this._transitionBuilder = transitionBuilder;
    }

    public build(dto: IStateMachineDTO): IStateMachine {
        if (!dto.parameters.hasOwnProperty('initialState')) {
            throw new Error('No initialState specified.');
        }

        if (!dto.parameters.hasOwnProperty('lifecycleState')) {
            throw new Error('No lifecycleState specified.');
        }

        if (!dto.parameters.hasOwnProperty('states')) {
            throw new Error('Wrong states config.');
        }

        if (!dto.parameters.hasOwnProperty('transitions')) {
            throw new Error('Wrong transitions config.');
        }

        const states: Array<IState> = [];
        const transitions: Array<ITransition> = [];

        Object.keys(dto.parameters.states).forEach((configKey) => {
            states.push(this._stateBuilder.build(dto.parameters.states[configKey]));
        });

        Object.keys(dto.parameters.transitions).forEach((configKey) => {
            transitions.push(this._transitionBuilder.build(dto.parameters.transitions[configKey]));
        });

        return new StateMachine(states, transitions, dto.parameters.initialState, dto.parameters.lifecycleState);
    }
}

import IDisposable from '../../../../../Foundation/Dispose/IDisposable';
import IIdentifiable from '../../../../../Foundation/Identify/IIdentifiable';
import ILifecycle from '../../../../../Foundation/Lifecycle/ILifecycle';
import ISuspendable from '../../../../../Foundation/Suspend/ISuspendable';
import IUpdatable from '../../../../../Foundation/Update/IUpdatable';
import IObserver from '../../../../../Notifications/IObserver';
import GameFieldSpinResultDTO from '../../../Configuration/DTO/SpinResult/GameFieldSpinResultDTO';
import GameFieldSpinResult from '../../Result/GameFieldSpinResult';
import IResultable from '../../Result/IResultable';
import ReelItemList from '../ItemLists/ReelItemList';
import IReelItemBuilderConsumer from '../Items/IReelItemBuilderConsumer';
import IReel from '../Reel/IReel';
import IVisibleItemsReportable from '../Report/IVisibleItemsReportable';

export default interface IGameField extends IIdentifiable, ILifecycle, IUpdatable, IObserver, ISuspendable,
    IDisposable, IResultable, IVisibleItemsReportable, IReelItemBuilderConsumer {
        addReel(reel: IReel): void;
        getReel(id: number): IReel;
        setItems(reelItemLists: Map<number, ReelItemList>): void;
        setSpinResult(result: GameFieldSpinResult, immediately: boolean): void;
        getVisibleDump(overHeadNum: number, overTailNum: number): GameFieldSpinResultDTO;
        randomize(): void;
}

import IDisposable from '../../../../Foundation/Dispose/IDisposable';
import ILifecycle from '../../../../Foundation/Lifecycle/ILifecycle';
import ISuspendable from '../../../../Foundation/Suspend/ISuspendable';
import IUpdatable from '../../../../Foundation/Update/IUpdatable';
import IObserver from '../../../../Notifications/IObserver';
import IReelItemsConfigDTO from '../../Configuration/DTO/IReelItemsConfigDTO';
import SpinResultDTO from '../../Configuration/DTO/SpinResult/SpinResultDTO';
import IReelsEngineConfiguration from '../Configuration/IReelsEngineConfiguration';
import IGameField from './GameField/IGameField';
import IReelItem from './Items/IReelItem';
import IVisibleItemsReport from './Report/IVisibleItemsReport';

export default interface IReelsEngine extends ILifecycle, IUpdatable, IObserver, IDisposable, ISuspendable {
    setConfiguration(configuration: IReelsEngineConfiguration): void;
    setItemsConfiguration(itemsConfiguration: IReelItemsConfigDTO): void;
    getGameField(id: number): IGameField;
    setItems(items: Map<number, Map<number, Array<number>>>): void;
    setSpinResult(result: SpinResultDTO, immediately: boolean): void;
    getVisibleDump(overHeadNum: number, overTailNum: number): SpinResultDTO;
    getVisibleItems(): IVisibleItemsReport;
    getNestedItem(gameFieldId: number, reelId: number, nestId: number): IReelItem;
    replaceItem(gameFieldId: number, reelId: number, nestId: number, newItemId: number): void;
    randomize(): void;
}

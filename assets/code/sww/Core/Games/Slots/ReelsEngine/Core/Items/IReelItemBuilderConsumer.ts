import IReelItemBuilder from './IReelItemBuilder';

export default interface IReelItemBuilderConsumer {
    setReelItemBuilder(builder: IReelItemBuilder);
}

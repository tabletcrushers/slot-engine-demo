import IPredicate from '../../Reel/ReelStateMachine/Predicate/IPredicate';

export default class TruePredicate implements IPredicate {

    public reset(): void {
    }

    public evaluate(): boolean {
        return true;
    }
}

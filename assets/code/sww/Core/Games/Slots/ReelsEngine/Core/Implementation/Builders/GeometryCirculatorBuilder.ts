import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import HorizontalRectangleGeometryCirculator from '../../../Presentation/Circulation/HorizontalRectangleGeometryCirculator';
import HorizontalSegmentGeometryCirculator from '../../../Presentation/Circulation/HorizontalSegmentGeometryCirculator';
import IGeometryCirculator from '../../../Presentation/Circulation/IGeometryCirculator';
import VerticalRectangleGeometryCirculator from '../../../Presentation/Circulation/VerticalRectangleGeometryCirculator';
import VerticalSegmentGeometryCirculator from '../../../Presentation/Circulation/VerticalSegmentGeometryCirculator';
import ITypeBuilder from './ITypeBuilder';

export default class GeometryCirculatorBuilder implements ITypeBuilder {

    public build(dto: ITypeDTO): IGeometryCirculator {
        switch (dto.type) {
            case 'VerticalSegmentGeometryCirculator':
                return new VerticalSegmentGeometryCirculator();

            case 'HorizontalSegmentGeometryCirculator':
                return new HorizontalSegmentGeometryCirculator();

            case 'VerticalRectangleGeometryCirculator':
                return new VerticalRectangleGeometryCirculator();

            case 'HorizontalRectangleGeometryCirculator':
                return new HorizontalRectangleGeometryCirculator();

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

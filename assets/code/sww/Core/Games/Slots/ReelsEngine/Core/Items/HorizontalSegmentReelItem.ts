import HorizontalSegment from '../../../../../Geometry/HorizontalSegment';
import Transform from '../../Presentation/Transform/Transform';
import IReelItem from './IReelItem';

export default class HorizontalSegmentReelItem implements IReelItem {

    public readonly geometry: HorizontalSegment;
    public renderGeometry: HorizontalSegment;
    public transform: Transform;
    public renderTransform: Transform;
    public visible: boolean;
    protected readonly _id: number;
    protected _frameRenderGeometry: HorizontalSegment;
    private _uuid: string;

    constructor(
        id: number,
        geometry: HorizontalSegment,
        frameRenderGeometry: HorizontalSegment
    ) {
        this._id = id;
        this.geometry = geometry;
        this._frameRenderGeometry = frameRenderGeometry;
        this.renderGeometry = this._frameRenderGeometry.clone();
        this.transform = new Transform();
        this.renderTransform = new Transform();
    }

    public get id(): number {
        return this._id;
    }

    // TODO - make AbstractReelItem
    public arrangeRenderGeometry(): void {
        this.renderGeometry.beginPoint.x = this.transform.x + this._frameRenderGeometry.beginPoint.x;
        this.renderGeometry.endPoint.x = this.transform.x + this._frameRenderGeometry.endPoint.x;
    }

    public getUUID(): string {
        return this._uuid;
    }
}

import AbstractReelConsumerCommand from '../../Reel/ReelStateMachine/Command/AbstractReelConsumerCommand';

export default class SimpleResetCommand extends AbstractReelConsumerCommand {

    public execute(): void {
        this._reel.movementRule.reset();
    }
}

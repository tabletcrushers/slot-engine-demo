import ITransitionDTO from '../../../../Configuration/DTO/ITransitionDTO';
import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import IPredicate from '../../Reel/ReelStateMachine/Predicate/IPredicate';
import ITransition from '../../Reel/ReelStateMachine/Transition/ITransition';
import Transition from '../../Reel/ReelStateMachine/Transition/Transition';
import ITypeBuilder from './ITypeBuilder';
import PredicateBuilder from './PredicateBuilder';

export default class TransitionBuilder implements ITypeBuilder {

    private readonly _predicateBuilder: PredicateBuilder;

    constructor(predicateBuilder: PredicateBuilder) {
        this._predicateBuilder = predicateBuilder;
    }

    public build(dto: ITransitionDTO): ITransition {

        const predicates: Array<IPredicate> = new Array<IPredicate>();

        if (dto.parameters.predicates) {
            dto.parameters.predicates.forEach((predicate) => {
                predicates.push(this._predicateBuilder.build(predicate));
            });
        }

        return new Transition(dto.parameters.fromState, dto.parameters.toState, dto.parameters.event, predicates);
    }
}

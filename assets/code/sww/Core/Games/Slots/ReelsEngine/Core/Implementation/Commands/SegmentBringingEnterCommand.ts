import Segment from '../../../../../../Geometry/Segment';
import AbstractBringingEnterCommand from './AbstractBringingEnterCommand';

export default class SegmentBringingEnterCommand extends AbstractBringingEnterCommand {

    public execute() {
        this._reel.movementRule.reset();
        this.setupMovementRule((this._reel.geometry as Segment).length);
    }
}

import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';

export default interface ITypeBuilder {
    build(dto: ITypeDTO): any;
}

import IReel from './IReel';

export default interface IReelConsumer {
    registerReel(reel: IReel);
}

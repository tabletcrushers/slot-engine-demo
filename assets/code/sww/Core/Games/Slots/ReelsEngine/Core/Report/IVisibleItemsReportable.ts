import IReelItem from '../Items/IReelItem';

export default interface IVisibleItemsReportable {
    reportVisibleItems(report: Array<IReelItem>): void;
}

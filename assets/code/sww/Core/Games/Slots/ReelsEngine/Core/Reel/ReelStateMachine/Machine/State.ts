import IMovementRule from '../../../../Presentation/Movement/IMovementRule';
import IStateCommand from '../Command/IStateCommand';
import IState from './IState';

export default class State implements IState {

    public readonly name: string;
    public readonly movementRule: IMovementRule;
    public readonly onEnterCommand: IStateCommand;
    public readonly onExitCommand: IStateCommand;

    constructor(name: string, movementRule: IMovementRule, onEnterCommand: IStateCommand, onExitCommand: IStateCommand) {
        this.name = name;
        this.movementRule = movementRule;
        this.onEnterCommand = onEnterCommand;
        this.onExitCommand = onExitCommand;
    }

    public onEnter() {
        if (this.onEnterCommand) {
            this.onEnterCommand.execute();
        }
    }

    public onExit() {
        if (this.onExitCommand) {
            this.onExitCommand.execute();
        }
    }
}

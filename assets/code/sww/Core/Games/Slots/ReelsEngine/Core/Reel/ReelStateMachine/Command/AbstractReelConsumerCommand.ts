import IReel from '../../IReel';
import IReelConsumer from '../../IReelConsumer';
import IStateCommand from './IStateCommand';

export default abstract class AbstractReelConsumerCommand implements IStateCommand, IReelConsumer {

    protected _reel: IReel;

    public registerReel(reel: IReel) {
        this._reel = reel;
    }

    public abstract execute(): void;
}

import HorizontalSegment from '../../../../../../Geometry/HorizontalSegment';
import Point from '../../../../../../Geometry/Point';
import Rectangle from '../../../../../../Geometry/Rectangle';
import VerticalSegment from '../../../../../../Geometry/VerticalSegment';
import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import IViewportDTO from '../../../../Configuration/DTO/IViewportDTO';
import Transform from '../../../Presentation/Transform/Transform';
import HorizontalSegmentViewport from '../../../Presentation/View/HorizontalSegmentViewport';
import IViewport from '../../../Presentation/View/IViewport';
import RectangleViewport from '../../../Presentation/View/RectangleViewport';
import VerticalSegmentViewport from '../../../Presentation/View/VerticalSegmentViewport';
import ITypeBuilder from './ITypeBuilder';

export default class ViewportBuilder implements ITypeBuilder {

    public build(dto: IViewportDTO): IViewport {
        const geometryParameters: any = dto.parameters.geometry.parameters;
        const pivot: Transform = new Transform(dto.parameters.pivot.x, dto.parameters.pivot.y);

        switch (dto.type) {
            case 'VerticalSegmentViewport':
                let beginPoin: Point = new Point(geometryParameters.beginPoint.x, geometryParameters.beginPoint.y);
                let endPoint: Point = new Point(geometryParameters.endPoint.x, geometryParameters.endPoint.y);
                const verticalGeometry: VerticalSegment = new VerticalSegment(beginPoin, endPoint);

                return new VerticalSegmentViewport(verticalGeometry, pivot);

            case 'HorizontalSegmentViewport':
                beginPoin = new Point(geometryParameters.beginPoint.x, geometryParameters.beginPoint.y);
                endPoint = new Point(geometryParameters.endPoint.x, geometryParameters.endPoint.y);
                const horizontalGeometry = new HorizontalSegment(beginPoin, endPoint);

                return new HorizontalSegmentViewport(horizontalGeometry, pivot);

            case 'RectangleViewport':
                const rectangle = new Rectangle(
                    geometryParameters.x, geometryParameters.y, geometryParameters.width, geometryParameters.height);

                return new RectangleViewport(rectangle, pivot);

            default:
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

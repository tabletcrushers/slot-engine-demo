import { LifecycleState } from '../../../../../Foundation/Lifecycle/LifecycleState';
import INotification from '../../../../../Notifications/INotification';
import Notification from '../../../../../Notifications/Notification';
import NotificationManager from '../../../../../Notifications/NotificationManager';
import GameFieldSpinResultDTO from '../../../Configuration/DTO/SpinResult/GameFieldSpinResultDTO';
import { ReelEngineNotification, ReelEngineNotificationTopic } from '../../../Notifications/SlotsGameNotifications';
import GameFieldSpinResult from '../../Result/GameFieldSpinResult';
import ReelItemList from '../ItemLists/ReelItemList';
import IReelItem from '../Items/IReelItem';
import IReelItemBuilder from '../Items/IReelItemBuilder';
import IReel from '../Reel/IReel';
import IGameField from './IGameField';

export default class GameField implements IGameField {
    private readonly _id: number;
    private _isActive: boolean;
    private _reels: Map<number, IReel>;
    private _state: LifecycleState;
    private _spinResultCleared: boolean;
    private readonly _startNotification: Notification;
    private readonly _stopNotification: Notification;
    private readonly _resultClearedNotification: Notification;

    constructor(id: number) {
        this._id = id;
        this._reels = new Map<number, IReel>();
        this._startNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldStart, { id: this._id });
        this._stopNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldStop, { id: this._id });
        this._resultClearedNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldSpinResultCleared, { id: this._id });
    }

    public addReel(reel: IReel): void {
        this._reels.set(reel.id, reel);
    }

    public getReel(id: number): IReel {
        return this._reels.get(id);
    }

    public start(): void {
        if (this._isActive) {
            this._reels.forEach((reel: IReel) => {
                reel.start();
            });
        }
    }

    public stop(): void {
        if (this._isActive) {
            this._reels.forEach((reel: IReel) => {
                reel.stop();
            });
        }
    }

    public update(delta: number): void {
        if (this._isActive) {
            this._reels.forEach((reel: IReel) => {
                reel.update(delta);
            });
        }
    }

    public dispose(): void {
        this.suspend();
        this._reels.forEach((reel: IReel) => {
            reel.dispose();
        });
    }

    public get id(): number {
        return this._id;
    }

    public get state(): LifecycleState {
        return this._state;
    }

    public get isActive(): boolean {
        return this._isActive;
    }

    public get isSpinResultCleared(): boolean {
        return this._spinResultCleared;
    }

    public setSpinResult(result: GameFieldSpinResult, immediately: boolean): void {
        this._reels.forEach((reel: IReel) => {
            reel.setSpinResult(result.get(reel.id), immediately);
        });
    }

    public getVisibleDump(overHeadNum: number, overTailNum: number): GameFieldSpinResultDTO {
        const dump: GameFieldSpinResultDTO = new GameFieldSpinResultDTO();
        this._reels.forEach((reel: IReel) => {
            dump.set(reel.id, reel.getVisibleDump(overHeadNum, overTailNum));
        });

        return dump;
    }

    public setItems(reelItemLists: Map<number, ReelItemList>): void {
        reelItemLists.forEach((itemList: ReelItemList, key: number) => {
            this._reels.get(key).itemList = itemList;
        });
    }

    public reportVisibleItems(report: Array<IReelItem>): void {
        this._reels.forEach((reel: IReel) => {
            reel.reportVisibleItems(report);
        });
    }

    public randomize(): void {
        this._reels.forEach((reel: IReel) => {
            reel.randomizePointer();
        });
    }

    public observe(notification: INotification): void {
        let areAllReelsQualified: boolean = true;

        switch (notification.id) {
            case ReelEngineNotification.ReelStart:
                this._reels.forEach((reel: IReel) => {
                    areAllReelsQualified = areAllReelsQualified && reel.state === LifecycleState.Started;
                });
                if (areAllReelsQualified) {
                    this._state = LifecycleState.Started;
                    NotificationManager.instance.dispatch(this._startNotification);
                }

                return;

            case ReelEngineNotification.ReelStop:
                this._reels.forEach((reel: IReel) => {
                    areAllReelsQualified = areAllReelsQualified && reel.state === LifecycleState.Stopped;
                });
                if (areAllReelsQualified) {
                    this._state = LifecycleState.Stopped;
                    NotificationManager.instance.dispatch(this._stopNotification);
                }

                return;

            case ReelEngineNotification.CorrectionDone:
                this.getReel(notification.arguments.id).applySpinResult();

                return;

            case ReelEngineNotification.ReelSpinResultCleared:
                this._reels.forEach((reel: IReel) => {
                    areAllReelsQualified = areAllReelsQualified && reel.isSpinResultCleared;
                });

                this._spinResultCleared = areAllReelsQualified;
                if (areAllReelsQualified) {
                    NotificationManager.instance.dispatch(this._resultClearedNotification);
                }

                return;

            default:
                return;
        }
    }

    // TODO:NOTE - same as resume/subscribe in RE
    public resume(): void {
        this._isActive = true;
        this._reels.forEach((reel: IReel) => {
            reel.resume();
        });
        this.subscribe();
    }

    public suspend(): void {
        this._isActive = false;
        this._reels.forEach((reel: IReel) => {
            reel.suspend();
        });
        this.unsubscribe();
    }

    public setReelItemBuilder(builder: IReelItemBuilder) {
        this._reels.forEach((reel: IReel) => {
            reel.setReelItemBuilder(builder);
        });
    }

    private subscribe(): void {
        NotificationManager.instance
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStart)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStop)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.CorrectionDone)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelSpinResultCleared);
    }

    private unsubscribe(): void {
        NotificationManager.instance
            .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStart)
            .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStop)
            .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.CorrectionDone)
            .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelSpinResultCleared);
    }
}

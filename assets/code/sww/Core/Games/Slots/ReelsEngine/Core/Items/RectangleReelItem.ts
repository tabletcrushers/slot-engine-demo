import UUIDUtils from '../../../../../Foundation/Identify/UUIDUtils';
import Rectangle from '../../../../../Geometry/Rectangle';
import Transform from '../../Presentation/Transform/Transform';
import IReelItem from './IReelItem';

export default class RectangleReelItem implements IReelItem {

    public readonly geometry: Rectangle;
    public renderGeometry: Rectangle;
    public transform: Transform;
    public renderTransform: Transform;
    public visible: boolean;
    protected readonly _id: number;
    protected _frameRenderGeometry: Rectangle;
    private _uuid: string;

    constructor(
        id: number,
        geometry: Rectangle,
        frameRenderGeometry: Rectangle
    ) {
        this._id = id;
        this.geometry = geometry;
        this._frameRenderGeometry = frameRenderGeometry;
        this.renderGeometry = this._frameRenderGeometry.clone();
        this.transform = new Transform();
        this.renderTransform = new Transform();
        this._uuid = UUIDUtils.getUUID();
    }

    public get id(): number {
        return this._id;
    }

    // TODO - make AbstractReelItem
    public arrangeRenderGeometry(): void {
        this.renderGeometry.x = this.transform.x + this._frameRenderGeometry.x;
        this.renderGeometry.y = this.transform.y + this._frameRenderGeometry.y;
    }

    public getUUID(): string {
        return this._uuid;
    }
}

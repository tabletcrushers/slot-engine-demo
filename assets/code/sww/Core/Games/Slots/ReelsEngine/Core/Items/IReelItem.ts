import IIdentifiable from '../../../../../Foundation/Identify/IIdentifiable';
import IUUID from '../../../../../Foundation/Identify/IUUID';
import IGeometry from '../../../../../Geometry/IGeometry';
import Transform from '../../Presentation/Transform/Transform';

export default interface IReelItem extends IIdentifiable, IUUID {
    geometry: IGeometry;
    renderGeometry: IGeometry;
    transform: Transform;
    renderTransform: Transform;
    visible: boolean;
}

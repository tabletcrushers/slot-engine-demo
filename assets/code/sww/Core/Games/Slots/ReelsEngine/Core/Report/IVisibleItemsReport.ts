import IDisposable from '../../../../../Foundation/Dispose/IDisposable';
import IReelItem from '../Items/IReelItem';

export default interface IVisibleItemsReport  extends IDisposable {
    // TODO:THINK - if we need to split them by reels. So far I don't think we need.
    registerGameField(id: number): void;
    getGameFieldReport(id: number): Array<IReelItem>;
    clear(): void;
}

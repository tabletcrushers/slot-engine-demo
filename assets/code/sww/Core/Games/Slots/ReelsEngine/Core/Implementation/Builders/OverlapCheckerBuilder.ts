import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import HorizontalRectangleOverlapChecker from '../../../Presentation/View/Overlap/HorizontalRectangleOverlapChecker';
import HorizontalSegmentOverlapChecker from '../../../Presentation/View/Overlap/HorizontalSegmentOverlapChecker';
import IOverlapChecker from '../../../Presentation/View/Overlap/IOverlapChecker';
import VerticalRectangleOverlapChecker from '../../../Presentation/View/Overlap/VerticalRectangleOverlapChecker';
import VerticalSegmentOverlapChecker from '../../../Presentation/View/Overlap/VerticalSegmentOverlapChecker';
import ITypeBuilder from './ITypeBuilder';

export default class OverlapCheckerBuilder implements ITypeBuilder {

    public build(dto: ITypeDTO): IOverlapChecker {
        switch (dto.type) {
            case 'VerticalSegmentOverlapChecker':
                return new VerticalSegmentOverlapChecker();

            case 'HorizontalSegmentOverlapChecker':
                return new HorizontalSegmentOverlapChecker();

            case 'VerticalRectangleOverlapChecker':
                return new VerticalRectangleOverlapChecker();

            case 'HorizontalRectangleOverlapChecker':
                return new HorizontalRectangleOverlapChecker();

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

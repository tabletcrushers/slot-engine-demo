import { EasingType } from '../../../../../../../Utils/Easing/EasingType';
import EasingUtil from '../../../../../../../Utils/Easing/EasingUtil';
import IScreenDTO from '../../../../Configuration/DTO/IScreenDTO';
import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import AcceleratingMovementRule from '../../../Presentation/Movement/AcceleratingMovementRule';
import BouncingMovementRule from '../../../Presentation/Movement/BouncingMovementRule';
import BringingMovementRule from '../../../Presentation/Movement/BringingMovementRule';
import IMovementRule from '../../../Presentation/Movement/IMovementRule';
import LinearMovementRule from '../../../Presentation/Movement/LinearMovementRule';
import NoMovementRule from '../../../Presentation/Movement/NoMovementRule';
import ITypeBuilder from './ITypeBuilder';

export default class MovementRuleBuilder implements ITypeBuilder {

    protected readonly _screenDTO: IScreenDTO;

    constructor(screenDTO: IScreenDTO) {
        this._screenDTO = screenDTO;
    }

    // TODO - for most al rules
    // move propertyToChange inside DTO
    // move velocity<ultiplier inside configurator
    // so minimal count of cases here
    public build(dto: ITypeDTO): IMovementRule {
        switch (dto.type) {
            case 'VerticalLinearMovementRule':
                return new LinearMovementRule(this._screenDTO.height * dto.parameters.velocityMultiplier, 'y');

            case 'VerticalAcceleratingMovementRule':
                return new AcceleratingMovementRule(
                    EasingUtil.getEasing(EasingType.backInQuartic),
                    this._screenDTO.height * dto.parameters.velocityMultiplierLimit,
                    dto.parameters.duration,
                    'y');

            case 'VerticalBringingMovementRule':
                return new BringingMovementRule(this._screenDTO.height * dto.parameters.velocityMultiplier, 'y');

            case 'VerticalBouncingMovementRule':
                return new BouncingMovementRule(
                    EasingUtil.getEasing(EasingType.easeOutBack),
                    this._screenDTO.height * dto.parameters.velocityMultiplier,
                    dto.parameters.duration,
                    'y');

            case 'HorizontalLinearMovementRule':
                return new LinearMovementRule(this._screenDTO.width * dto.parameters.velocityMultiplier, 'x');

            case 'HorizontalAcceleratingMovementRule':
                return new AcceleratingMovementRule(
                    EasingUtil.getEasing(EasingType.backInQuartic),
                    this._screenDTO.width * dto.parameters.velocityMultiplierLimit,
                    dto.parameters.duration,
                    'x');

            case 'HorizontalBouncingMovementRule':
                return new BouncingMovementRule(
                    EasingUtil.getEasing(EasingType.easeOutBack),
                    this._screenDTO.width * dto.parameters.velocityMultiplier,
                    dto.parameters.duration,
                    'x');

            case 'HorizontalBringingMovementRule':
                return new BringingMovementRule(this._screenDTO.width * dto.parameters.velocityMultiplier, 'x');

            case 'NoMovementRule':
                return new NoMovementRule();

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

import IReel from '../../IReel';
import IReelConsumer from '../../IReelConsumer';
import IPredicate from './IPredicate';

export default abstract class AbstracReelConsumerPredicate implements IPredicate, IReelConsumer {

    protected _reel: IReel;

    public registerReel(reel: IReel) {
        this._reel = reel;
    }

    public abstract reset(): void;
    public abstract evaluate(): boolean;
}

import IPredicate from '../Predicate/IPredicate';

export default interface ITransition {
    stateFrom: string;
    stateTo: string;
    event: string;
    predicates: Array<IPredicate>;
}

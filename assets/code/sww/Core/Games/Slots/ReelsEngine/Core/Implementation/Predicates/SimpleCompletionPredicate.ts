import TypeUtil from '../../../../../../../Utils/TypeUtil';
import ILimitedMovementRule from '../../../Presentation/Movement/ILimitedMovementRule';
import AbstracReelConsumerPredicate from '../../Reel/ReelStateMachine/Predicate/AbstracReelConsumerPredicate';

export default class SimpleCompletionPredicate extends AbstracReelConsumerPredicate {

    public evaluate(): boolean {
        return TypeUtil.castTo<ILimitedMovementRule>(this._reel.movementRule).completed;
    }

    public reset() {
    }
}

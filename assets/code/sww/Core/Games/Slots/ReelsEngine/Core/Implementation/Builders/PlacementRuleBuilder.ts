import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import HorizontalRectanglePlacementRule from '../../../Presentation/Placement/HorizontalRectanglePlacementRule';
import HorizontalSegmentPlacementRule from '../../../Presentation/Placement/HorizontalSegmentPlacementRule';
import IPlacementRule from '../../../Presentation/Placement/IPlacementRule';
import VerticalRectanglePlacementRule from '../../../Presentation/Placement/VerticalRectanglePlacementRule';
import VerticalSegmentPlacementRule from '../../../Presentation/Placement/VerticalSegmentPlacementRule';
import ITypeBuilder from './ITypeBuilder';

export default class PlacementRuleBuilder implements ITypeBuilder {

    public build(dto: ITypeDTO): IPlacementRule {
        switch (dto.type) {
            case 'VerticalSegmentPlacementRule':
                return new VerticalSegmentPlacementRule();

            case 'HorizontalSegmentPlacementRule':
                return new HorizontalSegmentPlacementRule();

            case 'VerticalRectanglePlacementRule':
                return new VerticalRectanglePlacementRule();

            case 'HorizontalRectanglePlacementRule':
                return new HorizontalRectanglePlacementRule();

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

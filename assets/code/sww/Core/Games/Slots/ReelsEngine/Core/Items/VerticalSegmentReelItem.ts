import VerticalSegment from '../../../../../Geometry/VerticalSegment';
import Transform from '../../Presentation/Transform/Transform';
import IReelItem from './IReelItem';

export default class VerticalSegmentReelItem implements IReelItem {

    public readonly geometry: VerticalSegment;
    public renderGeometry: VerticalSegment;
    public transform: Transform;
    public renderTransform: Transform;
    public visible: boolean;
    protected readonly _id: number;
    protected _frameRenderGeometry: VerticalSegment;
    private _uuid: string;

    constructor(
        id: number,
        geometry: VerticalSegment,
        frameRenderGeometry: VerticalSegment
    ) {
        this._id = id;
        this.geometry = geometry;
        this._frameRenderGeometry = frameRenderGeometry;
        this.renderGeometry = this._frameRenderGeometry.clone();
        this.transform = new Transform();
        this.renderTransform = new Transform();
    }

    public get id(): number {
        return this._id;
    }

    // TODO - make AbstractReelItem
    public arrangeRenderGeometry(): void {
        this.renderGeometry.beginPoint.y = this.transform.y + this._frameRenderGeometry.beginPoint.y;
        this.renderGeometry.endPoint.y = this.transform.y + this._frameRenderGeometry.endPoint.y;
    }

    public getUUID(): string {
        return this._uuid;
    }
}

import IMovementRule from '../../../../Presentation/Movement/IMovementRule';
import IStateCommand from '../Command/IStateCommand';

export default interface IState {
    name: string;
    movementRule: IMovementRule;
    onEnterCommand: IStateCommand;
    onExitCommand: IStateCommand;
    onEnter();
    onExit();
}

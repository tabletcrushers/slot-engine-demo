import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import IPredicate from '../../Reel/ReelStateMachine/Predicate/IPredicate';
import ResultTimePredicate from '../Predicates/ResultTimePredicate';
import SimpleCompletionPredicate from '../Predicates/SimpleCompletionPredicate';
import TimePredicate from '../Predicates/TimePredicate';
import TruePredicate from '../Predicates/TruePredicate';
import ITypeBuilder from './ITypeBuilder';

export default class PredicateBuilder implements ITypeBuilder {

    public build(dto: ITypeDTO): IPredicate {
        switch (dto.type) {
            case 'TruePredicate':
                return new TruePredicate();

            case 'TimePredicate':
                return new TimePredicate(dto.parameters.duration);

            case 'ResultTimePredicate':
                return new ResultTimePredicate(dto.parameters.duration);

            case 'SimpleCompletionPredicate':
                return new SimpleCompletionPredicate();

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

import MathUtil from '../../../../../../Utils/MathUtil';
import TypeUtil from '../../../../../../Utils/TypeUtil';
import { LifecycleState } from '../../../../../Foundation/Lifecycle/LifecycleState';
import IGeometry from '../../../../../Geometry/IGeometry';
import { OBSERVABLE_LIST_CHANGE_EVENT, ObservableListChangeAction } from '../../../../../Implementation/Lists/ObservableListChangeAction';
import ObservableListChangeArgs from '../../../../../Implementation/Lists/ObservableListChangeArgs';
import INotification from '../../../../../Notifications/INotification';
import Notification from '../../../../../Notifications/Notification';
import NotificationManager from '../../../../../Notifications/NotificationManager';
import ReelSpinResultDTO from '../../../Configuration/DTO/SpinResult/ReelSpinResultDTO';
import { ReelEngineNotification, ReelEngineNotificationTopic } from '../../../Notifications/SlotsGameNotifications';
import ReelConfiguration from '../../Configuration/ReelConfiguration';
import IGeometryCirculator from '../../Presentation/Circulation/IGeometryCirculator';
import ICorrector from '../../Presentation/Correction/ICorrector';
import IReelCorrector from '../../Presentation/Correction/IReelCorrector';
import IReelSpinResultCorrector from '../../Presentation/Correction/IReelSpinResultCorrector';
import IMovementRule from '../../Presentation/Movement/IMovementRule';
import { MovementDirection } from '../../Presentation/Movement/MovementDirection';
import IPlacementRule from '../../Presentation/Placement/IPlacementRule';
import ITransform from '../../Presentation/Transform/ITransform';
import Transform from '../../Presentation/Transform/Transform';
import IViewport from '../../Presentation/View/IViewport';
import IOverlapChecker from '../../Presentation/View/Overlap/IOverlapChecker';
import IReelSpinResult from '../../Result/IReelSpinResult';
import ReelSpinResult from '../../Result/ReelSpinResult';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import ReelItemList from '../ItemLists/ReelItemList';
import IReelItem from '../Items/IReelItem';
import IReelItemBuilder from '../Items/IReelItemBuilder';
import IReelItemBuilderConsumer from '../Items/IReelItemBuilderConsumer';
import IReel from './IReel';
import { ReelDynamicsType } from './ReelDynamicsType';
import IStateMachine from './ReelStateMachine/Machine/IStateMachine';

export default abstract class AbstractReel implements IReel {

    public viewport: IViewport;
    protected _itemList: ReelItemList;
    protected _dynamicsType: ReelDynamicsType;
    protected _stateMachine: IStateMachine;
    protected _startEvent: string;
    protected _stopEvent: string;
    protected _geometryCirculator: IGeometryCirculator;
    protected _overlapChecker: IOverlapChecker;
    protected _placementRule: IPlacementRule;
    protected _correctors: Array<ICorrector>;
    protected _state: LifecycleState;
    protected _pointer: ITransform;
    protected _spinResult: ReelSpinResult;
    protected _tailVisibleItem: IReelItem;
    protected _headVisibleItem: IReelItem;
    protected _bufferItem: IReelItem;
    protected _bufferItem2: IReelItem;
    protected _geometry: IGeometry;
    protected _previousMovementDirection: MovementDirection;
    protected _spinResultCleared: boolean;
    // TODO - remove this shit
    protected _isFirstStart: boolean;
    protected _isActive: boolean;
    protected readonly _id: number;
    protected readonly _startNotification: Notification;
    protected readonly _stopNotification: Notification;
    protected readonly _correctionDoneNotification: Notification;
    protected readonly _spinResultClearedNotification: Notification;

    constructor(
        id: number,
        configuration: ReelConfiguration
    ) {
        this._id = id;
        this._itemList = new ReelItemList();

        this.processConfiguration(configuration);

        this._pointer = new Transform();
        this._previousMovementDirection = this.movementRule.direction;
        this._isFirstStart = true;

        this._stateMachine.registerReel(this);

        this._startNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStart, { id: this._id });
        this._stopNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelStop, { id: this._id });
        this._correctionDoneNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.CorrectionDone, { id: this._id });
        this._spinResultClearedNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelSpinResultCleared, { id: this._id });
    }

    public update(delta: number): void {
        if (this._isActive) {
            this.tryToClearSpinResult();
            this._stateMachine.update(delta);
            this._stateMachine.currentState.movementRule.updateTransform(this._pointer, delta);
            this._geometryCirculator.circulate(this._pointer);
        }
    }

    public suspend(): void {
        this._isActive = false;
        this.unsubscribe();
    }

    public resume(): void {
        this._isActive = true;
        this.subscribe();
    }

    public dispose(): void {
        this.suspend();
        this.unsubscribe();
    }

    public setReelItemBuilder(builder: IReelItemBuilder) {
        this._correctors.forEach((corrector: ICorrector) => {
            if (TypeUtil.isReelItemBuilderConsumer(corrector)) {
                TypeUtil.castTo<IReelItemBuilderConsumer>(corrector)
                    .setReelItemBuilder(builder);
            }
        });
    }

    public start(): void {
        if (this._isActive) {
            this._stateMachine.fire(this._startEvent);
            if (this._dynamicsType === ReelDynamicsType.Static) {
                this._spinResultCleared = true;
                NotificationManager.instance.dispatch(this._spinResultClearedNotification);
            } else {
                this._spinResultCleared = false;
            }
        }
    }

    public stop(): void {
        if (this._isActive) {
            this._stateMachine.fire(this._stopEvent);
        }
    }

    public get id(): number {
        return this._id;
    }

    public get state(): LifecycleState {
        return this._state;
    }

    public get isActive(): boolean {
        return this._isActive;
    }

    public get itemList(): ReelItemList {
        return this._itemList;
    }
    public set itemList(value: ReelItemList) {
        // TODO - add .off
        this._itemList = value;
        this.itemList.on(OBSERVABLE_LIST_CHANGE_EVENT, this.onItemListChange.bind(this));
        this.onGeometryChange();
    }

    public get movementRule(): IMovementRule {
        return this._stateMachine.currentState.movementRule;
    }

    public get headVisibleItem(): IReelItem {
        return this._headVisibleItem;
    }

    public get tailVisibleItem(): IReelItem {
        return this._tailVisibleItem;
    }

    public get pointer(): ITransform {
        return this._pointer;
    }

    public get geometry(): IGeometry {
        return this._geometry;
    }

    public get isSpinResultCleared(): boolean {
        return this._spinResultCleared;
    }

    public setSpinResult(result: IReelSpinResult, immediately: boolean): void {
        if (immediately) {
            this._spinResult = TypeUtil.castTo<ReelSpinResult>(result);
            this._spinResultCleared = true;
            this.initiateHeadTailItems();
            this.processCorrectors();
            this.applySpinResultWithArrangement();
        } else if (this._dynamicsType !== ReelDynamicsType.Static) {
            this._spinResult = TypeUtil.castTo<ReelSpinResult>(result);
        }
    }

    public getSpinResult(): IReelSpinResult {
        return this._spinResult;
    }

    public applySpinResult(): void {
        const nextItem: IReelItem = this.movementRule.direction === MovementDirection.Negative
            ? this._tailVisibleItem
            : this.itemList.getNextByDirection(this._tailVisibleItem, this.movementRule.direction);
        const index: number = this.itemList.indexOf(nextItem);
        this.itemList.insertRange(index, this._spinResult);
    }

    public applyCorrectors(): void {
        if (this._spinResult !== undefined) {
            this.processCorrectors();
            NotificationManager.instance.dispatch(this._correctionDoneNotification);
        }
    }

    public extendCorrectors(itemsToAddNum: number, zone: ReelSpinResultZoneType): void {
        if (this._spinResult !== undefined) {
            this._correctors.forEach((corrector: ICorrector) => {
                if (TypeUtil.isReelSpinResultCorrector(corrector)) {
                    TypeUtil.castTo<IReelSpinResultCorrector>(corrector)
                        .extend(this._spinResult, itemsToAddNum, zone);
                }
            });
        }
    }

    public reportVisibleItems(report: Array<IReelItem>): void {
        const direction: MovementDirection = this.movementRule.direction;

        if (this.hasVisibleItems) {
            if (this._previousMovementDirection === direction) {
                this.calculateHeadTailItems();
            } else {
                this._previousMovementDirection = direction;
                this.initiateHeadTailItems();
            }
        } else {
            this.initiateHeadTailItems();
        }

        this._bufferItem = this._headVisibleItem;
        this._bufferItem2 = this.itemList.getNextByDirection(this._tailVisibleItem, direction);
        while (this._bufferItem !== this._bufferItem2) {
            // TODO - add renderable check here
            report.push(this._bufferItem);
            this._bufferItem = this.itemList.getNextByDirection(this._bufferItem, direction);
        }
    }

    public observe(notification: INotification) {
        if (notification.arguments.id === this._id) {
            if (notification.id === ReelEngineNotification.StateMachineStart) {
                this._state = LifecycleState.Started;
                NotificationManager.instance.dispatch(this._startNotification);
            } else {
                this._state = LifecycleState.Stopped;
                NotificationManager.instance.dispatch(this._stopNotification);
            }
        }
    }

    public getNestedItem(nestId: number): IReelItem {
        let result: IReelItem = this.itemList.find((item) => item.transform.equals(this.pointer));

        for (let i = 0; i < nestId; i++) {
            result = this.itemList.getNextByDirection(result, this.movementRule.direction);
        }

        return result;
    }

    public randomizePointer(): void {
        const id: number = MathUtil.getRandomInt(this._itemList.length);
        this.arrangePointerToItem(this._itemList[id]);
        this.initiateHeadTailItems();
    }

    public replaceItem(oldItem: IReelItem, newItem: IReelItem): void {
        let id: number = this.itemList.indexOf(oldItem);
        if (id === -1) {
            throw new Error('Reel does not contain such item.');
        }
        if (this._headVisibleItem === oldItem) {
            this._headVisibleItem = newItem;
        } else if (this._tailVisibleItem === oldItem) {
            this._tailVisibleItem = newItem;
        }
        this.itemList.replace(id, 1, [newItem]);
        if (this._spinResult !== undefined) {
            id = this._spinResult.indexOf(oldItem);
            if (id > -1) {
                if (this._spinResult.stopItem === this._spinResult[id]) {
                    this._spinResult.stopItem = newItem;
                }
                this._spinResult[id] = newItem;
            }
        }
    }

    public arrangePointerToItem(item: IReelItem): void {
        this._pointer.copyFrom(item.transform);
    }

    public abstract getVisibleDump(overHeadNum: number, overTailNum: number): ReelSpinResultDTO;

    protected processConfiguration(configuration: ReelConfiguration): void {
        this.viewport = configuration.viewport;
        this._dynamicsType = configuration.dynamicsType;
        this._stateMachine = configuration.stateMachine;
        this._geometryCirculator = configuration.geometryCirculator;
        this._overlapChecker = configuration.overlapChecker;
        this._placementRule = configuration.placementRule;
        this._correctors = configuration.correctors;
        this._startEvent = configuration.startEvent;
        this._stopEvent = configuration.stopEvent;
    }

    protected applySpinResultWithArrangement(): void {
        if (this._spinResult !== undefined) {
            this.applySpinResult();
            this.arrangePointerToItem(this._spinResult.stopItem);
        }
    }

    protected updateViewport(): void {
        this.viewport.updateTransform(this._pointer);
        this.viewport.overlaps = this._overlapChecker.check(this.viewport);
        this.viewport.offset = this.viewport.overlaps ? this._overlapChecker.offset : undefined;
    }

    protected initiateHeadTailItems(): void {
        this.updateViewport();

        const direction: MovementDirection = this.movementRule.direction;
        this._tailVisibleItem = this.movementRule.direction === MovementDirection.Negative ? this.itemList.first : this.itemList.last;
        this.viewport.processItem(this._tailVisibleItem);
        while (!this._tailVisibleItem.visible) {
            this._tailVisibleItem = this.itemList.getPreviousByDirection(this._tailVisibleItem, direction);
            this.viewport.processItem(this._tailVisibleItem);
        }

        this._headVisibleItem = this._tailVisibleItem;
        while (this._headVisibleItem.visible) {
            this._headVisibleItem = this.itemList.getPreviousByDirection(this._headVisibleItem, direction);
            this.viewport.processItem(this._headVisibleItem);
        }
        this._headVisibleItem = this.itemList.getNextByDirection(this._headVisibleItem, direction);

        if (this.viewport.overlaps) {
            while (this._tailVisibleItem.visible) {
                this._tailVisibleItem = this.itemList.getNextByDirection(this._tailVisibleItem, direction);
                this.viewport.processItem(this._tailVisibleItem);
            }
            this._tailVisibleItem = this.itemList.getPreviousByDirection(this._tailVisibleItem, direction);
        }
    }

    protected calculateHeadTailItems(): void {
        this.updateViewport();

        const direction: MovementDirection = this.movementRule.direction;
        this.viewport.processItem(this._headVisibleItem);
        while (!this._headVisibleItem.visible) {
            this._headVisibleItem = this.itemList.getNextByDirection(this._headVisibleItem, direction);
            this.viewport.processItem(this._headVisibleItem);
        }

        this._tailVisibleItem = this._headVisibleItem;
        while (this._tailVisibleItem.visible) {
            this._tailVisibleItem = this.itemList.getNextByDirection(this._tailVisibleItem, direction);
            this.viewport.processItem(this._tailVisibleItem);
        }
        this._tailVisibleItem = this.itemList.getPreviousByDirection(this._tailVisibleItem, direction);
    }

    protected get hasVisibleItems(): boolean {
        return (this._headVisibleItem !== undefined) && (this._tailVisibleItem !== undefined);
    }

    protected processCorrectors(): void {
        if (!this.hasVisibleItems) {
            this.initiateHeadTailItems();
        }
        this._correctors.forEach((corrector: ICorrector) => {
            if (TypeUtil.isReelSpinResultCorrector(corrector)) {
                TypeUtil.castTo<IReelSpinResultCorrector>(corrector)
                    .correct(this, this._spinResult);
            }
        });
        this._correctors.forEach((corrector: ICorrector) => {
            if (TypeUtil.isReelCorrector(corrector)) {
                TypeUtil.castTo<IReelCorrector>(corrector)
                    .correct(this);
            }
        });
    }

    protected onGeometryChange(): void {
        this._geometry =
        this._geometryCirculator.baseGeometry =
        this._overlapChecker.baseGeometry = this._placementRule.Apply(this.itemList);
    }

    protected tryToClearSpinResult(): boolean {
        if (!this._spinResultCleared && this._spinResult != undefined && !this._spinResult.visible) {
            this.clearSpinResult();

            return true;
        }

        return false;
    }

    protected clearSpinResult(): void {
        const index: number = this.itemList.indexOf(this._spinResult[0]);
        this.itemList.removeRange(index, this._spinResult.length);
        this.flushSpinResult();

        NotificationManager.instance.dispatch(this._spinResultClearedNotification);
    }

    protected onItemListChange(args: ObservableListChangeArgs): void {
        this.onGeometryChange();

        const headIndex: number = this.itemList.indexOf(this._headVisibleItem);
        const doRecalc: boolean = this.viewport.overlaps
            ? true
            : args.newIndex <= headIndex;

        switch (args.action) {
            case ObservableListChangeAction.Add:
            case ObservableListChangeAction.Insert:
                if (doRecalc) {
                    this.updatePointerOnListChange(args.newItems, 1);
                }
                break;

            case ObservableListChangeAction.Remove:
            case ObservableListChangeAction.RemoveAt:
                if (doRecalc) {
                    this.updatePointerOnListChange(args.oldItems, -1);
                }
                break;

            case ObservableListChangeAction.Replace:
                if (doRecalc) {
                    this.updatePointerOnListChange(args.oldItems, -1);
                    this.updatePointerOnListChange(args.newItems, 1);
                }
                break;

            default:
        }

        this.updateViewport();
        this.initiateHeadTailItems();
    }

    protected abstract updatePointerOnListChange(changeRange: Array<IReelItem>, direction: number): void;

    private flushSpinResult(): void {
        this._spinResult = undefined;
        this._spinResultCleared = true;
    }

    private subscribe(): void {
        NotificationManager.instance
        .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.StateMachineStart)
        .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.StateMachineStop);
    }

    private unsubscribe(): void {
        NotificationManager.instance
        .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.StateMachineStart)
        .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.StateMachineStop);
    }
}

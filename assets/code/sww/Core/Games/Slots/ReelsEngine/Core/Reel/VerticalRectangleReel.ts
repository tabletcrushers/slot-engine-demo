import MathUtil from '../../../../../../Utils/MathUtil';
import TypeUtil from '../../../../../../Utils/TypeUtil';
import Rectangle from '../../../../../Geometry/Rectangle';
import ReelSpinResultDTO from '../../../Configuration/DTO/SpinResult/ReelSpinResultDTO';
import ReelConfiguration from '../../Configuration/ReelConfiguration';
import { MovementDirection } from '../../Presentation/Movement/MovementDirection';
import RectangleReelItem from '../Items/RectangleReelItem';
import AbstractReel from './AbstractReel';

export default class VerticalRectangleReel extends AbstractReel {

    constructor(
        id: number,
        configuration: ReelConfiguration
    ) {
        super(id, configuration);
    }

    public getVisibleDump(overHeadNum: number, overTailNum: number): ReelSpinResultDTO {
        if (!this.hasVisibleItems) {
            this.initiateHeadTailItems();
        }

        const direction: MovementDirection = this.movementRule.direction;
        this._bufferItem = this._tailVisibleItem;
        for (let i = 0; i < overTailNum; i++) {
            this._bufferItem = this.itemList.getNextByDirection(this._bufferItem, direction);
        }
        this._bufferItem2 = this.itemList.getPreviousByDirection(this._headVisibleItem, direction);
        for (let i = 0; i < overHeadNum; i++) {
            this._bufferItem2 = this.itemList.getPreviousByDirection(this._bufferItem2, direction);
        }

        const dump: ReelSpinResultDTO = new ReelSpinResultDTO();
        let counter: number = 0;
        let tailItemsCount: number = 0;
        let headItemsCount: number = 0;
        let tailBoundPassed: boolean = false;
        let headBoundPassed: boolean = false;
        const tailBound = MathUtil.circulateInBounds(
            this._pointer.y + TypeUtil.castTo<Rectangle>(this.viewport.geometry).height,
            0,
            TypeUtil.castTo<Rectangle>(this.geometry).height);

        while (this._bufferItem !== this._bufferItem2) {
            dump.unshift(this._bufferItem.id);
            counter++;

            if (!tailBoundPassed && this._bufferItem.transform.y <= tailBound) {
                tailBoundPassed = true;
                tailItemsCount = counter;
            }
            if (this._bufferItem.transform.y <= this._pointer.y) {
                headBoundPassed = true;
            }
            if (headBoundPassed) {
                headItemsCount++;
            }

            this._bufferItem = this.itemList.getPreviousByDirection(this._bufferItem, direction);
        }

        headItemsCount--;
        dump.headItemsCount = headItemsCount;
        dump.tailItemsCount =
        dump.stopItemId = tailItemsCount;
        // TODO - implement more accurate percentage for tall items
        dump.stopItemPercentage = 0;

        return dump;
    }

    protected updatePointerOnListChange(changeRange: Array<RectangleReelItem>, direction: number): void {
        let shift: number = 0;
        changeRange.forEach((item) => {
            shift += item.geometry.height;
        });

        this._pointer.y += direction * shift;
        this._geometryCirculator.circulate(this._pointer);
    }
}

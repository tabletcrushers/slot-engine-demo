export default class FrameAtlasProvider {

    private static _instance: FrameAtlasProvider;

    private _atlases: Array<cc.SpriteAtlas>;

    public static get instance(): FrameAtlasProvider {
        if (FrameAtlasProvider._instance === undefined) {
           return new FrameAtlasProvider();
        } else {
            return FrameAtlasProvider._instance;
        }
    }

    constructor() {
        if (FrameAtlasProvider._instance === undefined) {
            FrameAtlasProvider._instance = this;
            this._atlases = new Array<cc.SpriteAtlas>();
        } else {
            throw(new Error('FrameAtlasProvider instnce already exists'));
        }

        return FrameAtlasProvider._instance;
    }

    public registerAtlas(atlas: cc.SpriteAtlas): void {
        this._atlases.push(atlas);
    }

    public getFrame(name: string): cc.SpriteFrame {
        for (const atlas of this._atlases) {
            const frame = atlas.getSpriteFrame(name);

            if (frame !== undefined) {
                return frame;
            }
        }

        return null;
    }

}

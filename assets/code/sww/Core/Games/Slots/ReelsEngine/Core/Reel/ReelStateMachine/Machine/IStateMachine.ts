import IUpdatable from '../../../../../../../Foundation/Update/IUpdatable';
import IReel from '../../IReel';
import IReelConsumer from '../../IReelConsumer';
import ITransition from '../Transition/ITransition';
import IState from './IState';

export default interface IStateMachine extends IUpdatable, IReelConsumer {
    currentState: IState;
    fire(event: string);
    canFire(event: string): boolean;
}

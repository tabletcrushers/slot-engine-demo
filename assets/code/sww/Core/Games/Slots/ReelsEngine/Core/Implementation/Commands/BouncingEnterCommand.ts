import SimpleResetCommand from './SimpleResetCommand';

export default class BouncingEnterCommand extends SimpleResetCommand {

    public execute(): void {
        super.execute();
        this._reel.movementRule.setup(this._reel.getSpinResult().stopItem.transform);
    }
}

import IState from '../Machine/IState';
import IPredicate from '../Predicate/IPredicate';
import ITransition from './ITransition';

export default class Transition implements ITransition {

    public readonly stateFrom: string;
    public readonly stateTo: string;
    public readonly event: string;
    public readonly predicates: IPredicate[];

    constructor(stateFrom: string, stateTo: string, event: string, predicates: IPredicate[]) {
        this.stateFrom = stateFrom;
        this.stateTo = stateTo;
        this.event = event;
        this.predicates = predicates;
    }
}

import IReel from '../../Reel/IReel';
import IReelConsumer from '../../Reel/IReelConsumer';
import TimePredicate from './TimePredicate';

export default class ResultTimePredicate extends TimePredicate implements IReelConsumer {

    protected _reel: IReel;

    constructor(lifeTime: number) {
        super(lifeTime);
    }

    public reset(): void {
        super.reset();
    }

    public evaluate(): boolean {
        return super.evaluate()
            && this._reel.isSpinResultCleared
            && this._reel.getSpinResult() !== undefined;
    }

    public registerReel(reel: IReel) {
        this._reel = reel;
    }
}

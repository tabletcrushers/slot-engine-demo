import IStateDTO from '../../../../Configuration/DTO/IStateDTO';
import IMovementRule from '../../../Presentation/Movement/IMovementRule';
import NoMovementRule from '../../../Presentation/Movement/NoMovementRule';
import IStateCommand from '../../Reel/ReelStateMachine/Command/IStateCommand';
import IState from '../../Reel/ReelStateMachine/Machine/IState';
import State from '../../Reel/ReelStateMachine/Machine/State';
import CommandBuilder from './CommandBuilder';
import ITypeBuilder from './ITypeBuilder';
import MovementRuleBuilder from './MovementRuleBuilder';

export default class StateBuilder implements ITypeBuilder {

    private readonly _movementRuleBuilder: MovementRuleBuilder;
    private readonly _commandBuilder: CommandBuilder;

    constructor(movementRuleBuilder: MovementRuleBuilder, commandBuilder: CommandBuilder) {
        this._movementRuleBuilder = movementRuleBuilder;
        this._commandBuilder = commandBuilder;
    }

    public build(dto: IStateDTO): IState {
        let movementRule: IMovementRule;
        let onEnterCommand: IStateCommand;
        let onExitCommand: IStateCommand;

        if (dto.parameters.hasOwnProperty('commands')) {
            const comandsConfig = dto.parameters.commands;

            if (comandsConfig.hasOwnProperty('onEnter')) {
                onEnterCommand = this._commandBuilder.build(comandsConfig.onEnter);
            }
            if (comandsConfig.hasOwnProperty('onExit')) {
                onExitCommand = this._commandBuilder.build(comandsConfig.onExit);
            }
        }

        if (dto.parameters.hasOwnProperty('movementRule')) {
            const movementRuleConfig = dto.parameters.movementRule;
            movementRule = this._movementRuleBuilder.build(movementRuleConfig);
        } else {
            movementRule = new NoMovementRule();
        }

        return new State(dto.parameters.name, movementRule, onEnterCommand, onExitCommand);
    }
}

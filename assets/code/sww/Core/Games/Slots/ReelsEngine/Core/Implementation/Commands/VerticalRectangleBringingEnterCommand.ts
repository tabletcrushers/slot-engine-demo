import Rectangle from '../../../../../../Geometry/Rectangle';
import AbstractBringingEnterCommand from './AbstractBringingEnterCommand';

export default class VerticalRectangleBringingEnterCommand extends AbstractBringingEnterCommand {

    public execute() {
        this._reel.movementRule.reset();
        this.setupMovementRule((this._reel.geometry as Rectangle).height);
    }
}

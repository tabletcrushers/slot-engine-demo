export default interface IPredicate {
    reset(): void;
    evaluate(): boolean;
}

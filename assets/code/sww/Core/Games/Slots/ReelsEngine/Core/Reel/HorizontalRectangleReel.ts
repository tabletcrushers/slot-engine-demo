import ReelSpinResultDTO from '../../../Configuration/DTO/SpinResult/ReelSpinResultDTO';
import ReelConfiguration from '../../Configuration/ReelConfiguration';
import RectangleReelItem from '../Items/RectangleReelItem';
import AbstractReel from './AbstractReel';

export default class HorizontalRectangleReel extends AbstractReel {

    constructor(
        id: number,
        configuration: ReelConfiguration
    ) {
        super(id, configuration);
    }

    public getVisibleDump(overHeadNum: number, overTailNum: number): ReelSpinResultDTO {
        throw new Error('Not implemented yet!');
    }

    protected updatePointerOnListChange(changeRange: Array<RectangleReelItem>, direction: number): void {
        let shift: number = 0;

        changeRange.forEach((item) => {
            shift += item.geometry.width;
        });

        this._pointer.x += direction * shift;
        this._geometryCirculator.circulate(this._pointer);
    }
}

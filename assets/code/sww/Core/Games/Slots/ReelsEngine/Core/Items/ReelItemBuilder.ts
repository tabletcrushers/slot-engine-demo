import Rectangle from '../../../../../Geometry/Rectangle';
import IGeometryDTO from '../../../Configuration/DTO/IGeometryDTO';
import IReelItemDTO from '../../../Configuration/DTO/IReelItemDTO';
import IReelItemsConfigDTO from '../../../Configuration/DTO/IReelItemsConfigDTO';
import GameFieldSpinResultDTO from '../../../Configuration/DTO/SpinResult/GameFieldSpinResultDTO';
import ReelSpinResultDTO from '../../../Configuration/DTO/SpinResult/ReelSpinResultDTO';
import SpinResultDTO from '../../../Configuration/DTO/SpinResult/SpinResultDTO';
import GameFieldSpinResult from '../../Result/GameFieldSpinResult';
import ReelSpinResult from '../../Result/ReelSpinResult';
import SpinResult from '../../Result/SpinResult';
import GeometryBuilder from '../Implementation/Builders/GeometryBuilder';
import ReelItemList from '../ItemLists/ReelItemList';
import IReelItem from './IReelItem';
import IReelItemBuilder from './IReelItemBuilder';
import RectangleReelItem from './RectangleReelItem';

export default class ReelItemBuilder implements IReelItemBuilder {

    private _itemsMap: Map<number, ItemGeometries>;
    private readonly _geometryBuilder: GeometryBuilder;

    constructor(configuration: IReelItemsConfigDTO) {
        this._itemsMap = new Map<number, ItemGeometries>();
        this._geometryBuilder = new GeometryBuilder();

        const geometries = new Map<string, IGeometryDTO>();

        configuration.geometries.forEach((geometryDto: IGeometryDTO) => {
            geometries.set(geometryDto.name, geometryDto);
        });

        // TODO - separated method for this
        configuration.items.forEach((itemDto: IReelItemDTO) => {
            const basicGeometryAlias: string = itemDto.basicGeometry.alias;
            const basicGeometry: IGeometryDTO = basicGeometryAlias === undefined
                ? itemDto.basicGeometry
                : geometries.get(basicGeometryAlias);

            const frameGeometryAlias: string = itemDto.frameGeometry.alias;
            const frameGeometry: IGeometryDTO = frameGeometryAlias === undefined
                ? itemDto.frameGeometry
                : geometries.get(frameGeometryAlias);

            this._itemsMap.set(itemDto.id, new ItemGeometries(basicGeometry, frameGeometry));
        });
    }

    public build(id: number): IReelItem {
        const geometries: ItemGeometries = this._itemsMap.get(id);
        if (geometries === undefined) {
            throw new Error(`ReelItem [${id}] wasn't registered yet!`);
        }

        switch (geometries.basicGeometry.type) {
            case 'Rectangle':
                return new RectangleReelItem(
                    id,
                    this._geometryBuilder.build(geometries.basicGeometry) as Rectangle,
                    this._geometryBuilder.build(geometries.frameGeometry) as Rectangle);

            default:
                throw new Error(`${geometries.basicGeometry.type} geometry wasn't registered yet!`);
        }
    }

    public buildReelItemsList(ids: number[]): ReelItemList {
        const list: ReelItemList = new ReelItemList();
        ids.forEach((id: number) => {
            list.push(this.build(id));
        });

        return list;
    }

    public buildSpinResult(spinResultDTO: SpinResultDTO): SpinResult {
        const spinResult: SpinResult = new SpinResult();
        spinResultDTO.forEach((gameFieldSpinResultDTO: GameFieldSpinResultDTO, gameFieldKey: number) => {
            const gameFieldSpinResult: GameFieldSpinResult = new GameFieldSpinResult();
            gameFieldSpinResultDTO.forEach((reelSpinResultDTO: ReelSpinResultDTO, reelKey: number) => {
                const reelSpinResult: ReelSpinResult = new ReelSpinResult();
                reelSpinResultDTO.forEach((id: number) => {
                    reelSpinResult.push(this.build(id));
                });

                reelSpinResult.stopItem = reelSpinResult[reelSpinResultDTO.stopItemId];
                reelSpinResult.stopItemPercentage = reelSpinResultDTO.stopItemPercentage;
                reelSpinResult.setZoneSizes(reelSpinResultDTO.headItemsCount, reelSpinResultDTO.tailItemsCount);

                gameFieldSpinResult.set(reelKey, reelSpinResult);
            });

            spinResult.set(gameFieldKey, gameFieldSpinResult);
        });

        return spinResult;
    }

    public dispose(): void {
        this._itemsMap.clear();
    }
}

class ItemGeometries {

    public readonly basicGeometry: IGeometryDTO;
    public readonly frameGeometry: IGeometryDTO;

    constructor(basicGeometry: IGeometryDTO, frameGeometry: IGeometryDTO) {
        this.basicGeometry = basicGeometry;
        this.frameGeometry = frameGeometry;
    }
}

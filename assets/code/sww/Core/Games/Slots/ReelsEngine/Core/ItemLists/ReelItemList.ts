import MathUtil from '../../../../../../Utils/MathUtil';
import ObservableList from '../../../../../Implementation/Lists/ObservableList';
import { MovementDirection } from '../../Presentation/Movement/MovementDirection';
import IReelItem from '../Items/IReelItem';

export default class ReelItemList extends ObservableList<IReelItem> {

    private readonly _bypassTable: Map<MovementDirection, number> = new Map<MovementDirection, number>([
        [ MovementDirection.Negative, -1 ],
        [ MovementDirection.None, 1 ],
        [ MovementDirection.Positive, 1 ]
    ]);

    constructor() {
        super();
        Object.setPrototypeOf(this, Object.create(ReelItemList.prototype));
    }

    public getNext(item: IReelItem): IReelItem {
        const index = this.indexOf(item);
        const nextIndex = MathUtil.circulate(index + 1, this.length);

        return this[nextIndex];
    }

    public getPrevious(item: IReelItem): IReelItem {
        const index = this.indexOf(item);
        const previousIndex = MathUtil.circulate(index - 1, this.length);

        return this[previousIndex];
    }

    public getNextByDirection(item: IReelItem, direction: MovementDirection): IReelItem {
        const index = this.indexOf(item);
        const previousIndex = MathUtil.circulate(index + this._bypassTable.get(direction), this.length);

        return this[previousIndex];
    }

    public getPreviousByDirection(item: IReelItem, direction: MovementDirection): IReelItem {
        const index = this.indexOf(item);
        const previousIndex = MathUtil.circulate(index - this._bypassTable.get(direction), this.length);

        return this[previousIndex];
    }

    public get first(): IReelItem {
        return this[0];
    }

    public get last(): IReelItem {
        return this[this.length - 1];
    }
}

import IDisposable from '../../../../../Foundation/Dispose/IDisposable';
import IIdentifiable from '../../../../../Foundation/Identify/IIdentifiable';
import ILifecycle from '../../../../../Foundation/Lifecycle/ILifecycle';
import ISuspendable from '../../../../../Foundation/Suspend/ISuspendable';
import IUpdatable from '../../../../../Foundation/Update/IUpdatable';
import IGeometry from '../../../../../Geometry/IGeometry';
import IObserver from '../../../../../Notifications/IObserver';
import ReelSpinResultDTO from '../../../Configuration/DTO/SpinResult/ReelSpinResultDTO';
import IMovementRule from '../../Presentation/Movement/IMovementRule';
import ITransform from '../../Presentation/Transform/ITransform';
import IViewport from '../../Presentation/View/IViewport';
import IReelSpinResult from '../../Result/IReelSpinResult';
import IResultable from '../../Result/IResultable';
import { ReelSpinResultZoneType } from '../../Result/ReelSpinResultZoneType';
import ReelItemList from '../ItemLists/ReelItemList';
import IReelItem from '../Items/IReelItem';
import IReelItemBuilderConsumer from '../Items/IReelItemBuilderConsumer';
import IVisibleItemsReportable from '../Report/IVisibleItemsReportable';

export default interface IReel extends IUpdatable, IIdentifiable, ILifecycle, IObserver, ISuspendable,
    IDisposable, IResultable, IVisibleItemsReportable, IReelItemBuilderConsumer {
        itemList: ReelItemList;
        geometry: IGeometry;
        headVisibleItem: IReelItem;
        tailVisibleItem: IReelItem;
        pointer: ITransform;
        viewport: IViewport;
        movementRule: IMovementRule;
        setSpinResult(result: IReelSpinResult, immediately: boolean): void;
        getSpinResult(): IReelSpinResult;
        getVisibleDump(overHeadNum: number, overTailNum: number): ReelSpinResultDTO;
        applySpinResult(): void;
        applyCorrectors(): void;
        extendCorrectors(itemsToAddNum: number, zone: ReelSpinResultZoneType): void;
        getNestedItem(nestId: number): IReelItem;
        randomizePointer(): void;
        arrangePointerToItem(item: IReelItem): void;
        replaceItem(oldItem: IReelItem, newItem: IReelItem): void;
}

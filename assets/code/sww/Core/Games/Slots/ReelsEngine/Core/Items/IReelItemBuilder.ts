import IDisposable from '../../../../../Foundation/Dispose/IDisposable';
import SpinResultDTO from '../../../Configuration/DTO/SpinResult/SpinResultDTO';
import SpinResult from '../../Result/SpinResult';
import ReelItemList from '../ItemLists/ReelItemList';
import IReelItem from './IReelItem';

export default interface IReelItemBuilder extends IDisposable {
    build(id: number): IReelItem;
    buildReelItemsList(ids: Array<number>): ReelItemList;
    buildSpinResult(spinResultDTO: SpinResultDTO): SpinResult;
}

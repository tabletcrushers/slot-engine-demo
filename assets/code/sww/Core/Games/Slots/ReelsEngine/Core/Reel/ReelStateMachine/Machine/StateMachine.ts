import TypeUtil from '../../../../../../../../Utils/TypeUtil';
import Notification from '../../../../../../../Notifications/Notification';
import NotificationManager from '../../../../../../../Notifications/NotificationManager';
import { ReelEngineNotification, ReelEngineNotificationTopic } from '../../../../../Notifications/SlotsGameNotifications';
import IReel from '../../IReel';
import IReelConsumer from '../../IReelConsumer';
import ITransition from '../Transition/ITransition';
import IState from './IState';
import IStateMachine from './IStateMachine';

export default class StateMachine implements IStateMachine {

    private readonly _lifecycleStateName: string;
    private _currentState: IState;
    private _states: Map<IState, Array<ITransition>>;
    private _reel: IReel;
    private _startNotification: Notification;
    private _stopNotification: Notification;
    private _notificationsInited: boolean;

    constructor(states: Array<IState>, transitions: Array<ITransition>, initialStateName: string, lifecycleStateName: string) {
        this._lifecycleStateName = lifecycleStateName;
        this._states = new Map<IState, Array<ITransition>>();
        states.forEach((state) => {
            const stateTransitions: Array<ITransition> = transitions.filter((transition) => state.name === transition.stateFrom);
            this._states.set(state, stateTransitions);
        });
        this._notificationsInited = false;
        this.setStateByName(initialStateName);
    }

    public get currentState(): IState {

        return this._currentState;
    }

    public canFire(event: string): boolean {

        return this.tryFire(event);
    }

    public fire(event: string): boolean {

        return this.tryFire(event, true);
    }

    public registerReel(reel: IReel): void {
        this._states.forEach((transitions, state) => {
            if (state.onEnterCommand !== undefined && TypeUtil.isReelConsumer(state.onEnterCommand)) {
                TypeUtil.castTo<IReelConsumer>(state.onEnterCommand).registerReel(reel);
            }
            if (state.onExitCommand !== undefined && TypeUtil.isReelConsumer(state.onExitCommand)) {
                TypeUtil.castTo<IReelConsumer>(state.onExitCommand).registerReel(reel);
            }

            transitions.forEach((transition) => {
                transition.predicates.forEach((predicate) => {
                    if (TypeUtil.isReelConsumer(predicate)) {
                        TypeUtil.castTo<IReelConsumer>(predicate).registerReel(reel);
                    }
                });
            });
        });

        this._reel = reel;
        this._startNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.StateMachineStart, { id: reel.id });
        this._stopNotification =
            new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.StateMachineStop, { id: reel.id });
        this._notificationsInited = true;
    }

    public update(delta: number): void {
        const transitions = this._states.get(this._currentState);
        transitions.forEach((transition) => {
            if (transition.predicates.length > 0) {
                if (transition.predicates.every((predicate) => predicate.evaluate())) {
                    this.fire(transition.event);

                    return;
                }
            }
        });
    }

    private tryFire(event: string, setState: boolean = false): boolean {
        this._states.forEach((transitions, state) => {
            transitions.forEach((transition) => {
                if ((transition.event === event) && (transition.stateFrom === this._currentState.name)) {
                    if (setState) {
                        this.setStateByName(transition.stateTo);
                    }

                    return true;
                }
            });
        });

        return false;
    }

    private setStateByName(stateName: string): void {
        this._states.forEach((transitions, state) => {
            if (state.name === stateName) {
                this.switchState(state);

                return;
            }
        });
    }

    private switchState(state: IState): void {
        if (this._currentState !== undefined) {
            if (this._notificationsInited && this._currentState.name === this._lifecycleStateName) {
                NotificationManager.instance.dispatch(this._startNotification);
            }
            this._currentState.onExit();
        }

        const transitions = this._states.get(state);
        transitions.forEach((transition) => {
            transition.predicates.forEach((predicate) => {
                predicate.reset();
            });
        });

        this._currentState = state;
        if (this._notificationsInited && this._currentState.name === this._lifecycleStateName) {
            NotificationManager.instance.dispatch(this._stopNotification);
        }
        this._currentState.onEnter();
    }
}

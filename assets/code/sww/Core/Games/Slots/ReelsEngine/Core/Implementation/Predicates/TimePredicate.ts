import IPredicate from '../../Reel/ReelStateMachine/Predicate/IPredicate';

export default class TimePredicate implements IPredicate {

    protected readonly _lifeTime: number;
    protected _startTime: number;

    constructor(lifeTime: number) {
        this._lifeTime = lifeTime;
        this._startTime = 0;
    }

    public reset(): void {
        this._startTime = 0;
    }

    public evaluate(): boolean {
        if (this._startTime === 0) {
            this._startTime = Date.now();
        }
        // tslint:disable-next-line:no-magic-numbers
        const elapsed: boolean = (Date.now() - this._startTime) / 1000 >= this._lifeTime;

        return elapsed;
    }
}

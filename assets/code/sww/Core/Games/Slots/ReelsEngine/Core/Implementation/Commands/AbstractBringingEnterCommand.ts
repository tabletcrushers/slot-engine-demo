import { MovementDirection } from '../../../Presentation/Movement/MovementDirection';
import SimpleResetCommand from './SimpleResetCommand';

export default abstract class AbstractBringingEnterCommand extends SimpleResetCommand {

    public abstract execute(): void;

    protected setupMovementRule(limitValue: number): void {
        const propertyToUpdate: string = this._reel.movementRule.propertyToUpdate;
        let boundaryEvaluated = true;
        let targetValue: number = this._reel.getSpinResult().stopItem.transform[propertyToUpdate];

        if (this._reel.movementRule.direction !== MovementDirection.Negative) {
            if (targetValue === 0) {
                targetValue = limitValue;
            }
            boundaryEvaluated = targetValue > this._reel.pointer[propertyToUpdate];
        }

        this._reel.movementRule.setup(targetValue, boundaryEvaluated);
    }
}

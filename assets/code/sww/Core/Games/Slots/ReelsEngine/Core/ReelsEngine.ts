import SlotGameDomain from '../../../../../Games/Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../../../Games/Host/SlotGameRepositoryEnum';
import ILine from '../../../../../Games/Host/WinLineRepository/interfaces/ILine';
import ILines from '../../../../../Games/Host/WinLineRepository/interfaces/ILines';
import TypeUtil from '../../../../../Utils/TypeUtil';
import DomainManager from '../../../../Foundation/Domain/DomainManager';
import { LifecycleState } from '../../../../Foundation/Lifecycle/LifecycleState';
import INotification from '../../../../Notifications/INotification';
import Notification from '../../../../Notifications/Notification';
import NotificationManager from '../../../../Notifications/NotificationManager';
import IReelItemsConfigDTO from '../../Configuration/DTO/IReelItemsConfigDTO';
import SpinResultDTO from '../../Configuration/DTO/SpinResult/SpinResultDTO';
import { ReelEngineNotification, ReelEngineNotificationTopic } from '../../Notifications/SlotsGameNotifications';
import IReelsEngineConfiguration from '../Configuration/IReelsEngineConfiguration';
import SpinResult from '../Result/SpinResult';
import IGameField from './GameField/IGameField';
import IReelsEngine from './IReelsEngine';
import ReelItemList from './ItemLists/ReelItemList';
import IReelItem from './Items/IReelItem';
import IReelItemBuilder from './Items/IReelItemBuilder';
import ReelItemBuilder from './Items/ReelItemBuilder';
import IReel from './Reel/IReel';
import IVisibleItemsReport from './Report/IVisibleItemsReport';
import VisibleItemsReport from './Report/VisibleItemsReport';

export default class ReelsEngine implements IReelsEngine {

    private _state: LifecycleState;
    private _visibleItemsReport: VisibleItemsReport;
    private _gameFields: Map<number, IGameField>;
    private _isActive: boolean;
    private _reelItemBuilder: IReelItemBuilder;
    private readonly _startNotification: Notification;
    private readonly _stopNotification: Notification;
    private readonly _resultClearedNotification: Notification;

    constructor() {

        this._gameFields = new Map<number, IGameField>();
        this._startNotification = new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStart);
        this._stopNotification = new Notification(ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStop);
        this._resultClearedNotification = new Notification(
            ReelEngineNotificationTopic.ReelsEngine,
            ReelEngineNotification.ReelsEngineSpinResultCleared);
    }

    public setConfiguration(configuration: IReelsEngineConfiguration): void {
        this.unsubscribe();
        this._gameFields.clear();
        this._visibleItemsReport = new VisibleItemsReport();

        const gameFields: Map<number, IGameField> = configuration.getGameFields();
        gameFields.forEach((gameField: IGameField) => {
            this.addGameField(gameField);
        });
        this.subscribe();
    }

    public setItemsConfiguration(itemsConfiguration: IReelItemsConfigDTO): void {
        this._reelItemBuilder = new ReelItemBuilder(itemsConfiguration);
        this._gameFields.forEach((gameField: IGameField) => {
            gameField.setReelItemBuilder(this._reelItemBuilder);
        });
    }

    public setItems(items: Map<number, Map<number, Array<number>>>): void {
        items.forEach((gameField: Map<number, Array<number>>, gameFieldKey: number) => {
            const reelItemLists: Map<number, ReelItemList> = new Map<number, ReelItemList>();
            gameField.forEach((reel: Array<number>, reelKey: number) => {
                reelItemLists.set(reelKey, this._reelItemBuilder.buildReelItemsList(reel));
            });
            this._gameFields.get(gameFieldKey).setItems(reelItemLists);
        });
    }

    public getGameField(id: number): IGameField {
        return this._gameFields.get(id);
    }

    public start(): void {
        if (this._isActive) {
            this._gameFields.forEach((gameField: IGameField) => {
                gameField.start();
            });
        }
    }

    public stop(): void {
        if (this._isActive) {
            this._gameFields.forEach((gameField: IGameField) => {
                gameField.stop();
            });
        }
    }

    public update(delta: number): void {
        if (this._isActive) {
            this._gameFields.forEach((gameField: IGameField) => {
                gameField.update(delta);
            });
        }
    }

    /* TODO:NOTE
     * think how to solve such situations
     * 1 - we have 2 GFs, 1 is suspended
     * 2 - we suspend RE so both GSs become suspended
     * 3 - we resume RE, so both GSs become resumed
     * 4 - BUT 1 of them should be suspended. So Feature shuld store suspendable map of RE before
     */
    public resume(): void {
        this._isActive = true;
        this._gameFields.forEach((gameField: IGameField) => {
            gameField.resume();
        });
        this.subscribe();
    }

    public suspend(): void {
        this._isActive = false;
        this._gameFields.forEach((gameField: IGameField) => {
            gameField.suspend();
        });
        this.unsubscribe();
    }

    public dispose(): void {
        this.unsubscribe();
        this._gameFields.forEach((gameField: IGameField) => {
            gameField.dispose();
        });
        this._gameFields.clear();

        if (this._reelItemBuilder !== undefined) {
            this._reelItemBuilder.dispose();
        }
        if (this._visibleItemsReport !== undefined) {
            this._visibleItemsReport.dispose();
        }
    }

    public get state(): LifecycleState {
        return this._state;
    }

    public get isActive(): boolean {
        return this._isActive;
    }

    public setSpinResult(result: SpinResultDTO, immediately: boolean): void {
        const spinResult: SpinResult = this._reelItemBuilder.buildSpinResult(result);
        this._gameFields.forEach((gameField: IGameField) => {
            gameField.setSpinResult(spinResult.get(gameField.id), immediately);
        });
    }

    public getVisibleDump(overHeadNum: number, overTailNum: number): SpinResultDTO {
        const dump: SpinResultDTO = new SpinResultDTO();
        this._gameFields.forEach((gameField: IGameField) => {
            dump.set(gameField.id, gameField.getVisibleDump(overHeadNum, overTailNum));
        });

        return dump;
    }

    public getVisibleItems(): IVisibleItemsReport {
        this._visibleItemsReport.clear();
        this._gameFields.forEach((gameField: IGameField) => {
            const report: Array<IReelItem> = this._visibleItemsReport.getGameFieldReport(gameField.id);
            gameField.reportVisibleItems(report);
        });

        return this._visibleItemsReport;
    }

    public observe(notification: INotification): void {
        let areAllGamefieldsQualified;

        switch (notification.id) {
            case ReelEngineNotification.GameFieldStart:
                areAllGamefieldsQualified = this.checkGameFieldsState(LifecycleState.Started);
                if (areAllGamefieldsQualified) {
                    this._state = LifecycleState.Started;
                    NotificationManager.instance.dispatch(this._startNotification);
                }

                return;

            case ReelEngineNotification.GameFieldStop:
                areAllGamefieldsQualified = this.checkGameFieldsState(LifecycleState.Stopped);
                if (areAllGamefieldsQualified) {
                    this._state = LifecycleState.Stopped;
                    NotificationManager.instance.dispatch(this._stopNotification);
                }

                return;

            case ReelEngineNotification.GameFieldSpinResultCleared:
                let areAllFieldsCleared = true;
                this._gameFields.forEach((gameField: IGameField) => {
                    areAllFieldsCleared = areAllFieldsCleared && gameField.isSpinResultCleared;
                });

                if (areAllFieldsCleared) {
                    NotificationManager.instance.dispatch(this._resultClearedNotification);
                }

                return;

            default:
                return;
        }
    }

    public getNestedItem(gameFieldId: number, reelId: number, nestId: number): IReelItem {
        return this._gameFields.get(gameFieldId).getReel(reelId).getNestedItem(nestId);
    }

    public replaceItem(gameFieldId: number, reelId: number, nestId: number, newItemId: number): void {
        const oldItem: IReelItem = this.getNestedItem(gameFieldId, reelId, nestId);
        const newItem: IReelItem = this._reelItemBuilder.build(newItemId);
        this.getGameField(gameFieldId).getReel(reelId).replaceItem(oldItem, newItem);
    }

    public randomize(counter: number = 0): void {

        // tslint:disable-next-line:no-magic-numbers
        if (counter > 700) {

            return;
        }

        counter++;

        this._gameFields.forEach((gameField: IGameField) => {

            gameField.randomize();

            if (this.isWinInLines(gameField)) {

                this.randomize(counter);
            }
        });
    }

    private reelVisibleItemsReport(reel: IReel): Array<IReelItem> {

        let report: Array<IReelItem> = new Array();
        reel.reportVisibleItems(report);
        report = report.reverse();

        return report;
    }

    private getWinLineSymbolId(gameField: IGameField, line: ILine, reelNumber: number): number {

        return this.reelVisibleItemsReport(gameField.getReel(reelNumber))[line.nodes[reelNumber]].id;
    }

    private isWinInLine(gameField: IGameField, line: ILine, fromLeftToRight: boolean): boolean {

        const wildId: number = this.getDomain().getPaytableSymbolByName('Wild').id;
        const scatterId: number = this.getDomain().getPaytableSymbolByName('Scatter').id;
        const minimumItemsToWin: number = this.getDomain().getPaytableSymbolByName('Scatter').symbolsPay[0].count;
        let startIndex: number = 0;
        let sampleId: number = this.getWinLineSymbolId(gameField, line, startIndex);

        if (!fromLeftToRight) {

            startIndex = line.nodes.length - minimumItemsToWin;
            sampleId = this.getWinLineSymbolId(gameField, line, startIndex);
        }

        if (sampleId === wildId || sampleId === scatterId) {

            return true;
        }

        for (let ind: number = startIndex + 1; ind < startIndex + minimumItemsToWin; ind++) {

            const currentId: number = this.getWinLineSymbolId(gameField, line, ind);

            if (currentId === scatterId) {

                return true;

            } else if (currentId !== sampleId && currentId !== wildId) {

                return false;
            }
        }

        return true;
    }

    private isWinInLinefromBothSides(gameField: IGameField, line: ILine): boolean {

        return this.isWinInLine(gameField, line, true) || this.isWinInLine(gameField, line, false);
    }

    private isWinInLines(gameField: IGameField): boolean {

        for (const line of this.getWinLinesData()) {

            if (this.isWinInLinefromBothSides(gameField, line)) {

                return true;
            }
        }

        return false;
    }

    private getDomain(): SlotGameDomain {

        return TypeUtil.castTo<SlotGameDomain>(DomainManager.instance.getDomain(SlotGameDomain.NAME));
    }

    private getWinLinesData(): Array<ILine> {

        return this.getDomain().getDALProvider().getRepository(SlotGameRepositoryEnum.LinesRepository).
        read<ILines>(SlotGameRepositoryDataEnum.Lines).lines;
    }

    private addGameField(gameField: IGameField): void {
        this._gameFields.set(gameField.id, gameField);
        this._visibleItemsReport.registerGameField(gameField.id);
    }

    private checkGameFieldsState(state: LifecycleState): boolean {
        let areAllFieldsInState = true;
        this._gameFields.forEach((gameField: IGameField) => {
            areAllFieldsInState = areAllFieldsInState && gameField.state === state;
        });

        return areAllFieldsInState;
    }

    // TODO - what about reel events?
    private subscribe(): void {
        NotificationManager.instance
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldStart)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldStop)
            .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldSpinResultCleared);
    }

    private unsubscribe(): void {
        NotificationManager.instance
        .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldStart)
        .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldStop)
        .removeObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.GameFieldSpinResultCleared);
    }
}

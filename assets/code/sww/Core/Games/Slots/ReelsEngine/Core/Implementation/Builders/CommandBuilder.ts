import ITypeDTO from '../../../../Configuration/DTO/ITypeDTO';
import IStateCommand from '../../Reel/ReelStateMachine/Command/IStateCommand';
import BouncingEnterCommand from '../Commands/BouncingEnterCommand';
import HorizontalRectangleBringingEnterCommand from '../Commands/HorizontalRectangleBringingEnterCommand';
import SegmentBringingEnterCommand from '../Commands/SegmentBringingEnterCommand';
import SimpleResetCommand from '../Commands/SimpleResetCommand';
import SpinningExitCommand from '../Commands/SpinningExitCommand';
import VerticalRectangleBringingEnterCommand from '../Commands/VerticalRectangleBringingEnterCommand';
import ITypeBuilder from './ITypeBuilder';

export default class CommandBuilder implements ITypeBuilder {

    public build(dto: ITypeDTO): IStateCommand {
        switch (dto.type) {
            case 'SimpleResetCommand':
                return new SimpleResetCommand();

            case 'SpinningExitCommand':
                return new SpinningExitCommand();

            case 'SegmentBringingEnterCommand':
                return new SegmentBringingEnterCommand();

            case 'VerticalRectangleBringingEnterCommand':
                return new VerticalRectangleBringingEnterCommand();

            case 'HorizontalRectangleBringingEnterCommand':
                return new HorizontalRectangleBringingEnterCommand();

            case 'BouncingEnterCommand':
                return new BouncingEnterCommand();

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

import AbstractReelConsumerCommand from '../../Reel/ReelStateMachine/Command/AbstractReelConsumerCommand';

export default class SpinningExitCommand extends AbstractReelConsumerCommand {

    public execute(): void {
        this._reel.applyCorrectors();
    }
}

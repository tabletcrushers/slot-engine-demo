export default interface IStateCommand {
    execute(): void;
}

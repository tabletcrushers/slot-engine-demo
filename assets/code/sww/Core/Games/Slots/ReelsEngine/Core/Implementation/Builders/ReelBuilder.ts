import IReelConfigDTO from '../../../../Configuration/DTO/IReelConfigDTO';
import ReelConfiguration from '../../../Configuration/ReelConfiguration';
import ICorrector from '../../../Presentation/Correction/ICorrector';
import HorizontalRectangleReel from '../../Reel/HorizontalRectangleReel';
import HorizontalSegmentReel from '../../Reel/HorizontalSegmentReel';
import IReel from '../../Reel/IReel';
import { ReelDynamicsType } from '../../Reel/ReelDynamicsType';
import IStateMachine from '../../Reel/ReelStateMachine/Machine/IStateMachine';
import VerticalRectangleReel from '../../Reel/VerticalRectangleReel';
import VerticalSegmentReel from '../../Reel/VerticalSegmentReel';
import GeometryCirculatorBuilder from './GeometryCirculatorBuilder';
import ITypeBuilder from './ITypeBuilder';
import OverlapCheckerBuilder from './OverlapCheckerBuilder';
import PlacementRuleBuilder from './PlacementRuleBuilder';
import StateMachineBuilder from './StateMachineBuilder';
import ViewportBuilder from './ViewportBuilder';

export default class ReelBuilder implements ITypeBuilder {

    private readonly _stateMachineBuilder: StateMachineBuilder;
    private readonly _viewportBuilder: ViewportBuilder;
    private readonly _geometryCircultorBuilder: GeometryCirculatorBuilder;
    private readonly _overlapCheckerBuilder: OverlapCheckerBuilder;
    private readonly _placementRuleBuilder: PlacementRuleBuilder;

    constructor(
        stateMachineBuilder,
        viewportBuilder,
        geometryCircultorBuilder,
        overlapCheckerBuilder,
        placementRuleBuilder
    ) {
        this._stateMachineBuilder = stateMachineBuilder;
        this._viewportBuilder = viewportBuilder;
        this._geometryCircultorBuilder = geometryCircultorBuilder;
        this._overlapCheckerBuilder = overlapCheckerBuilder;
        this._placementRuleBuilder = placementRuleBuilder;
    }

    public build(dto: IReelConfigDTO): IReel {
        const parameters: any = dto.configuration.parameters;
        const stateMachine: IStateMachine = this._stateMachineBuilder.build(dto.stateMachineConfiguration);
        const configuration: ReelConfiguration = new ReelConfiguration(
            this.matchDynamicsType(parameters.dynamicsType),
            stateMachine,
            this._viewportBuilder.build(parameters.viewport),
            this._geometryCircultorBuilder.build(parameters.geometryCirculator),
            this._overlapCheckerBuilder.build(parameters.overlapChecker),
            this._placementRuleBuilder.build(parameters.placementRule),
            // TODO:NOTE - temp empty. Finalize config first
            new Array<ICorrector>(),
            dto.startEvent,
            dto.stopEvent
        );

        switch (dto.configuration.type) {
            case 'VerticalSegmentReel':
                return new VerticalSegmentReel(
                    dto.id,
                    configuration
                );

            case 'HorizontalSegmentReel':
                return new HorizontalSegmentReel(
                    dto.id,
                    configuration
                );

            case 'VerticalRectangleReel':
                return new VerticalRectangleReel(
                    dto.id,
                    configuration
                );

            case 'HorizontalRectangleReel':
                return new HorizontalRectangleReel(
                    dto.id,
                    configuration
                );

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }

    // TODO - investigate auto-convertion
    private matchDynamicsType(dynamicsType: string): ReelDynamicsType {
        switch (dynamicsType) {
            case 'Dynamic':
                return ReelDynamicsType.Dynamic;

            case 'Static':
                return ReelDynamicsType.Static;

            default :
                throw new Error(`${dynamicsType} ReelDynamicsType doesn't exist!`);
        }
    }
}

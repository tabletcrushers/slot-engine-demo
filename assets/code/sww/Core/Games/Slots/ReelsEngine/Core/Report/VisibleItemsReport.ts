import IReelItem from '../Items/IReelItem';
import IVisibleItemsReport from './IVisibleItemsReport';

// TODO extend MapExtended
export default class VisibleItemsReport implements IVisibleItemsReport {

    private _gameFields: Map<number, Array<IReelItem>>;

    constructor() {
        this._gameFields = new Map<number, Array<IReelItem>>();
    }

    public registerGameField(id: number): void {
        if (!this._gameFields.has(id)) {
            this._gameFields.set(id, new Array<IReelItem>());
        }
    }

    public getGameFieldReport(id: number): Array<IReelItem> {
        return this._gameFields.get(id);
    }

    public clear(): void {
        this._gameFields.forEach((items, gameField) => {
            items.length = 0;
        });
    }

    public dispose(): void {
        this._gameFields.clear();
    }
}

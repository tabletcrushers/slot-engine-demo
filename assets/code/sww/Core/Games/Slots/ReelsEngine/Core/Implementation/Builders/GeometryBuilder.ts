import HorizontalSegment from '../../../../../../Geometry/HorizontalSegment';
import IGeometry from '../../../../../../Geometry/IGeometry';
import Point from '../../../../../../Geometry/Point';
import Rectangle from '../../../../../../Geometry/Rectangle';
import VerticalSegment from '../../../../../../Geometry/VerticalSegment';
import IGeometryDTO from '../../../../Configuration/DTO/IGeometryDTO';
import ITypeBuilder from './ITypeBuilder';

export default class GeometryBuilder implements ITypeBuilder {

    public build(dto: IGeometryDTO): IGeometry {
        switch (dto.type) {
            case 'Point' :
                return new Point(dto.parameters.x, dto.parameters.y);

            case 'Rectangle':
                return new Rectangle(dto.parameters.x, dto.parameters.y, dto.parameters.width, dto.parameters.height);

            case 'VerticalSegment' :
                return new VerticalSegment(
                    new Point(dto.parameters.beginPoint.x, dto.parameters.beginPoint.y),
                    new Point(dto.parameters.endPoint.x, dto.parameters.endPoint.y));

            case 'HorizontalSegment':
                return new HorizontalSegment(
                    new Point(dto.parameters.beginPoint.x, dto.parameters.beginPoint.y),
                    new Point(dto.parameters.endPoint.x, dto.parameters.endPoint.y));

            default :
                throw new Error(`${dto.type} wasn't registered yet!`);
        }
    }
}

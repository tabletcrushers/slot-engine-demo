import { ReelDynamicsType } from '../Core/Reel/ReelDynamicsType';
import IStateMachine from '../Core/Reel/ReelStateMachine/Machine/IStateMachine';
import IGeometryCirculator from '../Presentation/Circulation/IGeometryCirculator';
import ICorrector from '../Presentation/Correction/ICorrector';
import IPlacementRule from '../Presentation/Placement/IPlacementRule';
import IViewport from '../Presentation/View/IViewport';
import IOverlapChecker from '../Presentation/View/Overlap/IOverlapChecker';

export default class ReelConfiguration {

    private _dynamicsType: ReelDynamicsType;
    private _stateMachine: IStateMachine;
    private _viewport: IViewport;
    private _geometryCirculator: IGeometryCirculator;
    private _overlapChecker: IOverlapChecker;
    private _placementRule: IPlacementRule;
    private _correctors: Array<ICorrector>;
    private _startEvent: string;
    private _stopEvent: string;

    constructor(
        dynamicsType: ReelDynamicsType,
        stateMachine: IStateMachine,
        viewport: IViewport,
        geometryCirculator: IGeometryCirculator,
        overlapChecker: IOverlapChecker,
        placementRule: IPlacementRule,
        correctors: Array<ICorrector>,
        startEvent: string,
        stopEvent: string
    ) {
        this._dynamicsType = dynamicsType;
        this._stateMachine = stateMachine;
        this._viewport = viewport;
        this._geometryCirculator = geometryCirculator;
        this._overlapChecker = overlapChecker;
        this._placementRule = placementRule;
        this._correctors = correctors;
        this._startEvent = startEvent;
        this._stopEvent = stopEvent;
    }

    public get dynamicsType(): ReelDynamicsType {
        return this._dynamicsType;
    }

    public get stateMachine(): IStateMachine {
        return this._stateMachine;
    }

    public get viewport(): IViewport {
        return this._viewport;
    }

    public get geometryCirculator(): IGeometryCirculator {
        return this._geometryCirculator;
    }

    public get overlapChecker(): IOverlapChecker {
        return this._overlapChecker;
    }

    public get placementRule(): IPlacementRule {
        return this._placementRule;
    }

    public get correctors(): Array<ICorrector> {
        return this._correctors;
    }

    public get startEvent(): string {
        return this._startEvent;
    }

    public get stopEvent(): string {
        return this._stopEvent;
    }
}

import IGameField from '../Core/GameField/IGameField';
import IReelsEngineConfiguration from './IReelsEngineConfiguration';

export default class ReelsEngineConfiguration implements IReelsEngineConfiguration {

    private readonly _gameFields: Map<number, IGameField>;

    constructor(gameFields: Map<number, IGameField>) {
        this._gameFields = gameFields;
    }

    public getGameFields(): Map<number, IGameField> {
        return this._gameFields;
    }
}

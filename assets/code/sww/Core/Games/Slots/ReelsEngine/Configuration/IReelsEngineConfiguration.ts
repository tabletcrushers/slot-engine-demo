import IGameField from '../Core/GameField/IGameField';

export default interface IReelsEngineConfiguration {
    getGameFields(): Map<number, IGameField>;
}

import MapExtended from '../../../../Implementation/Enumerables/MapExtended';
import IReelSpinResult from './IReelSpinResult';

export default class GameFieldSpinResult extends MapExtended<number, IReelSpinResult> {

    constructor() {
        super();
    }
}

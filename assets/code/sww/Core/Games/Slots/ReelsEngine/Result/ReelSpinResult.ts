import ObservableList from '../../../../Implementation/Lists/ObservableList';
import IReelItem from '../Core/Items/IReelItem';
import IReelSpinResult from './IReelSpinResult';
import { ReelSpinResultZoneType } from './ReelSpinResultZoneType';

export default class ReelSpinResult extends ObservableList<IReelItem> implements IReelSpinResult {

    public stopItemPercentage: number;
    public stopItemForced: boolean;
    private _stopItem: IReelItem;
    private _headItemsCount: number;
    private _bodyItemsCount: number;
    private _tailItemsCount: number;

    constructor(list?: Array<IReelItem>) {
        super(list);

        Object.setPrototypeOf(this, Object.create(ReelSpinResult.prototype));

        this.stopItemPercentage = 0;
        this.stopItemForced = false;
        this._headItemsCount =
        this._bodyItemsCount =
        this._tailItemsCount = 0;
    }

    public get stopItem(): IReelItem {
        return this._stopItem === undefined ? this[this._headItemsCount] : this._stopItem;
    }
    public set stopItem(value: IReelItem) {
        this._stopItem = value;
    }

    public get visible(): boolean {
        return this.some((item) => item.visible);
    }

    public addItemToZone(item: IReelItem, zone: ReelSpinResultZoneType): void {
        switch (zone) {
            case ReelSpinResultZoneType.Body :
                this.insert(this._tailItemsCount + this._bodyItemsCount, item);
                this._bodyItemsCount++;

                return;

            case ReelSpinResultZoneType.Head :
                this.push(item);
                this._headItemsCount++;

                return;

            case ReelSpinResultZoneType.Tail :
                this.insert(0, item);
                this._tailItemsCount++;
                if (!this.stopItemForced) {
                    this._stopItem = this[this._tailItemsCount];
                }

                return;

            default:
                return;
        }
    }

    public getItemsCountInZone(zone: ReelSpinResultZoneType): number {
        switch (zone) {
            case ReelSpinResultZoneType.Body :
                return this._bodyItemsCount;

            case ReelSpinResultZoneType.Head :
                return this._headItemsCount;

            case ReelSpinResultZoneType.Tail :
                return this._tailItemsCount;

            default:
                return 0;
        }
    }

    public setZoneSizes(headSize: number, tailSize: number): void {
        this._headItemsCount = headSize;
        this._tailItemsCount = tailSize;
        this._bodyItemsCount = this.length - headSize - tailSize;
        this._stopItem = this[this._tailItemsCount];
        this.stopItemForced = true;
    }
}

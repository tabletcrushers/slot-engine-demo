import IReelItem from '../Core/Items/IReelItem';
import { ReelSpinResultZoneType } from './ReelSpinResultZoneType';

export default interface IReelSpinResult {

    stopItem: IReelItem;
    stopItemPercentage: number;
    stopItemForced: boolean;
    visible: boolean;

    addItemToZone(item: IReelItem, zone: ReelSpinResultZoneType): void;
    getItemsCountInZone(zone: ReelSpinResultZoneType): number;
    setZoneSizes(headSize: number, tailSize: number): void;
}

import MapExtended from '../../../../Implementation/Enumerables/MapExtended';
import GameFieldSpinResult from './GameFieldSpinResult';

export default class SpinResult extends MapExtended<number, GameFieldSpinResult> {

    constructor() {
        super();
    }
}

import AggregateNode from '../../../../../../../../showcases/Network/AggregateNode';
import Color from '../../../../../../Supply/Custom/Color';
import IRootCarrier from '../../../../../../Supply/Interfaces/IRootCarrier';

export default class AnimationFrameShowing implements IRootCarrier {

    private _rootNode: cc.Node;
    private _prefab: cc.Prefab;

    constructor(prefab: cc.Prefab) {
        this._prefab = prefab;
    }

    public setRoot(root: AggregateNode): void {
        this._rootNode = root;
    }

    public showWinLine(winLine: Array<number>, points: Array<any>, color: Color): void {
        winLine.forEach((element, index) => {
            if (element > -1) {
                const frame = cc.instantiate(this._prefab);

                frame.color = new cc.Color(color.red, color.green, color.blue, color.alpha);
                frame.x = points[index].x;
                frame.y = points[index].y;

                this._rootNode.addChild(frame);
                frame.getComponent(cc.Animation).play();
            }
        });
    }

    public clear(): void {
        this._rootNode.removeAllChildren();
    }
}

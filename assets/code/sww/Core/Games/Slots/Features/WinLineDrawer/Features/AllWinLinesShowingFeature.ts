// TODO need add logic for show all frames to Particle
import ISlotWinLine from '../../../../../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import Notification from '../../../../../Notifications/Notification';
import NotificationManager from '../../../../../Notifications/NotificationManager';
import { SlotGameNotificationTopic, WinLineNotification } from '../../../Notifications/SlotsGameNotifications';
import AnimationFrameShowing from '../Utils/AnimationFrameShowing';
import LineDrawerFeature from './WinLinesFeature';

// tslint:disable:no-magic-numbers
export default class AllWinLinesShowingFeature extends LineDrawerFeature {

    public static NAME: string = 'AllWinLinesShowerFeature';

    protected _particleFrameDrawer: AnimationFrameShowing;

    constructor(particleFrameDrawer: AnimationFrameShowing) {
        super();
        this._particleFrameDrawer = particleFrameDrawer;
    }

    public start(): void {
        if (this.spinResultWinLines.length > 0) {
            this._isActive = true;
            this._updateCount = 100;
            this._winLineIterator = 0;
        }

        this.spinResultWinLines.forEach((element, index) => {
            this.showParticleFrames(this.spinResultWinLines, this.linesRepository.linesView, index);
        });
    }

    protected showParticleFrames(spinResultWinLines: Array<ISlotWinLine>, linesView, index: number): void {
        const lineId = spinResultWinLines[index].winLineId;

        this._particleFrameDrawer.showWinLine(spinResultWinLines[this._winLineIterator].winSymbolsLine,
                                              linesView[lineId].anchorPoints,
                                              this._lines[lineId].color);
    }

    public update(dt): void {

        if (this._isActive) {
            this._updateCount--;
            if (this._updateCount === 0) {
                NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                           WinLineNotification.AllWinLineShowed,
                                           this._winLineIterator));
            }
        }
    }

    public stop(): void {
        this._particleFrameDrawer.clear();
        this._isActive = false;
        clearTimeout(this._winTimeout);
    }
}

// tslint:disable:no-magic-numbers
import WinLineSegment from '../../../../../../../showcases/LinesConfigurator/WinLineSegment';
import AggregateNode from '../../../../../../../showcases/Network/AggregateNode';
import SlotGameDomain from '../../../../../Games/Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../../../Games/Host/SlotGameRepositoryEnum';
import IDrawLine from '../../../../../Games/Host/WinLineRepository/interfaces/IDrawLine';
import ILine from '../../../../../Games/Host/WinLineRepository/interfaces/ILine';
import ILines from '../../../../../Games/Host/WinLineRepository/interfaces/ILines';
import ISlotWinLine from '../../../../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import Color from '../../../../../Supply/Custom/Color';
import IRootCarrier from '../../../../../Supply/Interfaces/IRootCarrier';
import TypeUtil from '../../../../../Utils/TypeUtil';
import LinePointsCalculator from '../../../../../Utils/WinLines/LinePointsCalculator';
import DomainManager from '../../../../Foundation/Domain/DomainManager';
import IDomain from '../../../../Foundation/Domain/IDomain';
import IFeature from '../../../../Foundation/Feature/IFeature';
import Point from '../../../../Geometry/Point';
import WinLinesDrawer from './Utils/WinLinesDrawer';

export default class LineDrawerFeature implements IFeature, IRootCarrier {

    public static NAME: string = 'LineDrawerFeature';

    public _costil: Array<Array<any>>;
    public _isActive: boolean;
    public _costilWindth: number = 256;
    public _costilHeight: number = 220;

    private _domain: IDomain;

    private _winLineIterator: number;
    private _winLineDrawer: WinLinesDrawer;
    private _drawMode: boolean;

    private _winTimeout: number;

    private _lines: Array<ILine>;

    // temp dessition
    private _tempShadow: Color;
    private _blinkCount: number;

    constructor() {
        this._winLineDrawer = new WinLinesDrawer();

        this._domain = DomainManager.instance.getDomain(SlotGameDomain.NAME);

        this._costil = [
            [{ x: 328, y: 330 }, { x: 328, y: 550 }, { x: 328, y: 770 }],
            [{ x: 584, y: 330 }, { x: 584, y: 550 }, { x: 584, y: 770 }],
            [{ x: 840, y: 330 }, { x: 840, y: 550 }, { x: 840, y: 770 }],
            [{ x: 1096, y: 330 }, { x: 1096, y: 550 }, { x: 1096, y: 770 }],
            [{ x: 1352, y: 330 }, { x: 1352, y: 550 }, { x: 1352, y: 770 }],
        ];

        this._tempShadow = new Color();
        this._tempShadow.red = 0;
        this._tempShadow.green = 0;
        this._tempShadow.blue = 0;
        this._tempShadow.alpha = 140;

        this._blinkCount = 0;
    }

    public setRoot(root: AggregateNode): void {
        this._winLineDrawer.setRoot(root);
    }

    public prepareFeature(drawMode: boolean = false): void {
        const calculator = new LinePointsCalculator();
        this._drawMode = drawMode;

        const lines = this.lines.lines;
        this._lines = this.lines.lines;

        this.lines.linesView = new Array<IDrawLine>();

        lines.forEach((element) => {
            const linesView = TypeUtil.provideInterfaceInstance<IDrawLine>();
            const segments: Array<WinLineSegment> = calculator.calculeLineSegments(element.nodes,
                                                                                   this._costil,
                                                                                   this._costilWindth,
                                                                                   this._costilHeight);

            const centerRecatnglePoints: Array<Point> = calculator.calculateWinPointsPosition(element.nodes,
                                                                                              this._costil,
                                                                                              this._costilWindth,
                                                                                              this._costilHeight);

            linesView.segments = segments;
            linesView.anchorPoints = centerRecatnglePoints;

            this.lines.linesView.push(linesView);
        });
    }

    public start(): void {
    }

    public stop(): void {
        this._winLineDrawer.clear();
        this._isActive = false;
        clearTimeout(this._winTimeout);
    }

    public goToNextLine(spinResultWinLines: Array<ISlotWinLine>): void {
        if (this._winLineIterator === spinResultWinLines.length - 1) {
            this._winLineIterator = 0;
        } else {
            this._winLineIterator++;
        }
    }

    public get lines(): ILines {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.LinesRepository)
            .read<ILines>(SlotGameRepositoryDataEnum.Lines);
    }

    public get spinResultWinLines(): Array<ISlotWinLine> {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<Array<ISlotWinLine>>(SlotGameRepositoryDataEnum.SpinResultWinLines);
    }
}

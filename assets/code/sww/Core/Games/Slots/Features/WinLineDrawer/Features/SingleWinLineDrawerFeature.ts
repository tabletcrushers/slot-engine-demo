import ISlotWinLine from '../../../../../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import Notification from '../../../../../Notifications/Notification';
import NotificationManager from '../../../../../Notifications/NotificationManager';
import { SlotGameNotificationTopic, WinLineNotification } from '../../../Notifications/SlotsGameNotifications';
import WinLinesDrawer from '../Utils/WinLinesDrawer';
import LineDrawerFeature from './WinLinesFeature';

// tslint:disable:no-magic-numbers

export default class SingleWinLineDrawerFeature extends LineDrawerFeature {

    public static NAME: string = 'SingleWinLineDrawerFeature';

    private _winLineDrawer: WinLinesDrawer;

    constructor(winLineDrawer: WinLinesDrawer) {
        super();
        this._winLineDrawer = winLineDrawer;
        this._winLineIterator = 0;
    }

    public start(): void {
        this._isActive = true;
        this._updateCount = 60;
        this._winLineDrawer.clear();

        this.darwLineSegments(this.spinResultWinLines, this.linesRepository.linesView);
    }

    protected darwLineSegments(spinResultWinLines: Array < ISlotWinLine > ,
                               linesView): void {

        const lineId = spinResultWinLines[this._winLineIterator].winLineId;

        this._winLineDrawer.drawLineWithRect(
        linesView[lineId].segments,
        spinResultWinLines[this._winLineIterator].winSymbolsLine,
        this._tempShadow,
        20);

        this._winLineDrawer.drawLineWithRect(
        linesView[lineId].segments,
        spinResultWinLines[this._winLineIterator].winSymbolsLine,
        this._lines[lineId].color,
        10);

        this._winLineDrawer.drawWinLineRects(
        spinResultWinLines[this._winLineIterator].winSymbolsLine,
        linesView[lineId].anchorPoints,
        this._tempShadow, 20);

        this._winLineDrawer.drawWinLineRects(
        spinResultWinLines[this._winLineIterator].winSymbolsLine,
        linesView[lineId].anchorPoints,
        this._lines[lineId].color, 10);

        this.goToNextLine(spinResultWinLines);
    }

    public update(dt): void {
        if (this._isActive) {
            this._updateCount--;
            if (this._updateCount === 0) {
                NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                           WinLineNotification.WinLineShowed,
                                           this._winLineIterator));
            }
        }
    }

    public stop(): void {
        this._winLineDrawer.clear();
        this._isActive = false;
        this._winLineIterator = 0;
        clearTimeout(this._winTimeout);
    }
}

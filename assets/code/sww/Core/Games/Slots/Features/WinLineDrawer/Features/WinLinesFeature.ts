// tslint:disable:no-magic-numbers
import WinLineSegment from '../../../../../../../../showcases/LinesConfigurator/WinLineSegment';
import SlotGameDomain from '../../../../../../Games/Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../../../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../../../../Games/Host/SlotGameRepositoryEnum';
import IDrawLine from '../../../../../../Games/Host/WinLineRepository/interfaces/IDrawLine';
import ILine from '../../../../../../Games/Host/WinLineRepository/interfaces/ILine';
import ILines from '../../../../../../Games/Host/WinLineRepository/interfaces/ILines';
import ISlotWinLine from '../../../../../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import Color from '../../../../../../Supply/Custom/Color';
import TypeUtil from '../../../../../../Utils/TypeUtil';
import LinePointsCalculator from '../../../../../../Utils/WinLines/LinePointsCalculator';
import DomainManager from '../../../../../Foundation/Domain/DomainManager';
import IDomain from '../../../../../Foundation/Domain/IDomain';
import IFeature from '../../../../../Foundation/Feature/IFeature';
import Point from '../../../../../Geometry/Point';

export default abstract class LineDrawerFeature implements IFeature {

    public static NAME: string = 'LineDrawerFeature';

    public _costil: Array<Array<any>>;
    public _isActive: boolean;
    public _costilWindth: number = 256;
    public _costilHeight: number = 220;

    protected _domain: IDomain;

    protected _winLineIterator: number;

    protected _winTimeout: number;
    protected _updateCount: number;

    protected _lines: Array<ILine>;
    // temp dessition
    protected _tempShadow: Color;
    protected _blinkCount: number;

    constructor() {
        this._domain = DomainManager.instance.getDomain(SlotGameDomain.NAME);

        this._costil = [
            [{x: 328, y: 330}, {x: 328, y: 550}, {x: 328, y: 770}],
            [{x: 584, y: 330}, {x: 584, y: 550}, {x: 584, y: 770}],
            [{x: 840, y: 330}, {x: 840, y: 550}, {x: 840, y: 770}],
            [{x: 1096, y: 330}, {x: 1096, y: 550}, {x: 1096, y: 770}],
            [{x: 1352, y: 330}, {x: 1352, y: 550}, {x: 1352, y: 770}],
        ];

        this._tempShadow = new Color();
        this._tempShadow.red = 0;
        this._tempShadow.green = 0;
        this._tempShadow.blue = 0;
        this._tempShadow.alpha = 140;

        this._blinkCount = 0;
    }

    public prepareFeature(): void {

        const calculator = new LinePointsCalculator();

        this._lines = this.linesRepository.lines;
        this.linesRepository.linesView = new Array<IDrawLine>();

        this._lines.forEach((element) => {
            const linesView = TypeUtil.provideInterfaceInstance<IDrawLine>();
            const segments: Array<WinLineSegment> = calculator.calculeLineSegments(element.nodes,
                                                                                   this._costil,
                                                                                   this._costilWindth,
                                                                                   this._costilHeight);

            const centerRecatanglePoints: Array<Point> = calculator.calculateWinPointsPosition(element.nodes,
                                                                                               this._costil,
                                                                                               this._costilWindth,
                                                                                               this._costilHeight);

            linesView.segments = segments;
            linesView.anchorPoints = centerRecatanglePoints;

            this.linesRepository.linesView.push(linesView);
        });
    }

    public start(): void {
    }

    public stop(): void {
    }

    protected goToNextLine(spinResultWinLines: Array<ISlotWinLine>): void {
        if (this._winLineIterator === spinResultWinLines.length - 1) {
            this._winLineIterator = 0;
        } else {
            this._winLineIterator++;
        }
    }

    protected get linesRepository(): ILines {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.LinesRepository)
            .read<ILines>(SlotGameRepositoryDataEnum.Lines);
    }

    protected get spinResultWinLines(): Array<ISlotWinLine> {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<Array<ISlotWinLine>>(SlotGameRepositoryDataEnum.SpinResultWinLines);
    }
}

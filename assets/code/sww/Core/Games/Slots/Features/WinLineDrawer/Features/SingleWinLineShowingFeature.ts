import ISlotWinLine from '../../../../../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import Notification from '../../../../../Notifications/Notification';
import NotificationManager from '../../../../../Notifications/NotificationManager';
import { SlotGameNotificationTopic, WinLineNotification } from '../../../Notifications/SlotsGameNotifications';
import AnimationFrameShowing from '../Utils/AnimationFrameShowing';
import LineDrawerFeature from './WinLinesFeature';

// tslint:disable:no-magic-numbers

export default class SingleWinLineShowingFeature extends LineDrawerFeature {

    public static NAME: string = 'SingleWinLineShowerFeature';

    protected _particleFrameDrawer: AnimationFrameShowing;

    constructor(particleFrameDrawer: AnimationFrameShowing) {
        super();
        this._particleFrameDrawer = particleFrameDrawer;
        this._winLineIterator = 0;
    }

    public start(): void {

        this._isActive = true;
        this._updateCount = 60;
        this._particleFrameDrawer.clear();

        this.showParticleFrames(this.spinResultWinLines, this.linesRepository.linesView);
    }

    protected showParticleFrames(spinResultWinLines: Array<ISlotWinLine>, linesView): void {
        cc.log(this._winLineIterator);
        const lineId = spinResultWinLines[this._winLineIterator].winLineId;

        this._particleFrameDrawer.showWinLine(spinResultWinLines[this._winLineIterator].winSymbolsLine,
                                              linesView[lineId].anchorPoints,
                                              this._lines[lineId].color);

        this.goToNextLine(spinResultWinLines);
    }

    public update(dt): void {

        if (this._isActive) {
            this._updateCount--;
            if (this._updateCount === 0) {
                NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                           WinLineNotification.WinLineShowed,
                                           this._winLineIterator));
            }
        }
    }

    public stop(): void {
        this._particleFrameDrawer.clear();
        this._winLineIterator = 0;
        this._isActive = false;
        clearTimeout(this._winTimeout);
    }
}

import WinLineSegment from '../../../../../../../../showcases/LinesConfigurator/WinLineSegment';
import { WinLineSegmentType } from '../../../../../../../../showcases/LinesConfigurator/WinLineSegmentType';
import AggregateNode from '../../../../../../../../showcases/Network/AggregateNode';
import Color from '../../../../../../Supply/Custom/Color';
import IRootCarrier from '../../../../../../Supply/Interfaces/IRootCarrier';
import TypeUtil from '../../../../../../Utils/TypeUtil';
import Point from '../../../../../Geometry/Point';

// tslint:disable:no-magic-numbers

// tslint:disable:no-magic-numbers
export default class WinLinesDrawer implements IRootCarrier {

    public _renderNode: AggregateNode;
    public _graphic: cc.Graphics;

    public setRoot(root: AggregateNode): void {
        // Что это? Что мы сюда передаем тогда вообще?
        this._renderNode = TypeUtil.castTo<AggregateNode>(cc.find('Canvas/LineRenderNode'));

        this._graphic = this._renderNode.addComponent(cc.Graphics);
    }

    public drawWinLine(segments: Array<any>, color: Color, lineWidth: number = 5): void {
        segments.forEach((segment) => {
            this.drawSegment(segment.point1, segment.point2);
        });
        this.setGraphicsSetting(color, lineWidth);
        this._graphic.stroke();
    }

    public drawTileBySegments(segments: Array<any>, spriteFrame: cc.Texture2D, scale: number = 1): void {
        segments.forEach((segment, index) => {

            const node = new cc.Node(`Node${index}`);
            node.setAnchorPoint(1, 0.5);
            this._renderNode.addChild(node);
            node.parent = this._renderNode;

            const sprite = node.addComponent(cc.Sprite);
            sprite.type = cc.Sprite.Type.TILED;
            sprite.spriteFrame = new cc.SpriteFrame(spriteFrame);

            const angle = this.angleBetweenPoints(segment.point1, segment.point2);
            const width = this.bisectrix(segment.point1, segment.point2);

            node.x = segment.point1.x;
            node.y = segment.point1.y;
            node.rotation = - Math.round(this.radiansToDegrees(angle));
            node.width = width;

        });
    }

    public drawWinLineRects(winLine: Array<number>,
                            points: Array<any>,
                            color: Color,
                            lineWidth: number = 5): void {

        winLine.forEach((element, index) => {

            if (element > -1) {
                this._graphic.rect(
                    (points[index].x - 110),
                    (points[index].y - 100),
                    220,
                    200);
            }
        });

        this.setGraphicsSetting(color, lineWidth);
        this._graphic.stroke();
    }

    public drawLineWithRect(segments: Array<WinLineSegment>,
                            winLine: Array<number>,
                            color: Color,
                            lineWidth: number = 5): void {

        segments.forEach((segment) => {
            if (segment.type === WinLineSegmentType.Inside) {
                if (segment.relateIndex !== undefined && winLine[segment.relateIndex] === -1) {
                    this.drawSegment(segment.point1, segment.point2);
                }
            } else {
                this.drawSegment(segment.point1, segment.point2);
            }
        });

        this.setGraphicsSetting(color, lineWidth);
        this._graphic.stroke();
    }

    public drawSegment(point1: Point, point2: Point): void {
        this._graphic.moveTo(point1.x, point1.y);
        this._graphic.lineTo(point2.x, point2.y);
    }

    public clear(): void {
        this._graphic.clear();
        this._renderNode.removeAllChildren();
    }

    public angleBetweenPoints(p1: Point, p2: Point): number {

        return Math.atan2(p1.y - p2.y, p1.x - p2.x);
    }

    public bisectrix(p1: Point, p2: Point): number {

        return Math.sqrt(Math.pow(p2.y - p1.y, 2) + Math.pow(p2.x - p1.x, 2));
    }

    public radiansToDegrees(radians: number): number {
        // tslint:disable-next-line:no-magic-numbers
        return radians / Math.PI * 180;
    }

    public tempEnable(value: boolean): void {
        this._graphic.enabled = value;
    }

    private setGraphicsSetting(color: Color, lineWidth: number): void {
        this._graphic.lineCap = cc.Graphics.LineCap.ROUND;
        this._graphic.lineWidth = lineWidth;
        this._graphic.strokeColor = cc.color(color.red, color.green, color.blue, color.alpha);
        this._graphic.stroke();
    }
}

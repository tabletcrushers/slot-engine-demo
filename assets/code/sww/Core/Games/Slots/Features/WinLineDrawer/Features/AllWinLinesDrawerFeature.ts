// tslint:disable:no-magic-numbers
import Notification from '../../../../../Notifications/Notification';
import NotificationManager from '../../../../../Notifications/NotificationManager';
import { SlotGameNotificationTopic, WinLineNotification } from '../../../Notifications/SlotsGameNotifications';
import WinLinesDrawer from '../Utils/WinLinesDrawer';
import LineDrawerFeature from './WinLinesFeature';

export default class AllWinLinesDrawerFeature extends LineDrawerFeature {

    public static NAME: string = 'AllWinLinesDrawerFeature';

    private _winLineDrawer: WinLinesDrawer;

    constructor(winLineDrawer: WinLinesDrawer) {
        super();
        this._winLineDrawer = winLineDrawer;
    }

    public start(): void {
        if (this.spinResultWinLines.length > 0) {
            this._isActive = true;
            this._updateCount = 100;
            this._winLineIterator = 0;
        }

        this.spinResultWinLines.forEach((winLine) => {
            this._winLineDrawer
                .drawWinLine(this.linesRepository.linesView[winLine.winLineId].segments, this._tempShadow, 20);
            this._winLineDrawer
                .drawWinLine(this.linesRepository.linesView[winLine.winLineId].segments, this._lines[winLine.winLineId].color, 10);
        });
    }

    public update(dt): void {

        if (this._isActive) {
            this._updateCount--;
            if (this._updateCount === 0) {
                this.stop();
                NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain,
                                           WinLineNotification.AllWinLineShowed,
                                           this._winLineIterator));
            }
        }
    }

    public stop(): void {
        this._winLineDrawer.clear();
        this._isActive = false;
        clearTimeout(this._winTimeout);
    }
}

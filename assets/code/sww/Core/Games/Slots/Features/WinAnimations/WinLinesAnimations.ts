import { SlotGameRepositoryDataEnum } from '../../../../../Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../../../Games/Host/SlotGameRepositoryEnum';
import ISlotWinLine from '../../../../../Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import ITriggerSymbols, { ISymbolPosition } from '../../../../../Games/Host/WinLineRepository/interfaces/ITriggerSymbols';
import FlamingPearlDomain from '../../../../../Games/Slots/FlamingPearl/Domain/FlamingPearlDomain';
import RenderEngine from '../../../../../Supply/RenderEngine/RenderEngine';
import DomainManager from '../../../../Foundation/Domain/DomainManager';
import IFeature from '../../../../Foundation/Feature/IFeature';
import Notification from '../../../../Notifications/Notification';
import NotificationManager from '../../../../Notifications/NotificationManager';
import { SlotGameNotificationTopic, WinLineNotification } from '../../Notifications/SlotsGameNotifications';
import IReelsEngine from '../../ReelsEngine/Core/IReelsEngine';
import IWinLine from './IWinLine';

export default class WinLinesAnimation implements IFeature {

    public static NAME: string = 'WinLinesAnimation';

    private _reelsEngine: IReelsEngine;
    private _renderEngine: RenderEngine;
    private _currentLine: number;
    private _winTimer;
    private _winLines: Array<IWinLine> = [];
    private _noWinSymbols: Array<IWinLine> = [];
    private _fadeMode: boolean = true;

    constructor(reelEngine: IReelsEngine, renderEngine: RenderEngine) {
        this._reelsEngine = reelEngine;
        this._renderEngine = renderEngine;
        this._renderEngine.fadeMode = this._fadeMode;
    }

    public start(): void {
        const domain = DomainManager.instance.getDomain(FlamingPearlDomain.NAME) as FlamingPearlDomain;

        const spinResultWinLines = domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<Array<ISlotWinLine>>(SlotGameRepositoryDataEnum.SpinResultWinLines);

        const winLines = []; // new Array<ISlotWinLine>();
        const noWinSymbols = [];

        this._winLines = [];
        this._noWinSymbols = [];
        this._currentLine = 0;

        spinResultWinLines.forEach((line) => {
            winLines.push(line);
            noWinSymbols.push(domain.getLine(line.winLineId));
        });

        for (let i = 0; i < winLines.length; i++) {
            const lineItems = [];
            const noWinItems = [];

            for (let j = 0; j < winLines[i].winSymbolsLine.length; j++) {
                if (winLines[i].winSymbolsLine[j] !== -1) {
                    // TODO: remove RE dependency and replace 0 to GameFieldId
                    lineItems.push(this._reelsEngine.getNestedItem(0, j, winLines[i].winSymbolsLine[j]));
                } else if (this._fadeMode) {
                    noWinItems.push(this._reelsEngine.getNestedItem(0, j, noWinSymbols[i].nodes[j]));
                }
            }

            this._winLines.push({ lineItems });

            if (this._fadeMode) {
                this._noWinSymbols.push({ lineItems: noWinItems });
            }
        }

        if (this._winLines.length > 0) {
            if (this._fadeMode) {
                this._renderEngine.showSolidBackground();
            }

            this.playAllWinSymbolsLine();
        }
    }

    // TODO: temp need rewrite
    public startScatter(): void {
        const domain = DomainManager.instance.getDomain(FlamingPearlDomain.NAME) as FlamingPearlDomain;
        const spinResultWinLines: Array<ITriggerSymbols> = domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<Array<ITriggerSymbols>>(SlotGameRepositoryDataEnum.TriggerSymbols);

        const symbols = [];

        this._winLines = [];

        spinResultWinLines.forEach((line) => {
            symbols.push(line.winSymbolsLine);
        });

        symbols.forEach((symb) => {
            const lineItems = [];
            symb.forEach((element) => {
                lineItems.push(this._reelsEngine.getNestedItem(0, element.reel, element.indexOnReel));
            });

            this._winLines.push({ lineItems });
        });

        if (this._winLines.length > 0) {
            this.playAllWinSymbolsLine();

            setTimeout(
                () => {
                    NotificationManager.instance.dispatch(
                        new Notification(SlotGameNotificationTopic.Mode, WinLineNotification.TriggerSymbolsWinLineShowed));
                },
                // tslint:disable-next-line:no-magic-numbers
                1500);
        }
    }

    public playAllWinSymbolsLine(): void {
        this._renderEngine.stopWinLineAnimations();

        for (let i = 0; i < this._winLines.length; i++) {
            this._renderEngine.renderWinLineAnimations(this._winLines[i], this._noWinSymbols[i]);
        }
    }

    public playWinSymbolsLine(lineId: number): void {
        this._renderEngine.stopWinLineAnimations();
        this._renderEngine.renderWinLineAnimations(this._winLines[lineId], this._noWinSymbols[lineId]);
    }

    protected playWinLineCycle(): void {
        if (this._currentLine === this._winLines.length) {
            this._currentLine = 0;
        }

        this._renderEngine.renderWinLineAnimations(this._winLines[this._currentLine], this._noWinSymbols[this._currentLine]);
        this._winTimer = setTimeout(() => {
            this._renderEngine.stopWinLineAnimations();
            this._currentLine++;
            this.playWinLineCycle();
            // tslint:disable-next-line:no-magic-numbers align
        }, 1000);
    }

    public stop(): void {
        if (this._winLines.length === 0) {
            return;
        }

        if (this._winTimer) {
            clearTimeout(this._winTimer);
        }

        this._renderEngine.stopWinLineAnimations();

        if (this._fadeMode) {
            this._renderEngine.hideSolidBackground();
        }
    }
}

export default interface IAnimationDTO {
    feature: string;
    clipName: string;
    offsetX: number;
    offsetY: number;
}

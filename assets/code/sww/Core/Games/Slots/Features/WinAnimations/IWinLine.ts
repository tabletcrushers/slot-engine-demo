import IReelItem from '../../ReelsEngine/Core/Items/IReelItem';

export default interface IWinLine {
    lineItems: Array<IReelItem>;
}

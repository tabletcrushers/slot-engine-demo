export enum ReelsEngineModeActionType {
    None,
    Randomize,
    RandomDump,
    SaveVisible
}

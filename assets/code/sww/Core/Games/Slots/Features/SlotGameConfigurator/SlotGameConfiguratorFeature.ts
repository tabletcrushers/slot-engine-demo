import SlotGameDomain from '../../../../../Games/Host/SlotGameDomain';
import { SlotGameRepositoryEnum } from '../../../../../Games/Host/SlotGameRepositoryEnum';
import RenderEngine from '../../../../../Supply/RenderEngine/RenderEngine';
import DomainManager from '../../../../Foundation/Domain/DomainManager';
import IFeature from '../../../../Foundation/Feature/IFeature';
import Notification from '../../../../Notifications/Notification';
import NotificationManager from '../../../../Notifications/NotificationManager';
import IModeDTO from '../../Configuration/DTO/IModeDTO';
import IReelItemsConfigDTO from '../../Configuration/DTO/IReelItemsConfigDTO';
import SpinResultDTO from '../../Configuration/DTO/SpinResult/SpinResultDTO';
import GameConfiguration from '../../Configuration/GameConfiguration';
import {
    SlotGameNotification,
    SlotGameNotificationTopic
} from '../../Notifications/SlotsGameNotifications';
import IReelsEngine from '../../ReelsEngine/Core/IReelsEngine';
import IAnimationDTO from '../WinAnimations/IAnimationDTO';
import { ReelsEngineModeActionType } from './ReelsEngineModeActionType';

export default class SlotGameConfiguratorFeature implements IFeature {

    public static NAME: string = 'SlotGameConfiguratorFeature';

    public _reelsEngine: IReelsEngine;
    public _renderEngine: RenderEngine;

    constructor(reelsEngine: IReelsEngine, renderEngine: RenderEngine) {
        this._reelsEngine = reelsEngine;
        this._renderEngine = renderEngine;
    }

    public start() {
        this.makeInitialSetup();
        // move 'RegularSpin' to some upper config file. How about FS restoring?
        this.configureGame('RegularSpin', ReelsEngineModeActionType.RandomDump);
    }

    public stop(): void {
    }

    public configureGame(modeId: string, action: ReelsEngineModeActionType): void {
        switch (action) {
            // TODO:VERY_IMPORTANT !!! - implement NON_WIN randomizer
            case ReelsEngineModeActionType.Randomize:
                this.setupReelsEngine(modeId);
                this._reelsEngine.randomize();
                break;

            case ReelsEngineModeActionType.RandomDump:
                this.setupReelsEngine(modeId);
                this._reelsEngine.randomize();
                // NOTE: 1, 1 is custom default. Reassign it in top-level config
                let dump: SpinResultDTO = this._reelsEngine.getVisibleDump(1, 1);
                this._reelsEngine.setSpinResult(dump, true);
                break;

            case ReelsEngineModeActionType.SaveVisible:
                // NOTE: 1, 1 is custom default. Reassign it in top-level config
                dump = this._reelsEngine.getVisibleDump(1, 1);
                this.setupReelsEngine(modeId);
                this._reelsEngine.setSpinResult(dump, true);
                break;

            default:
        }

        this._reelsEngine.resume();

        NotificationManager.instance
            .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.UnlockUpdate));
    }

    // TODO - I am not sure this should be done on this level. RenderEngine setup is higher level task
    private makeInitialSetup(): void {
        const domain: SlotGameDomain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;
        const gameConfiguration: GameConfiguration = domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.GameConfigurationRepository).read(SlotGameRepositoryEnum.GameConfigurationRepository);
        const itemsConfiguration: IReelItemsConfigDTO = gameConfiguration.itemsConfiguration;

        this._renderEngine.hideSolidBackground();
        this._renderEngine.configure(itemsConfiguration.items);
        this._renderEngine.configureAnimations(this.getAllClipByFeature(itemsConfiguration, 'winLine'));
    }

    private setupReelsEngine(modeId: string): void {
        const domain: SlotGameDomain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;
        const gameConfiguration: GameConfiguration = domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.GameConfigurationRepository).read(SlotGameRepositoryEnum.GameConfigurationRepository);

        const modeConfiguration: IModeDTO = gameConfiguration.get(modeId);

        this._reelsEngine.dispose();
        // TODO - this operation should be performed only ONCE! on RE ready. Place it right once modes fixed
        this._reelsEngine.setItemsConfiguration(gameConfiguration.itemsConfiguration);
        this._reelsEngine.setConfiguration(modeConfiguration.reelsEngineConfiguration);
        this._reelsEngine.setItems(modeConfiguration.fakeItemsConfiguration);
    }

    private getAllClipByFeature(itemsconfig: IReelItemsConfigDTO, feature: string): Map<number, IAnimationDTO> {
        const animationsConfig = new Map<number, IAnimationDTO>();

        for (const item of itemsconfig.items) {
            for (const animation of item.animations) {
                if (animation.feature === feature) {
                    animationsConfig.set(item.id, animation);
                }
            }
        }

        return animationsConfig;
    }
}

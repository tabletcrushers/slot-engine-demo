import AggregateNode from '../../../../../../../showcases/Network/AggregateNode';
import SlotGameDomain from '../../../../../Games/Host/SlotGameDomain';
import IRootCarrier from '../../../../../Supply/Interfaces/IRootCarrier';
import DomainManager from '../../../../Foundation/Domain/DomainManager';
import IFeature from '../../../../Foundation/Feature/IFeature';
import INotification from '../../../../Notifications/INotification';
import IObserver from '../../../../Notifications/IObserver';

export default class FreeSpinFeature implements IFeature, IObserver, IRootCarrier {

    private _domain: SlotGameDomain;
    private _root: AggregateNode;

    constructor() {
        this._domain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;
    }

    public setRoot(root: AggregateNode): void {
        this._root = root;
    }

    public start(): void {
        // need generate and show popup
    }

    public observe(notification: INotification) {
        // react close start/finish free spin popup
        switch (notification.id) {
            default:
                return;
        }
    }

    public showStartPopup(): void {
    }

    public showFinishedPopup(): void {
    }

    public get isActive(): boolean {
        return true; // this._domain.getFreeSpins().count > 0;
    }
}

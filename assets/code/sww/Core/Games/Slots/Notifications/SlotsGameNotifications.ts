// topics
export enum SlotGameNotificationTopic {
    Network = 'Network',
    Domain = 'Domain',
    Mode = 'Mode'
}

export enum ReelEngineNotificationTopic {
    ReelsEngine = 'ReelsEngine'
}

export enum ActionPanelTopic {
    ActionPanel = 'ActionPanel',
    ActionPanelView = 'ActionPanelView'
}

// id
export enum SlotGameNotification {
    // network
    SlotGameToken = 'SlotGameToken',
    SlotStartGameInformation = 'SlotStartGameInformation',
    SpinResult = 'SpinResult',
    SpinRequest = 'SpinRequest',
    // game
    LockUpdate = 'LockUpdate',
    UnlockUpdate = 'UnlockUpdate',
    SlotGameStarted = 'SlotGameStarted',
    // mode
    SwitchMode = 'SwitchMode',
}

export enum ReelEngineNotification {
    ReelsEngineStart = 'ReelsEngineStart',
    ReelsEngineStop = 'ReelsEngineStop',
    GameFieldStart = 'GameFieldStart',
    GameFieldStop = 'GameFieldStop',
    ReelStart = 'ReelStart',
    ReelStop = 'ReelStop',
    StateMachineStart = 'StateMachineStart',
    StateMachineStop = 'StateMachineStop',
    CorrectionDone = 'CorrectionDone',
    ReelsEngineSpinResultCleared = 'ReelsEngineSpinResultCleared',
    GameFieldSpinResultCleared = 'GameFieldSpinResultCleared',
    ReelSpinResultCleared = 'ReelSpinResultCleared',
    VisibleItemsUpdated = 'VisibleItemsUpdated'
}

export enum ActionPanelNotification {
    SpinStart = 'SpinStart',
    NegativeBet = 'NegativeBet',
    BetIncrease = 'BetIncrease',
    BetDecrease = 'BetDecrease',
    AutoSpin = 'AutoSpin',
    SpinStop = 'SpinStop'
}

export enum WinLineNotification {
    AllWinLineShowed = 'AllWinLineShowed',
    WinLineShowed = 'WinLineShowed',
    TriggerSymbolsWinLineShowed = 'TriggerSymbolsWinLineShowed',
}

export enum FreeSpinsNotification {
    FreeSpinsStarted = 'FreeSpinsStarted',
    FreeSpinsFinished = 'FreeSpinsFinished',
    FreeSpinStart = 'FreeSpinStart'
}

export enum AutoPlayNotificationTopic {
    AutoPlay = 'AutoPlay'
}

export enum AutoPlayNotification {
    AutoPlayStart = 'AutoPlayStart',
    AutoPlaySpinCount = 'AutoPlaySpinCount',
    AutoPlayEnded = 'AutoPlayEnded'
}

import Easing from '../../code/sww/Utils/Easing/Easing';
import EasingUtil, { EasingType, IPolynominalOrigins, IPolynominals } from '../../code/sww/Utils/Easing/EasingUtil';
import MathUtil from '../../code/sww/Utils/MathUtil';

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.ScrollView)
    public defaultEasings: cc.ScrollView = null;
    @property(cc.Color)
    public color: cc.Color = cc.Color.BLUE;
    @property(cc.Node)
    public easingButtonTemplate: cc.Node = null;
    @property(cc.Label)
    public polynomsLabel: cc.Label = null;
    @property(cc.EditBox)
    public easingTypeBox: cc.EditBox = null;

    private readonly graphicScale: number = 100;
    private readonly drawStep: number = 0.01;
    private readonly positonRange: number  = 3;
    private readonly decimalPlacesNum: number  = 3;

    private _graphics: cc.Graphics;
    private _sliders: Array<cc.Slider>;
    private _polynominals: IPolynominals;
    private _markerPositions: IPolynominalOrigins;

    public start(): void {

        this._markerPositions = this.getMarkerPositions(EasingType.noEasing);

        this._polynominals = EasingUtil.getPolynominals(this._markerPositions);

        this._sliders = this.getComponentsInChildren(cc.Slider);
        this.easingButtonTemplate.active = false;
        this._graphics = this.getComponentInChildren(cc.Graphics);
        this._graphics.strokeColor = this.color;

        this.addDefaultEasings();
        this.slidersReset();
    }

    public update(delta: number): void {

        this.updateMarkerLabels();
        this.updatePolynomsLabel();
        this.updateGraphic();
    }

    public pSliderCallback(slider: cc.Slider): void {

        // tslint:disable-next-line:no-magic-numbers
        const pos: number = slider.progress * 2 * this.positonRange - this.positonRange;
        this[`_mPosition${slider.name.substr(1, 1)}`] = pos;
        this._markerPositions[`position${slider.name.substr(1, 1)}`] = pos;
    }

    public reset(): void {

        this.setMarkersPositon(this.getMarkerPositions(EasingType.noEasing));
        this.easingTypeBox.string = EasingType.noEasing;
    }

    public defaultEasingButtonClick(e: cc.Event): void {

        const easingName: string = e.target.getComponentInChildren(cc.Label).string;
        this.easingTypeBox.string = easingName;
        this.setMarkersPositon(this.getMarkerPositions(EasingType[easingName]));
    }

    public downloadConfig(): void {

        this.download(this.polynomsLabel.string, `${this.easingTypeBox.string}.json`, 'text/plain');
    }

    private download(strData, strFileName, strMimeType): void {

        const a = document.createElement('a');
        a.href = `data:${strMimeType}charset=utf-8,${escape(strData)}`;

        if ('download' in a) {

            a.setAttribute('download', strFileName);

            setTimeout(() => {
                    const clickEvent = document.createEvent('MouseEvents');
                    clickEvent.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    a.dispatchEvent(clickEvent);
                },     1);
        }
    }

    private getMarkerPositions(easingType: EasingType): IPolynominalOrigins {

        return EasingUtil.getEasing(easingType).getPolynomOrigins();
    }

    private updateGraphic(): void {

        this._graphics.clear();
        this._graphics.moveTo(0, 0);

        for (let x = 0; x < 1; x += this.drawStep) {
            x = Math.round(x * this.graphicScale) / this.graphicScale;
            this._graphics.lineTo(x * this.graphicScale + 1, this.bezierFunction(x + this.drawStep) * this.graphicScale);
        }

        this._graphics.stroke();
    }

    private bezierFunction(t: number): number {

        const ts = t * t;
        const tc = ts * t;

        return this._polynominals.p5 * tc * ts + this._polynominals.p4 *
            ts * ts + this._polynominals.p3 * tc + this._polynominals.p2 * ts + this._polynominals.p1 * t;
    }

    private updateMarkerLabels(): void {

        this._polynominals = EasingUtil.getPolynominals(this._markerPositions);

        // tslint:disable-next-line:forin
        for (const i in this._sliders) {
            const markerId: number = Number(i) + 1;
            const slider = this._sliders[i];
            const pos = this._markerPositions[`position${markerId}`];
            slider.node.getComponentInChildren(cc.Label).string = MathUtil.round(pos, this.decimalPlacesNum).toString();
        }
    }

    private updatePolynomsLabel(): void {

        this.polynomsLabel.string = `
        {
            "type": "${this.easingTypeBox.string}",
            "polynoms":
            {
                "p5": ${MathUtil.round(this._polynominals.p5, this.decimalPlacesNum)},
                "p4": ${MathUtil.round(this._polynominals.p4, this.decimalPlacesNum)},
                "p3": ${MathUtil.round(this._polynominals.p3, this.decimalPlacesNum)},
                "p2": ${MathUtil.round(this._polynominals.p2, this.decimalPlacesNum)},
                "p1": ${MathUtil.round(this._polynominals.p1, this.decimalPlacesNum)}
            }
        }`;
    }

    private addDefaultEasings(): void {

        // tslint:disable-next-line:forin
        for (const enumMember in EasingType) {

            const node = cc.instantiate(this.easingButtonTemplate);
            this.defaultEasings.content.addChild(node);
            node.active = true;
            // tslint:disable-next-line:no-magic-numbers
            node.y = -this.defaultEasings.content.children.length * 29 + 50;
            const label = node.getComponentInChildren(cc.Label);
            label.string = enumMember;
        }
    }

    private setMarkersPositon(positions: IPolynominalOrigins): void {

        this._markerPositions.position1 = positions.position1;
        this._markerPositions.position2 = positions.position2;
        this._markerPositions.position3 = positions.position3;
        this._markerPositions.position4 = positions.position4;
        this.slidersReset();
    }

    private getProgressBasedOnValue(value: number): number {

        // tslint:disable-next-line:no-magic-numbers
        return (value + this.positonRange) / this.positonRange / 2;
    }

    private slidersReset(): void {

        this._sliders[0].progress = this.getProgressBasedOnValue(this._markerPositions.position1);
        this._sliders[1].progress = this.getProgressBasedOnValue(this._markerPositions.position2);
        // tslint:disable-next-line:no-magic-numbers
        this._sliders[2].progress = this.getProgressBasedOnValue(this._markerPositions.position3);
        // tslint:disable-next-line:no-magic-numbers
        this._sliders[3].progress = this.getProgressBasedOnValue(this._markerPositions.position4);
    }
}

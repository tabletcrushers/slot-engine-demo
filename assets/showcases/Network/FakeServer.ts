import SlotResponseParser from './SlotResponseParser';

export default class FakeServer {

    public getRes(): JSON {
        return {events:
                [
                    {event:
                    'config',
                     context: {
                        symbols: ['WILD', 'PIC1', 'PIC2', 'PIC3', 'PIC4', 'ACE', 'KING', 'QUEEN', 'JACK', 'TEN', 'SCAT'],
                        symbolsPay:
                        {line: ['PIC1', 'WILD', 'PIC2', 'PIC3', 'PIC4', 'ACE', 'KING', 'QUEEN', 'JACK', 'TEN'],
                         scatter: ['SCAT']},
                        wildSymbols: ['WILD'],
                        lineAlign: 'left',
                        lineCoinciding: false,
                        window: {reels: 5, rows: 3},
                        availablePayLines: [
                            [1, 1, 1],
                            [0, 0, 0],
                            [2, 2, 2],
                            [0, 1, 2],
                            [2, 1, 0],
                            [0, 0, 1],
                            [2, 2, 1],
                            [1, 2, 2],
                            [1, 0, 0],
                            [1, 0, 1]],
                        paytable: {
                            line: [
                                {on: {occurs: [3, 4, 5], of: 'PIC1', mode: 'line'}, pay: [50, 500, 2000]},
                                {on: {occurs: [3, 4, 5], of: 'PIC2', mode: 'line'}, pay: [50, 200, 1000]},
                                {on: {occurs: [3, 4, 5], of: 'PIC3', mode: 'line'}, pay: [50, 75, 150]},
                                {on: {occurs: [3, 4, 5], of: 'PIC4', mode: 'line'}, pay: [50, 75, 150]},
                                {on: {occurs: [3, 4, 5], of: 'ACE', mode: 'line'}, pay: [25, 50, 100]},
                                {on: {occurs: [3, 4, 5], of: 'KING', mode: 'line'}, pay: [25, 50, 75]},
                                {on: {occurs: [3, 4, 5], of: 'QUEEN', mode: 'line'}, pay: [15, 25, 50]},
                                {on: {occurs: [3, 4, 5], of: 'JACK', mode: 'line'}, pay: [15, 25, 50]},
                                {on: {occurs: [3, 4, 5], of: 'TEN', mode: 'line'}, pay: [15, 25, 50]}],
                            scatter: [
                                {on: {occurs: [2], of: 'SCAT', mode: 'scatter'}, pay: [2]}]
                            }
                        }
                    }
                ],
                platform: {balance: 50000}};
    }

    public getSpinResultNotWin(): any {
        return {events: [
                {event: 'bet',
                 context: {
                    total: 25,
                    betPerLine: 1,
                    paylines: [[1, 1, 1, 1, 1], [0, 0, 0, 0, 0], [2, 2, 2, 2, 2], [0, 1, 2, 1, 0], [2, 1, 0, 1, 2], [0, 0, 1, 2, 2], [2, 2, 1, 0, 0], [1, 2, 2, 2, 1], [1, 0, 0, 0, 1], [1, 0, 1, 2, 1]],
                    maxWinCap: 0}
                },
                {event: 'gameStart',
                 context: {
                    totalBet: 25,
                    betPerLine: 1}
                },
                {event: 'spinStart',
                 context: {
                    symbols: ['WILD', 'PIC1', 'PIC2', 'PIC3', 'PIC4', 'ACE', 'KING', 'QUEEN', 'JACK', 'TEN', 'SCAT'],
                    symbolsPay: {
                        line: ['PIC1', 'WILD', 'PIC2', 'PIC3', 'PIC4', 'ACE', 'KING', 'QUEEN', 'JACK', 'TEN'],
                        scatter: ['SCAT']
                    },
                    wildSymbols: ['WILD'],
                    lineAlign: 'left',
                    lineCoinciding: false}
                },
                {event: 'playedSpin',
                 context: [
                    ['Pic1', 'Pic1', 'Pic2'],
                    ['A', 'K', 'Q'],
                    ['A', 'K', 'Q'],
                    ['J', 'Pic2', 'Pic2'],
                    ['A', 'Pic3', 'Pic3']
                ]},
                {event: 'gameEnd',
                 context: {
                    win: 0
                }},
                {event: 'gameRoundOver',
                 context: {
                    win: 0
                }}
            ],
                platform: {
                gameRound: {id: 904689163},
                balance: 49975
            }
        };
    }

    public getSpinResultWithFreeSpin(): JSON {
        return {events:
                [
                    {event: 'bet',
                     context: {
                        total: 25,
                        betPerLine: 1,
                        paylines: [
                            [1, 1, 1, 1, 1],
                            [0, 0, 0, 0, 0],
                            [2, 2, 2, 2, 2],
                            [0, 1, 2, 1, 0],
                            [2, 1, 0, 1, 2],
                            [0, 0, 1, 2, 2],
                            [2, 2, 1, 0, 0],
                            [1, 2, 2, 2, 1],
                            [1, 0, 0, 0, 1],
                            [1, 0, 1, 2, 1]
                        ],
                        maxWinCap: 0}
                    },
                    {event: 'gameStart',
                     context: {
                        totalBet: 25,
                        betPerLine: 1}
                    }, {event: 'spinStart',
                        context: {
                        symbols: ['WILD', 'PIC1', 'PIC2', 'PIC3', 'PIC4', 'ACE', 'KING', 'QUEEN', 'JACK', 'TEN', 'SCAT'],
                        symbolsPay: {
                            line: ['PIC1', 'WILD', 'PIC2', 'PIC3', 'PIC4', 'ACE', 'KING', 'QUEEN', 'JACK', 'TEN'],
                            scatter: ['SCAT']
                        },
                        wildSymbols: ['WILD'],
                        lineAlign: 'left',
                        lineCoinciding: false}
                    }, {event: 'spinWin',
                        context: {
                        what: 'SCAT',
                        occurs: 2,
                        mode: 'scatter',
                        pay: 50,
                        mpInfo: {
                            mp: 1,
                            replacements: 0
                        },
                        mpBonusInfo: null,
                        context: [
                            {row: 1, reel: 0},
                            {row: 1, reel: 4}
                        ]}},
                    {event: 'spinTrigger',
                     context: {
                            spins: [{prob: 1, spins: 15}],
                            occurs: 0,
                            bonus: 'freespins',
                            trigger: {occurs: [0]}
                        }},
                    {event: 'playedSpin',
                     context: [
                            ['PIC2', 'SCAT', 'JACK'],
                            ['WILD', 'JACK', 'TEN'],
                            ['ACE', 'KING', 'PIC3'],
                            ['JACK', 'TEN', 'ACE'],
                            ['PIC3', 'SCAT', 'KING']
                        ]},
                    {event: 'enterBonus',
                     context: {
                            prob: 1,
                            additionalPrice: 0,
                            triggers: 1,
                            played: 0,
                            left: 15,
                            multiplier: {},
                            bonusTriggers: {freespins: 1},
                            bonusPlayed: {},
                            spins: [
                                {spins: 15,
                                 bonus: 'freespins',
                                 trigger: {occurs: [0]}}
                            ], triggering: {
                                bonus: 'freespins',
                                trigger: {occurs: [0]}
                                }
                            }}
                        ],
                platform: {
                            gameRound: {id: 905077905},
                            balance: 49580}
                        };
    }

    public generateFakeToken(): number {
        // tslint:disable-next-line:no-magic-numbers
        return Math.random()  * 100;
    }
}

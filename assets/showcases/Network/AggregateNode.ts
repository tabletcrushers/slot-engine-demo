import TypeUtil from '../../code/sww/Utils/TypeUtil';

export default class AggregateNode extends cc.Node {

    public getChildByName(name: string): AggregateNode {
        return TypeUtil.castTo<AggregateNode>(super.getChildByName(name));
    }

    public getChildren(): AggregateNode[] {
        return TypeUtil.castTo<Array<AggregateNode>>(this.children);
    }

    public setProperty(name: string, val: any) {
        this.attr({[name]: val});
    }

    public hasProperty(propertyName: string): boolean {
        return this.hasOwnProperty(propertyName);
    }
}

import Point from '../../code/sww/Core/Geometry/Point';
import { WinLineSegmentType } from './WinLineSegmentType';

export default class WinLineSegment {

    public relateIndex: number;
    public type: WinLineSegmentType;
    public point1: Point;
    public point2: Point;

    constructor(point1?: Point, point2?: Point, type?: WinLineSegmentType, relateIndex?: number) {
        this.point1 = point1;
        this.point2 = point2;
        this.type = type;
        this.relateIndex = relateIndex;
    }
}

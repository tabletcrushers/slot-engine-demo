import DomainManager from '../../../code/sww/Core/Foundation/Domain/DomainManager';
import SlotGameConfiguratorFeature from '../../../code/sww/Core/Games/Slots/Features/SlotGameConfigurator/SlotGameConfiguratorFeature';
import AllWinLinesDrawerFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/AllWinLinesDrawerFeature';
import SingleWinLineDrawerFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/SingleWinLineDrawerFeature';
import SingleWinLineShowingFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/SingleWinLineShowingFeature';
import LineDrawerFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/WinLinesFeature';
import {
    SlotGameNotification,
    SlotGameNotificationTopic
} from '../../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import ReelsEngine from '../../../code/sww/Core/Games/Slots/ReelsEngine/Core/ReelsEngine';
import INotification from '../../../code/sww/Core/Notifications/INotification';
import IObserver from '../../../code/sww/Core/Notifications/IObserver';
import NotificationManager from '../../../code/sww/Core/Notifications/NotificationManager';
import SlotGameDomain from '../../../code/sww/Games/Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../code/sww/Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../code/sww/Games/Host/SlotGameRepositoryEnum';
import SlotServerSpinResult from '../../../code/sww/Games/Host/SlotSpinResultRepository/SlotServerSpinResult';
import RenderEngine from '../../../code/sww/Supply/RenderEngine/RenderEngine';
import AggregateNode from '../../Network/AggregateNode';
import SlotSpinMode from './SlotSpinMode';

export default class SpinMode extends SlotSpinMode implements IObserver {

    protected _root: AggregateNode;
    protected _domain: SlotGameDomain;
    constructor() {
        super();
    }

    public setRoot(root: AggregateNode): void {
        this._root = root;
    }

    public init(): void {
        this._domain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;
        this._domain.getFeature<SlotGameConfiguratorFeature>(SlotGameConfiguratorFeature.NAME).start();

        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).prepareFeature();
        this._domain.getFeature<SingleWinLineShowingFeature>(SingleWinLineShowingFeature.NAME).prepareFeature();
    }

    public observe(notifaction: INotification): void {
        switch (notifaction.id) {
            case SlotGameNotification.SpinResult:

            const serverSpinResult = this._domain.getDALProvider()
                .getRepository(SlotGameRepositoryEnum.SpinResult)
                .read<SlotServerSpinResult>(SlotGameRepositoryDataEnum.ServerSpinResult);

            const reelsEngine: ReelsEngine = this._domain.getFeature<ReelsEngine>('ReelsEngine');

            setTimeout(() => {
                reelsEngine.setSpinResult(serverSpinResult.toSpinResultDTO(), false);
            // tslint:disable-next-line:no-magic-numbers
                    }, 1000);

            return;

            default:
                return;
        }
    }

    public update(delta: number): void {
        this._domain.getFeature<ReelsEngine>('ReelsEngine').update(delta);

        if (this._domain.getFeature<RenderEngine>('RenderEngine').isActive) {
            // // TODO: replace '0' to logical definition
            this._domain.getFeature<RenderEngine>('RenderEngine')
                .renderReelItems(
                    this._domain.getFeature<ReelsEngine>('ReelsEngine').getVisibleItems().getGameFieldReport(0)
                );
        }
    }

    public startSpin(spinData: any): void {
        this._domain.getFeature<ReelsEngine>('ReelsEngine').start();
        this._domain.getFeature<RenderEngine>('RenderEngine').resume();
    }

    public stopSpin(): void {
        this._domain.getFeature<RenderEngine>('RenderEngine').suspend();
    }

    public renderVisibleItems(): void {
        super.renderVisibleItems();

        // // TODO: replace '0' to logical definition
        this._domain.getFeature<RenderEngine>('RenderEngine')
            .renderReelItems(
                this._domain.getFeature<ReelsEngine>('ReelsEngine').getVisibleItems().getGameFieldReport(0)
            );
    }

    public registerListeners(): void {
        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotStartGameInformation)
            .addObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult);
    }

    public removeListeners(): void {
        NotificationManager.instance
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SlotStartGameInformation)
            .removeObserver(this, SlotGameNotificationTopic.Domain, SlotGameNotification.SpinResult);
    }
}

import INotification from '../../../code/sww/Core/Notifications/INotification';
import IObserver from '../../../code/sww/Core/Notifications/IObserver';
import ISlotGameMode from './ISlotSpinMode';

export default class FreeSpinMode implements ISlotGameMode, IObserver {

    public observe(notifaction: INotification): void {
        switch (notifaction.id) {
            default:
                return;
        }
    }

    public startSpin(): void {
    }

    public stopSpin(): void {
    }

    public addFeature(key: string, featute: any) {
        throw new Error('Method not implemented.');
    }

    public getFeature<T>(key: string): T {
        throw new Error('Method not implemented.');
    }

    public onEnter(): void {
        throw new Error('Method not implemented.');
    }

    public onExit(): void {
        throw new Error('Method not implemented.');
    }

    constructor() {
    }
}

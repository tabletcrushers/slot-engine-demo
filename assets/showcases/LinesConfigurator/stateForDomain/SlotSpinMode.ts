import IFeature from '../../../code/sww/Core/Foundation/Feature/IFeature';
import IUpdatable from '../../../code/sww/Core/Foundation/Update/IUpdatable';
import TypeUtil from '../../../code/sww/Utils/TypeUtil';
import ISlotSpinMode from './ISlotSpinMode';

export default abstract class SlotSpinMode implements ISlotSpinMode, IUpdatable {

    protected _features: Map<string, IFeature>;

    constructor() {
        this._features = new Map<string, IFeature>();
    }

    public addFeature(key: string, feature: IFeature): void {
        this._features.set(key, feature);
    }

    public renderVisibleItems(): void {
    }

    public getFeature<T>(key: string): T {
        return TypeUtil.castTo<T>(this._features.get(key));
    }

    public init(): void {
    }

    public onEnter(): void {
        this.registerListeners();
    }

    public onExit(): void {
        this.removeListeners();
    }

    public update(delta: number): void {
    }

    public startSpin(spinData: any): void {
    }

    public stopSpin(): void {
    }

    public registerListeners(): void {
    }

    public removeListeners(): void {
    }
}

import WinLinesAnimation from '../../../code/sww/Core/Games/Slots/Features/WinAnimations/WinLinesAnimations';
import AllWinLinesDrawerFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/AllWinLinesDrawerFeature';
import SingleWinLineShowingFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/SingleWinLineShowingFeature';
import {
    SlotGameNotification,
    SlotGameNotificationTopic,
    WinLineNotification
} from '../../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import INotification from '../../../code/sww/Core/Notifications/INotification';
import Notification from '../../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../../code/sww/Core/Notifications/NotificationManager';
import ISlotFreeSpins from '../../../code/sww/Games/Host/SlotBonusesRepository/Intefaces/ISlotFreeSpins';
import { SlotGameRepositoryDataEnum } from '../../../code/sww/Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../code/sww/Games/Host/SlotGameRepositoryEnum';
import IWin from '../../../code/sww/Games/Host/SlotSpinResultRepository/Interfaces/IWin';
import FlamingPearlActionPanel from '../../../code/sww/Games/Slots/FlamingPearl/Features/ActionPanel/FlamingPearlActionPanel';
import { PopupsNotifications, PopupsNotificationTopic } from '../../../code/sww/Supply/Popups/Notification/PopupsNotifications';
import SpinMode from './SpinMode';

export default class FlamingPearlSpinMode extends SpinMode {

    constructor() {
        super();
    }

    public init(): void {
        super.init();
        this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).start();
    }

    public observe(notification: INotification): void {
        switch (notification.id) {
            case WinLineNotification.AllWinLineShowed:
            case WinLineNotification.WinLineShowed:

                this._domain.getFeature<SingleWinLineShowingFeature>(SingleWinLineShowingFeature.NAME).start();
                this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).playWinSymbolsLine(notification.arguments);

                return;
            case PopupsNotifications.OpenPopup:
                this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).setButtonsActivity(false);

                return;
            case PopupsNotifications.ClosePopup:
                this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).setButtonsActivity(true);

                return;
            default:
                super.observe(notification);
        }
    }

    public update(delta: number): void {
        super.update(delta);
        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).update(delta);
        this._domain.getFeature<SingleWinLineShowingFeature>(SingleWinLineShowingFeature.NAME).update(delta);
    }

    public startSpin(spinData: any): void {
        super.startSpin(spinData);
        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).stop();
        this._domain.getFeature<SingleWinLineShowingFeature>(SingleWinLineShowingFeature.NAME).stop();

        this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).stop();

        this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onReelsStart();
        this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).reduceBalance();
    }

    public stopSpin(): void {

        // this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).start();
        // this._domain.getFeature<LineDrawerFeature>(LineDrawerFeature.NAME).showAllWinLines();
        const spinResultWin = this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        if (this.freeSpinFeature.isActive) {

            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'FreeSpinMode'));
        } else {
            this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).start();
            this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).start();

            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onAllReelsStopped();
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME)
                .updateBalance();
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME)
                .updateWin(spinResultWin.win);
        }

        super.stopSpin();
    }

    public registerListeners(): void {
        super.registerListeners();
        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.WinLineShowed)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.ClosePopup)
            .addObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.OpenPopup);
    }

    public removeListeners(): void {
        super.removeListeners();
        NotificationManager.instance
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.WinLineShowed)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.ClosePopup)
            .removeObserver(this, PopupsNotificationTopic.Default, PopupsNotifications.OpenPopup);
    }

    private get freeSpinFeature(): ISlotFreeSpins {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<ISlotFreeSpins>(SlotGameRepositoryDataEnum.FreeSpins);
    }
}

import IFeature from '../../../code/sww/Core/Foundation/Feature/IFeature';

export default interface ISlotSpinMode {
    addFeature(key: string, featute: IFeature);
    getFeature<T>(key: string): T;

    onEnter(): void;
    onExit(): void;

    startSpin(spinData: any): void;
    stopSpin(): void;
    renderVisibleItems(): void;
}

import DomainManager from '../../../code/sww/Core/Foundation/Domain/DomainManager';
import { ReelsEngineModeActionType } from '../../../code/sww/Core/Games/Slots/Features/SlotGameConfigurator/ReelsEngineModeActionType';
import SlotGameConfiguratorFeature from '../../../code/sww/Core/Games/Slots/Features/SlotGameConfigurator/SlotGameConfiguratorFeature';
import WinLinesAnimation from '../../../code/sww/Core/Games/Slots/Features/WinAnimations/WinLinesAnimations';
import AllWinLinesDrawerFeature from '../../../code/sww/Core/Games/Slots/Features/WinLineDrawer/Features/AllWinLinesDrawerFeature';
import {
    FreeSpinsNotification,
    SlotGameNotification,
    SlotGameNotificationTopic,
    WinLineNotification
} from '../../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import INotification from '../../../code/sww/Core/Notifications/INotification';
import Notification from '../../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../../code/sww/Core/Notifications/NotificationManager';
import ISlotFreeSpins from '../../../code/sww/Games/Host/SlotBonusesRepository/Intefaces/ISlotFreeSpins';
import SlotGameDomain from '../../../code/sww/Games/Host/SlotGameDomain';
import { SlotGameRepositoryDataEnum } from '../../../code/sww/Games/Host/SlotGameRepositoryDataEnum';
import { SlotGameRepositoryEnum } from '../../../code/sww/Games/Host/SlotGameRepositoryEnum';
import IWin from '../../../code/sww/Games/Host/SlotSpinResultRepository/Interfaces/IWin';
import ISlotWinLine from '../../../code/sww/Games/Host/WinLineRepository/interfaces/ISlotWinLine';
import FlamingPearlActionPanel from '../../../code/sww/Games/Slots/FlamingPearl/Features/ActionPanel/FlamingPearlActionPanel';
import TestFreeSpinsPopupComponent from '../../FreeSpinsPopups/TestFreeSpinsPopupComponent';
import SpinMode from './SpinMode';

export default class FlamingPearlFreeSpinMode extends SpinMode {

    private _freeSpinJustStart: boolean;
    private _freeSpinJustStop: boolean;
    constructor() {
        super();
    }

    public init(): void {
        this._domain = DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain;

        // super.init();
        // this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).start();
    }

    public onEnter(): void {
        super.onEnter();
        this._freeSpinJustStart = true;
        this._freeSpinJustStop = false;

        const spinResultWinLines = this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<Array<ISlotWinLine>>(SlotGameRepositoryDataEnum.SpinResultWinLines);

        if (spinResultWinLines.length > 0) {
            this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).start();
            this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).start();
        } else {
            this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).stop();
            this.showFreeSpinAnim();
        }

        const spinResultWin = this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).updateWin(spinResultWin.spinWin);
    }

    public observe(notification: INotification): void {
        switch (notification.id) {
            case WinLineNotification.AllWinLineShowed:
                this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).stop();
                this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).stop();

                if (this._freeSpinJustStart) {
                    this.showFreeSpinAnim();
                    // this.openStartFreeSpinPopup();

                    this._freeSpinJustStart = false;
                } else if (this._freeSpinJustStop) {
                    this.openFinishFreeSpinPopup();

                    this._domain.getFeature<SlotGameConfiguratorFeature>(SlotGameConfiguratorFeature.NAME)
                        .configureGame('RegularSpin', ReelsEngineModeActionType.SaveVisible);

                } else {
                    NotificationManager.instance
                        .dispatch(new Notification(SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsStarted));
                }

                return;

            case WinLineNotification.TriggerSymbolsWinLineShowed:
                    this.openStartFreeSpinPopup();

                    this._domain.getFeature<SlotGameConfiguratorFeature>(SlotGameConfiguratorFeature.NAME)
                        .configureGame('FreeSpin', ReelsEngineModeActionType.SaveVisible);

                    return;

            default:
                super.observe(notification);
        }
    }

    public update(delta: number): void {
        super.update(delta);
        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).update(delta);
    }

    public startSpin(spinData: any): void {
        NotificationManager.instance.dispatch(new Notification(SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinStart));

        super.startSpin(spinData);
        this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).stop();
        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).stop();
        // this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onReelsStart();
    }

    public stopSpin(): void {
        super.stopSpin();

        this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).start();
        this._domain.getFeature<AllWinLinesDrawerFeature>(AllWinLinesDrawerFeature.NAME).start();

        const spinResultWin = this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        if (!this.freeSpinFeature.isActive) {
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).updateBalance();
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).setAccumulateWinToZero();
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME)
                .updateWin(spinResultWin.win);

            this._freeSpinJustStop = true;
        } else {
            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME)
                .accumulateWin(spinResultWin.spinWin);

            if (spinResultWin.spinWin === 0) {
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsStarted));
            }
        }
    }

    public registerListeners(): void {
        super.registerListeners();
        NotificationManager.instance
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .addObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.WinLineShowed)
            .addObserver(this, SlotGameNotificationTopic.Mode, WinLineNotification.TriggerSymbolsWinLineShowed);
    }

    public removeListeners(): void {
        super.removeListeners();
        NotificationManager.instance
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.AllWinLineShowed)
            .removeObserver(this, SlotGameNotificationTopic.Domain, WinLineNotification.WinLineShowed)
            .removeObserver(this, SlotGameNotificationTopic.Mode, WinLineNotification.TriggerSymbolsWinLineShowed);
    }

    private openStartFreeSpinPopup(): void {
        const popupRenderNode = cc.find('Canvas/PopupRenderNode');
        const component = popupRenderNode.getComponent('TestFreeSpinsPopupComponent') as TestFreeSpinsPopupComponent;
        component.openStartFreeSpinPopup(this.freeSpinFeature.count);
    }

    private openFinishFreeSpinPopup(): void {
        const popupRenderNode = cc.find('Canvas/PopupRenderNode');
        const component = popupRenderNode.getComponent('TestFreeSpinsPopupComponent') as TestFreeSpinsPopupComponent;
        const spinResultWin = this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<IWin>(SlotGameRepositoryDataEnum.SpinResultWin);

        component.openFinishFreeSpinPopup(spinResultWin.win, () => {
            const isAutoPlayMode = (DomainManager.instance.getDomain(SlotGameDomain.NAME) as SlotGameDomain).getDALProvider()
                    .getRepository(SlotGameRepositoryEnum.AutoPlayRepository).read('autoPlayMode');

            if (isAutoPlayMode) {
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'AutoPlayMode'));
            } else {
                NotificationManager.instance
                    .dispatch(new Notification(SlotGameNotificationTopic.Domain, SlotGameNotification.SwitchMode, 'SpinMode'));
            }

            this._domain.getFeature<FlamingPearlActionPanel>(FlamingPearlActionPanel.NAME).onAllReelsStopped();
        });
    }

    private showFreeSpinAnim(): void {
        this._domain.getFeature<WinLinesAnimation>(WinLinesAnimation.NAME).startScatter();
    }

    private get freeSpinFeature(): ISlotFreeSpins {
        return this._domain.getDALProvider()
            .getRepository(SlotGameRepositoryEnum.SpinResult)
            .read<ISlotFreeSpins>(SlotGameRepositoryDataEnum.FreeSpins);
    }
}

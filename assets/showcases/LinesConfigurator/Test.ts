// tslint:disable:no-magic-numbers
import Color from '../../code/sww/Supply/Custom/Color';
import WinLinesDrawer from './WinLinesDrawer';

const {ccclass, property} = cc._decorator;

@ccclass
export default class Test extends cc.Component {

    public onLoad(): void {

        const drawLine = new WinLinesDrawer();
        drawLine.setRoot(this.node.getChildByName('LineNode'));

        const tempArr = [{x: 0, y: 0}, {x: 50, y: 0}, {x: 100, y: 50}, {x: 150, y: 0}, {x: 200, y: 50}, {x: 250, y: 0}, {x: 300, y: 0}];

        const colr = new Color();
        colr.red = 255;
        colr.green = 255;
        colr.blue = 0;
        colr.alpha = 255;
        drawLine.drawWinLine(tempArr, colr, 5);
    }
}

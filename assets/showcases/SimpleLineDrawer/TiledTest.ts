
const {ccclass, property} = cc._decorator;

// tslint:disable:no-magic-numbers
@ccclass
export default class TiledTest extends cc.Component {

    // @property(cc.SpriteFrame)
    // public frame: cc.SpriteFrame = undefined;

    @property(sp.Skeleton)
    public sp: sp.Skeleton = null;

    public onLoad() {

        cc.loader.loadRes('/FlamingPearl/Lines/line.png', (err, res) => {
            const tiledSprite = cc.find('TiledSprite');

            const node1 = new cc.Node('Node1');
            node1.scaleY = 0.1;
            node1.setAnchorPoint(cc.v2(0, 0));
            tiledSprite.addChild(node1);
            node1.parent = tiledSprite;

            const sprite = node1.addComponent(cc.Sprite);
            sprite.type = cc.Sprite.Type.TILED;
            sprite.spriteFrame = new cc.SpriteFrame(res);

            node1.width = 440;

            const node2 = new cc.Node('Node2');
            node2.setAnchorPoint(cc.v2(0, 0));
            node2.scaleY = 0.1;
            node2.rotation = 38;
            node2.y = node1.y;
            node2.x = node1.x + 440;
            tiledSprite.addChild(node2);
            node1.parent = tiledSprite;

            const sprite2 = node2.addComponent(cc.Sprite);
            sprite2.type = cc.Sprite.Type.TILED;
            sprite2.spriteFrame = new cc.SpriteFrame(res);
            node2.width = 100;
        });

    }
}

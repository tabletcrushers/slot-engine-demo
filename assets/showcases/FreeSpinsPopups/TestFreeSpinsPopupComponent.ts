import { FreeSpinsNotification, SlotGameNotificationTopic } from '../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import Notification from '../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../code/sww/Core/Notifications/NotificationManager';

const {ccclass, property} = cc._decorator;

@ccclass
export default class TestFreeSpinsPopupComponent extends cc.Component {

    @property(cc.Prefab)
    public startFreeSpinPopup: cc.Prefab = null;

    @property(cc.Prefab)
    public finishFreeSpinPopup: cc.Prefab = null;

    public onLoad() {
    }

    public openStartFreeSpinPopup(freeSpinsCount: number): void {
        const p = cc.instantiate(this.startFreeSpinPopup);
        this.node.addChild(p);

        const countLable  = p.getChildByName('FreeSpinsCount').getComponent(cc.Label);
        countLable.string = freeSpinsCount.toString();

        const btn = p.getChildByName('StartFreeSpinButton').getComponent(cc.Button);
        btn.node.on(cc.Node.EventType.TOUCH_END, () => {
            this.node.removeChild(p);

            NotificationManager.instance
                .dispatch(new Notification(SlotGameNotificationTopic.Domain, FreeSpinsNotification.FreeSpinsStarted));
        });
    }

    public openFinishFreeSpinPopup(coins: number, onClose?: Function): void {
        const p = cc.instantiate(this.finishFreeSpinPopup);
        this.node.addChild(p);

        const countLable  = p.getChildByName('WinCoins').getComponent(cc.Label);
        countLable.string = coins.toString();

        const btn = p.getChildByName('ContinueButton').getComponent(cc.Button);
        btn.node.on(cc.Node.EventType.TOUCH_END, () => {
            this.node.removeChild(p);

            onClose && onClose();
        });
    }
}

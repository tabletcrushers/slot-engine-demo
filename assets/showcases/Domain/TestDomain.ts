import IDALRepository from '../../code/sww/Core/Foundation/DAL/IDALRepository';
import SlotGameDomain from '../../code/sww/Games/Host/SlotGameDomain';

const { ccclass } = cc._decorator;

@ccclass
export default class TestDomain extends cc.Component {

    public start() {
        const gameHost = new SlotGameDomain();
        const provider = gameHost.getDALProvider();

        provider.addRepository('Jackpot1');
        provider.addRepository('Jackpot2');

        const repa1 = provider.getRepository('Jackpot1');
        const repa2 = provider.getRepository('Jackpot2');

        repa1.write('id', 1);
        repa1.write('logic', {
            type: 'mini',
            win: 200
        });
        // tslint:disable-next-line:no-magic-numbers
        repa2.write('id', 2);
        repa2.write('logic', {
            type: 'major',
            win: 2000
        });

        cc.log(repa1.read('id'));
        cc.log(repa1.read('logic'));
        cc.log(repa2.read('id'));
        cc.log(repa2.read('logic'));

        provider.removeRepository('Jackpot1');
        provider.removeRepository('Jackpot2');

        cc.log(provider.getRepository('Jackpot1'));
        cc.log(provider.getRepository('Jackpot2'));

        // concrete casts sample
        provider.addRepository('UserProfile');
        const repository: IDALRepository = provider.getRepository('UserProfile');
        // tslint:disable-next-line:no-magic-numbers
        repository.write('dataAsClass', new UserProfile('Vasya', 1000));
        repository.write('dataAsObject', {
            name: 'Petya',
            balance: 9.99
        });

        const dataAsClass: IUserProfile = repository.read<IUserProfile>('dataAsClass');
        const dataAsObject: IUserProfile = repository.read<IUserProfile>('dataAsObject');

        cc.log(dataAsClass.name);
        cc.log(dataAsObject.name);
    }
}

export interface IUserProfile {
    balance: number;
    name: string;
}

export class UserProfile {

    private _name: string;
    private _balance: number;

    constructor(name: string, balance: number) {
        this._name = name;
        this._balance = balance;
    }

    public get name(): string {
        return this._name;
    }

    public get balance(): number {
        return this._balance;
    }
    public set balance(value: number) {
        if (value < 0) {
            throw new Error('Воу-воу, мы в долг не работаем!');
        }
        this._balance = value;
    }
}

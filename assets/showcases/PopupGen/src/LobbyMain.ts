import Notification from '../../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../../code/sww/Core/Notifications/NotificationManager';
import { PopupsNotifications, PopupsNotificationTopic } from '../../../code/sww/Supply/Popups/Notification/PopupsNotifications';
import PopupGenerator from '../../../code/sww/Supply/Popups/PopupGenerator';
import { PopupType } from '../../../code/sww/Supply/Popups/PopupsWarehouse';
import LoaderUtil from '../../../code/sww/Utils/LoaderUtil';
import NodeUtil from '../../../code/sww/Utils/NodeUtil';
import TypeUtil from '../../../code/sww/Utils/TypeUtil';
import AggregateNode from '../../Network/AggregateNode';
import ICommonUISettingsDTO from './ICommonUISettingsDTO';
import PopupFeature from './PopupFeature';
import PopupsColection from './PopupsColection';

const {ccclass, property} = cc._decorator;

@ccclass
export default class LobbyMain extends cc.Component {

    public staticNodes: AggregateNode;
    public _popupFeature: PopupFeature;
    public _simplePopupBttn: AggregateNode;
    public _multiplePopupBttn: AggregateNode;
    public _leftArrowBttn: AggregateNode;
    public _rightArrowBttn: AggregateNode;
    public _popupsConfig: any;
    public _commonsUISettings: ICommonUISettingsDTO;

    public onLoad(): void {
        this.initStaticLements();
        this.initPopups();
        this.registerListener();
    }

    public start(): void {
        NotificationManager.instance
            .dispatch(new Notification(PopupsNotificationTopic.Default, PopupsNotifications.DisablePopupButton, 'ActionButton'))
            .dispatch(new Notification(PopupsNotificationTopic.Default, PopupsNotifications.SetTextToPopup, 'UHAHAHA'));
    }

    private initStaticLements(): void {
        this.staticNodes = TypeUtil.castTo<AggregateNode>(this.node.getChildByName('StaticElements'));

        // load JSON
        const url = this.staticNodes.getChildByName('Popups').getComponent('PopupsColection').jsonConfig;
        cc.loader.load(url, (error, result) => {
            PopupGenerator.instance.config = result;
            if (this._commonsUISettings !== undefined && this._commonsUISettings !== null) {
                this._multiplePopupBttn.active = true;
            }
        });

        this._simplePopupBttn = TypeUtil.castTo<AggregateNode>(this.node.getChildByName('Lobby').getChildByName('OpenPopupBttn'));
        this._simplePopupBttn.active = false;
        this._multiplePopupBttn = TypeUtil.castTo<AggregateNode>(this.node.getChildByName('Lobby').getChildByName('PaytableButton'));
        this._multiplePopupBttn.getComponent(cc.Button).enabled = false;
    }

    private initPopups(): void {
        const popups: PopupsColection = this.staticNodes.getChildByName('Popups').getComponent('PopupsColection');
        if (popups === null || popups === undefined) {
            throw(new Error('Component PopupsColection not found in StaticElements'));
        }

        const popupPrefabs: Array<cc.Prefab> = popups.popupPrefabs;
        PopupGenerator.instance.partsContainer = NodeUtil.cloneNode(TypeUtil.castTo<AggregateNode>(popupPrefabs[0]), false);

        LoaderUtil.loadJSON<ICommonUISettingsDTO>('resources/UIConfig/commonUISettings.json').then((config) => {
            this._commonsUISettings = config;
            this._popupFeature = new PopupFeature();
            this._popupFeature.setRoot(this.staticNodes);
            this._popupFeature.setPaytableConfig(config.paytable);

            if (PopupGenerator.instance.config !== undefined && PopupGenerator.instance.config !== null) {
                this._multiplePopupBttn.getComponent(cc.Button).enabled = true;
            }
        });
    }

    private registerListener(): void {
        this._simplePopupBttn.on(cc.Node.EventType.TOUCH_START, () => { this._popupFeature.showPopup(PopupType.SimplePopup); });
        this._multiplePopupBttn.on(cc.Node.EventType.TOUCH_START, () => { this._popupFeature.showMultiplePopup(PopupType.PaytablePopup); });
    }
}

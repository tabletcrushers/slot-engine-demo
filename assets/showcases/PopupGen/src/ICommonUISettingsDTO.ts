import IPaytablePaylineConfigDTO from '../../../code/sww/Supply/Popups/interfaces/IPaytablePaylineConfigDTO';

export default interface ICommonUISettingsDTO {
    paytable: IPaytablePaylineConfigDTO;
}

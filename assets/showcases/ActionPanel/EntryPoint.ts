import DomainManager from '../../code/sww/Core/Foundation/Domain/DomainManager';
import ActionPanelView from './ActionPanelView';
import TestActionPanel from './TestActionPanel';
import TestGameSlot from './TestGameHost';
import TestUserProfileHost from './TestUserProfileHost';

const { ccclass } = cc._decorator;

@ccclass
export default class EntryPoint extends cc.Component {

    public onLoad() {
        const profileHost = new TestUserProfileHost();

        DomainManager.instance.addDomain('TestUserProfileHost', profileHost);
        profileHost.userData = {
            name: 'Fedir Petrovuch',
            level: 1,
            balance: 50
        };

        DomainManager.instance.addDomain('TestGameSlot', new TestGameSlot());

        const actionPanelView = new ActionPanelView();
        const actionPanel = new TestActionPanel(actionPanelView,
                                                (DomainManager.instance.getDomain('TestGameSlot') as TestGameSlot).betManager);
    }
}

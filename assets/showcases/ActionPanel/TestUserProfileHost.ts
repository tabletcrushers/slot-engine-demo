import IUserProfileData from '../../code/sww/Application/Host/Repository/IUserProfileData';
import { ApplicationNotificationTopic, UserProfilerNotification } from '../../code/sww/Application/Notifications/ApplicationNotifications';
import IDALRepository from '../../code/sww/Core/Foundation/DAL/IDALRepository';
import Domain from '../../code/sww/Core/Foundation/Domain/Domain';
import { ActionPanelNotification, ActionPanelTopic } from '../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import IObserver from '../../code/sww/Core/Notifications/IObserver';
import Notification from '../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../code/sww/Core/Notifications/NotificationManager';

export default class TestUserProfileHost extends Domain implements IObserver {
    private _userProfile: IDALRepository;
    constructor() {
        super();

        this.getDALProvider().addRepository('UserProfile');
        this._userProfile = this.getDALProvider().getRepository('UserProfile');

        NotificationManager.instance.addObserver(this, ActionPanelTopic.ActionPanel, ActionPanelNotification.SpinStart);
        NotificationManager.instance.addObserver(this,
                                                 ApplicationNotificationTopic.UserProfile, UserProfilerNotification.NotifyBalance);
    }

    public get userData(): IUserProfileData {
        return this._userProfile.read<IUserProfileData>('UserData');
    }

    public set userData(userData: IUserProfileData) {
        this._userProfile.write('UserData', userData);
    }

    public notifyBalance(): void {
        NotificationManager.instance.dispatch(
            new Notification(ApplicationNotificationTopic.UserProfile,
                             UserProfilerNotification.BalanceChanged, { balance: this.userData.balance }));
    }

    public isPossibleBet(bet: number): boolean {
        return (this.userData.balance - bet) >= 0;
    }

    public observe(notification: Notification): void {
        switch (notification.id) {
            case ActionPanelNotification.SpinStart:
                this.updateBalance(notification.arguments.bet);
                break;
            case UserProfilerNotification.NotifyBalance:
                this.notifyBalance();
                break;
            default:
        }
    }

    private updateBalance(bet: number): void {
        this.userData.balance -= bet;

        this.notifyBalance();
    }
}

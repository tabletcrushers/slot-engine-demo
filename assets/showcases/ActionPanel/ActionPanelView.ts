import { ApplicationNotificationTopic } from '../../code/sww/Application/Notifications/ApplicationNotifications';
import IActionPanelView from '../../code/sww/Core/Games/Slots/ActionPanel/IActionPanelView';
import {
    ActionPanelNotification,
    ActionPanelTopic,
    SlotGameNotification } from '../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import Notification from '../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../code/sww/Core/Notifications/NotificationManager';
import AggregateNode from '../Network/AggregateNode';

export default class ActionPanelView implements IActionPanelView {

    private _node: AggregateNode;
    private _betValue: cc.Node;
    private _balanceValue: cc.Node;
    private _winValue: cc.Node;
    private _betDecreaseBtn: cc.Node;
    private _betIncreaseBtn: cc.Node;
    private _spinBtn: cc.Node;
    private _soundOnBtn: cc.Node;
    private _soundOffBtn: cc.Node;

    private readonly _spinStartNotification: Notification;
    private readonly _betIncreaseNotification: Notification;
    private readonly _betDecreaseNotification: Notification;
    private readonly _spinRequest: Notification;

    constructor() {
        this._spinStartNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.SpinStart);
        this._spinRequest = new Notification(ApplicationNotificationTopic.Server, SlotGameNotification.SpinRequest);
        this._betIncreaseNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.BetIncrease);
        this._betDecreaseNotification = new Notification(ActionPanelTopic.ActionPanelView, ActionPanelNotification.BetDecrease);
    }

    public setRoot(root: AggregateNode): void {
        this._node = root;
    }

    public init(): void {
        this.addUIElements();
    }

    public disable(): void {
        this.setVisibleState(false);
    }

    public enable(): void {
        this.setVisibleState(true);
    }

    public disableSpinBtn(): void {
        this._spinBtn.getComponent(cc.Button).interactable = false;
    }

    public enableSpinBtn(): void {
        this._spinBtn.getComponent(cc.Button).interactable = true;
    }

    public updateBet(bet: string): void {
        this._betValue.getComponent(cc.Label).string = bet;
    }

    public updateBalance(balance: string): void {
        this._balanceValue.getComponent(cc.Label).string = balance;
    }

    public updateWin(win: string): void {
        this._winValue.getComponent(cc.Label).string = win;
    }

    private setVisibleState(val: boolean): void {
        this._betDecreaseBtn.getComponent(cc.Button).interactable = val;
        this._betIncreaseBtn.getComponent(cc.Button).interactable = val;
        this._spinBtn.getComponent(cc.Button).interactable = val;
    }

    private addUIElements() {
        this._betValue = cc.find('Canvas/UI/betValue');
        this._balanceValue = cc.find('Canvas/UI/balanceValue');
        this._winValue = cc.find('Canvas/UI/winValue');
        this._betDecreaseBtn = cc.find('Canvas/UI/Buttons/betMinusBtn');
        this._betIncreaseBtn = cc.find('Canvas/UI/Buttons/betPlusBtn');
        this._spinBtn = cc.find('Canvas/UI/Buttons/spinBtn');
        this._soundOnBtn = cc.find('Canvas/UI/Buttons/soundOnBtn');
        this._soundOffBtn = cc.find('Canvas/UI/Buttons/soundOffBtn');

        this._betDecreaseBtn.on(cc.Node.EventType.TOUCH_END, this.onBetDecreaseBtnClick.bind(this));
        this._betIncreaseBtn.on(cc.Node.EventType.TOUCH_END, this.onBetIncreaseBtnClick.bind(this));
        this._spinBtn.on(cc.Node.EventType.TOUCH_END, this.onSpinBtnClick.bind(this));
        this._soundOnBtn.on(cc.Node.EventType.TOUCH_END, this.onSoundOnBtnClick.bind(this));
        this._soundOffBtn.on(cc.Node.EventType.TOUCH_END, this.onSoundOffBtnClick.bind(this));
    }

    private onBetDecreaseBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._betDecreaseNotification);
        }
    }

    private onBetIncreaseBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._betIncreaseNotification);
        }
    }

    private onSpinBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            NotificationManager.instance.dispatch(this._spinStartNotification);
            NotificationManager.instance.dispatch(this._spinRequest);
        }
    }

    private onSoundOnBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            this._soundOnBtn.active = false;
            this._soundOffBtn.active = true;
        }
    }

    private onSoundOffBtnClick(event): void {
        if (event.target.getComponent(cc.Button).interactable === true) {
            this._soundOffBtn.active = false;
            this._soundOnBtn.active = true;
        }
    }
}

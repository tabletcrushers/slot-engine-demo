import IBetManager from '../../code/sww/Application/User/IBetManager';
import SlotGameDomain from '../../code/sww/Games/Host/SlotGameDomain';

export default class TestGameSlot extends SlotGameDomain {

    public get betManager(): IBetManager {
        return this.betManager;
    }
}

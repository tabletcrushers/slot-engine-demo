import {
    ApplicationNotificationTopic,
    UserProfilerNotification } from '../../code/sww/Application/Notifications/ApplicationNotifications';
import IBetManager from '../../code/sww/Application/User/IBetManager';
import ActionPanel from '../../code/sww/Core/Games/Slots/ActionPanel/ActionPanel';
import IActionPanelView from '../../code/sww/Core/Games/Slots/ActionPanel/IActionPanelView';
import Notification from '../../code/sww/Core/Notifications/Notification';
import NotificationManager from '../../code/sww/Core/Notifications/NotificationManager';
export default class TestActionPanel extends ActionPanel {

    constructor(view: IActionPanelView, betManager: IBetManager) {
        super(view, betManager);

        NotificationManager.instance
            .addObserver(this, ApplicationNotificationTopic.UserProfile, UserProfilerNotification.BalanceChanged)
            .dispatch(new Notification(ApplicationNotificationTopic.UserProfile, UserProfilerNotification.NotifyBalance));
    }

    public observe(notification: Notification): void {
        super.observe(notification);

        switch (notification.id) {
            case UserProfilerNotification.BalanceChanged:
                this.view.updateBalance(notification.arguments.balance);
                break;

            default:
        }
    }
}

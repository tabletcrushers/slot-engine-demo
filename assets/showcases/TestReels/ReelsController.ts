import Reel from './Reel';

const {ccclass, property} = cc._decorator;

@ccclass
export class ReelsController extends cc.Component {

    private _reelsCount: number = 5;
    private _reels: Array<Reel>;

    public start() {
        this._reels = [];

        for (let i = 0; i < this._reelsCount; i++) {
            this._reels.push(this.node.children[i].getComponent(Reel));
        }
    }
}

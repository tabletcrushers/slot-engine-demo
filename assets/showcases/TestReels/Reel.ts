// tslint:disable:no-magic-numbers
// tslint:disable:prefer-for-of
const { ccclass, property } = cc._decorator;

@ccclass
export default class Reel extends cc.Component {
    @property(cc.SpriteAtlas)
    public symbolsAtlas: cc.SpriteAtlas = null;

    private readonly symbolsName: Array<string> = ['PIC1_ChinesePot', 'PIC2_GoldenLuckyCat',
                                                   'PIC3_ChineseVase', 'PIC4_ChineseTemple', 'Royal_A', 'Royal_J'];
    private readonly _size: number = 3;
    private readonly _additionalSymbols: number = 2;
    private readonly symbolSize: cc.Vec2 = new cc.Vec2(250, 220);
    private readonly _velocity: number = 800;

    private _topY = 0;

    public start(): void {
        for (let i = 0; i < this._size + this._additionalSymbols; i++) {
            const node = new cc.Node();
            const sprite = node.addComponent(cc.Sprite);

            node.anchorX = 0.5;
            node.anchorY = 0;
            sprite.spriteFrame = this.getRandomFrame();
            node.y = this._topY - this.symbolSize.y * i;

            this.node.addChild(node);
        }
    }

    public update(dt: number): void {
        this.updateTransform();

        for (let i = 0; i < this.node.children.length; i++) {
            this.node.children[i].y -= this._velocity * dt;
        }
    }

    private updateTransform(): void {
        for (let i = 0; i < this.node.children.length; i++) {
            const symbol = this.node.children[i];

            if (symbol.y < - (5 * this.symbolSize.y)) {
                // symbol.stopAllActions();
                symbol.getComponent(cc.Sprite).spriteFrame = this.getRandomFrame();
                symbol.y = this._topY - (-5 * this.symbolSize.y - symbol.y);
            }
        }
    }

    private getRandomFrame(): cc.SpriteFrame {
        return this.symbolsAtlas.getSpriteFrame(this.symbolsName[3]);
    }
}

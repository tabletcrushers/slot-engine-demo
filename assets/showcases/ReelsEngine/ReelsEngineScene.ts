import IGameConfigDTO from '../../code/sww/Core/Games/Slots/Configuration/DTO/IGameConfigDTO';
import IReelItemsConfigDTO from '../../code/sww/Core/Games/Slots/Configuration/DTO/IReelItemsConfigDTO';
import { JsonConfigurator } from '../../code/sww/Core/Games/Slots/Configuration/JsonConfigurator';
import { IAnimationDTO } from '../../code/sww/Core/Games/Slots/Features/WinAnimations/IAnimationDTO';
import { WinLinesAnimation } from '../../code/sww/Core/Games/Slots/Features/WinAnimations/WinLinesAnimations';
import { ReelEngineNotification, ReelEngineNotificationTopic } from '../../code/sww/Core/Games/Slots/Notifications/SlotsGameNotifications';
import ReelItemBuilder from '../../code/sww/Core/Games/Slots/ReelsEngine/Core/Implementation/Builders/ReelItemBuilder';
import FrameAtlasProvider from '../../code/sww/Core/Games/Slots/ReelsEngine/Core/Implementation/Providers/FrameAtlasProvider';
import IReelsEngine from '../../code/sww/Core/Games/Slots/ReelsEngine/Core/IReelsEngine';
import IReelItem from '../../code/sww/Core/Games/Slots/ReelsEngine/Core/Reel/ReelItem/IReelItem';
import ReelsEngine from '../../code/sww/Core/Games/Slots/ReelsEngine/Core/ReelsEngine';
import IReelSpinResult from '../../code/sww/Core/Games/Slots/ReelsEngine/Result/IReelSpinResult';
import { ISpinResult } from '../../code/sww/Core/Games/Slots/ReelsEngine/Result/ISpinResult';
import ReelSpinResult from '../../code/sww/Core/Games/Slots/ReelsEngine/Result/ReelSpinResult';
import SpinResult from '../../code/sww/Core/Games/Slots/ReelsEngine/Result/SpinResult';
import INotification from '../../code/sww/Core/Notifications/INotification';
import IObserver from '../../code/sww/Core/Notifications/IObserver';
import NotificationManager from '../../code/sww/Core/Notifications/NotificationManager';
import RenderEngine from '../../code/sww/Supply/RenderEngine/RenderEngine';
import RenderReelItem from '../../code/sww/Supply/RenderEngine/RenderReelItem';
import LoaderUtil from '../../code/sww/Utils/LoaderUtil';
import TypeUtil from '../../code/sww/Utils/TypeUtil';

const {ccclass, property} = cc._decorator;

// TODO - RE DOES NOT implement component
@ccclass
export default class ReelsEngineSample extends cc.Component implements IObserver {

    @property(cc.SpriteAtlas)
    public symbolsAtlas: cc.SpriteAtlas = null;

    private startButton: cc.Node;
    private spinResultButton: cc.Node;
    private background: cc.Node;

    private itemsPool: Array<RenderReelItem>;
    private reelsEngine: IReelsEngine;
    private reelItemBuilder: ReelItemBuilder;
    private frameAtlasProvider: FrameAtlasProvider;
    private configured = false;
    private renderEngine: RenderEngine;
    private winLinesAnimation: WinLinesAnimation;

    public start() {
        this.frameAtlasProvider = new FrameAtlasProvider();
        this.frameAtlasProvider.registerAtlas(this.symbolsAtlas);

        this.startButton = cc.find('start_btn');
        this.startButton.on(cc.Node.EventType.TOUCH_END, (event) => {
            this.onSpinStart();
        });
        this.startButton.active = false;

        this.spinResultButton = cc.find('spinresult_btn');
        this.spinResultButton.on(cc.Node.EventType.TOUCH_END, (event) => {
            this.onSetSpinResult();
        });
        this.spinResultButton.active = false;

        // reels behaviour variations
        const configurations = {
            verticalSegments: {
                bgName: 'viewport_vertical_segments',
                configPath: 'resources/ReelsEngineConfiguration-VerticalSegments/GameConfig.json'
            },
            horizontalSegments: {
                bgName: 'viewport_horizontal_segments',
                configPath: 'resources/ReelsEngineConfiguration-HorizontalSegments/GameConfig.json'
            },
            rectangleMixed: {
                bgName: 'viewport_rectangle_mixed',
                configPath: 'resources/ReelsEngineConfiguration-RectangleMixed/GameConfig.json'
            }
        };
        // tslint:disable-next-line:forin
        for (const config in configurations) {
            cc.find(configurations[config].bgName).active = false;
        }

        cc.find('vertical_reels_btn').on(cc.Node.EventType.TOUCH_END, (event) => {
            if (this.winLinesAnimation) {
                this.winLinesAnimation.stop();
            }

            this.runCustomConfiguration(configurations.verticalSegments);
        });

        cc.find('horizontal_reels_btn').on(cc.Node.EventType.TOUCH_END, (event) => {
            if (this.winLinesAnimation) {
                this.winLinesAnimation.stop();
            }

            this.runCustomConfiguration(configurations.horizontalSegments);
        });

        cc.find('rectangle_mixed_reels_btn').on(cc.Node.EventType.TOUCH_END, (event) => {
            if (this.winLinesAnimation) {
                this.winLinesAnimation.stop();
            }

            this.runCustomConfiguration(configurations.rectangleMixed);
        });
    }

    public update(delta: number) {
        if (!this.configured) {
            return;
        }

        this.reelsEngine.update(delta);
        this.renderEngine.renderReelItems(this.reelsEngine.getVisibleItems().getGameFieldReport(0));
    }

    public observe(notification: INotification) {
        switch (notification.id) {
            case ReelEngineNotification.ReelsEngineStop:
                this.startButton.active = true;

                return;

            case ReelEngineNotification.ReelsEngineSpinResultCleared:
                this.spinResultButton.active = true;

                return;

            default:
        }
    }

    private onSpinStart(): void {
        this.reelsEngine.start();
        this.startButton.active = false;
    }

    private onSetSpinResult(): void {
        // tslint:disable-next-line:no-magic-numbers
        const result: SpinResult = this.generateSpinResult(1, 7, 5);
        this.reelsEngine.setSpinResult(result);
        this.spinResultButton.active = false;
    }

    // TODO - rework it all
    // RE should receive set of numbers and create all items inside
    private generateSpinResult(gameFiledsCount: number, reelsCount: number, resultLength: number): SpinResult {
        const result: SpinResult = new SpinResult();

        for (let i = 0; i < gameFiledsCount; i++) {
            const gameFieldResult: Array<IReelSpinResult> = new Array<ReelSpinResult>();
            for (let j = 0; j < reelsCount; j++) {
                const reelResult: ReelSpinResult = new ReelSpinResult();
                for (let k = 0; k < resultLength; k++) {
                    // tslint:disable-next-line:no-magic-numbers
                    reelResult.add(this.reelItemBuilder.build(k + 6));
                }
                gameFieldResult.push(reelResult);
            }
            result.gameFields.set(i, gameFieldResult);
        }

        return result;
    }

    private runCustomConfiguration(gameConfiguration: any): void {
        if (this.background !== undefined) {
            this.background.active = false;
        }
        this.background = cc.find('background');
        this.background.active = true;

        this.reelItemBuilder = undefined;
        this.reelsEngine = undefined;
        this.configured = false;
        this.startButton.active = false;
        this.spinResultButton.active = false;

        NotificationManager.instance.flush();

        LoaderUtil.loadGameConfiguration(gameConfiguration.configPath).then((gameConfig: IGameConfigDTO) => {
            this.background.active = false;
            this.background = cc.find(gameConfiguration.bgName);
            this.background.active = true;

            this.reelItemBuilder = new ReelItemBuilder();
            this.reelItemBuilder.configure(gameConfig.itemsConfiguration);
            const configurator: JsonConfigurator = new JsonConfigurator();
            const reelEngineConfig = configurator.makeReelsEngineConfiguration(
                gameConfig.modes[0].configuration,
                this.reelItemBuilder,
                gameConfig.screen);

            this.reelsEngine = new ReelsEngine();
            this.renderEngine = new RenderEngine();
            this.renderEngine.frameAtlasProvider = this.frameAtlasProvider;

            this.renderEngine.configureAnimations(this.getAllClipByFeature(gameConfig.itemsConfiguration, 'winLine'));

            this.winLinesAnimation = new WinLinesAnimation(this.reelsEngine, this.renderEngine);
            this.configured = true;

            NotificationManager.instance
                .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineStop)
                .addObserver(this, ReelEngineNotificationTopic.ReelsEngine, ReelEngineNotification.ReelsEngineSpinResultCleared);

            this.startButton.active = true;
        });
    }

    private getAllClipByFeature(itemsconfig: IReelItemsConfigDTO, feature: string): Map<number, IAnimationDTO> {
        const animationsConfig = new Map<number, IAnimationDTO>();

        for (const item of itemsconfig.items) {
            for (const animation of item.animations) {
                if (animation.feature === feature) {
                    animationsConfig.set(item.id, animation);
                }
            }
        }

        return animationsConfig;
    }
}
